# MuZero
This project is a muzero implementation for tf-agents / tensorflow. It is specificaly GPU oriented design. All these steps are batched-processed with tf.function + XLA:
* Muzero Agent
  * add exploration noise
  * MCTS : selection / "simulation" / expansion / backpropagation
  * action selection
* Training
  * batch creation
  * backpropagation for network
* Environment simulation (Connect 4 is provided as a demo)
This way agent can look for action on 1024 connect 4 games at once.

This design allows connect 4's basic training on a RTX2080Ti with 4GB of RAM usage in less than 3 hours. 

MuZero based on the Google DeepMind [paper](https://arxiv.org/abs/1911.08265) (Nov 2019) and the associated [pseudocode](https://arxiv.org/src/1911.08265v2/anc/pseudocode.py).

## Features
* Full batched MCTS on GPU (root batching)
* Support of complex/tuple observation space and latent space
* Support of action masking (provided with environment's observations)
* Compact representation-state storing for mcts's tree on int8
* Support of scalar_to_vector conversion for value and reward network output
* support training on f16


# install
Tested on Ubuntu 22.04

```
apt install python3-dev python3-virtualenv
apt install nvidia-cuda-toolkit
virtualenv venv
python -m pip install --upgrade pip
pip install -r requirements.txt
```

You may add environment variable TF_XLA_FLAGS=--tf_xla_enable_xla_devices
## offline install

# run
## training

### tictactoe

```
python -m src.main configs/tictactoe_train_muzero.gin
```

### Connect4 with small memory GPU

start training with
```
python -m src.main configs/c4_tfa_muzero_4GB.gin
```

### tictactoe

```
python -m src.main configs/tictactoe_train_muzero.gin
```

### Connect4

The default network has 4 resblock for dynamic and 8 resblock for prediction (128 features)
The compact storage of lattent space allows to process batch size of 1024 with 700 simulations for connect4.
That's 1024*700*6*7*64 = 2GB for this tensor only on (RTX2080Ti has 11GB of RAM)

start basic training with
```
python -m src.main configs/c4_tfa_muzero_4GB.gin
```

training details:
- 250 self_play games with 50 simulations par action, grouped by batch of 1024 games
- 25k backprop steps on batch of 256 samples
- regular evaluations
    - network policy against [kaggle dataset](https://www.kaggle.com/petercnudde/1k-connect4-validation-set)
    - network MCTS against kaggle dataset

customize training through config file in configs/c4_tfa_muzero_4GB.gin

reference training with 
```
python -m src.main configs/c4_tfa_muzero.gin
```
Speed reach 76000 inferences per second (640 fps * 120 simulations), who allows a training with 500k games in 10 hours with RTX2080Ti.

Results are pretty good, this model is in the [kaggle leaderboard](https://www.kaggle.com/competitions/connectx/leaderboard) top 10! [(2022/10/15)](doc/C4_kaggle_leaderboard.png)


offline training with 
```
python -m src.main configs/c4_tfa_muzero.gin
```
the initial phase collects random trajectories. There is no reanalysis, so training progresses on value estimation, not policy.

#### Loss evolution
![loss_policy](doc/loss_policy.png)

![loss_value](doc/loss_value.png)

![loss_reward](doc/loss_reward.png)

#### tensorboard's images
The shown images contains the board's observation with
- [perfect moves](https://connect4.gamesolver.org/)
- current policy from trained neural network

#### tensorboard's eval on kaggle dataset
![eval_kaggle](doc/C4_kaggle.png)

#### offline training
This training is based on random games, and try to train value with dynamic. This way we can use MCTS to get a right policy without nn's policy prediction.
start training with
```
python -m src.main configs/c4_tfa_muzero_offline.gin
```
TODO: add reanalyse to compute policy based on MCTS evaluations


### Cartpole
```
python -m src.main configs/gym_tfa_muzero.gin
```

### LunarLander-v2
```
python -m src.main configs/gym_tfa_muzero.gin entrypoint.env_id=LunarLander-v2
```
Note: training doesn't reach highest score


customize training through
* config file in configs/c4_tfa_muzero_4GB.gin
* training script in src/apps/train_muzero_c4.py

#### Loss evolution
![loss_policy](doc/loss_policy.png)

![loss_value](doc/loss_value.png)

![loss_reward](doc/loss_reward.png)

#### tensorboard's images
The shown images contains the board's observation with
- [perfect moves](https://connect4.gamesolver.org/)
- current policy from trained neural network

#### tensorboard's eval on kaggle dataset
![eval_kaggle](doc/C4_kaggle.png)

#### offline training
This training is based on random games, and try to train value with dynamic. This way we can use MCTS to get a right policy without nn's policy prediction.
start training with
```
python -m src.main configs/c4_tfa_muzero_offline.gin
```
TODO: add reanalyse to compute policy based on MCTS evaluations

### Cartpole
```
python -m src.main configs/gym_tfa_muzero.gin
```

### LunarLander-v2
```
python -m src.main configs/gym_tfa_muzero.gin entrypoint.env_id=LunarLander-v2
```
Note: training doesn't reach highest score

# monitor

## tensorboard
```bash
tensorboard --logdir out/c4
```

### scalars
- collect stats: episodes's length and rewards
- eval stats: episode's lengh and rewards with evaluation settings (no dirichlet noise on observation, low temperature)
- train stats: loss for policy, value, reward, temperature monitoring if decay, learning rate monitoring if decay
- mem: technical monitoring of memory usage for process and gpu
- replay buffer: monitoring of replay buffer's size
- time: monitoring of collect (fps stands for frame or step per second), train (ups stands for batch update per second), backup

### distributions or histograms
- collect & eval: reward histogram, episode length histogram
- hidden_state_dyn: dynamic's network output latent state
- hidden_state_rep: representation's network output latent state
- policy/value/reward _target: histogram of values that comes from replay buffer and networks must match
- policy/value/reward _prediction: histogram of values that networks outputs

## mlflow
```
mlflow ui
```
monitor runs with used parameters

## optuna
```
optuna-dashboard sqlite:///out/c4/study.db
```

# Directory organisation
* src/agents/... the muzero algorithm with unit tests
* src/envs/... demo environment (connect4)
* src/apps/... training scripts


# QA

## unit tests
```bash
python -m unittest discover src
python -m unittest discover src/envs -t .
python -m unittest src.envs.connect4.test.test_connect4_tfa
SLOW_TESTS=1 python -m unittest discover src
```

## check typing
```
mypy src\agents\tfagents\muzero
mypy src\agents\tfagents\muzero_agent.py
```

## coverage
```bash
coverage run -m unittest discover src
coverage report -m
coverage html
```

# Todo
* Tune/fix C4 training to get better scores
* eval C4 agent against MC
* Fix normalisation (enabling training flag)
* Fix mixed_precision (f16 training)
* HP search example with optuna
* PER replay buffer
* parallel/distributed training

for details look at backlog.md

