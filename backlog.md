muzero
- tf.function(jit) for update_weights with complex observations (ie many tensors)
- fix policysaver / policyCheckpointer: continue training
- hyperparameters checklist
- hyperparameters searching script (optuna)
- reanalyze / sample efficient
- mirror strategy backpropagation for multiGPU training
- distributed training (cf SAC https://github.com/tensorflow/agents/tree/master/tf_agents/experimental/distributed/examples/sac) + train https://www.tensorflow.org/guide/distributed_training
- collect trajectories with tf_driver
- implement PER (https://arxiv.org/abs/1511.05952 et https://arxiv.org/abs/1803.00933)
  * https://github.com/JimOhman/model-based-rl/blob/master/replay_buffer.py
  * https://github.com/werner-duvaud/muzero-general/blob/master/replay_buffer.py et https://github.com/werner-duvaud/muzero-general/blob/master/trainer.py
- handle eval env with different shape from train env (NN must fit like full conv)
- check of TODOs: int32 GPU tensor, one_hot with bools, ... 
- monitoring average muzero's predicted value on collect
- tensorboard policy logging (gymenv render + policy + value + reward) on environment key steps (0/25/50/75/100 % of time), video
- tensorboard log states transitions exhibit highest/lowest values, highest value variation and highest value error while learning
- tensorboard log knn of action<->state (to exhibit select 'typical/archetype state') ? 
- log tops/flops episodes (short/longer, top/lowest rewards).
- log average value while collect
- detect key steps: high value delta ?
- monitor replay buffer content
- check typing with mypy / Pyre
- reindent with black or another indent tool
- clean reverb binding
- generic training script for gym envs. Exemple fast cpu avec LunarLander/CartPole (MLP 512+30simus par exemple)
- support long episodes (ex:ATARI 108k frames (30 min) / chunks of 200 frames)
- basic settings finger law with heuristics (root_dirichlet_alpha, nb_simus)
- rename internal functions with  _
- simplify MuzeroNetwork to rely on tf-agents and remove networks.py
- train reward/values loss with kl divergence => better on "random" reward ? (but how to distinct mode if many?)
- generic padding function for dataset reading
- handle final state when cut by time (when we are good the post-final state musn't be 0 value)
- cleanup tree using parent's node index instead of extended nodes, like MCTX
- cleanup tree checkoing custom type, T = TypeVar("T"), like MCTX
- integrate RebeL logic ? + "Learning Strategies for Imperfect Information Board Games using Depth-limited Counterfactual Regret Minimization and Belief State" from CoG
  https://ai.facebook.com/blog/rebel-a-general-game-playing-ai-bot-that-excels-at-poker-and-more/
  https://github.com/facebookresearch/rebel
  https://arxiv.org/abs/2007.13544
- MuzeroRandomPolicy: simplify code path mask/no mask


Connect4
- A0 training script
- batchsize max ? (A/V/P100)
- splited convolutions
- Squeeze&Excite like efficientnet
- never give up; Adaptive Exploration?
- bench with other implems like muzero-general
- handle ParallelPyEnvironment cf => https://github.com/tensorflow/agents/blob/master/tf_agents/agents/ddpg/examples/v2/train_eval.py 
- add evals VS MC
- test disabling BN/LN
- monitor replay buffer duplicate states
- train with quantisation aware, collect/execute quantized
- openning/best moves dictionnary ?


generic ideas
- collab/notebook demo for easy testing
- continuous action space
- S-MuZero (sampling envs with lot of actions)
- stochastic Muzero
- Path consistency (Efficient Learning for AlphaZero via Path Consistency) https://proceedings.mlr.press/v162/zhao22h.html https://github.com/CMACH508/PCZero
- new envs
  - atari
  - cartpole, lunar lander, tic-tac-toe (cf https://github.com/JimOhman/model-based-rl Breakout-ramNoFrameskip-v4:1day,score>400 Pong-ramNoFrameskip-v4:5h LunarLander-v2:2h 240fps 8ups Tic-Tac-Toe)
  - https://github.com/deepmind/open_spiel 
  - https://github.com/openai/procgen
  - ALE https://github.com/mgbellemare/Arcade-Learning-Environment
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge
  - https://github.com/Kautenja/gym-super-mario-bros
- bench perfs for Go (16 res-blocks 256 couches => 800 inférences en 0.1s). history : 8 boards
- bench perfs for Atari (200M frames / 12j): history: 32 frames+action, input 96x96
- async tf while env update -- double buffering: tf.config.experimental.set_synchronous_execution
- TU reverb
- test a multimodal NN, like perceiver / more (cf keras samples, deepmind paper)
- test a kind of dynamic space jump, like A* jump point search
- test a classing -- pretrained -- image NN as representation
- version tensor core (multiple of 8+checks)
- checks tensor specs on interfaces
- test Imitation Learning
- Debug: NN de decoding latent space, using previous space observations too as input (latent space doesn't needs observation's stype -- like space ship animation in a shmup)
- Check tf-agents shapes on Networks
    tf_agents.utils.nest_utils.
    assert_matching_dtypes_and_inner_shapes
    is_batched_nested_tensors
    assert_same_structure
    assert_value_spec
- use tf_agents NN / no custom code
- replay buffer datatviz (episode length, % episode with reward)
- agnostic data augmentation: mixup
- separate env representation + dynamic training (using vicreg?) from prediction (value & policy => objectif)
- pre-train rep+dyn using vicreg/Jepa/MC-Jepa, then train with objective => (cf MC-JEPA: A Joint-Embedding Predictive Architecture for Self-Supervised Learning of Motion and Content Features)
- use deeepmind's GaTo as latent space, or PerformerIO, or use transformer already trained (like GPT2)
- forward action mask through MaskSplitterNetwork (cf https://www.tensorflow.org/agents/api_docs/python/tf_agents/networks/mask_splitter_network/MaskSplitterNetwork)
- Better selection of exploration actions (cf In-context Reinforcement Learning with Algorithm Distillation)
- Boost exploration with volatiliy of model ensemble prediction (Planning With Uncertainty: Deep Exploration in Model-Based Reinforcement Learning / E-MCTS: Deep Exploration in
Model-Based Reinforcement Learning by Planning with Epistemic Uncertainty)

Links:
  https://medium.com/oracledevs/lessons-from-implementing-alphazero-7e36e9054191



Add a "new env tutorial"
- Think small at first
  * if something could fail, it will happens, so the most simplistic is your first environment the easier it will be (think about tictactoe: you can compute probabilities by hand) 
- Design internal NN
  * think about observations, actions and rewards shapes, how to map them from/to internal space
  * soon, you will need to bench with another policy: random is very basic, but monte carlo could be a good trade-off if you have all the env logic
- Test a static training on a fixed environment
  * design a small env by hand, with the solution
  * test nn learning on these steps: good value estimation, action choice and rewards for each step ?
- Test each learning phase, one step at a time, to check everything fits (could be on a fixed-seed environment)
  * generate random training data with rewards
  * Monitor first collected datas: mean value, reward
  * 1st train : try to match value and reward (loss must decrease)
  * check if environment prediction allows to predict value of next step
  * so MCTS must ba able to provide a better action's selection
  * collect new datas with this policy
  * Monitor new collected datas: as the value/reward is trained actions must be better and reward/value too
  * 2nd train: this time policy loss must decrease to match your expectations, predicted's policy must match labels.
  * collect last datas
  * Monitor last collected datas: with trained policy, results keeps increasing
- Monitor :
  * quantitatively: value/reward from collect
  * qualitatively: starting, finishing steps with their associated value/reward/policy. some full episodes, steps with high value delta

Known Issues:
  * Muzero fails on stochastic actions (cf VQ-muzero / stochastic-muzero)
  * non applicatble to continuous env (cf S-Muzero)
  * minimisation of immediate regret (cf Rebel)
  