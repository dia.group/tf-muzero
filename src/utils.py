import gin
import os
import sys
from gin import configurable


@configurable
class BasicLogger:
    """Log to stdout and/or a file using format() function API"""

    def __init__(self, path=None, echo=True):
        if path is not None:
            os.makedirs(os.path.dirname(os.path.abspath(path)), exist_ok=True)
            self._f = open(path, 'w')
        else:
            self._f = None
        self._echo: bool = echo
        if echo and path is not None:
            self("Start logging in '{}'", path)

    def __call__(self, *args):
        s = str(args[0])
        if len(args) > 1:
            s = s.format(*args[1:])
        if self._echo:
            print(s)
        if self._f is not None:
            self._f.write(s + '\n')
            self._f.flush()


def roulette_wheel(re, probs, sum_probs):
    """Returns index of chosen element, with probability proportionate to its value"""
    # return tf.random.categorical([tf.math.log(probs)], 1).numpy()[0][0]
    # return np.random.multinomial(1, probs/sum_probs).argmax()
    n = re.uniform(0, sum_probs)
    for i, p in enumerate(probs):
        if n <= p:
            return i
        n -= p
    raise ValueError("{} should be equal to {}".format(sum_probs, sum(probs)))


def parse_command_line_for_gin(default_configurable=None):
    """Allow specification of parameters and/or gin config files from command line"""

    config_files = []
    config_vars = []

    for arg in sys.argv[1:]:
        if arg[0] == '-':
            raise ValueError(
                "Expecting gin config file or gin settings (configurable.parameter=value ...)")
        if arg.find('=') != -1:
            name, value = arg.split('=')

            if name[0:2] == '--':
                name = name[2:]
            if name.find(".") == -1:
                if default_configurable is None:
                    raise ValueError("Not a configurable parameter: " + name)
                else:
                    name = default_configurable + '.' + name

            if value[0] != '@':
                try:
                    value = eval(value)
                    if type(value) is str:
                        value = repr(value)
                except SyntaxError:
                    value = repr(value)
                except NameError:
                    value = repr(value)

            config_vars.append("{}={} ".format(name, value))

        elif os.path.exists(arg):
            config_files.append(arg)
        else:
            raise OSError("File not found: " + os.path.abspath(arg))

    gin.parse_config_files_and_bindings(config_files, config_vars)
