import gin
import sys
from typing import Callable, Optional
from src.utils import parse_command_line_for_gin


@gin.configurable
def main(
    entrypoint: Callable = gin.REQUIRED,
    init: Optional[Callable] = lambda: None,
    term: Optional[Callable] = lambda: None,
):
  init()
  entrypoint()
  term()


if __name__ == "__main__":

  if len(sys.argv) == 1:
    raise ValueError(
        "You must give path to gin configuration file in argument")

  parse_command_line_for_gin('entrypoint')

  main()

  exit(0)
