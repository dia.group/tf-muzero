from importlib import import_module

from typing import List, Dict, Tuple, Optional
import sys
from gin import configurable

from src.utils import BasicLogger


@configurable
class GinTemplateReader:
    """Parses a gin template file to provide a parameters space to search."""

    def __init__(self, logger=BasicLogger()):

        self._log = logger

        # List of prologues lines, to output a valid gin file (with import statements)
        self.prologue_lines: List[str] = []

        # List of all variables in the original order
        self.vars: List[str] = []

        # List of searched variables in the original order
        self.searched_vars: List[str] = []

        # Variables by categories
        self.fixed_vars: Dict = {}
        self.uniform_vars: Dict[str, Tuple[float, float, float]] = {}
        self.log_uniform_vars: Dict[str, Tuple[float, float, float]] = {}
        self.uniform_discrete_vars: Dict[str, Tuple[int, int, int]] = {}
        self.log_uniform_discrete_vars: Dict[str, Tuple[int, int, int]] = {}
        self.categorical_vars: Dict[str, List] = {}
        self.config_id_var: Optional[str] = None

    def load_file(self, gin_file_template: str):
        self._log("Load file {}", gin_file_template)

        with open(gin_file_template) as f:

            line_number = 0
            for line in f.readlines():

                line_number += 1
                try:
                    self.parse_line(line)
                except (NameError, ValueError, TypeError, SyntaxError) as e:
                    sys.stderr.write('File "%s", line %d: %s' %
                                     (gin_file_template, line_number, str(e)))
                    raise

    @staticmethod
    def unpack2or3(to_unpack, default3):
        try:
            a, b, c = to_unpack
        except ValueError:
            c = default3
            a, b = to_unpack
        return a, b, c

    def parse_line(self, line: str):

        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            return

        if line.find('=') == -1:
            import_tag, module_name = line.split(None, 1)
            if import_tag != "import":
                raise SyntaxError("Invalid line")
            self.prologue_lines.append(line)
            return

        var, val = line.split('=', 1)
        var = var.strip()
        val = val.strip()

        self.vars.append(var)

        if val[0] != '?':
            self.fixed_vars[var] = val
            return

        val = val[1:].strip()
        self.searched_vars.append(var)

        if val == 'config_id':
            self.config_id_var = var
        elif val.startswith("range_int"):
            val = val[9:].strip()
            begin, end, step = self.unpack2or3(eval(val), 1)
            self.uniform_discrete_vars[var] = (begin, end, step)
            self._log("{}=uniform-discrete({},{},{})", var, begin, end, step)
        elif val.startswith("range"):
            val = val[5:].strip()
            begin, end, step = self.unpack2or3(eval(val), 1e-7)
            self.uniform_vars[var] = (begin, end, step)
            self._log("{}=uniform({},{},{})", var, begin, end, step)
        elif val.startswith("log_int"):
            val = val[7:].strip()
            begin, end, low_step = self.unpack2or3(eval(val), 1)
            self.log_uniform_discrete_vars[var] = (begin, end, low_step)
            self._log("{}=log-uniform-discrete({},{},{})",
                      var, begin, end, low_step)
        elif val.startswith("log"):
            val = val[3:].strip()
            begin, end, low_step = self.unpack2or3(eval(val), 1e-7)
            self.log_uniform_vars[var] = (begin, end, low_step)
            self._log("{}=log-uniform({},{},{})", var, begin, end, low_step)
        elif val == "bool":
            self.categorical_vars[var] = [False, True]
            self._log("{}=bool", var)
        else:
            try:
                val = eval(val)
            except (ValueError, NameError, SyntaxError):
                val = val.strip()
                assert val[0] == '[' and val[-1] == ']'
                val = val[1:-1].split(',')
                val = list(map(str.strip, val))
            val = list(map(repr, val))
            self.categorical_vars[var] = val
            self._log("{}=categorical({})", var, val)
