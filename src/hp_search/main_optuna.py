from concurrent.futures import ThreadPoolExecutor
import importlib.util
import multiprocessing
import gin
import os
import optuna
from optuna.samplers import TPESampler
import sys
from typing import Optional

from src.hp_search.optimizer import OptimizerBase


@gin.configurable("optuna_entrypoint")
class OptunaOptimizer(OptimizerBase):
    """Implementation using optuna library."""

    def __init__(
        self,
        command: str = "python -m src.main",
        gin_file_template: str = gin.REQUIRED,
        output_directory: Optional[str] = "out",
        storage: str = gin.REQUIRED,
        study_name: str = "default_study",
        max_evals: int = gin.REQUIRED,
    ):

        os.makedirs(output_directory, exist_ok=True)
        super().__init__(command, gin_file_template, output_directory)

        self._max_evals = max_evals

        optuna.logging.set_verbosity(optuna.logging.WARNING)
        sampler = TPESampler()
        self._study = optuna.create_study(
            storage=storage, sampler=sampler, study_name=study_name, load_if_exists=True)
        print(f"monitor with: optuna-dashboard {storage}")

    def _loss(self, trial: optuna.Trial):

        # Create a parameters dictionary using optuna suggestions
        variables = {}
        for var, val in self.template.fixed_vars.items():
            variables[var] = val

        for var, (begin, end, step) in self.template.uniform_vars.items():
            variables[var] = trial.suggest_uniform(var, begin, end)

        for var, (begin, end, step) in self.template.log_uniform_vars.items():
            variables[var] = trial.suggest_loguniform(var, begin, end)

        for var, (begin, end, step) in self.template.uniform_discrete_vars.items():
            variables[var] = trial.suggest_discrete_uniform(
                var, begin, end, step)

        for var, (begin, end, low_step) in self.template.log_uniform_discrete_vars.items():
            v = trial.suggest_loguniform(var, begin, end)
            v = begin + round((v - begin) / low_step) * low_step
            variables[var] = v

        for var, values in self.template.categorical_vars.items():
            variables[var] = trial.suggest_categorical(var, values)

        # Run the evaluation
        return self.run_evaluation(variables)

    def __call__(self):
        self._study.optimize(self._loss, n_trials=self._max_evals)

        # https://optuna.readthedocs.io/en/stable/tutorial/10_key_features/005_visualization.html#
        if importlib.util.find_spec("pyplot"):
          fig = optuna.visualization.plot_optimization_history(self._study)
          fig.write_image(os.path.join(self.out_dir, "optimization_history.png"))
          # fig = optuna.visualization.plot_intermediate_values(self._study)
          # fig.write_image(os.path.join(self.out_dir, "intermediate_values.png"))
          fig = optuna.visualization.plot_parallel_coordinate(self._study)
          fig.write_image(os.path.join(self.out_dir, "parallel_coordinate.png"))

        print(" Best returned value: ", self._study.best_trial.value)
        print(" Best parameters: ")
        for key, value in self._study.best_trial.params.items():
            print(f"    {key}: {value}")


if __name__ == '__main__':

    try:
        config_template, study_dir, num_eval_runs = sys.argv[1:]
    except ValueError:
        sys.stderr.write(
            "Expecting 3 parameters: <config-template> <study-directory> <num-evals>\n")
        sys.exit(1)

    OptunaOptimizer(gin_file_template=config_template,
                    study_directory=study_dir, max_evals=int(num_eval_runs))()
