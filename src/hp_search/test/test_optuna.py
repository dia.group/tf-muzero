import datetime
import gin
import math
import optuna
import os
import shutil
import sys
import tempfile
import unittest

from .. import main_optuna


@gin.configurable("entrypoint")
def test(parameter: float = 0.0):
    print(f"\r{1.0 -  math.sqrt(abs(parameter + 2.0))}", end='', flush=True)
    return True

@unittest.skipIf(sys.platform == "win32", "windows")
class TestOptuna(unittest.TestCase):

    def test_running(self):

        gin_file_template = os.path.join(
            os.path.dirname(__file__), "test.tpl.gin")

        output_directory = os.path.join(tempfile.gettempdir(
        ), datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
        os.makedirs(output_directory, exist_ok=True)

        db_path = f"sqlite:///" + os.path.join(output_directory, "study.db")
        optuna.create_study(storage=db_path, load_if_exists=False)

        optimizer = main_optuna.OptunaOptimizer(
            gin_file_template=gin_file_template,
            output_directory=output_directory,
            storage=db_path,
            max_evals=4,
        )

        # call
        optimizer()

        #cleanup
        shutil.rmtree(output_directory)
