import multiprocessing
import os
import subprocess
from tempfile import mkstemp

import gin
from gin import configurable
from typing import Dict

from src.hp_search.reader import GinTemplateReader
from src.utils import BasicLogger


@configurable
class OptimizerBase:
    """Base class to implement an hyper-parameter optimizer using gin templates."""

    def __init__(self,
                 command: str = gin.REQUIRED,
                 gin_file_template: str = gin.REQUIRED,
                 output_directory: str = "out"):
        """
        @param command: a command executable in shell, taking a gin-config file as unique argument,
        and printing the evaluation value to maximize in stdout
        @param gin_file_template: a template of gin-config file to use expressing parameters search spaces.
        @param output_directory: where to write generated configuration files
        """
        self._command = command

        self._log = BasicLogger(self._create_tmp_path(
            os.path.join(output_directory, 'log.txt')))

        self.out_dir = output_directory
        self.gin_file_template = gin_file_template
        self.global_csv_report = os.path.join(
            output_directory, 'global_report.csv')

        self.template = GinTemplateReader(logger=self._log)
        self.template.load_file(gin_file_template)

    def run_evaluation(self, config: Dict):
        """
        Runs evaluation command, parse score in the last line of stdout and returns the value to minimize as the
        opposite of this score
        """

        f, gin_path = self._create_gin_file()
        config_id = os.path.splitext(os.path.basename(gin_path))[0]
        self._write_gin_file(f, config, config_id)
        command_line = self._command + ' ' + gin_path
        self._log("Run command: {}", command_line)
        output = subprocess.check_output(command_line, shell=True)
        result = float(output.splitlines()[-1])
        self._log("Result for config {}: {}", gin_path, result)
        return -result

    def _create_gin_file(self):
        base, ext = os.path.splitext(os.path.basename(self.gin_file_template))

        fd, path = mkstemp(prefix=base + '_', suffix=ext, dir=self.out_dir)
        f = open(path, "w")
        os.close(fd)
        return f, path

    def _create_tmp_path(self, model_path):
        base, ext = os.path.splitext(os.path.basename(model_path))
        fd, path = mkstemp(prefix=base + '_', suffix=ext,
                           dir=os.path.dirname(model_path))
        os.close(fd)
        return path

    def _write_gin_file(self, f, parameters_value, config_id):

        for line in self.template.prologue_lines:
            f.write(line + '\n')

        if self.template.config_id_var is not None:
            f.write('\n')
            f.write(f"{self.template.config_id_var}=\"{config_id}\"\n")

        f.write('\n')
        for var in self.template.vars:
            if var == self.template.config_id_var:
                continue
            val = parameters_value[var]
            if var in self.template.uniform_discrete_vars:
                val = int(val)
            f.write(f"{var}={val}\n")

        f.close()
