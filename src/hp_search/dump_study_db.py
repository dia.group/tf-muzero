import sqlite3
import pandas as pd

#con = sqlite3.connect(":memory:")
con = sqlite3.connect("out\\hp_ppo\\study.db")
cursor = con.cursor()
cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
tables = cursor.fetchall()
for table_name in tables:
  table_name = table_name[0]
  table = pd.read_sql_query("SELECT * from %s" % table_name, con)
  table.to_csv('out\\hp_ppo\\' + table_name + '.csv', index_label='index')

cursor.close()
