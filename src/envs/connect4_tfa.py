"""
  Connect4 TF-Agents
  Connect4 with tf-agents API, supporting batching computing, with full Tensor impementation (Graph compatible)
"""
import gin  # type: ignore
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from tf_agents.trajectories import time_step  # type: ignore

from . import connect4_tf as connect4


@gin.configurable
class Connect4RandomEnv(tf_agents.environments.random_tf_environment.RandomTFEnvironment):
  """
  Randomly generated environment with observations matching Connect4's for TFAgents
  """

  def __init__(self, batch_size=1):
    action_spec = tf_agents.specs.BoundedTensorSpec(
        (), tf.int32, minimum=0, maximum=6, name='action')
    # tf.bool and tf.int8 are not supported...
    observation_spec = tf_agents.specs.BoundedTensorSpec(
        [6, 7, 3], tf.uint8, minimum=0, maximum=1, name='observation')
    time_step_spec = time_step.time_step_spec(observation_spec)
    super(Connect4RandomEnv, self).__init__(
        time_step_spec, action_spec, batch_size)


@gin.configurable
class Connect4TFEnv(tf_agents.environments.tf_environment.TFEnvironment):
  """
  Connect4 batched environment matching TFAgents's API
  """

  def __init__(self, batch_size=1, **kwargs):
    action_spec = tf_agents.specs.BoundedTensorSpec(
        [], tf.int32, minimum=0, maximum=6, name='action')
    if kwargs.get('action_masking', False):
      self.enable_action_masking = True
      observation_spec = tuple([tf_agents.specs.BoundedTensorSpec(
        [6, 7, 3], tf.uint8, minimum=0, maximum=1, name='observation'),
      tf_agents.specs.BoundedTensorSpec(
        [7], tf.uint8, minimum=0, maximum=1, name='action_mask')
      ])
    else:
      self.enable_action_masking = False
      observation_spec = tf_agents.specs.BoundedTensorSpec(
        [6, 7, 3], tf.uint8, minimum=0, maximum=1, name='observation')

    reward_spec = tf_agents.specs.BoundedTensorSpec(
        (), tf.float32, minimum=0.0, maximum=1.0, name='reward')
    time_step_spec = time_step.time_step_spec(observation_spec, reward_spec)
    super(Connect4TFEnv, self).__init__(
        time_step_spec, action_spec, batch_size)
    #self._current_time_step = self._reset()

    c4init = connect4.Connect4Environment.init(self.batch_size)
    self.state_board = tf.Variable(c4init.board, name="board")
    self.state_turn = tf.Variable(c4init.turn, name="turn")
    self.state_done = tf.Variable(c4init.done, name="done")
    self.state_winner = tf.Variable(c4init.winner, name="winner")
    self.state_observation = tf.Variable(tf.cast(
        connect4.Connect4Environment.observation(c4init), tf.uint8), name="observation")

    self.state_reward = tf.Variable(
        tf.zeros(batch_size, dtype=tf.float32), name="reward")

    if self.enable_action_masking:
      self.action_mask = tf.Variable(tf.cast(
        connect4.Connect4Environment.legal_actions(c4init), tf.uint8), name="action_mask")
      self._time_step = time_step.restart(
        tuple([self.state_observation.value(), self.action_mask.value()]),
        self._batch_size)
    else:
      self._time_step = time_step.restart(self.state_observation.value(), self._batch_size)

  def _current_time_step(self) -> time_step.TimeStep:
    return self._time_step

  def _reset(self) -> time_step.TimeStep:
    """Returns the current `TimeStep` after resetting the Environment."""
    #self._state = 0
    cur_state = connect4.Connect4Environment.init(self.batch_size)
    self.state_board.assign(cur_state.board)
    self.state_turn.assign(cur_state.turn)
    self.state_done.assign(cur_state.done)
    self.state_winner.assign(cur_state.winner)
    observation = tf.cast(
        connect4.Connect4Environment.observation(cur_state), tf.uint8)
    self.state_observation.assign(observation)
    self.state_reward.assign(tf.zeros(self.batch_size, dtype=tf.float32))

    if self.enable_action_masking:
      action_mask = tf.cast(
        connect4.Connect4Environment.legal_actions(cur_state), tf.uint8)
      self.action_mask.assign(action_mask)
      self._time_step = time_step.restart(
        tuple([observation, action_mask]),
        self._batch_size)
    else:
      self._time_step = time_step.restart(observation, self._batch_size)
    return self._time_step

  def _step(self, action:tf.Tensor) -> time_step.TimeStep:
    """Applies the action and returns the new `TimeStep`."""
    #   Returns:
    # A `TimeStep` namedtuple containing:
    #   step_type: A `StepType` value.
    #   reward: Reward at this time_step.
    #   discount: A discount in the range [0, 1].
    #   observation: A Tensor, or a nested dict, list or tuple of Tensors
    #     corresponding to `observation_spec()`.
    assert(self.action_spec().is_compatible_with(action[0]))

    # If we generalize the batched data to not terminate at the same time, we
    # will need to only reset the correct batch_inidices.
    cur_state = connect4.Connect4Environment(self.state_board.value(
    ), self.state_turn.value(), self.state_done.value(), self.state_winner.value())

    new_state, reward = connect4.Connect4Environment.step(cur_state, action)

    clear_state = connect4.Connect4Environment.init(self.batch_size)
    new_state = connect4.Connect4Environment(
        tf.where(tf.expand_dims(tf.expand_dims(cur_state.done, -1), -1),
                 clear_state.board, new_state.board),
        tf.where(cur_state.done, clear_state.turn, new_state.turn),
        tf.where(cur_state.done, clear_state.done, new_state.done),
        tf.where(cur_state.done, clear_state.winner, new_state.winner)
    )

    observation = tf.cast(
        connect4.Connect4Environment.observation(new_state), tf.uint8)
    action_mask = tf.cast(
        connect4.Connect4Environment.legal_actions(new_state), tf.uint8)

    self._time_step = time_step.TimeStep(
        step_type=tf.where(cur_state.done, time_step.StepType.FIRST, tf.where(
            new_state.done, time_step.StepType.LAST, time_step.StepType.MID)),
        reward=reward,
        discount=tf.ones_like(reward),
        observation=observation if not self.enable_action_masking else tuple([observation, action_mask]))

    self.state_board.assign(new_state.board)
    self.state_turn.assign(new_state.turn)
    self.state_done.assign(new_state.done)
    self.state_winner.assign(new_state.winner)
    self.state_observation.assign(observation)
    if self.enable_action_masking:
      self.action_mask.assign(action_mask)
    self.state_reward.assign(reward)

    return self._time_step

  def render(self) -> tf.Tensor:
    red = tf.constant([255, 0, 0], tf.uint8)[None, None, None, :]
    yellow = tf.constant([255, 255, 0], tf.uint8)[None, None, None, :]
    black = tf.constant([0, 0, 0], tf.uint8)[None, None, None, :]
    obs = tf.reverse(self.state_observation.value(), axis=[1])
    r = tf.where((obs[..., 0] > 0)[..., None], red, tf.where(
        (obs[..., 1] > 0)[..., None], yellow, black))
    return r
