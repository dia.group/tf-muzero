"""
  TicTacToe TF-Agents
  TicTacToe with tf-agents API, supporting batching computing, with full Tensor impementation (Graph compatible)
"""
import gin  # type: ignore
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from tf_agents.trajectories import time_step  # type: ignore

from . import tictactoe_tf as tictactoe


@gin.configurable
class TicTacToeRandomEnv(tf_agents.environments.random_tf_environment.RandomTFEnvironment):
  """
  Randomly generated environment with observations matching tictactoe's for TFAgents
  """

  def __init__(self, batch_size=1):
    action_spec = tf_agents.specs.BoundedTensorSpec(
        [], tf.int32, minimum=0, maximum=8, name='action')
    # tf.bool and tf.int8 are not supported...
    observation_spec = tf_agents.specs.BoundedTensorSpec(
        [3, 3, 2], tf.uint8, minimum=0, maximum=1, name='observation')
    time_step_spec = time_step.time_step_spec(observation_spec)
    super(TicTacToeRandomEnv, self).__init__(
        time_step_spec, action_spec, batch_size)


@gin.configurable
class TicTacToeTFEnv(tf_agents.environments.tf_environment.TFEnvironment):
  """
  TicTacToe batched environment matching TFAgents's API
  """

  def __init__(self, batch_size=1, **kwargs):
    action_spec = tf_agents.specs.BoundedTensorSpec(
        [], tf.int32, minimum=0, maximum=8, name='action')
    if kwargs.get('action_masking', False):
        self.enable_action_masking = True
        observation_spec = tuple([tf_agents.specs.BoundedTensorSpec(
            [3, 3, 3], tf.int32, minimum=0, maximum=1, name='observation'),
        tf_agents.specs.BoundedTensorSpec(
            [3, 3], tf.int32, minimum=0, maximum=1, name='action_mask')
        ])
    else:
        self.enable_action_masking = False
        observation_spec = tf_agents.specs.BoundedTensorSpec(
            [3, 3, 3], tf.int32, minimum=0, maximum=1, name='observation')
    reward_spec = tf_agents.specs.BoundedTensorSpec(
        (), tf.float32, minimum=0.0, maximum=1.0, name='reward')
    time_step_spec = time_step.time_step_spec(observation_spec, reward_spec)
    super(TicTacToeTFEnv, self).__init__(
        time_step_spec, action_spec, batch_size)
    #self._current_time_step = self._reset()

    tictacinit = tictactoe.TictactoeEnvironment.init(self.batch_size)
    self.state_board = tf.Variable(tictacinit.board, name="board")
    self.state_turn = tf.Variable(tictacinit.turn, name="turn")
    self.state_done = tf.Variable(tictacinit.done, name="done")
    self.state_winner = tf.Variable(tictacinit.winner, name="winner")
    self.state_observation = tf.Variable(tf.cast(
        tictactoe.TictactoeEnvironment.observation(tictacinit), tf.int32), name="observation")
    self.state_reward = tf.Variable(
        tf.zeros(batch_size, dtype=tf.float32), name="reward")

    if self.enable_action_masking:
        self.action_mask = tf.Variable(tf.cast(
            tictactoe.TictactoeEnvironment.legal_actions(tictacinit), tf.int32), name="action_mask")
        self._time_step = time_step.restart(
            tuple([self.state_observation.value(), self.action_mask.value()]),
            self._batch_size)
    else:
        self._time_step = time_step.restart(self.state_observation.value(), self._batch_size)

  def _current_time_step(self) -> time_step.TimeStep:
    return self._time_step

  def _reset(self) -> time_step.TimeStep:
    """Returns the current `TimeStep` after resetting the Environment."""
    #self._state = 0
    cur_state = tictactoe.TictactoeEnvironment.init(self.batch_size)
    self.state_board.assign(cur_state.board)
    self.state_turn.assign(cur_state.turn)
    self.state_done.assign(cur_state.done)
    self.state_winner.assign(cur_state.winner)
    observation = tf.cast(
        tictactoe.TictactoeEnvironment.observation(cur_state), tf.int32)
    self.state_observation.assign(observation)
    self.state_reward.assign(tf.zeros(self.batch_size, dtype=tf.float32))

    if self.enable_action_masking:
        action_mask = tf.cast(
            tictactoe.TictactoeEnvironment.legal_actions(cur_state), tf.int32)
        self.action_mask.assign(action_mask)
        self._time_step = time_step.restart(
            tuple([observation, action_mask]),
            self._batch_size)
    else:
        self._time_step = time_step.restart(observation, self._batch_size)
    return self._time_step

  def _step(self, action) -> time_step.TimeStep:
    """Applies the action and returns the new `TimeStep`."""
    #   Returns:
    # A `TimeStep` namedtuple containing:
    #   step_type: A `StepType` value.
    #   reward: Reward at this time_step.
    #   discount: A discount in the range [0, 1].
    #   observation: A Tensor, or a nested dict, list or tuple of Tensors
    #     corresponding to `observation_spec()`.

    # If we generalize the batched data to not terminate at the same time, we
    # will need to only reset the correct batch_inidices.
    cur_state = tictactoe.TictactoeEnvironment(self.state_board.value(
    ), self.state_turn.value(), self.state_done.value(), self.state_winner.value())

    new_state, reward = tictactoe.TictactoeEnvironment.step(cur_state, action)

    clear_state = tictactoe.TictactoeEnvironment.init(self.batch_size)
    new_state = tictactoe.TictactoeEnvironment(
        tf.where(tf.expand_dims(tf.expand_dims(tf.expand_dims(cur_state.done, -1), -1),-1),
                 clear_state.board, new_state.board),
        tf.where(cur_state.done, clear_state.turn, new_state.turn),
        tf.where(cur_state.done, clear_state.done, new_state.done),
        tf.where(cur_state.done, clear_state.winner, new_state.winner)
    )

    observation = tf.cast(
        tictactoe.TictactoeEnvironment.observation(new_state), tf.int32)
    action_mask = tf.cast(
        tictactoe.TictactoeEnvironment.legal_actions(new_state), tf.int32)

    self._time_step = time_step.TimeStep(
        step_type=tf.where(cur_state.done, time_step.StepType.FIRST, tf.where(
            new_state.done, time_step.StepType.LAST, time_step.StepType.MID)),
        reward=reward,
        discount=tf.ones_like(reward),
        observation=observation if not self.enable_action_masking else tuple([observation, action_mask])
        )

    self.state_board.assign(new_state.board)
    self.state_turn.assign(new_state.turn)
    self.state_done.assign(new_state.done)
    self.state_winner.assign(new_state.winner)
    self.state_observation.assign(observation)
    if self.enable_action_masking:
      self.action_mask.assign(action_mask)
    self.state_reward.assign(reward)

    return self._time_step

  def render(self):
    white = tf.constant([255, 255, 255], tf.uint8)[None, None, None, :]
    grey = tf.constant([100, 100, 100], tf.uint8)[None, None, None, :]
    black = tf.constant([0, 0, 0], tf.uint8)[None, None, None, :]
    obs = self.state_board
    r = tf.where((obs[0, 0] > 0)[..., None], black, tf.where(
        (obs[0, 1] > 0)[..., None], grey, white))
    return r

