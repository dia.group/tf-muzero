"""
  Connect4 TensorFlow
  a full Tensorflow version of Connect4, allowing massive parallelisation
"""
#from __future__ import annotations  # not compatible with kaggle
from enum import Enum
import tensorflow as tf  # type: ignore
from typing import Optional, NamedTuple, Tuple


class Player(Enum):
  # this code triggers tf init/gpu alloc : (tf.constant(1, tf.int32))
  white = 1
  black = -1  # same: (tf.constant(-1, tf.int32))


class Connect4Environment(NamedTuple):
  """The environment MuZero is interacting with."""
  action_space_size = 7

  board: tf.Tensor
  turn: tf.Tensor
  done: tf.Tensor
  winner: tf.Tensor

  @staticmethod
  @tf.function
  def init(batch_size: int = 1) -> 'Connect4Environment':
    assert(isinstance(batch_size, int))

    # TODO: add GPU support with dtype=tf.int8 and dtype=tf.bool
    board = tf.zeros((batch_size, 6, 7), dtype=tf.int32)
    turn = tf.zeros([batch_size], dtype=tf.int32)
    done = tf.fill([batch_size], False)
    winner = tf.zeros(batch_size, dtype=tf.int32)
    return Connect4Environment(board, turn, done, winner)

  @staticmethod
  @tf.function(jit_compile=True)
  # return legal actions as one hot encoding
  def legal_actions(self: 'Connect4Environment') -> tf.Tensor:
    ret = self.board[..., -1, :] == 0
    return tf.logical_and(tf.logical_not(self.done)[:, None], ret)

  # this function must be reintrant when game is finished, as we batch games
  @staticmethod
  @tf.function(jit_compile=True)
  def step(self: 'Connect4Environment', action: tf.Tensor, action_valid: Optional[tf.Tensor] = None) -> Tuple['Connect4Environment', tf.Tensor]:
      """
      Parameters:
      action : array of actions to apply
      action_valid : (Tensor(bool)) to apply action or not
      Returns:
      tuple with (Connect4Environment, reward)
      """
      """
      tf.assert_equal(tf.rank(action), 1)
      tf.assert_equal(tf.shape(action)[0], tf.shape(self.done)[0])
      tf.assert_equal(action.dtype, tf.int32)
      tf.assert_equal(tf.rank(action_valid), 1)
      tf.assert_equal(action_valid.dtype, tf.bool)
      #tf.assert_equal(tf.logical_or(tf.gather(self.board[...,-1,:], action, batch_dims=1, axis=1)==[0], tf.logical_or(self.done, tf.logical_not(action_valid))), [True]) # network have to learn invalid actions
      """
      if action_valid is None:
        action_valid = tf.constant([True])

      cur_board = self.board
      cur_turn = self.turn
      cur_done = self.done
      cur_winner = self.winner

      with tf.name_scope("apply_action"):
        done_mask = tf.cast(tf.logical_not(cur_done), tf.int32)
        action_valid = tf.expand_dims(
            tf.cast(action_valid, tf.int32), -1) * tf.expand_dims(done_mask, -1)
        action_mask = tf.expand_dims(tf.one_hot(
            action, 7, dtype=tf.int32) * action_valid, 1)  # withdraw action if already done
        # tf.debugging.assert_equal(action_mask, action_mask * tf.expand_dims(tf.cast(Connect4Environment.legal_actions(self), tf.int32), 1) ) # assert on legal actions
        b_mask = tf.math.abs(cur_board)
        sel_mask = (tf.slice(tf.pad(b_mask, [[0, 0], [1, 0], [0, 0]], constant_values=1), [0, 0, 0], [tf.shape(b_mask)[0], 6, 7]) * (
            1-b_mask) * action_mask)  # ajout une ligne vide+crop, puis détection les emplacement bordure, puis multiplication avec action
        player = Connect4Environment.to_play(cur_turn)
        opponent = player * -1
        new_board = tf.where(sel_mask == 1, tf.expand_dims(tf.expand_dims(
            player, -1), -1), cur_board)
        new_turn = cur_turn + tf.cast(done_mask, tf.int32)

      with tf.name_scope("check_winner"):
        new_winner = tf.where(Connect4Environment.check_winner(
            new_board, player), player, cur_winner)  # p if w else self.winner
        # check legal move
        forbidden_move = tf.reduce_any(tf.logical_and(tf.logical_not(
            Connect4Environment.legal_actions(self)), action_mask[:, 0, :] == 1), axis=1)
        new_winner = tf.where(forbidden_move, opponent, new_winner)

      with tf.name_scope("check_end_of_game_or_draw"):
        new_done = tf.logical_or(
            tf.math.greater_equal(new_turn, 6*7), new_winner != 0)

      with tf.name_scope("compute_reward"):
        reward = tf.where(new_winner == player, 1.0, tf.where(new_winner == opponent, -1.0, -0.01))
        reward = tf.where(new_done, reward, 0.0)
        reward = tf.where(cur_done, 0.0, reward)

      return Connect4Environment(new_board, new_turn, new_done, new_winner), reward

  @staticmethod
  @tf.function(jit_compile=True)
  def check_winner(board: tf.Tensor, p: tf.Tensor) -> tf.Tensor:
    """
    tf.assert_equal(tf.rank(board), 3)
    tf.assert_equal(tf.shape(board)[1:], [6,7])
    tf.assert_equal(tf.rank(p), 1)
    tf.assert_equal(p.dtype, tf.int32)
    tf.assert_equal(tf.shape(board)[0], tf.shape(p)[0])
    """

    batch_size = tf.shape(board)[0]
    p_mask = tf.expand_dims(tf.expand_dims(p, -1), -1)
    pad = tf.pad(board, [[0, 0], [3, 0], [3, 3]], constant_values=0)

    sh1 = tf.slice(pad, [0, 3, 0], [batch_size, 6, 7])
    sh2 = tf.slice(pad, [0, 3, 1], [batch_size, 6, 7])
    sh3 = tf.slice(pad, [0, 3, 2], [batch_size, 6, 7])
    h_mask = tf.reduce_any(tf.math.logical_and(tf.math.logical_and(
        board == p_mask, board == sh1), tf.math.logical_and(board == sh2, board == sh3)), [1, 2])

    sv1 = tf.slice(pad, [0, 0, 3], [batch_size, 6, 7])
    sv2 = tf.slice(pad, [0, 1, 3], [batch_size, 6, 7])
    sv3 = tf.slice(pad, [0, 2, 3], [batch_size, 6, 7])
    v_mask = tf.reduce_any(tf.math.logical_and(tf.math.logical_and(
        board == p_mask, board == sv1), tf.math.logical_and(board == sv2, board == sv3)), [1, 2])

    sd11 = tf.slice(pad, [0, 0, 0], [batch_size, 6, 7])
    sd12 = tf.slice(pad, [0, 1, 1], [batch_size, 6, 7])
    sd13 = tf.slice(pad, [0, 2, 2], [batch_size, 6, 7])
    d1_mask = tf.reduce_any(tf.math.logical_and(tf.math.logical_and(
        board == p_mask, board == sd11), tf.math.logical_and(board == sd12, board == sd13)), [1, 2])

    sd21 = tf.slice(pad, [0, 0, 6], [batch_size, 6, 7])
    sd22 = tf.slice(pad, [0, 1, 5], [batch_size, 6, 7])
    sd23 = tf.slice(pad, [0, 2, 4], [batch_size, 6, 7])
    d2_mask = tf.reduce_any(tf.math.logical_and(tf.math.logical_and(
        board == p_mask, board == sd21), tf.math.logical_and(board == sd22, board == sd23)), [1, 2])

    w = tf.logical_or(tf.logical_or(h_mask, v_mask),
                      tf.logical_or(d1_mask, d2_mask))
    return w

  @staticmethod
  def to_play(turn: tf.Tensor) -> tf.Tensor:
    return tf.where(tf.math.floormod(turn, 2) == 0, Player.white.value, Player.black.value)

  @staticmethod
  def render(self: 'Connect4Environment', index: int) -> None:
    print("\nRound: " + str(self.turn.numpy()))

    for j in range(5, -1, -1):
      print("\t", end="")
      for i in range(7):
        p = self.board[index][j][i].numpy()
        ch = ' ' if p == 0 else 'X' if p == 1 else 'O'
        print("| " + ch, end=" ")
      print("|")
    print("\t  _   _   _   _   _   _   _ ")
    print("\t  1   2   3   4   5   6   7 ")

    if self.done[index].numpy():
      print("Game Over!")
      if self.winner[index] == 1:
        print("X is the winner")
      elif self.winner[index] == -1:
        print("O is the winner")
      else:
        print("Game was a draw")

  @staticmethod
  @tf.function(jit_compile=True)
  def observation(self: 'Connect4Environment') -> tf.Tensor:
    to_play = tf.tile(tf.reshape(tf.math.floormod(self.turn, 2) != 0, [
                      tf.shape(self.board)[0], 1, 1]), [1, 6, 7])
    return tf.stack([self.board > 0, self.board < 0, to_play], -1)

  def obs2env(observation: tf.Tensor) -> 'Connect4Environment':
    """
    Build Connect4Environment from observation
    @param image game observation
    @return Connect4Environment with environment
    """
    assert(observation.shape[1:] == [6,7,3])
    assert(observation.dtype == tf.bool)

    batch_size = tf.shape(observation)[0]
    board = tf.where(observation[..., 0], tf.constant(1, tf.int32), tf.where(
        observation[..., 1], tf.constant(-1, tf.int32), 0))
    turn = tf.reduce_sum(
        tf.cast(observation[..., :2], tf.int32), axis=[-1, -2, -3])
    p0 = Connect4Environment.check_winner(
        board, tf.fill([batch_size], Player.white.value))
    p1 = Connect4Environment.check_winner(
        board, tf.fill([batch_size], Player.black.value))
    winner = tf.where(p0, Player.white.value, tf.where(
        p1, Player.black.value, 0))  # tf.zeros(batch_size, dtype=tf.int8)
    done = tf.logical_or(winner != 0, turn >= 6*7)

    return Connect4Environment(board, turn, done, winner)




def assert_Connect4Environment(self: Connect4Environment):

  batch_size = tf.shape(self.board)[0]

  assert(isinstance(self.board, tf.Tensor))
  tf.debugging.assert_type(self.board, tf.int32)
  tf.debugging.assert_rank(self.board, 3)
  tf.assert_equal(tf.shape(self.board)[0], batch_size)
  tf.assert_equal(tf.shape(self.board)[1], 6)
  tf.assert_equal(tf.shape(self.board)[2], 7)

  assert(isinstance(self.turn, tf.Tensor))
  tf.debugging.assert_type(self.turn, tf.int32)
  tf.debugging.assert_rank(self.turn, 1)
  tf.assert_equal(tf.shape(self.turn)[0], batch_size)

  assert(isinstance(self.done, tf.Tensor))
  tf.debugging.assert_type(self.done, tf.bool)
  tf.debugging.assert_rank(self.done, 1)
  tf.assert_equal(tf.shape(self.done)[0], batch_size)

  assert(isinstance(self.winner, tf.Tensor))
  tf.debugging.assert_type(self.winner, tf.int32)
  tf.debugging.assert_rank(self.winner, 1)
  tf.assert_equal(tf.shape(self.winner)[0], batch_size)
