import logging
import tensorflow as tf  # type: ignore
import unittest

from ... import connect4_tf as connect4
from .. import __devtools__ as connect4_devtools


class TestConnect4(unittest.TestCase):
  """
    Connect 4 unit tests
    /!\ arrays start from bottom /!\
  """

  def test_environment_legal_actions(self):
    batch_size = 1

    #default_board = tf.zeros((batch_size,6,7), dtype=tf.int8)
    default_turn = tf.zeros(batch_size, dtype=tf.int32)
    default_done = tf.fill([batch_size], False)
    default_winner = tf.zeros(batch_size, dtype=tf.int32)

    # test actions on emtpy board
    env = connect4.Connect4Environment.init(batch_size)
    actions = connect4.Connect4Environment.legal_actions(env)
    tf.assert_equal(actions, tf.constant(
        [[True, True, True, True, True, True, True]]))

    # test actions on quasi-full board
    board = tf.constant(
        [[
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [0, 1, 0, 1, 1, 1, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, default_turn, default_done, default_winner)
    actions = connect4.Connect4Environment.legal_actions(env)
    tf.assert_equal(actions, tf.constant(
        [[True, False, True, False, False, False, True]]))

    # test legal/unlegal action
    connect4.assert_Connect4Environment(env)
    action = tf.constant(0)
    _, reward = connect4.Connect4Environment.step(env, action)
    tf.assert_equal(reward, 1.0)

    action = tf.constant(1)
    _, reward = connect4.Connect4Environment.step(env, action)
    tf.assert_equal(reward, -1.0)

    # test actions on full board
    board = tf.constant(
        [[
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, default_turn, default_done, default_winner)
    actions = connect4.Connect4Environment.legal_actions(env)
    tf.assert_equal(actions, tf.constant(
        [[False, False, False, False, False, False, False]]))

  def test_environment_opening(self):
    """
      test internal representation of environment for every moves up to depth 3
    """
    batch_size = 1

    #test board on oppening
    env0 = connect4.Connect4Environment.init(batch_size)
    actions0 = connect4.Connect4Environment.legal_actions(env0)
    for action_index0 in tf.cast(tf.where(tf.squeeze(actions0, 0)), tf.int32):
      env1, reward01 = connect4.Connect4Environment.step(env0, action_index0)
      tf.assert_equal(reward01[0], 0.0)
      tf.assert_equal(tf.reduce_sum(tf.cast(env1.board == 1, tf.int32)), 1)
      tf.assert_equal(tf.reduce_sum(tf.cast(env1.board == -1, tf.int32)), 0)
      tf.assert_equal(tf.cast(env1.turn, tf.int32), 1)

      actions1 = connect4.Connect4Environment.legal_actions(env1)
      for action_index1 in tf.cast(tf.where(tf.squeeze(actions1, 0)), tf.int32):
        env2, reward12 = connect4.Connect4Environment.step(env1, action_index1)
        tf.assert_equal(reward12[0], 0.0)
        tf.assert_equal(tf.reduce_sum(tf.cast(env2.board == 1, tf.int32)), 1)
        tf.assert_equal(tf.reduce_sum(tf.cast(env2.board == -1, tf.int32)), 1)
        tf.assert_equal(tf.cast(env2.turn, tf.int32), 2)

        actions2 = connect4.Connect4Environment.legal_actions(env2)
        for action_index2 in tf.cast(tf.where(tf.squeeze(actions2, 0)), tf.int32):
          env3, reward23 = connect4.Connect4Environment.step(
              env2, action_index2)
          tf.assert_equal(reward23[0], 0.0)
          tf.assert_equal(tf.reduce_sum(tf.cast(env3.board == 1, tf.int32)), 2)
          tf.assert_equal(tf.reduce_sum(
              tf.cast(env3.board == -1, tf.int32)), 1)
          tf.assert_equal(tf.cast(env3.turn, tf.int32), 3)

          # actions3 = connect4.Connect4Environment.legal_actions(env3)
          # for action_index3 in tf.cast(tf.where(tf.squeeze(actions3, 0)), tf.int32):
          #   env4, reward34 = connect4.Connect4Environment.step(env3, action_index3)
          #   tf.assert_equal(reward34[0], 0.0)
          #   tf.assert_equal(tf.reduce_sum(tf.cast(env4.board==1, tf.int32)), 2)
          #   tf.assert_equal(tf.reduce_sum(tf.cast(env4.board==-1, tf.int32)), 2)
          #   tf.assert_equal(tf.cast(env4.turn, tf.int32), 4)

  def test_environment_win(self):
    """
      test win trigger
    """
    batch_size = 1
    default_done = tf.fill([batch_size], False)
    default_winner = tf.zeros(batch_size, dtype=tf.int32)

    turnP0 = tf.fill([batch_size], tf.constant(0, dtype=tf.int32))
    turnP1 = tf.fill([batch_size], tf.constant(1, dtype=tf.int32))

    # P0 -- horizontal
    board = tf.constant(
        [[
            [0, 1, 1, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)
    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)
    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 1, 1, 1, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)
    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    # P0 -- vertical
    board = tf.constant(
        [[
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    # P0 -- diagonal
    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [-1, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0, 1, -1],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP0, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    # P1 -- horizontal
    board = tf.constant(
        [[
            [0, -1, -1, -1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, -1, -1, -1, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)
    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [0, -1, -1, -1, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)
    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, -1, -1, -1, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)
    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    # P1 -- vertical
    board = tf.constant(
        [[
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    # P1 -- diagonal
    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, -1, 0, 0, 0, 0, 0],
            [0, 0, -1, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, -1, 0],
            [0, 0, 0, 0, -1, 0, 0],
            [0, 0, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0],
            [0, 0, -1, 0, 0, 0, 0],
            [1, -1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([0], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

    board = tf.constant(
        [[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, -1, 0, 0],
            [0, 0, 0, 0, 0, -1, 1],
            [0, 0, 0, 0, 0, 0, 0],
        ]], dtype=tf.int32)
    env = connect4.Connect4Environment(
        board, turnP1, default_done, default_winner)

    action = tf.constant([6], tf.int32)
    new_env, reward = connect4.Connect4Environment.step(env, action)
    # connect4.Connect4Environment.render(new_env, 0)
    tf.assert_equal(new_env.winner[0], tf.cast(-1, tf.int32))
    tf.assert_equal(new_env.done[0], True)
    tf.assert_equal(reward[0], 1.0)

  def test_Connect4Environment_equal(self, batch_size: int = 256):
    """
    Test connect4.Connect4Environment_equal function, using random actions on game plays 
    """
    env = connect4.Connect4Environment.init(batch_size)
    for _ in tf.range(6*7):
      actions = connect4.Connect4Environment.legal_actions(env)
      rnd = tf.random.uniform(tf.shape(actions), 0, 1)
      action = tf.math.argmax(tf.cast(actions, tf.float32)
                              * rnd, axis=1, output_type=tf.int32)
      env, _ = connect4.Connect4Environment.step(env, action)

      obs = connect4.Connect4Environment.observation(env)
      rp = connect4_devtools.Connect4Environment_obs2rp(obs)
      obs_p = connect4_devtools.Connect4Environment_rp2obs(rp)
      env_p = connect4.Connect4Environment.obs2env(obs_p)
      connect4_devtools.Connect4Environment_assert_equal(env, env_p)

  def test_environment_timecluster(self):
    """
    test simulating env at different time steps
    """
    batch_size = 4
    env0 = connect4.Connect4Environment.init(batch_size)

    legal_actions0 = connect4.Connect4Environment.legal_actions(env0)
    rnd = tf.random.uniform(tf.shape(legal_actions0), 0, 1)
    action0 = tf.math.argmax(
        tf.cast(legal_actions0, tf.float32) * rnd, axis=1, output_type=tf.int32)
    env1, _ = connect4.Connect4Environment.step(env0, action0)

    legal_actions1 = connect4.Connect4Environment.legal_actions(env0)
    rnd = tf.random.uniform(tf.shape(legal_actions1), 0, 1)
    action1 = tf.math.argmax(
        tf.cast(legal_actions1, tf.float32) * rnd, axis=1, output_type=tf.int32)
    env2, _ = connect4.Connect4Environment.step(env1, action1)

    legal_actions2 = connect4.Connect4Environment.legal_actions(env0)
    rnd = tf.random.uniform(tf.shape(legal_actions2), 0, 1)
    action2 = tf.math.argmax(
        tf.cast(legal_actions2, tf.float32) * rnd, axis=1, output_type=tf.int32)
    env3, _ = connect4.Connect4Environment.step(env2, action2)

    final_env = env3
    for _ in tf.range(6*7):
      legal_actions = connect4.Connect4Environment.legal_actions(final_env)
      rnd = tf.random.uniform(tf.shape(legal_actions), 0, 1)
      final_action = tf.math.argmax(
          tf.cast(legal_actions, tf.float32) * rnd, axis=1, output_type=tf.int32)
      final_env, _ = connect4.Connect4Environment.step(final_env, final_action)

    board = tf.concat([env0.board, env1.board, env2.board, final_env.board], 0)
    turn = tf.concat([env0.turn, env1.turn, env2.turn, final_env.turn], 0)
    done = tf.concat([env0.done, env1.done, env2.done, final_env.done], 0)
    winner = tf.concat(
        [env0.winner, env1.winner, env2.winner, final_env.winner], 0)
    all_env = connect4.Connect4Environment(board, turn, done, winner)
    #all_legal_actions = connect4.Connect4Environment.legal_actions(all_env)
    #rnd = tf.random.uniform(tf.shape(all_legal_actions), 0, 1)
    #all_action = tf.math.argmax(tf.cast(all_legal_actions, tf.float32) * rnd, axis=1, output_type=tf.int32)
    all_action = tf.concat([action0, action1, action2, final_action], 0)
    all_env, _ = connect4.Connect4Environment.step(all_env, all_action)

    tf.assert_equal(all_env.board[batch_size*0:batch_size*1], env1.board)
    tf.assert_equal(all_env.turn[batch_size*0:batch_size*1], env1.turn)
    tf.assert_equal(all_env.done[batch_size*0:batch_size*1], env1.done)
    tf.assert_equal(all_env.winner[batch_size*0:batch_size*1], env1.winner)

    tf.assert_equal(all_env.board[batch_size*1:batch_size*2], env2.board)
    tf.assert_equal(all_env.turn[batch_size*1:batch_size*2], env2.turn)
    tf.assert_equal(all_env.done[batch_size*1:batch_size*2], env2.done)
    tf.assert_equal(all_env.winner[batch_size*1:batch_size*2], env2.winner)

    tf.assert_equal(all_env.board[batch_size*2:batch_size*3], env3.board)
    tf.assert_equal(all_env.turn[batch_size*2:batch_size*3], env3.turn)
    tf.assert_equal(all_env.done[batch_size*2:batch_size*3], env3.done)
    tf.assert_equal(all_env.winner[batch_size*2:batch_size*3], env3.winner)

    tf.assert_equal(all_env.board[batch_size*3:batch_size*4], final_env.board)
    tf.assert_equal(all_env.turn[batch_size*3:batch_size*4], final_env.turn)
    tf.assert_equal(all_env.done[batch_size*3:batch_size*4], final_env.done)
    tf.assert_equal(
        all_env.winner[batch_size*3:batch_size*4], final_env.winner)

  def test_mc_openning(self):
    """
      Check win policy ratios for first 2 steps
      - 1st step : column 3 has the top ratio, then 2&4, 1&5, 0&6
    """

    play_batch_size = 1024*1024
    action_space_size = connect4.Connect4Environment.action_space_size
    mc_env = connect4.Connect4Environment.init(play_batch_size)

    legal_actions = connect4.Connect4Environment.legal_actions(
        mc_env)  # list available actions

    r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
    root_action = tf.math.argmax(
        tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32)
    mc_env, _ = connect4.Connect4Environment.step(mc_env, root_action)

    for _ in tf.range(6*7):
      legal_actions = connect4.Connect4Environment.legal_actions(mc_env)
      r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
      action = tf.math.argmax(
          tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32)
      mc_env, _ = connect4.Connect4Environment.step(mc_env, action)

    # test whites
    action_simu = play_batch_size / action_space_size
    win_3 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 3, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    tf.assert_greater(win_3, 0.5)
    win_2 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 2, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    win_4 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 4, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_2, win_3)
    tf.assert_less(win_4, win_3)
    win_1 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 1, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    win_5 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 5, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_1, win_2)
    tf.assert_less(win_1, win_4)
    tf.assert_less(win_5, win_2)
    tf.assert_less(win_5, win_4)
    win_0 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 0, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    win_6 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 6, mc_env.winner == tf.constant(1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_0, win_1)
    tf.assert_less(win_0, win_5)
    tf.assert_less(win_6, win_1)
    tf.assert_less(win_6, win_5)
    logging.info(
        f"white win proba {win_0:0.2f}, {win_1:0.2f}, {win_2:0.2f}, {win_3:0.2f}, {win_4:0.2f}, {win_5:0.2f}, {win_6:0.2f}")

    # test blacks
    mc_env = connect4.Connect4Environment.init(play_batch_size)
    action = tf.fill([play_batch_size], 3, tf.int32)
    mc_env, _ = connect4.Connect4Environment.step(mc_env, action)

    legal_actions = connect4.Connect4Environment.legal_actions(
        mc_env)  # list available actions

    r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
    root_action = tf.math.argmax(
        tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32)
    mc_env, _ = connect4.Connect4Environment.step(mc_env, root_action)

    for _ in tf.range(6*7):  # execute the whole game
      legal_actions = connect4.Connect4Environment.legal_actions(mc_env)
      r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
      action = tf.math.argmax(
          tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32)
      mc_env, _ = connect4.Connect4Environment.step(mc_env, action)

    action_simu = play_batch_size / action_space_size
    win_3 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 3, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_3, 0.5)
    win_2 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 2, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    win_4 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 4, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(tf.math.abs(win_2-win_3), 0.1)
    tf.assert_less(tf.math.abs(win_4-win_3), 0.1)
    win_1 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 1, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    win_5 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 5, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_1, win_2)
    tf.assert_less(win_1, win_4)
    tf.assert_less(win_5, win_2)
    tf.assert_less(win_5, win_4)
    win_0 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 0, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    win_6 = tf.reduce_sum(tf.cast(tf.logical_and(
        root_action == 6, mc_env.winner == tf.constant(-1, tf.int32)), tf.float32)) / action_simu
    tf.assert_less(win_0, win_1)
    tf.assert_less(win_0, win_5)
    tf.assert_less(win_6, win_1)
    tf.assert_less(win_6, win_5)
    logging.info(
        f"black win proba (after action:3) {win_0:0.2f}, {win_1:0.2f}, {win_2:0.2f}, {win_3:0.2f}, {win_4:0.2f}, {win_5:0.2f}, {win_6:0.2f}")


if __name__ == "__main__":
  #tf.config.run_functions_eagerly(True) # for debugging
  tf.config.optimizer.set_jit(True)

  unittest.main()

  # test_environment_legal_actions()
  # test_environment_opening()
  # test_environment_win()
  # test_Connect4Environment_equal()
  # test_environment_timecluster()

  # t = time.perf_counter()
  # test_mc_openning(config)
  # elapsed_time = time.perf_counter() - t
  # logging.info(f"tests took {elapsed_time:04f} secondes")
