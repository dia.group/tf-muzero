import logging
import numpy as np
import tensorflow as tf  # type: ignore
import unittest

from .. import refmoves_kaggle
from ... import connect4_tf as connect4
from .. import __devtools__ as connect4_devtools


class TestRefMovesKaggle(unittest.TestCase):

  def setUp(self) -> None:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        )
    np.set_printoptions(precision=4, floatmode='fixed', suppress=True)
    #tf.config.run_functions_eagerly(True) # debug
    return super().setUp()

  def test_loading(self):
    observation, moves = refmoves_kaggle.load_ref_moves()

    self.assertTrue(isinstance(observation, tf.Tensor))
    self.assertTrue(tf.reduce_all(
        tf.shape(observation) == (1000, 6, 7, 3)))
    self.assertTrue(observation.dtype == tf.uint8)

    self.assertTrue(isinstance(moves, tf.Tensor))
    self.assertTrue(tf.reduce_all(tf.shape(moves) == (1000, 7)))
    self.assertTrue(moves.dtype == tf.int32)

    self.assertTrue(tf.reduce_max(observation) == 1)
    self.assertTrue(tf.reduce_min(observation) == 0)

    self.assertTrue(tf.reduce_min(moves) == -99)  # special value
    self.assertTrue(tf.reduce_max(moves) == 14)

  def test_vs_mc(self):
    observation, moves = refmoves_kaggle.load_ref_moves()
    environment = connect4.Connect4Environment.obs2env(
        observation != 0)
    num_simulations = 64

    #actions = connect4_devtools.select_action_mc_parallel(environment, num_simulations)  # need too much memory
    actions = connect4_devtools.select_action_mc_sequential(
        environment, num_simulations)

    agent_score = tf.gather(moves, actions, axis=1, batch_dims=1)

    perfect_score = tf.reduce_max(moves, axis=1)
    perfect_move_count = tf.reduce_sum(
        tf.where(agent_score == perfect_score, 1, 0))

    def win_loss_draw(score):
        return tf.where(score > 0, 1, tf.where(score < 0, -1, 0))
    good_move_count = tf.reduce_sum(tf.cast(win_loss_draw(
        perfect_score) == win_loss_draw(agent_score), tf.int32))
    perfect_move_ratio = perfect_move_count/observation.shape[0]
    good_move_ratio = good_move_count/observation.shape[0]
    logging.info(f"MC simulations: {num_simulations}")
    logging.info(f"perfect_move_ratio: {perfect_move_ratio}")
    logging.info(f"good_move_ratio: {good_move_ratio}")
    random_perfect_move_ratio = 0.22
    random_good_move_ratio = 0.67
    self.assertTrue(perfect_move_ratio > random_perfect_move_ratio +
                    (1.0-random_perfect_move_ratio)*0.2)
    self.assertTrue(good_move_ratio > random_good_move_ratio +
                    (1.0-random_good_move_ratio)*0.2)

  def test_vs_mcts(self):
    from src.agents.tfagents.muzero import common as muzero_common
    from src.agents.tfagents.muzero import play as muzero_play

    class MOCKNetworkSimuValueStrategy(object):
      def __init__(self, local_config):
        self.action_space_size = local_config.action_space_size

      def training_steps(self):
        return 0

      def initial_inference(self, image) -> muzero_common.NetworkOutput:
        env = connect4.Connect4Environment.obs2env(image)
        to_play = connect4.Connect4Environment.to_play(env.turn)

        v, p = self.compute_v_p(env, to_play)

        value_relative = tf.expand_dims(
            tf.where(to_play == connect4.Player.white.value, 1.0, -1.0), -1)
        v *= value_relative
        h = image
        return muzero_common.NetworkOutput(v, tf.zeros_like(v), p, h)

      def recurrent_inference(self, hidden_state, action) -> muzero_common.NetworkOutput:
        env = connect4.Connect4Environment.obs2env(hidden_state!=0)
        next_env, reward = connect4.Connect4Environment.step(env, action)
        to_play = connect4.Connect4Environment.to_play(env.turn)

        v, p = self.compute_v_p(next_env, to_play)

        h = connect4.Connect4Environment.observation(next_env)
        return muzero_common.NetworkOutput(v, tf.expand_dims(reward, -1), p, h)

      @tf.function
      def compute_v_p(self, env, to_play):
        simu_env = env
        turn_to_end = 6*7 - tf.cast(tf.reduce_min(env.turn), tf.int32)
        value = tf.zeros_like(env.turn, dtype=tf.float32)
        for _ in tf.range(turn_to_end):  # execute the whole game
          actions = connect4.Connect4Environment.legal_actions(simu_env)
          r = tf.random.uniform(tf.shape(actions), 0, 1)
          action = tf.math.argmax(tf.cast(actions, tf.float32)
                                  * r, axis=-1, output_type=tf.int32)
          reward_relative = tf.where(to_play == connect4.Connect4Environment.to_play(simu_env.turn), -1.0, 1.0)
          simu_env, reward = connect4.Connect4Environment.step(simu_env, action)
          value += reward * reward_relative
        value = tf.expand_dims(value, -1)
        legal_actions = connect4.Connect4Environment.legal_actions(env)
        p = tf.cast(legal_actions, tf.float32)
        return value, p

    def select_action_nn_mcts(environment: connect4.Connect4Environment, config, network: muzero_common.MuzeroNetwork) -> tf.Tensor:
      rp_shape = tf.TensorSpec([6,7,3], tf.int32)
      batch_size = tf.shape(environment.turn)[0]
      tree = muzero_play.Tree.init(
        batch_size, config.action_space_size, config.num_simulations, rp_shape)
      current_observation = connect4.Connect4Environment.observation(environment)
      tree = muzero_play.expand_node(
        tree,
        tree.rootNode(),
        config.to_play(environment.turn),
        connect4.Connect4Environment.legal_actions(environment),
        network.initial_inference(current_observation),
        config.action_space_size)
      #tree = add_exploration_noise(config, tree) # no exploration to avoid unlegal moves
      tree, _ = muzero_play.run_mcts(
        config=config,
        tree=tree,
        history_depth=environment.turn,
        network=network,
        prnd=tf.convert_to_tensor([0, 42]),
        )
      action = muzero_play.select_action(
        tree,
        config.action_space_size,
        temperature=0.0,
        prnd=tf.convert_to_tensor([0, 42]),
        )
      return action
      
    observation, moves = refmoves_kaggle.load_ref_moves()
    environment = connect4.Connect4Environment.obs2env(observation != 0)

    num_simulations = 64
    local_config = connect4_devtools.make_muzero_connect4_config(num_simulations=num_simulations)
    network = MOCKNetworkSimuValueStrategy(local_config)

    #actions = connect4_devtools.select_action_mc_parallel(environment, num_simulations)  # need too much memory
    # actions = connect4_devtools.select_action_mc_sequential(
    #     environment, num_simulations)
    actions = select_action_nn_mcts(environment, local_config, network)

    agent_score = tf.gather(moves, actions, axis=1, batch_dims=1)

    perfect_score = tf.reduce_max(moves, axis=1)
    perfect_move_count = tf.reduce_sum(
        tf.where(agent_score == perfect_score, 1, 0))

    def win_loss_draw(score):
        return tf.where(score > 0, 1, tf.where(score < 0, -1, 0))
    good_move_count = tf.reduce_sum(tf.cast(win_loss_draw(
        perfect_score) == win_loss_draw(agent_score), tf.int32))
    perfect_move_ratio = perfect_move_count/observation.shape[0]
    good_move_ratio = good_move_count/observation.shape[0]
    logging.info(f"MCTS simulations: {num_simulations}")
    logging.info(f"perfect_move_ratio: {perfect_move_ratio}")
    logging.info(f"good_move_ratio: {good_move_ratio}")
    random_perfect_move_ratio = 0.22
    random_good_move_ratio = 0.67
    self.assertTrue(perfect_move_ratio > random_perfect_move_ratio +
                    (1.0-random_perfect_move_ratio)*0.2)
    self.assertTrue(good_move_ratio > random_good_move_ratio +
                    (1.0-random_good_move_ratio)*0.2)
