import logging
import tensorflow as tf  # type: ignore
import tf_agents   # type: ignore
import unittest

from ... import connect4_tfa as connect4


class TestConnect4(unittest.TestCase):

  def test_Connect4RandomEnv(self):
    c4env = connect4.Connect4RandomEnv(4)
    time_step = c4env.reset()
    first = time_step.is_first()
    last = time_step.is_last()
    time_step = c4env.current_time_step()
    logging.info(c4env.action_spec())
    logging.info(c4env.observation_spec())
    action = tf_agents.specs.tensor_spec.sample_bounded_spec(
        c4env.action_spec(), outer_dims=[c4env.batch_size])
    #action = tensor_spec.sample_spec_nest(c4env.action_spec(), outer_dims=(1,))
    next_time_step = c4env.step(action)
    logging.info(next_time_step)

  def test_Connect4TFEnv(self):
    c4env = connect4.Connect4TFEnv(4)
    time_step = c4env.reset()
    self.assertTrue(c4env.observation_spec(
    ).is_compatible_with(time_step.observation[0]))
    self.assertTrue(
        c4env.reward_spec().is_compatible_with(time_step.reward[0]))
    first = time_step.is_first()
    self.assertTrue(all(first))
    last = time_step.is_last()
    self.assertTrue(all(last == False))
    #time_step = c4env.current_time_step()
    action = tf_agents.specs.tensor_spec.sample_bounded_spec(
        c4env.action_spec(), outer_dims=[c4env.batch_size])
    self.assertTrue(c4env.action_spec().is_compatible_with(action[0]))
    #action = tensor_spec.sample_spec_nest(c4env.action_spec(), outer_dims=(1,))
    next_time_step = c4env.step(action)

  def testConnect4TFEnvContinuousCollect(self):
    from tf_agents.replay_buffers import episodic_replay_buffer  # type: ignore

    tf_env = connect4.Connect4TFEnv(4)
    my_random_tf_policy = tf_agents.policies.random_tf_policy.RandomTFPolicy(
        action_spec=tf_env.action_spec(), time_step_spec=tf_env.time_step_spec())

    replay_buffer = episodic_replay_buffer.EpisodicReplayBuffer(
        completed_only=True,
        data_spec=my_random_tf_policy.collect_data_spec,
        capacity=16
    )

    traj_obs = episodic_replay_buffer.StatefulEpisodicReplayBuffer(
        replay_buffer, num_episodes=tf_env.batch_size).add_batch

    current_policy_state = my_random_tf_policy.get_initial_state(
        tf_env.batch_size)
    current_env_state = tf_env.reset()
    collect_driver = tf_agents.drivers.dynamic_episode_driver.DynamicEpisodeDriver(
        tf_env,
        my_random_tf_policy,
        observers=[traj_obs],
        num_episodes=16)

    for _ in range(4):
      current_env_state, current_policy_state = collect_driver.run(
          time_step=current_env_state, policy_state=current_policy_state)

    # replay_buffer.num_frames()
    self.assertGreaterEqual(len(replay_buffer._completed_episodes()), 16-4)
    #all_items = replay_buffer.gather_all()
    for episode in replay_buffer.as_dataset(single_deterministic_pass=True):
      #self.assertTrue(tf.reduce_sum(episode.reward)!=0.0)  # False for draw games
      self.assertTrue(tf.reduce_sum(episode.observation[0]) == 0)
      self.assertTrue(episode.step_type[0] == 0)
      self.assertTrue(episode.next_step_type[0] == 1)
      # TODO: fix -- not always True
      #self.assertTrue(len(episode.step_type) >= 4)
      #self.assertTrue(episode.step_type[-1]==2)
      #self.assertTrue(episode.next_step_type[-1]==0)
      #self.assertTrue(episode.next_step_type[-2]==2)


if __name__ == '__main__':
  unittest.main()
