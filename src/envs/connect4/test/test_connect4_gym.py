import logging
import unittest

from ... import connect4_gym as connect4gym


class TestConnect4Gym(unittest.TestCase):

  def test_environment_creation(self):
    c4env = connect4gym.Connect4GymEnv()
    logging.info("obs_space: " + str(c4env.observation_space))
    logging.info("act_space: " + str(c4env.action_space))
    obs = c4env.reset()
    logging.info("obs:" + str(obs))

    #action = [3]
    action = c4env.action_space.sample()
    obs, reward, done, user_data = c4env.step(action)

    logging.info("action:" + str(action))
    logging.info("next obs:" + str(obs))
    logging.info("reward:" + str(reward))
    logging.info("done:" + str(done))


if __name__ == "__main__":
  unittest.main()
