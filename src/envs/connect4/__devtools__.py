"""
  Tools for testings
"""
import math
import numpy as np
import tensorflow as tf  # type: ignore

from .. import connect4_tf as connect4
from src.agents.tfagents.muzero import common as muzero_common


g_num_filters = 3  # aka num_features of network processings
g_rp_shape = (6, 7, g_num_filters)  # internal representation shape


# declare function out of scope to avoid retracing
def _temperature_fn(num_moves, training_steps):
  return 0.0


def make_muzero_connect4_config(num_simulations=50, **kwargs):
  config = muzero_common.MuZeroConfig(
      player_turn=2,   # C4 is a 2 player step per game state
      action_space_size=7,
      known_bounds=muzero_common.KnownBounds(-1, 1),
      num_simulations=num_simulations,
      visit_softmax_temperature_fn=_temperature_fn,
      pb_c_init=1.0,
      **kwargs,
  )
  return config


def Connect4Environment_obs2rp(observation: tf.Tensor):
  """
  Encode Connect4 observation to hidden_state/representation
  @param image Connect4 observation
  @return hidden_state
  """
  tf.debugging.assert_rank(observation, 4)
  tf.debugging.assert_type(observation, tf.bool)
  assert(observation.shape[1:] == [6, 7, 3])

  h = tf.cast(observation, tf.float32)
  return h


def Connect4Environment_rp2obs(rp: tf.Tensor):
  """
  Extract Connect4 observation from hidden_state/representation
  @param rp hidden_state
  @return Connect4 observation
  """
  tf.debugging.assert_rank(rp, 4)
  tf.debugging.assert_type(rp, tf.float32)
  assert(rp.shape[1:] == [6, 7, 3])

  observation = rp > 0.0
  return observation


def Connect4Environment_assert_equal(c4env: connect4.Connect4Environment, c4env_other: connect4.Connect4Environment):
  """
  Test Equality between two Connect4Environment
  turn is not tested when beyond 8, as we can't check turn once finished (turn could be increased after game ends)  
  """
  tf.assert_equal(tf.shape(c4env.board), tf.shape(c4env_other.board))
  tf.assert_equal(c4env.board, c4env_other.board)
  tf.assert_equal(c4env.turn, c4env_other.turn)
  tf.assert_equal(c4env.done, c4env_other.done)
  tf.assert_equal(c4env.winner, c4env_other.winner)


@tf.function
def Connect4Environment_tile(env: connect4.Connect4Environment, tile_size: int):
  board = tf.tile(env.board, [tile_size, 1, 1])
  turn = tf.tile(env.turn, [tile_size])
  done = tf.tile(env.done, [tile_size])
  winner = tf.tile(env.winner, [tile_size])
  return connect4.Connect4Environment(board, turn, done, winner)


@tf.function
def Connect4Environment_untile(env: connect4.Connect4Environment, tile_size: int):
  batch_size = tf.shape(env.board)[0]//tile_size
  board = tf.reshape(env.board, [tile_size, batch_size, 6, 7])
  turn = tf.reshape(env.turn, [tile_size, batch_size])
  done = tf.reshape(env.done, [tile_size, batch_size])
  winner = tf.reshape(env.winner, [tile_size, batch_size])
  return connect4.Connect4Environment(board, turn, done, winner)


@tf.function
def select_action_random(environment: connect4.Connect4Environment) -> tf.Tensor:
  actions = connect4.Connect4Environment.legal_actions(environment)
  rnd = tf.random.uniform(tf.shape(actions), 0, 1)
  action = tf.math.argmax(tf.cast(actions, tf.float32)
                          * rnd, axis=1, output_type=tf.int32)
  return action


@tf.function(jit_compile=True)
def select_action_mc_sequential(environment: connect4.Connect4Environment, num_simulations: int) -> tf.Tensor:
  """
  sequential computing of mc (a bit slower, but needs less memory than parallel computing)
  """
  legal_actions = connect4.Connect4Environment.legal_actions(
      environment)  # list available actions
  actions_counter = tf.zeros_like(legal_actions, tf.int32)
  actions_accumulator = tf.zeros_like(legal_actions, tf.int32)
  with tf.name_scope("simulation"):
    #for _ in tf.range(num_simulations): # TODO: GPU allocation fails
    def simulation_cond(_actions_counter, _actions_accumulator, _simulation_counter):
      return _simulation_counter > 0

    def simulation_loop(_actions_counter, _actions_accumulator, _simulation_counter):
      r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
      branch = tf.math.argmax(
          tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32)
      mc_env, _ = connect4.Connect4Environment.step(environment, branch)
      turn_to_end = 6*7 - tf.cast(tf.reduce_min(mc_env.turn), tf.int32)
      with tf.name_scope("forward"):  # execute the whole game to the end
        #for _ in tf.range(turn_to_end): # TODO: GPU allocation fails
        def forward_cond(mc_env, forward_counter):
          return forward_counter > 0

        def forward_loop(mc_env, forward_counter):
          actions = connect4.Connect4Environment.legal_actions(mc_env)
          r = tf.random.uniform(tf.shape(actions), 0, 1)
          action = tf.math.argmax(
              tf.cast(actions, tf.float32) * r, axis=-1, output_type=tf.int32)
          mc_env, _ = connect4.Connect4Environment.step(mc_env, action)
          return mc_env, forward_counter - 1
        mc_env, _ = tf.while_loop(
            forward_cond, forward_loop, (mc_env, turn_to_end))

      node_indices2D = tf.stack([tf.range(tf.shape(branch)[0]), branch], -1)
      new_actions_counter = tf.tensor_scatter_nd_add(
          _actions_counter, node_indices2D, tf.ones_like(branch))

      p0 = tf.cast(mc_env.winner == connect4.Player.white.value, tf.int32)
      p1 = tf.cast(mc_env.winner == connect4.Player.black.value, tf.int32)
      w = tf.where(connect4.Connect4Environment.to_play(
          environment.turn) == connect4.Player.white.value, p0, p1)
      new_actions_accumulator = tf.tensor_scatter_nd_add(
          _actions_accumulator, node_indices2D, w)
      return new_actions_counter, new_actions_accumulator, _simulation_counter - 1

    actions_counter, actions_accumulator, _ = tf.while_loop(
        simulation_cond, simulation_loop, (actions_counter, actions_accumulator, num_simulations))

  win_ratio = tf.where(actions_counter > 0, tf.cast(
      actions_accumulator, tf.float32) / tf.cast(actions_counter, tf.float32), 0.0)
  r = tf.random.uniform(tf.shape(legal_actions), 0, 1e-10)
  best_action = tf.math.argmax(win_ratio+r, axis=-1, output_type=tf.int32)
  return best_action


@tf.function
def compute_action_mc_parallel(environment, tile):
  """
  parallel computing of MC (faster than sequential computing, but needs more memory)
  @return action simulation counter and action simulation where current player wins
  """
  assert(tile > 0)

  batch_size = tf.shape(environment.turn)[0]
  action_space = 7
  max_moves = 6*7

  mc_env = Connect4Environment_tile(environment, tile)
  legal_actions = connect4.Connect4Environment.legal_actions(
      mc_env)  # list available actions
  r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
  branch = tf.math.argmax(tf.cast(legal_actions, tf.float32)
                          * r, axis=-1, output_type=tf.int32)
  mc_env, _ = connect4.Connect4Environment.step(mc_env, branch)
  turn_to_end = max_moves - tf.cast(tf.reduce_min(mc_env.turn), tf.int32)
  for _ in tf.range(turn_to_end):  # execute the whole game
    actions = connect4.Connect4Environment.legal_actions(mc_env)
    r = tf.random.uniform(tf.shape(actions), 0, 1)
    action = tf.math.argmax(tf.cast(actions, tf.float32)
                            * r, axis=-1, output_type=tf.int32)
    mc_env, _ = connect4.Connect4Environment.step(mc_env, action)

  mc_final = Connect4Environment_untile(mc_env, tile)
  branch_untile = tf.transpose(tf.reshape(branch, [tile, batch_size]))
  winner = tf.transpose(mc_final.winner)
  winner_relative = tf.cast(tf.expand_dims(
      connect4.Connect4Environment.to_play(environment.turn), -1) == winner, tf.int32)
  actions_counter = tf.zeros([batch_size, action_space], tf.int32)
  actions_accumulator = tf.zeros([batch_size, action_space], tf.int32)
  for action_index in tf.range(action_space):
    node_indices2D = tf.stack(
        [tf.range(batch_size), tf.fill([batch_size], action_index)], -1)
    action_count = tf.reduce_sum(
        tf.cast(branch_untile == action_index, tf.int32), axis=1)
    actions_counter = tf.tensor_scatter_nd_add(
        actions_counter, node_indices2D, action_count)
    w = tf.reduce_sum(tf.cast(branch_untile == action_index,
                      tf.int32) * winner_relative, axis=1)
    actions_accumulator = tf.tensor_scatter_nd_add(
        actions_accumulator, node_indices2D, w)
  return actions_counter, actions_accumulator, winner


@tf.function
def select_action_mc_parallel(environment, num_simulations):
  """
  parallel computing of monte-carlo
  num_simulations is for all actions, c4.action_space_size is 7, so each active has num_simulations/7
  """

  actions_counter, actions_accumulator, _ = compute_action_mc_parallel(
      environment, num_simulations)
  win_ratio = tf.where(actions_counter > 0, tf.cast(
      actions_accumulator, tf.float32) / tf.cast(actions_counter, tf.float32), 0.0)
  r = tf.random.uniform(tf.shape(win_ratio), 0, 1e-10)
  best_action = tf.math.argmax(win_ratio+r, axis=-1, output_type=tf.int32)
  return best_action


connect4_seq_P0_wins = np.array([
    3, 0, 3, 6, 3, 2, 3,  # P0 wins playing on column 3 with 7 steps
])

connect4_seq_P1_wins = np.array([
    0, 3, 6, 2, 0, 4, 6, 1,  # P1 wins playing on column 3 with 8 steps
])


def build_connect4_gameXp(history, discount=0.9):
  action_space = 7  # connect4 action space
  env = connect4.Connect4Environment.init()
  rewards = []
  values = []
  observations = [connect4.Connect4Environment.observation(env)]
  for action in history:
    env, reward = connect4.Connect4Environment.step(env, action=action)
    observations.append(connect4.Connect4Environment.observation(env))
    rewards.append(reward[0])
    values.append(0.0)
    for value_index in range(1, len(values)+1):
      value_relative = -1.0 if value_index % 2 == 0 else 1.0
      values[-value_index] += math.pow(discount,
                                       value_index) * reward[0] * value_relative

  gameXp = muzero_common.GameXp(
      history=tf.convert_to_tensor([history]),
      rewards=tf.convert_to_tensor([rewards]),
      history_length=tf.convert_to_tensor([len(history)]),
      child_visits=tf.expand_dims(tf.one_hot(history, action_space), 0),
      root_values=tf.convert_to_tensor([values]),
  )
  gameXp.assert_struct()
  return gameXp, tf.cast(tf.stack(observations, axis=1), tf.float32)
