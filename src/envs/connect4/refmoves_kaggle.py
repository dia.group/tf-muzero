"""
  Based on https://www.kaggle.com/petercnudde/1k-connect4-validation-set
  Scores a connect-x agent with the dataset
  for information, random agent performs like this
  scoring  <function random_agent at 0x7fdf586b7378>
  perfect move percentage:  0.27 / 0.22
  good moves percentage:  0.7 / 0.67

  Minimax Algorithm looking 7 moves ahead
  perfect move percentage: 0.753
  good moves percentage: 0.907

  Minimax Algorithm looking 8 moves ahead
  perfect move percentage: 0.761
  good moves percentage: 0.915

  Minimax Algorithm looking 9 moves ahead
  perfect move percentage: 0.781
  good moves percentage: 0.918

  Negamax Algorithm looking iteratively deeper with a scaled benchmark time limit of 1 second (average search depth ~18-22)
  perfect move percentage: 0.883
  good moves percentage: 0.947

  "A neural net that I use in my best agent t (1267 score on 2/24/20) score as follows"
  perfect move percentage: 0.737
  good moves percentage: 0.939
  Peter Cnudde:  0.891 /  0.992

  My tests:
  MC (MonteCarlo) evals
  num_simulations; perfect_move_ratio; good_move_ratio
  16;   0.327; 0.729
  64;   0.452; 0.778
  256;  0.599; 0.865 
  1024; 0.691; 0.910
  4096; 0.718; 0.914

  MCTS evals
  num_simulations; perfect_move_ratio; good_move_ratio
  16;   0.339; 0.713
  64;   0.452; 0.781
  256;  0.616; 0.869
  1024; 0.686; 0.905
  4096; 0.730; 0.929

  Another helpfull reference -- but train/test dataset is not available:
  https://medium.com/oracledevs/lessons-from-implementing-alphazero-7e36e9054191

  Supervised - policy
  perfect move ratio: 0.9620
  good moves ratio: 0.9893

  Supervised - MCTS
  perfect move ratio: 0.9729
  good moves ratio: 0.9979

  A0 - policy
  perfect move ratio: 0.9570
  good moves ratio: 0.9883

  A0 - MCTS
  perfect move ratio: 0.9695
  good moves ratio: 0.9976
"""

import json
import os
import numpy
import tensorflow as tf  # type: ignore

from src.agents.tfagents.muzero import common as muzero_common
from src.agents.tfagents.muzero import play as muzero_play
from src.agents.tfagents.muzero import support as support


def load_ref_moves():
  with open(os.path.join(os.path.dirname(__file__), "refmoves1k_kaggle")) as f:
    observation = []
    moves = []
    for line in f:
      data = json.loads(line)
      p1_board = numpy.reshape(numpy.array(data["board"]) == 1, [6, 7])
      p2_board = numpy.reshape(numpy.array(data["board"]) == 2, [6, 7])
      p_board = numpy.empty_like(p1_board)
      p_board.fill(numpy.sum(p1_board) != numpy.sum(p2_board))
      observation.append(numpy.stack([p1_board, p2_board, p_board], axis=-1))
      moves.append(data["move score"])
    observation = tf.convert_to_tensor(
        numpy.stack(observation), dtype=tf.uint8)
    moves = tf.convert_to_tensor(moves)
  return tf.reverse(observation, [1]), moves


def _win_loss_draw(score):
  return tf.where(score > 0, 1, tf.where(score < 0, -1, 0))

def compute_ref_score(observation: tf.Tensor, moves: tf.Tensor, network: muzero_common.MuzeroNetwork, config: muzero_common.MuZeroConfig):
  nw_output_raw = network.initial_inference(observation)
  nw_output = muzero_common.NetworkOutput(
      nw_output_raw.value if config.support_size_value is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.value),
          config.support_size_value,
          config.support_scale_value),
      nw_output_raw.reward if config.support_size_reward is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.reward),
          config.support_size_reward,
          config.support_scale_reward,
      ),
      nw_output_raw.policy_logits,
      nw_output_raw.hidden_state,
  )
  agent_move = tf.argmax(nw_output.policy_logits, axis=1)
  agent_score = tf.gather(moves, agent_move, axis=1, batch_dims=1)

  perfect_score = tf.reduce_max(moves, axis=1)
  perfect_move_count = tf.reduce_sum(
      tf.where(agent_score == perfect_score, 1, 0))

  good_move_count = tf.reduce_sum(tf.cast(_win_loss_draw(
      perfect_score) == _win_loss_draw(agent_score), tf.int32))

  # Score = 0: Game will be a draw
  # Score > 0: Current player will win (the bigger the number the sooner the player will win). The score is the half the ammount of plies from the end the game will be won. So +5 is means the win will be in ply 42 - 2*5 = 32.
  # Score < 0: Current player will lose (the bigger the number the sooner the player will lose)
  # Score = -99: simply indicates that that was not a legal move
  nw_value = tf.squeeze(nw_output.value, -1)
  value_score = tf.reduce_sum(tf.cast(tf.logical_or(tf.logical_or(
    tf.logical_and(nw_value>0.25, perfect_score>0),
    tf.logical_and(nw_value<0.25, perfect_score<0)),
    perfect_score==0), tf.int32))

  return perfect_move_count, good_move_count, value_score

def compute_ref_score_mcts(observation: tf.Tensor, moves: tf.Tensor, network: muzero_common.MuzeroNetwork, config: muzero_common.MuZeroConfig):
  tf.debugging.assert_equal(tf.shape(observation)[1:], [6, 7, 3])
  tf.debugging.assert_equal(tf.shape(moves)[1:], [7])

  batch_size = int(tf.shape(observation)[0])
  dict_config = {field: value for field, value in zip(config._fields, config)}
  dict_config['pb_c_init'] = 1.25
  local_config = muzero_common.MuZeroConfig(**dict_config)

  history_length = tf.reduce_sum(
      tf.cast(observation[..., 0:2] != 0, tf.int32), axis=[1, 2, 3])
  def map_state_spec(spec):
    return tf.TensorSpec(
      tf.squeeze(spec, 0).shape, spec.dtype if config.tree_representation_storage == None else config.tree_representation_storage
      )
  hidden_state_spec = tf.nest.map_structure(
      map_state_spec, network.nw_prediction.input)
  tree = muzero_play.Tree.init(
      batch_size, local_config.action_space_size, local_config.num_simulations, hidden_state_spec)
  legal_actions = (observation[:, -1, :, 0] | observation[:, -1, :, 1]) == 0
  nw_output_raw = network.initial_inference(observation)
  nw_output = muzero_common.NetworkOutput(
      nw_output_raw.value if local_config.support_size_value is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.value),
          local_config.support_size_value,
          local_config.support_scale_value),
      nw_output_raw.reward if local_config.support_size_reward is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.reward),
          local_config.support_size_reward,
          local_config.support_scale_reward,
      ),
      nw_output_raw.policy_logits,
      nw_output_raw.hidden_state,
  )
  tree = muzero_play.expand_node(tree,
                                 tree.rootNode(),
                                 local_config.to_play(history_length),
                                 legal_actions,
                                 nw_output,
                                 local_config.action_space_size)
  #tree = add_exploration_noise(local_config, tree)
  tree, _ = muzero_play.run_mcts(
    config=local_config,
    tree=tree,
    history_depth=history_length,
    network=network,
    prnd=tf.convert_to_tensor([0, 42]),
    )

  agent_move = muzero_play.select_action(
    tree,
    local_config.action_space_size,
    temperature=0.0,
    prnd=tf.convert_to_tensor([0, 42]),
    )

  #agent_move = tf.argmax(nw_output.policy_logits, axis=1)
  agent_score = tf.gather(moves, agent_move, axis=1, batch_dims=1)

  perfect_score = tf.reduce_max(moves, axis=1)
  perfect_move_count = tf.reduce_sum(
      tf.where(agent_score == perfect_score, 1, 0))

  good_move_count = tf.reduce_sum(tf.cast(_win_loss_draw(
      perfect_score) == _win_loss_draw(agent_score), tf.int32))
  
  root_values = muzero_play.Tree.value(tree, tree.rootNode())
  value_score = tf.reduce_sum(tf.cast(tf.logical_or(tf.logical_or(
    tf.logical_and(root_values>0.25, perfect_score>0),
    tf.logical_and(root_values<0.25, perfect_score<0)),
    perfect_score==0), tf.int32))

  return perfect_move_count, good_move_count, value_score
