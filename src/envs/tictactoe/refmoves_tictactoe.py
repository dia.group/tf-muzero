import numpy as np

import os
import numpy
import tensorflow as tf  # type: ignore

from src.agents.tfagents.muzero import common as muzero_common
from src.agents.tfagents.muzero import play as muzero_play
from src.agents.tfagents.muzero import support as support


def load():
    with open(os.path.join(os.path.dirname(__file__), "boards_ttt.npy"), "rb") as f:
        boards = np.load(f)
    with open(os.path.join(os.path.dirname(__file__), "scores_ttt.npy"), "rb") as g:
        scores = np.load(g)
    assert boards.shape == (1000, 9)
    assert scores.shape == (1000, 9)
    return boards,scores

def load_ref_moves():
   boards, scores = load()
   observation = []
   for board in boards :
       p1_board = numpy.reshape(board == 1, [3, 3])
       p2_board = numpy.reshape(board == -1, [3, 3])
       to_play = numpy.full((3, 3), 0 if board.sum()==0 else 1)
       observation.append(numpy.stack([p1_board, p2_board, to_play], -1))
   observation = tf.convert_to_tensor(numpy.stack(observation), dtype=tf.int32)
   moves = tf.convert_to_tensor(scores, dtype=tf.int32)
   return observation, moves

def compute_ref_score(observation: tf.Tensor, moves: tf.Tensor, network: muzero_common.MuzeroNetwork):
  batch_size = int(tf.shape(observation)[0])
  nw_output = network.initial_inference(observation)
  agent_move = tf.argmax(nw_output.policy_logits, axis=1)
  agent_score = tf.gather(moves, agent_move, axis=1, batch_dims=1)
  perfect_score = tf.reduce_max(moves, axis=1)
  perfect_move_count = tf.reduce_sum(
      tf.where(agent_score == perfect_score, 1, 0))

  def win_loss_draw(score):
    return tf.where(score > 0, 1, tf.where(score < 0, -1, 0))
  good_move_count = tf.reduce_sum(tf.cast(win_loss_draw(
      perfect_score) == win_loss_draw(agent_score), tf.int32))
  return good_move_count

def compute_ref_score_mcts(observation: tf.Tensor, moves: tf.Tensor, network: muzero_common.MuzeroNetwork, config: muzero_common.MuZeroConfig):
  batch_size = int(tf.shape(observation)[0])
  tf.debugging.assert_equal(tf.shape(observation)[1:], [3, 3, 3])
  tf.debugging.assert_equal(tf.shape(moves)[1:], [9])


  dict_config = {field: value for field, value in zip(config._fields, config)}
  dict_config['pb_c_init'] = 1.25
  local_config = muzero_common.MuZeroConfig(**dict_config)

  history_length = tf.reduce_sum(
      tf.cast(observation[..., 0:2] != 0, tf.int32), axis=[1, 2, 3])

  def map_state_spec(spec): return tf.TensorSpec(
      spec.shape[1:], spec.dtype if config.tree_representation_storage == None else config.tree_representation_storage)
  hidden_state_spec = tf.nest.map_structure(
      map_state_spec, network.nw_prediction.input)

  tree = muzero_play.Tree.init(
      batch_size,
      local_config.action_space_size,
      local_config.num_simulations,
      hidden_state_spec)

  legal_actions = moves[:, :] != -2

  nw_output_raw = network.initial_inference(observation)
  nw_output = muzero_common.NetworkOutput(
      tf.squeeze(nw_output_raw.value) if local_config.support_size_value is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.value),
          local_config.support_size_value,
          local_config.support_scale_value),
      tf.squeeze(nw_output_raw.reward) if local_config.support_size_reward is None else support.support_to_scalar(
          tf.nn.softmax(nw_output_raw.reward),
          local_config.support_size_reward,
          local_config.support_scale_reward,
      ),
      nw_output_raw.policy_logits,
      nw_output_raw.hidden_state,
  )
  tree = muzero_play.expand_node(tree,
                                 tree.rootNode(),
                                 local_config.to_play(history_length),
                                 legal_actions,
                                 nw_output,
                                 local_config.action_space_size,
                                 )

  #tree = add_exploration_noise(local_config, tree)
  seed = tf.convert_to_tensor([0, 42])  # keep same random numbers for more stability
  tree, _ = muzero_play.run_mcts(
      config=local_config,
      tree=tree,
      history_depth=history_length,
      network=network,
      prnd=seed,
      )

  agent_move = muzero_play.select_action(
      tree=tree,
      action_count=local_config.action_space_size,
    temperature=0.0,
      prnd=seed,
    )

  #agent_move = tf.argmax(nw_output.policy_logits, axis=1)
  agent_score = tf.gather(moves, agent_move, axis=1, batch_dims=1)

  perfect_score = tf.reduce_max(moves, axis=1)
  perfect_move_count = tf.reduce_sum(
      tf.where(agent_score == perfect_score, 1, 0))

  def win_loss_draw(score):
    return tf.where(score > 0, 1, tf.where(score < 0, -1, 0))
  good_move_count = tf.reduce_sum(tf.cast(win_loss_draw(
      perfect_score) == win_loss_draw(agent_score), tf.int32))
  return good_move_count