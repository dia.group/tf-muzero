import logging
import tensorflow as tf  # type: ignore
from tf_agents.trajectories import trajectory
import unittest

from src.envs.tictactoe import utils
from src.envs import tictactoe_tfa


class TestDataAugmentation(unittest.TestCase):
  def setUp(self) -> None:
    # np.set_printoptions(precision=4, floatmode='fixed')
    # logging.basicConfig(level=logging.INFO,
    #         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    #         datefmt='%Y-%m-%d %H:%M:%S',
    #         )
    # tf.config.run_functions_eagerly(True) # to debug
    return super().setUp()


  @staticmethod
  def _build_episode():
    env_kwargs = {"action_masking": True}
    tf_env = tictactoe_tfa.TicTacToeTFEnv(batch_size=4, **env_kwargs)

    observations = [tf_env.current_time_step().observation]
    action = tf.convert_to_tensor([0, 2, 3, 6])

    policy_0 = [tf.cast(tf.tile(tf.range(9)[None], (4,1)), tf.float32)]
    policy_1 = [tf.zeros(4)]

    actions = []
    rewards = []
    discounts = []
    rewards = []
    for _ in range(4):
      ts = tf_env.step(action=action)
      observations.append(ts.observation)
      actions.append(action)
      policy_0.append(policy_0[-1])
      policy_1.append(policy_1[-1])
      rewards.append(ts.reward)
      discounts.append(ts.discount)
      action = tf.math.floormod(action+1, 9)

    observations = tf.stack([o[0] for o in observations], 1),\
      tf.stack([o[1] for o in observations], 1)

    return trajectory.from_episode(
      observation=observations,
      action=tf.stack(actions, 1),
      policy_info=(tf.stack(policy_0, 1), tf.stack(policy_1, 1)),
      reward=tf.stack(rewards, 1),
      discount=tf.stack(discounts, 1),
    )


  def test_structure(self):
    traj_in:trajectory.Trajectory = TestDataAugmentation._build_episode()

    traj_out, _ = utils.ttt_data_augmentation(traj_in, None)
    tf.nest.assert_same_structure(traj_in, traj_out)


  def test_hflip(self):
    traj_in:trajectory.Trajectory = TestDataAugmentation._build_episode()

    action_col_in = traj_in.action % 3
    action_row_in = traj_in.action // 3

    policy_info_0_in = tf.reshape(traj_in.policy_info[0], tf.concat([tf.shape(traj_in.policy_info[0])[:-1], [3, 3]], -1))
    policy_info_1_in = traj_in.policy_info[1]
    #hflip_flag = tf.random.uniform(batch_size[None])>0.5
    hflip_flag = tf.fill((4), True)
    observation, (policy_info_0, policy_info_1), (action_row, action_col) = utils.hflip(
      hflip_flag,
      traj_in.observation,
      (policy_info_0_in, policy_info_1_in),
      (action_row_in, action_col_in)
    )
    # test obs
    ## action on position 0
    tf.debugging.assert_equal(traj_in.observation [0][0,1, 0,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][0,1, 0,2,0], tf.convert_to_tensor(1))
    ## action on position 2
    tf.debugging.assert_equal(traj_in.observation [0][1,1, 0,2,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][1,1, 0,0,0], tf.convert_to_tensor(1))
    ## action on position 3
    tf.debugging.assert_equal(traj_in.observation [0][2,1, 1,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][2,1, 1,2,0], tf.convert_to_tensor(1))
    ## action on position 6
    tf.debugging.assert_equal(traj_in.observation [0][3,1, 2,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][3,1, 2,2,0], tf.convert_to_tensor(1))

    # test mask
    tf.debugging.assert_equal(traj_in.observation [1][0,1, 0,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][0,1, 0,2], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][1,1, 0,2], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][1,1, 0,0], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][2,1, 1,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][2,1, 1,2], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][3,1, 2,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][3,1, 2,2], tf.convert_to_tensor(0))

    # test policy
    #tf.debugging.assert_equal(policy_info_0_in[0,1], tf.convert_to_tensor([])
    tf.debugging.assert_equal(policy_info_0[0,1, 0], tf.convert_to_tensor([2.0, 1.0, 0.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 1], tf.convert_to_tensor([5.0, 4.0, 3.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 2], tf.convert_to_tensor([8.0, 7.0, 6.0]))

    # test action
    tf.debugging.assert_equal(traj_in.action[:,0], tf.convert_to_tensor([0, 2, 3, 6]))
    tf.debugging.assert_equal(action_row_in[:,0], tf.convert_to_tensor([0, 0, 1, 2]))
    tf.debugging.assert_equal(action_row[:,0], tf.convert_to_tensor([0, 0, 1, 2]))
    tf.debugging.assert_equal(action_col_in[:,0], tf.convert_to_tensor([0, 2, 0, 0]))
    tf.debugging.assert_equal(action_col[:,0], tf.convert_to_tensor([2, 0, 2, 2]))


  def test_vflip(self):
    traj_in:trajectory.Trajectory = TestDataAugmentation._build_episode()

    action_col_in = traj_in.action % 3
    action_row_in = traj_in.action // 3

    policy_info_0_in = tf.reshape(traj_in.policy_info[0], tf.concat([tf.shape(traj_in.policy_info[0])[:-1], [3, 3]], -1))
    policy_info_1_in = traj_in.policy_info[1]
    #hflip_flag = tf.random.uniform(batch_size[None])>0.5
    vflip_flag = tf.fill((4), True)
    observation, (policy_info_0, policy_info_1), (action_row, action_col) = utils.vflip(
      vflip_flag,
      traj_in.observation,
      (policy_info_0_in, policy_info_1_in),
      (action_row_in, action_col_in)
    )
    # test obs
    ## action on position 0
    tf.debugging.assert_equal(traj_in.observation [0][0,1, 0,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][0,1, 2,0,0], tf.convert_to_tensor(1))
    ## action on position 2
    tf.debugging.assert_equal(traj_in.observation [0][1,1, 0,2,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][1,1, 2,2,0], tf.convert_to_tensor(1))
    ## action on position 3
    tf.debugging.assert_equal(traj_in.observation [0][2,1, 1,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][2,1, 1,0,0], tf.convert_to_tensor(1))
    ## action on position 6
    tf.debugging.assert_equal(traj_in.observation [0][3,1, 2,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][3,1, 0,0,0], tf.convert_to_tensor(1))

    # test mask
    tf.debugging.assert_equal(traj_in.observation [1][0,1, 0,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][0,1, 2,0], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][1,1, 0,2], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][1,1, 2,2], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][2,1, 1,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][2,1, 1,0], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][3,1, 2,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][3,1, 0,0], tf.convert_to_tensor(0))

    # test policy
    #tf.debugging.assert_equal(policy_info_0_in[0,1], tf.convert_to_tensor([])
    tf.debugging.assert_equal(policy_info_0[0,1, 0], tf.convert_to_tensor([6.0, 7.0, 8.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 1], tf.convert_to_tensor([3.0, 4.0, 5.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 2], tf.convert_to_tensor([0.0, 1.0, 2.0]))

    # test action
    tf.debugging.assert_equal(traj_in.action[:,0], tf.convert_to_tensor([0, 2, 3, 6]))
    tf.debugging.assert_equal(action_row_in[:,0], tf.convert_to_tensor([0, 0, 1, 2]))
    tf.debugging.assert_equal(action_row[:,0], tf.convert_to_tensor([2, 2, 1, 0]))
    tf.debugging.assert_equal(action_col_in[:,0], tf.convert_to_tensor([0, 2, 0, 0]))
    tf.debugging.assert_equal(action_col[:,0], tf.convert_to_tensor([0, 2, 0, 0]))


  def test_transpose(self):
    traj_in:trajectory.Trajectory = TestDataAugmentation._build_episode()

    action_col_in = traj_in.action % 3
    action_row_in = traj_in.action // 3

    policy_info_0_in = tf.reshape(traj_in.policy_info[0], tf.concat([tf.shape(traj_in.policy_info[0])[:-1], [3, 3]], -1))
    policy_info_1_in = traj_in.policy_info[1]
    #hflip_flag = tf.random.uniform(batch_size[None])>0.5
    tflip_flag = tf.fill((4), True)
    observation, (policy_info_0, policy_info_1), (action_row, action_col) = utils.transpose(
      tflip_flag,
      traj_in.observation,
      (policy_info_0_in, policy_info_1_in),
      (action_row_in, action_col_in)
    )
    # test obs
    ## action on position 0
    tf.debugging.assert_equal(traj_in.observation [0][0,1, 0,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][0,1, 0,0,0], tf.convert_to_tensor(1))
    ## action on position 2
    tf.debugging.assert_equal(traj_in.observation [0][1,1, 0,2,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][1,1, 2,0,0], tf.convert_to_tensor(1))
    ## action on position 3
    tf.debugging.assert_equal(traj_in.observation [0][2,1, 1,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][2,1, 0,1,0], tf.convert_to_tensor(1))
    ## action on position 6
    tf.debugging.assert_equal(traj_in.observation [0][3,1, 2,0,0], tf.convert_to_tensor(1))
    tf.debugging.assert_equal(observation         [0][3,1, 0,2,0], tf.convert_to_tensor(1))

    # test mask
    tf.debugging.assert_equal(traj_in.observation [1][0,1, 0,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][0,1, 0,0], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][1,1, 0,2], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][1,1, 2,0], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][2,1, 1,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][2,1, 0,1], tf.convert_to_tensor(0))

    tf.debugging.assert_equal(traj_in.observation [1][3,1, 2,0], tf.convert_to_tensor(0))
    tf.debugging.assert_equal(observation         [1][3,1, 0,2], tf.convert_to_tensor(0))

    # test policy
    #tf.debugging.assert_equal(policy_info_0_in[0,1], tf.convert_to_tensor([])
    tf.debugging.assert_equal(policy_info_0[0,1, 0], tf.convert_to_tensor([0.0, 3.0, 6.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 1], tf.convert_to_tensor([1.0, 4.0, 7.0]))
    tf.debugging.assert_equal(policy_info_0[0,1, 2], tf.convert_to_tensor([2.0, 5.0, 8.0]))

    # test action
    tf.debugging.assert_equal(traj_in.action[:,0], tf.convert_to_tensor([0, 2, 3, 6]))
    tf.debugging.assert_equal(action_row_in[:,0], tf.convert_to_tensor([0, 0, 1, 2]))
    tf.debugging.assert_equal(action_row[:,0], tf.convert_to_tensor([0, 2, 0, 0]))
    tf.debugging.assert_equal(action_col_in[:,0], tf.convert_to_tensor([0, 2, 0, 0]))
    tf.debugging.assert_equal(action_col[:,0], tf.convert_to_tensor([0, 0, 1, 2]))


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    # tf.config.run_functions_eagerly(True) # to debug

    unittest.main()
