import itertools
import os
import numpy as np
import random


def won(c, n):
  if c[0] == n and c[1] == n and c[2] == n: return 1
  if c[3] == n and c[4] == n and c[5] == n: return 1
  if c[6] == n and c[7] == n and c[8] == n: return 1

  if c[0] == n and c[3] == n and c[6] == n: return 1
  if c[1] == n and c[4] == n and c[7] == n: return 1
  if c[2] == n and c[5] == n and c[8] == n: return 1

  if c[0] == n and c[4] == n and c[8] == n: return 1
  if c[2] == n and c[4] == n and c[6] == n: return 1

  return 0


transposition_table = {}

def minimax(board, maximizerMark, isMaxTurn:bool)->int:
    board_key = board.tobytes()
    if board_key in transposition_table:
        s = transposition_table[board_key]
        return s

    if won(board, 1):
        ret = 1
        transposition_table[board_key] = ret
        return ret
    if won(board, -1):
        ret = -1
        transposition_table[board_key] = ret
        return ret
    if (board != 0).all():
        transposition_table[board_key] = 0
        return 0

    currentMark = 1 if board.sum() == 0 else -1
    scores = []
    for i in range(9):
        if board[i] == 0:
            board[i] = currentMark
            s = minimax(board, maximizerMark, not isMaxTurn)
            board[i] = 0
            scores.append(s)

    ret = max(scores) if isMaxTurn else min(scores)
    transposition_table[board_key] = ret
    return ret


# force generation of all states
minimax(board=np.zeros(9, dtype=np.int8), maximizerMark=1, isMaxTurn=True)


# debug test (cf https://ksvi.mff.cuni.cz/~dingle/2019-20/prog_2/notes_13.html)
board = np.array([ 0, 0, 0,  0, 0, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 0, 0, 0,  1, 0, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 1, 0, 0,  0, 0, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 0, 0, 0,  0, 1, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 0, 0, 0,  1,-1, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 0,-1, 0,  1, 0, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 1

board = np.array([ 0, 0, 0,  1,-1, 1,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == -1

board = np.array([ 1, 0, 0,  1,-1, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 0

board = np.array([ 0,-1, 0,  1, 1, 0,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == 1

board = np.array([ 0,-1, 0,  1, 0, 1,  0, 0, 0], dtype=np.int8)
assert transposition_table[board.tobytes()] == -1


def create_boards(nb):
    boards=[]
    scores=[]

    # board = np.array([[ 0,  0,  1],
    # [ 0, -1,  0],
    # [ 1,  1, -1]], dtype=np.int8).flatten()
    #board = np.zeros(9, dtype=np.int8)
    # mark = 1
    # scores = []
    # for i in range(3):
    #     if board[i] == 0:
    #         board[i] = mark
    #         s = minimax(board, maximizerMark=1, isMaxTurn=False)
    #         board[i] = 0
    #         scores.append(s)

    keys = list(transposition_table.keys())
    assert(len(keys) == 5477+1)  # cf https://stackoverflow.com/questions/7466429/generate-a-list-of-all-unique-tic-tac-toe-boards
    random.seed(42)
    random.shuffle(keys)
    keys_it = itertools.cycle(keys)
    while len(boards) < nb:
        key = next(keys_it)
        board = np.copy(np.frombuffer(key, dtype=np.int8, count=9))
        if won(board, 1) or won(board, -1) or (board != 0).all():  # skip leaf states
            continue
        mark = 1 if np.sum(board) == 0 else -1
        score = []
        for j in range(9):
            if board[j] == 0:
                board[j] = mark
                board_key = board.tobytes()
                s = transposition_table[board_key] * mark  # make state values relative to current player => highest value is always the best move
                board[j] = 0
            else:
                s = -2
            score.append(s)
        boards.append(board)
        scores.append(score)

    return np.array(boards, dtype=np.int8), np.array(scores, dtype=np.int8)

boards, scores = create_boards(1000)

# debug : display some samples for checking
for i in range(5):
    print(np.reshape(boards[i], (3, 3)))
    print(np.reshape(scores[i], (3, 3)))
    print()

with open(os.path.join(os.path.dirname(__file__), "boards_ttt.npy"), "wb") as f:
    np.save(f, boards)
with open(os.path.join(os.path.dirname(__file__), "scores_ttt.npy"), "wb") as f:
    np.save(f, scores)
