import logging
import tensorflow as tf  # type: ignore
import unittest

from src.envs import tictactoe_tf as tictactoe


class TestTictactoe(unittest.TestCase):
    """ Tictactoe unit tests
  /!\ arrays start from bottom /!\
  """

    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed')
        # logging.basicConfig(level=logging.INFO,
        #           format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        #           datefmt='%Y-%m-%d %H:%M:%S',
        #           )
        # tf.config.run_functions_eagerly(True) # to debug
        return super().setUp()

    def test_environment_legal_actions(self):
        batch_size = 1

        # test actions on emtpy board
        env = tictactoe.TictactoeEnvironment.init(batch_size)
        actions = tictactoe.TictactoeEnvironment.legal_actions(env)
        tf.assert_equal(
            actions,
            tf.constant([[True, True, True], [True, True, True], [True, True, True]]),
        )

        # test actions on quasi-full board
        default_turn = env.turn
        default_done = env.done
        default_winner = env.winner

        board = tf.constant(
            [
                [
                    [
                        [1, 0, 1],
                        [0, 0, 1],
                        [0, 1, 0],
                    ],
                    [
                        [0, 1, 0],
                        [1, 1, 0],
                        [1, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, default_turn, default_done, default_winner
        )
        tictactoe.TictactoeEnvironment.assert_struct(env)
        actions = tictactoe.TictactoeEnvironment.legal_actions(env)
        tf.assert_equal(
            actions,
            tf.constant(
                [[False, False, False], [False, False, False], [False, False, True]]
            ),
        )

        # test legal/unlegal action
        action = tf.constant(0)
        _, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tf.assert_equal(reward, -1.0)

        action = tf.constant(8)
        _, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tf.assert_equal(reward, 1.0)

        # test actions on full board
        board = tf.constant(
            [
                [
                    [
                        [1, 0, 1],
                        [0, 1, 1],
                        [0, 1, 0],
                    ],
                    [
                        [0, 1, 0],
                        [1, 0, 0],
                        [1, 0, 1],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, default_turn, default_done, default_winner
        )
        actions = tictactoe.TictactoeEnvironment.legal_actions(env)
        tf.assert_equal(
            actions,
            tf.constant(
                [[False, False, False], [False, False, False], [False, False, False]]
            ),
        )

    def test_environment_opening(self):
        """
        test internal representation of environment for every moves up to depth 3
        """
        batch_size = 1

        # test board on oppening
        env0 = tictactoe.TictactoeEnvironment.init(batch_size)
        actions0 = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env0), (1, 9)
        )
        for action_index0 in tf.cast(tf.where(actions0[0]), tf.int32):
            env1, reward01 = tictactoe.TictactoeEnvironment.step(env0, action_index0)
            tf.assert_equal(reward01[0], 0.0)
            tf.assert_equal(tf.reduce_sum(env1.board[0, 0]), 1)
            tf.assert_equal(tf.reduce_sum(env1.board[0, 1]), 0)
            tf.assert_equal(env1.turn[0], 1)

            actions1 = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(env1), (1, 9)
            )
            for action_index1 in tf.cast(tf.where(tf.squeeze(actions1, 0)), tf.int32):
                env2, reward12 = tictactoe.TictactoeEnvironment.step(
                    env1, action_index1
                )
                tf.assert_equal(reward12[0], 0.0)
                tf.assert_equal(tf.reduce_sum(env2.board[0, 0]), 1)
                tf.assert_equal(tf.reduce_sum(env2.board[0, 1]), 1)
                tf.assert_equal(env2.turn[0], 2)

                actions2 = tf.reshape(
                    tictactoe.TictactoeEnvironment.legal_actions(env2), (1, 9)
                )
                for action_index2 in tf.cast(
                    tf.where(tf.squeeze(actions2, 0)), tf.int32
                ):
                    env3, reward23 = tictactoe.TictactoeEnvironment.step(
                        env2, action_index2
                    )
                    tf.assert_equal(reward23[0], 0.0)
                    tf.assert_equal(tf.reduce_sum(env3.board[0, 0]), 2)
                    tf.assert_equal(tf.reduce_sum(env3.board[0, 1]), 1)
                    tf.assert_equal(env3.turn[0], 3)

                    # actions3 = tictactoe.TictactoeEnvironment.legal_actions(env3)
                    # for action_index3 in tf.cast(tf.where(tf.squeeze(actions3, 0)), tf.int32):
                    #   env4, reward34 = tictactoe.TictactoeEnvironment.step(env3, action_index3)
                    #   tf.assert_equal(reward34[0], 0.0)
                    #   tf.assert_equal(tf.reduce_sum(tf.cast(env4.board==1, tf.int32)), 2)
                    #   tf.assert_equal(tf.reduce_sum(tf.cast(env4.board==-1, tf.int32)), 2)
                    #   tf.assert_equal(tf.cast(env4.turn, tf.int32), 4)

    def test_environment_win(self):
        """
        test win trigger
        """
        batch_size = 1
        default_done = tf.fill([batch_size], False)
        default_winner = tf.zeros(batch_size, dtype=tf.int32)

        turnP0 = tf.fill([batch_size], tf.constant(0, dtype=tf.int32))
        turnP1 = tf.fill([batch_size], tf.constant(1, dtype=tf.int32))

        # P0 -- horizontal
        board = tf.constant(
            [
                [
                    [
                        [1, 1, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([2], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [1, 1, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )
        action = tf.constant([5], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 1, 1],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )
        action = tf.constant([3], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 1, 1],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )
        action = tf.constant([6], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        # P0 -- vertical
        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [1, 0, 0],
                        [1, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([0], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 1, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([7], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 1, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([1], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 1],
                        [0, 0, 1],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([8], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        # P0 -- diagonal
        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 0, 1],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([0], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [1, 0, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([8], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [1, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([2], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 1],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP0, default_done, default_winner
        )

        action = tf.constant([6], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([0], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        # P1 -- horizontal
        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 1, 1],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([0], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [1, 1, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )
        action = tf.constant([2], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 1],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )
        action = tf.constant([3], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [1, 1, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )
        action = tf.constant([8], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        # P1 -- vertical
        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [1, 0, 0],
                        [1, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([0], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 1, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([7], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 1, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([1], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 1],
                        [0, 0, 1],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([8], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        # P1 -- diagonal
        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 0, 1],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([0], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [1, 0, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([8], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 1],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([6], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

        board = tf.constant(
            [
                [
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [1, 0, 0],
                    ],
                ]
            ],
            dtype=tf.int32,
        )
        env = tictactoe.TictactoeEnvironment(
            board, turnP1, default_done, default_winner
        )

        action = tf.constant([2], tf.int32)
        new_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        tictactoe.TictactoeEnvironment.render(new_env, 0)
        tf.assert_equal(new_env.winner[0], tf.constant([1], tf.int32))
        tf.assert_equal(new_env.done[0], True)
        tf.assert_equal(reward[0], 1.0)

    def test_TictactoeEnvironment_equal(self, batch_size: int = 256):
        """
        Test tictactoe.TictactoeEnvironment_equal function, using random actions on game plays
        """
        env = tictactoe.TictactoeEnvironment.init(batch_size)
        tictactoe.TictactoeEnvironment.assert_struct(env)

        for _ in tf.range(3 * 3):
            actions = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(env), (batch_size, 3 * 3)
            )
            rnd = tf.random.uniform(tf.shape(actions), 0, 1)
            action = tf.math.argmax(
                tf.cast(actions, tf.float32) * rnd, axis=1, output_type=tf.int32
            )
            env, _ = tictactoe.TictactoeEnvironment.step(env, action)

            obs = tictactoe.TictactoeEnvironment.observation(env)
            env_p = tictactoe.TictactoeEnvironment.obs2env(obs)
            assert tictactoe.TictactoeEnvironment.equals(env, env_p)

        tictactoe.TictactoeEnvironment.assert_struct(env)
        tf.debugging.assert_greater_equal(env.turn, 5)
        tf.debugging.assert_less_equal(env.turn, 9)
        tf.debugging.assert_greater_equal(env.winner, -1)
        tf.debugging.assert_less_equal(env.winner, 1)
        tf.assert_equal(env.done, True)

    def test_environment_timecluster(self):
        """
        test simulating env at different time steps
        """
        batch_size = 4
        env0 = tictactoe.TictactoeEnvironment.init(batch_size)

        legal_actions0 = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env0), (batch_size, 9)
        )
        rnd = tf.random.uniform(tf.shape(legal_actions0), 0, 1)
        action0 = tf.math.argmax(
            tf.cast(legal_actions0, tf.float32) * rnd, axis=1, output_type=tf.int32
        )
        env1, _ = tictactoe.TictactoeEnvironment.step(env0, action0)

        legal_actions1 = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env0), (batch_size, 9)
        )
        rnd = tf.random.uniform(tf.shape(legal_actions1), 0, 1)
        action1 = tf.math.argmax(
            tf.cast(legal_actions1, tf.float32) * rnd, axis=1, output_type=tf.int32
        )
        env2, _ = tictactoe.TictactoeEnvironment.step(env1, action1)

        legal_actions2 = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env0), (batch_size, 9)
        )
        rnd = tf.random.uniform(tf.shape(legal_actions2), 0, 1)
        action2 = tf.math.argmax(
            tf.cast(legal_actions2, tf.float32) * rnd, axis=1, output_type=tf.int32
        )
        env3, _ = tictactoe.TictactoeEnvironment.step(env2, action2)

        final_env = env3
        for _ in tf.range(3 * 3):
            legal_actions = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(final_env), (batch_size, 9)
            )
            rnd = tf.random.uniform(tf.shape(legal_actions), 0, 1)
            final_action = tf.math.argmax(
                tf.cast(legal_actions, tf.float32) * rnd, axis=1, output_type=tf.int32
            )
            final_env, _ = tictactoe.TictactoeEnvironment.step(final_env, final_action)

        board = tf.concat([env0.board, env1.board, env2.board, final_env.board], 0)
        turn = tf.concat([env0.turn, env1.turn, env2.turn, final_env.turn], 0)
        done = tf.concat([env0.done, env1.done, env2.done, final_env.done], 0)
        winner = tf.concat([env0.winner, env1.winner, env2.winner, final_env.winner], 0)
        all_env = tictactoe.TictactoeEnvironment(board, turn, done, winner)
        all_action = tf.concat([action0, action1, action2, final_action], 0)
        all_env, _ = tictactoe.TictactoeEnvironment.step(all_env, all_action)

        tf.assert_equal(all_env.board[batch_size * 0 : batch_size * 1], env1.board)
        tf.assert_equal(all_env.turn[batch_size * 0 : batch_size * 1], env1.turn)
        tf.assert_equal(all_env.done[batch_size * 0 : batch_size * 1], env1.done)
        tf.assert_equal(all_env.winner[batch_size * 0 : batch_size * 1], env1.winner)

        tf.assert_equal(all_env.board[batch_size * 1 : batch_size * 2], env2.board)
        tf.assert_equal(all_env.turn[batch_size * 1 : batch_size * 2], env2.turn)
        tf.assert_equal(all_env.done[batch_size * 1 : batch_size * 2], env2.done)
        tf.assert_equal(all_env.winner[batch_size * 1 : batch_size * 2], env2.winner)

        tf.assert_equal(all_env.board[batch_size * 2 : batch_size * 3], env3.board)
        tf.assert_equal(all_env.turn[batch_size * 2 : batch_size * 3], env3.turn)
        tf.assert_equal(all_env.done[batch_size * 2 : batch_size * 3], env3.done)
        tf.assert_equal(all_env.winner[batch_size * 2 : batch_size * 3], env3.winner)

        tf.assert_equal(all_env.board[batch_size * 3 : batch_size * 4], final_env.board)
        tf.assert_equal(all_env.turn[batch_size * 3 : batch_size * 4], final_env.turn)
        tf.assert_equal(all_env.done[batch_size * 3 : batch_size * 4], final_env.done)
        tf.assert_equal(
            all_env.winner[batch_size * 3 : batch_size * 4], final_env.winner
        )

    def test_mc_openning(self):
        """
        Check win policy ratios for first 2 steps
        - 1st step : column 3 has the top ratio, then 2&4, 1&5, 0&6
        """

        batch_size = 1024 * 1024
        action_space_size = tictactoe.TictactoeEnvironment.action_space_size
        mc_env = tictactoe.TictactoeEnvironment.init(batch_size)

        legal_actions = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(mc_env), (batch_size, 9)
        )  # list available actions

        r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
        root_action = tf.math.argmax(
            tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32
        )
        mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, root_action)

        for _ in tf.range(3 * 3):
            legal_actions = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(mc_env), (batch_size, 9)
            )
            r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
            action = tf.math.argmax(
                tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32
            )
            mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, action)

        # test whites
        action_simu = batch_size / action_space_size
        win_0 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 0, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_1 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 1, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_2 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 2, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_3 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 3, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_4 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 4, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_5 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 5, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_6 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 6, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_7 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 7, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_8 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 8, mc_env.winner == tf.constant(0, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_center = win_4
        win_corner = (win_0 + win_2 + win_6 + win_8) / 4.0
        win_border = (win_1 + win_3 + win_5 + win_7) / 4.0
        logging.info(
            f"white win proba {win_center:0.2f}(center), {win_corner:0.2f}(corner), {win_border:0.2f}(border)"
        )
        tf.assert_greater(win_center, 0.5)
        tf.assert_greater(win_center, win_corner)
        tf.assert_greater(win_corner, win_border)

        # test blacks
        mc_env = tictactoe.TictactoeEnvironment.init(batch_size)
        action = tf.fill([batch_size], 4, tf.int32)
        mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, action)

        legal_actions = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(mc_env), (batch_size, 9)
        )  # list available actions

        r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
        root_action = tf.math.argmax(
            tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32
        )
        mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, root_action)

        for _ in tf.range(3 * 3):  # execute the whole game
            legal_actions = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(mc_env), (batch_size, 9)
            )
            r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
            action = tf.math.argmax(
                tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32
            )
            mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, action)

        action_simu = batch_size / action_space_size
        win_0 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 0, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_1 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 1, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_2 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 2, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_3 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 3, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_5 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 5, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_6 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 6, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_7 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 7, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_8 = (
            tf.reduce_sum(
                tf.cast(
                    tf.logical_and(
                        root_action == 8, mc_env.winner == tf.constant(1, tf.int32)
                    ),
                    tf.float32,
                )
            )
            / action_simu
        )
        win_corner = (win_0 + win_2 + win_6 + win_8) / 4.0
        win_border = (win_1 + win_3 + win_5 + win_7) / 4.0
        logging.info(
            f"black win proba (after action:4) {win_corner:0.2f}(corner), {win_border:0.2f}(border)"
        )
        tf.assert_greater(win_corner, win_border)

    def test_obs2env_finishedGame(self):
        board = tf.convert_to_tensor(
            [
                [
                    [
                        [1, 0, 1],
                        [1, 0, 0],
                        [0, 1, 1],
                    ],
                    [
                        [0, 1, 0],
                        [0, 1, 1],
                        [1, 0, 0],
                    ],
                ]
            ]
        )
        env = tictactoe.TictactoeEnvironment.init(board=board)
        tictactoe.TictactoeEnvironment.assert_struct(env)
        obs = tictactoe.TictactoeEnvironment.observation(env)
        env2 = tictactoe.TictactoeEnvironment.obs2env(obs)
        tf.assert_equal(env.board, env2.board)
        obs2 = tictactoe.TictactoeEnvironment.observation(env2)
        tf.assert_equal(obs, obs2)

        self.assertTrue(env2.done[0])
        p = tictactoe.TictactoeEnvironment.legal_actions(env2)
        self.assertFalse(tf.reduce_any(p))
