import tensorflow as tf  # type: ignore
from tf_agents.trajectories import trajectory


def hflip(flip_flag, observation, policy, action):
  observation = (
    tf.where(flip_flag[:, None, None, None, None], tf.reverse(observation[0], [3]), observation[0]),
    tf.where(flip_flag[:, None, None, None], tf.reverse(observation[1], [3]), observation[1]),
  )
  action_row, action_col = action
  action_col = tf.where(flip_flag[:, None], 2-action_col, action_col)
  policy_info_0 = policy[0]
  policy_info_0 = tf.where(flip_flag[:, None, None, None], tf.reverse(policy_info_0, [3]), policy_info_0)  # policy
  policy_info_1 = policy[1]
  policy_info_1 = tf.where(flip_flag[:, None, None, None], tf.reverse(policy_info_1, [3]), policy_info_1)  # qvalue

  return observation, (policy_info_0, policy_info_1, policy[2]), (action_row, action_col)


def vflip(flip_flag, observation, policy, action):
  observation = (
    tf.where(flip_flag[:, None, None, None, None], tf.reverse(observation[0], [2]), observation[0]),
    tf.where(flip_flag[:, None, None, None], tf.reverse(observation[1], [2]), observation[1]),
  )
  action_row, action_col = action
  action_row = tf.where(flip_flag[:, None], 2-action_row, action_row)
  policy_info_0 = policy[0]
  policy_info_0 = tf.where(flip_flag[:, None, None, None], tf.reverse(policy_info_0, [2]), policy_info_0)  # policy
  policy_info_1 = policy[1]
  policy_info_1 = tf.where(flip_flag[:, None, None, None], tf.reverse(policy_info_1, [2]), policy_info_1)  # qvalue

  return observation, (policy_info_0, policy_info_1, policy[2]), (action_row, action_col)


def transpose(trans_flag, observation, policy, action):
  observation = (
    tf.where(trans_flag[:, None, None, None, None], tf.transpose(observation[0], [0,1,3,2,4]), observation[0]),
    tf.where(trans_flag[:, None, None, None], tf.transpose(observation[1], [0,1,3,2]), observation[1]),
  )
  action_row_in, action_col_in = action
  action_row = tf.where(trans_flag[:, None], action_col_in, action_row_in)
  action_col = tf.where(trans_flag[:, None], action_row_in, action_col_in)
  policy_info_0 = policy[0]
  policy_info_0 = tf.where(trans_flag[:, None, None, None], tf.transpose(policy_info_0, [0,1,3,2]), policy_info_0)  # policy
  policy_info_1 = policy[1]
  policy_info_1 = tf.where(trans_flag[:, None, None, None], tf.transpose(policy_info_1, [0,1,3,2]), policy_info_1)  # qvalue

  return observation, (policy_info_0, policy_info_1, policy[2]), (action_row, action_col)


# apply random up<->down and left<->right flip; a rotation is missing but it's a bit more complex to code
@tf.function(jit_compile=True)
def ttt_data_augmentation(x: trajectory.Trajectory, y):
  """
    x: tf_agent trajectory
    y:
  """
  batch_size = tf.shape(x.step_type)[0]
  # observation[0] shape [batch, ep_length, with, height, tile]
  # observation[1] shape [batch, ep_length, with, height]
  observation = x.observation
  action_col = x.action % 3
  action_row = x.action // 3
  # policy shape [batch, ep_length, 9]
  policy_info_0 = tf.reshape(x.policy_info[0], tf.concat([tf.shape(x.policy_info[0])[:-1], [3, 3]], -1))
  policy_info_1 = tf.reshape(x.policy_info[1], tf.concat([tf.shape(x.policy_info[1])[:-1], [3, 3]], -1))
  # value shape [batch, ep_length]
  policy_info_2 = x.policy_info[2]  # value
  policy = policy_info_0, policy_info_1, policy_info_2
  action = action_row, action_col

  ## hflip
  hflip_flag = tf.random.uniform(batch_size[None])>0.5
  observation, policy, action = hflip(hflip_flag, observation, policy, action)

  ## vflip
  vflip_flag = tf.random.uniform(batch_size[None])>0.5
  observation, policy, action = vflip(vflip_flag, observation, policy, action)

  ## transpose
  trans_flag = tf.random.uniform(batch_size[None])>0.5
  observation, policy, action = transpose(trans_flag, observation, policy, action)

  action_row, action_col = action
  policy_info_0, policy_info_1, policy_info_2 = policy

  # rebuild
  action = action_row * 3 + action_col
  policy_info_0 = tf.reshape(policy_info_0, tf.shape(x.policy_info[0]))
  policy_info_1 = tf.reshape(policy_info_1, tf.shape(x.policy_info[1]))
  
  return trajectory.Trajectory(
    x.step_type,
    observation,
    action,
    (policy_info_0, policy_info_1, policy_info_2),
    x.next_step_type,
    x.reward,
    x.discount
    ), y


