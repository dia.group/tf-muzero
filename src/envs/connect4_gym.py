"""
    A Connect4 gym environment
"""
import numpy as np
import gym   # type: ignore
import tensorflow as tf  # type: ignore
import gin  # type: ignore

from . import connect4_tf as connect4


@gin.configurable
class Connect4GymEnv(gym.Env):
  def __init__(self):
    self.action_space = gym.spaces.Discrete(7)
    # tf.bool and tf.int8 are not supported...
    self.observation_space = gym.spaces.Box(
        low=0, high=1, shape=(6, 7, 3), dtype=np.uint8)
    self.reward_range = (-float(1.0), float(1.0))

    c4init = connect4.Connect4Environment.init()
    self.state_board = tf.Variable(c4init.board, name="board")
    self.state_turn = tf.Variable(c4init.turn, name="turn")
    self.state_done = tf.Variable(c4init.done, name="done")
    self.state_winner = tf.Variable(c4init.winner, name="winner")

  def step(self, action):
    cur_state = connect4.Connect4Environment(self.state_board.value(
    ), self.state_turn.value(), self.state_done.value(), self.state_winner.value())

    new_state, reward = connect4.Connect4Environment.step(
        cur_state, tf.convert_to_tensor(action))

    self.state_board.assign(new_state.board)
    self.state_turn.assign(new_state.turn)
    self.state_done.assign(new_state.done)
    self.state_winner.assign(new_state.winner)

    obs = tf.cast(connect4.Connect4Environment.observation(
        new_state), tf.uint8)
    return obs.numpy()[0], reward.numpy()[0], new_state.done.numpy()[0], None

  def reset(self) -> np.ndarray:
    """
    @return observation (object): the initial observation
    """
    c4init = connect4.Connect4Environment.init()
    self.state_board.assign(c4init.board)
    self.state_turn.assign(c4init.turn)
    self.state_done.assign(c4init.done)
    self.state_winner.assign(c4init.winner)
    obs = tf.cast(
        connect4.Connect4Environment.observation(c4init), tf.uint8)
    return obs.numpy()[0]
