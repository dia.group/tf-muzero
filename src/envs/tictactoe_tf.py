"""
  TicTacToe TensorFlow
  a batched Tensorflow version of TicTacToe, allowing massive parallelisation
"""
from enum import Enum
import logging
import tensorflow as tf  # type: ignore
from typing import List, NamedTuple, Optional, Tuple


class Player(Enum):
    # use literal as this code triggers tf init/gpu alloc : (tf.constant(1, tf.int32))
    cross = 0
    circle = 1  # same: (tf.constant(-1, tf.int32))


# TODO: test tf.function compatibility with class inherited from NamedTuple
class TictactoeEnvironment(NamedTuple):
    """A blazing fast environment for tests."""

    action_space_size = 9

    board: tf.Tensor
    turn: tf.Tensor
    done: tf.Tensor
    winner: tf.Tensor

    @staticmethod
    @tf.function
    def init(batch_size: int = 1, board: Optional[tf.Tensor] = None) -> 'TictactoeEnvironment':
        assert isinstance(batch_size, int)

        # TODO: add GPU support with dtype=tf.int8 and dtype=tf.bool
        if board is None:
            board = tf.zeros((batch_size, 2, 3, 3), dtype=tf.int32)  # batch, player, height, width
            turn = tf.zeros([batch_size], dtype=tf.int32)
            done = tf.fill([batch_size], False)
            winner = tf.fill([batch_size], -1)
        else:
            if tf.executing_eagerly():
                tf.assert_equal(tf.shape(board)[-3:], (2,3,3))  # [P0-1, height, width]

            turn = tf.reduce_sum(board, axis=[-3, -2, -1])
            player = TictactoeEnvironment.to_play(turn)
            opponent = 1 - player
            winner_player = TictactoeEnvironment.check_winner(board, player)
            winner_opponent = TictactoeEnvironment.check_winner(board, opponent)
            winner = tf.where(
                winner_player,
                player,
                tf.where(winner_opponent, opponent, -1)
            )
            done = tf.logical_or(turn == 9, winner >= 0)

        return TictactoeEnvironment(board, turn, done, winner)

    @staticmethod
    @tf.function(jit_compile=True)
    # return legal actions as one hot encoding
    def legal_actions(self: 'TictactoeEnvironment') -> tf.Tensor:
        ret = tf.logical_and(self.board[:, 0, :, :] == 0, self.board[:, 1, :, :] == 0)
        return tf.logical_and(tf.logical_not(self.done)[:, None, None], ret)

    # this function must be reintrant when game is finished, as we batch games
    @staticmethod
    @tf.function(jit_compile=True)
    def step(
        self: 'TictactoeEnvironment',
        action: tf.Tensor,
        action_valid: Optional[tf.Tensor] = None,
    ) -> Tuple['TictactoeEnvironment', tf.Tensor]:
        """
        Parameters:
        action : array of actions to apply
        action_valid : (Tensor(bool)) to apply action or not
        Returns:
        tuple with (TictactoeEnvironment, reward)
        """
        """
      tf.assert_equal(tf.rank(action), 1)
      tf.assert_equal(tf.shape(action)[0], tf.shape(self.done)[0])
      tf.assert_equal(action.dtype, tf.int32)
      tf.assert_equal(tf.rank(action_valid), 1)
      tf.assert_equal(action_valid.dtype, tf.bool)
      #tf.assert_equal(tf.logical_or(tf.gather(self.board[...,-1,:], action, batch_dims=1, axis=1)==[0], tf.logical_or(self.done, tf.logical_not(action_valid))), [True]) # network have to learn invalid actions
      """
        if action_valid is None:
            action_valid = tf.fill((1, 3, 3), True)

        cur_board = self.board
        cur_turn = self.turn
        cur_done = self.done
        cur_winner = self.winner
        batch_size = tf.shape(cur_board)[0]

        with tf.name_scope("apply_action"):
            done_mask = tf.cast(tf.logical_not(cur_done), tf.int32)
            action_valid = tf.cast(action_valid, tf.int32) * done_mask[:, None, None]
            action_mask = tf.expand_dims(
                tf.reshape(tf.one_hot(action, 9, dtype=tf.int32), (batch_size, 3, 3))
                * action_valid,
                1,
            )  # withdraw action if already done
            player = TictactoeEnvironment.to_play(cur_turn)
            opponent = 1 - player
            player_mask = tf.one_hot(player, 2, dtype=tf.int32)[:, :, None, None]
            new_board = tf.where(action_mask != 0, player_mask, cur_board)
            new_turn = cur_turn + tf.cast(done_mask, tf.int32)

        with tf.name_scope("check_winner"):
            new_winner = tf.where(
                TictactoeEnvironment.check_winner(new_board, player), player, cur_winner
            )
            # check legal move
            forbidden_move = tf.reduce_any(
                tf.logical_and(
                    tf.logical_not(TictactoeEnvironment.legal_actions(self)),
                    action_mask[:, 0, :] != 0,
                ),
                axis=[-2, -1],
            )
            new_winner = tf.where(forbidden_move, opponent, new_winner)

        with tf.name_scope("check_end_of_game_or_draw"):
            new_done = tf.logical_or(
                tf.math.greater_equal(new_turn, 3 * 3), new_winner != -1
            )

        with tf.name_scope("compute_reward"):
            reward = tf.where(
                new_winner == player, 1.0, tf.where(new_winner == opponent, -1.0, -0.01)
            )
            reward = tf.where(new_done, reward, 0.0)
            reward = tf.where(cur_done, 0.0, reward)

        return (
            TictactoeEnvironment(new_board, new_turn, new_done, new_winner),
            reward,
        )

    @staticmethod
    @tf.function(jit_compile=True)
    def check_winner(board: tf.Tensor, p: tf.Tensor) -> tf.Tensor:
        """
        tf.assert_equal(tf.rank(board), 3)
        tf.assert_equal(tf.shape(board)[1:], [6,7])
        tf.assert_equal(tf.rank(p), 1)
        tf.assert_equal(p.dtype, tf.int32)
        tf.assert_equal(tf.shape(board)[0], tf.shape(p)[0])
        """

        p_board = tf.gather(board, p, axis=1, batch_dims=1) > 0
        v_mask = tf.reduce_any(
            tf.math.logical_and(
                tf.math.logical_and(p_board[:, 0, :], p_board[:, 1, :]),
                p_board[:, 2, :],
            ),
            -1,
        )
        h_mask = tf.reduce_any(
            tf.math.logical_and(
                tf.math.logical_and(p_board[:, :, 0], p_board[:, :, 1]),
                p_board[:, :, 2],
            ),
            -1,
        )

        d1_mask = tf.math.logical_and(
            tf.math.logical_and(p_board[:, 0, 0], p_board[:, 1, 1]), p_board[:, 2, 2]
        )
        d2_mask = tf.math.logical_and(
            tf.math.logical_and(p_board[:, 0, 2], p_board[:, 1, 1]), p_board[:, 2, 0]
        )

        w = tf.logical_or(
            tf.logical_or(h_mask, v_mask), tf.logical_or(d1_mask, d2_mask)
        )
        return w

    @staticmethod
    def to_play(turn: tf.Tensor) -> tf.Tensor:
        return tf.where(
            tf.math.floormod(turn, 2) == 0, Player.cross.value, Player.circle.value
        )

    @staticmethod
    def render(self: 'TictactoeEnvironment', index: int = 0) -> None:
        logging.info(f"Board round: {self.turn[index].numpy()}")

        for j in range(3):
            line: List = []
            for i in range(3):
                p0 = self.board[index][0][j][i].numpy()
                p1 = self.board[index][1][j][i].numpy()
                ch = "X" if p0 == 1 else "O" if p1 == 1 else "."
                line += ch + " "
            logging.info("".join(line))

        if self.done[index].numpy():
            logging.info("Game Over!")
            if self.winner[index] == 0:
                logging.info("X is the winner")
            elif self.winner[index] == 1:
                logging.info("O is the winner")
            else:
                logging.info("Game was a draw")

    @staticmethod
    @tf.function(jit_compile=True)
    def observation(self: 'TictactoeEnvironment') -> tf.Tensor:
        """ """
        to_play = tf.tile(TictactoeEnvironment.to_play(self.turn)[..., None, None], (1, 3, 3))
        return tf.stack([self.board[:,0,...], self.board[:,1,...], to_play], -1)

    @staticmethod
    @tf.function()
    def obs2env(obs: tf.Tensor) -> 'TictactoeEnvironment':
        """
        Rebuild an Environment from observation
        Warning : If a player choose a bad move, the winner is designated as opponent, but we couldn't rebuild environment in such situation
        """
        board = tf.stack([obs[..., 0], obs[..., 1]], 1)
        turn = tf.reduce_sum(board, axis=[-3, -2, -1])
        player = TictactoeEnvironment.to_play(turn)
        opponent = 1 - player
        winner_player = TictactoeEnvironment.check_winner(board, player)
        winner_opponent = TictactoeEnvironment.check_winner(board, opponent)
        winner = tf.where(
            winner_player, player, tf.where(winner_opponent, opponent, -1)
        )
        done = tf.logical_or(turn == 9, winner >= 0)

        ret = TictactoeEnvironment(board, turn, done, winner)
        if tf.executing_eagerly():
            TictactoeEnvironment.assert_struct(ret)
        return ret

    @staticmethod
    def equals(
        self: 'TictactoeEnvironment', other: 'TictactoeEnvironment'
    ) -> tf.Tensor:
        if self is None or other is None:
            return False
        if type(self) != type(other):
            return False
        if tf.reduce_any(tf.logical_not(tf.equal(self.board, other.board))):
            return False
        if tf.reduce_any(tf.logical_not(tf.equal(self.turn, other.turn))):
            return False
        if tf.reduce_any(tf.logical_not(tf.equal(self.done, other.done))):
            return False
        if tf.reduce_any(tf.logical_not(tf.equal(self.winner, other.winner))):
            return False

        return True

    @staticmethod
    def assert_struct(self: 'TictactoeEnvironment') -> None:
        batch_size = tf.shape(self.board)[0]

        assert isinstance(self.board, tf.Tensor)
        tf.debugging.assert_type(self.board, tf.int32)
        tf.debugging.assert_rank(self.board, 4)
        tf.assert_equal(tf.shape(self.board)[0], batch_size)
        tf.assert_equal(tf.shape(self.board)[1], 2)
        tf.assert_equal(tf.shape(self.board)[2], 3)
        tf.assert_equal(tf.shape(self.board)[3], 3)

        assert isinstance(self.turn, tf.Tensor)
        tf.debugging.assert_type(self.turn, tf.int32)
        tf.debugging.assert_rank(self.turn, 1)
        tf.assert_equal(tf.shape(self.turn)[0], batch_size)

        assert isinstance(self.done, tf.Tensor)
        tf.debugging.assert_type(self.done, tf.bool)
        tf.debugging.assert_rank(self.done, 1)
        tf.assert_equal(tf.shape(self.done)[0], batch_size)

        assert isinstance(self.winner, tf.Tensor)
        tf.debugging.assert_type(self.winner, tf.int32)
        tf.debugging.assert_rank(self.winner, 1)
        tf.assert_equal(tf.shape(self.winner)[0], batch_size)
