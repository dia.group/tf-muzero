#from __future__ import annotations  # not compatible with kaggle
import tensorflow as tf  # type: ignore
import tensorflow_probability as tfp  # type: ignore
from typing import Callable, NamedTuple, Optional, Tuple, Union

# pylint: disable=relative-beyond-top-level
from .common import *
from .support import *


class Tree(NamedTuple):
    visit_count: tf.Tensor
    prior: tf.Tensor
    value_sum: tf.Tensor
    reward: tf.Tensor
    to_expanded: tf.Tensor
    expanded_to_play: tf.Tensor
    expanded_hidden_state: tf.Tensor
    expanded_size: tf.Tensor

    @staticmethod
    def init(
        batch_size: int,
        action_count: int,
        num_simulations: int,
        hidden_state_spec: Union[tf.TensorSpec, Tuple[tf.TensorSpec, ...]],
    ) -> 'Tree':
        """
        Perameters:
        batch_size (int): the size of the batch processing
        action_count (int): size of action space (mono action)
        num_simulations (int): mcts node count at max
        hidden_state_spec (tf.TensorSpec): hidden state specs without batch dimension (will be inserted as first one)
        Returns:
        Tree: the empty tree ready for mcts
        """
        compute_dtype = tf.float32  # tf.keras.backend.floatx()

        # 1 per node (1 per child in expanded nodes) + z-elt
        # z-elt + root + (root_children+simu)*actions
        size_max = 1 + 1 + (1 + num_simulations) * action_count

        visit_count = tf.zeros([batch_size, size_max], dtype=tf.int32)
        prior = tf.zeros([batch_size, size_max], dtype=compute_dtype)
        value_sum = tf.zeros([batch_size, size_max], dtype=compute_dtype)
        reward = tf.zeros([batch_size, size_max], dtype=compute_dtype)
        to_expanded = tf.zeros([batch_size, size_max], dtype=tf.int32)

        # 1 per expanded nodes + z-elt
        expanded_size_max = 1 + 1 + num_simulations
        expanded_to_play = tf.zeros(
            [batch_size, expanded_size_max], dtype=tf.int32
        )

        def init_exp_hidden_state(spec):
            return tf.zeros([batch_size, expanded_size_max, *spec.shape], spec.dtype)

        expanded_hidden_state = tf.nest.map_structure(
            init_exp_hidden_state, hidden_state_spec
        )
        expanded_size = tf.ones(batch_size, dtype=tf.int32)

        return Tree(
            visit_count,
            prior,
            value_sum,
            reward,
            to_expanded,
            expanded_to_play,
            expanded_hidden_state,
            expanded_size,
        )

    def rootNode(self: 'Tree') -> tf.Tensor:
        """
        @return expand_node id of root Node
        """
        batch_size = tf.shape(self.to_expanded)[0]
        return tf.ones(batch_size, tf.int32)

    @staticmethod
    def nodeChildren(
        expanded_node: tf.Tensor,
        action_count: int,
    ) -> tf.Tensor:
        """
        @return children node id from expanded parent node_id
        """
        assert expanded_node.dtype == tf.int32
        assert isinstance(action_count, int)

        expanded_node = tf.expand_dims(expanded_node, -1)
        base_index = expanded_node - 1  # fix dim and remove z-elt index
        ret = (
            base_index * action_count + 2 + tf.expand_dims(tf.range(action_count), 0)
        )  # 2: z-elt + root
        return tf.where(expanded_node == 0, 0, ret)

    def expandNode(
        self: 'Tree',
        node: tf.Tensor,
        to_play: tf.Tensor,
        hidden_state: tf.Tensor,
        reward: tf.Tensor,
        children_prior: tf.Tensor,
        action_count: int,
    ) -> 'Tree':
        """
        expand node: create a new expanded_node to store children's datas
        """
        batch_size = tf.shape(self.to_expanded)[0]
        if tf.executing_eagerly():
          tf.assert_rank(node, 1)
          tf.assert_equal(tf.shape(node)[0], batch_size)
          tf.assert_rank(to_play, 1)
          tf.assert_equal(tf.shape(to_play)[0], batch_size)
          # tf.assert_rank(hidden_state, 4)
          # tf.assert_equal(tf.shape(hidden_state)[0], batch_size)  # hidden_state could be nested
          tf.assert_rank(reward, 2)
          tf.assert_equal(tf.shape(reward)[0], batch_size)
          tf.assert_equal(tf.shape(reward)[1], 1)
          tf.assert_rank(children_prior, 2)
          tf.assert_equal(tf.shape(children_prior)[0], batch_size)
          # tf.assert_equal(tf.shape(children_prior)[1], self.action_space)
          tf.assert_equal(tf.reduce_all(node!=0), True) # never expand this node (z)
          # tf.debugging.assert_less_equal(node, 1+self.expanded_size*self.action_space) # valid expand range
          tf.assert_equal(tf.gather(self.to_expanded, node, axis=1, batch_dims=1), 0) # never expanded

        node_indices2D = tf.stack([tf.range(batch_size), node], -1)
        expand_index = self.expanded_size
        new_to_expanded = tf.tensor_scatter_nd_update(
            self.to_expanded,
            node_indices2D,
            expand_index,
        )  # next free id
        expand_index2D = tf.stack([tf.range(batch_size), expand_index], -1)
        new_expanded_to_play = tf.tensor_scatter_nd_update(
            self.expanded_to_play,
            expand_index2D,
            to_play,
        )

        if tf.nest.is_nested(self.expanded_hidden_state):
            new_expanded_hidden_state = tuple(
                [
                    tf.tensor_scatter_nd_update(tree_hstate, expand_index2D, hstate)
                    for tree_hstate, hstate in zip(
                        self.expanded_hidden_state, hidden_state
                    )
                ]
            )
        else:
            if (
                self.expanded_hidden_state.dtype == tf.int8
                or self.expanded_hidden_state.dtype == tf.float16
                or self.expanded_hidden_state.dtype == tf.bfloat16
            ):
                hidden_state = hidden_state * 128.0
            if self.expanded_hidden_state.dtype == tf.int8:
                hidden_state = tf.clip_by_value(tf.math.round(hidden_state), -128, 127)
            hidden_state = tf.cast(hidden_state, self.expanded_hidden_state.dtype)
            new_expanded_hidden_state = tf.tensor_scatter_nd_update(
                self.expanded_hidden_state, expand_index2D, hidden_state
            )

        node_range2D = tf.stack(
            [
                tf.tile(tf.expand_dims(tf.range(batch_size), -1), [1, action_count]),
                Tree.nodeChildren(expand_index, action_count),
            ],
            -1,
        )
        new_prior = tf.tensor_scatter_nd_update(
            self.prior, node_range2D, children_prior
        )
        new_reward = tf.tensor_scatter_nd_update(
            self.reward, node_indices2D, tf.squeeze(reward, -1)
        )
        new_expanded_size = self.expanded_size + 1

        return Tree(
            self.visit_count,
            new_prior,
            self.value_sum,
            new_reward,
            new_to_expanded,
            new_expanded_to_play,
            new_expanded_hidden_state,
            new_expanded_size,
        )

    def expanded(self: 'Tree', node: tf.Tensor) -> tf.Tensor:
        return (
            tf.gather(self.to_expanded, node, axis=1, batch_dims=1)
            != 0
        )

    def value(self: 'Tree', node: tf.Tensor) -> tf.Tensor:
        visit_count = tf.gather(self.visit_count, node, axis=1, batch_dims=1)
        value = tf.gather(self.value_sum, node, axis=1, batch_dims=1) / tf.cast(
            visit_count, dtype=self.value_sum.dtype
        )
        return tf.where(visit_count == 0, 0.0, value)

    def get_hidden_state(self: 'Tree', node: tf.Tensor) -> tf.Tensor:
        to_expanded = tf.cast(
            tf.gather(self.to_expanded, node, axis=1, batch_dims=1), tf.int32
        )

        def gather_hidden_state(tree_hstate):
            return tf.gather(tree_hstate, to_expanded, axis=1, batch_dims=1)

        if tf.nest.is_nested(self.expanded_hidden_state):
            hidden_state = tuple(
                [
                    gather_hidden_state(tree_hstate)
                    for tree_hstate in self.expanded_hidden_state
                ]
            )
        else:
            hidden_state = gather_hidden_state(self.expanded_hidden_state)
            if (
                self.expanded_hidden_state.dtype == tf.int8
                or self.expanded_hidden_state.dtype == tf.float16
                or self.expanded_hidden_state.dtype == tf.bfloat16
            ):
                hidden_state = tf.cast(hidden_state, tf.float32) * 1.0 / 128.0
        return hidden_state

    # Debugging
    def render(
        self: 'Tree',
        action_count: int,
        batch_index: int = 0,
        _prefix: str = "",
        _node: int = 1,
    ) -> None:
        """
        log tree at index 0
        """
        import logging

        visit_count = self.visit_count[batch_index, _node]
        if visit_count > 0:
            value = self.value_sum[batch_index, _node] / tf.cast(visit_count, tf.float32)
            print(
                f"{_prefix}(N{_node-1} : visit_count: {visit_count}; value: {value:0.4f})"
            )
            expand_index = self.to_expanded[batch_index, _node]
            if expand_index != 0:
                expand = Tree.nodeChildren(
                    expand_index, action_count
                )
                for action_index, node_children in enumerate(expand[0]):
                    prefix = _prefix + f"A{action_index}(P:{self.prior[batch_index, node_children]:0.4f}; R:{self.reward[batch_index, node_children]:0.4f}):"
                    self.render(action_count, batch_index, prefix, node_children)


##################################
####### Part 1: Self-Play ########

# Core Monte Carlo Tree Search algorithm.
# To decide on an action, we run N simulations, always starting at the root of
# the search tree and traversing the tree according to the UCB formula until we
# reach a leaf node.


@tf.function(jit_compile=True)
def run_mcts(
    config: MuZeroConfig,
    tree: Tree,
    history_depth: tf.Tensor,
    network: MuzeroNetwork_I,
    prnd: tf.Tensor,
    action_mask: Optional[tf.Tensor] = None,
    min_max_stats: Optional[MinMaxStats] = None,
) -> Tuple[Tree, MinMaxStats]:

    batch_size = tf.shape(history_depth)[0]
    if min_max_stats is None:
        min_max_stats = MinMaxStats.init(config.known_bounds)
    if action_mask is None:
        action_mask = tf.ones([batch_size, config.action_space_size])

    history_action = tf.TensorArray(
        dtype=tf.int32,
        size=config.num_simulations + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=history_depth.shape,
    )  # allocation for the whole function
    search_path = tf.TensorArray(
        dtype=tf.int32,
        size=config.num_simulations + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=history_depth.shape,
    )  # +1 for added node

    for _ in tf.range(config.num_simulations):

        node = tf.ones_like(history_depth, tf.int32)
        # reset history_action array and search_path array
        history_action_length = history_depth
        search_path = search_path.write(0, node)
        search_path_length = tf.ones_like(node)

        with tf.name_scope("Selection"):
            node_expanded = tree.expanded(node)
            step = 1
            while tf.reduce_any(node_expanded):
                action, next_node = select_child(config, tree, node, tf.where(step==1, action_mask, 1), min_max_stats, prnd)

                history_action = history_action.write(step, action)
                history_action_length += tf.cast(node_expanded, tf.int32)

                search_path = search_path.write(step, next_node)
                search_path_length += tf.cast(node_expanded, tf.int32)
                node_expanded = tree.expanded(next_node)
                step += 1
                node = next_node

        history_action_stack = tf.transpose(history_action.stack(), [1, 0])
        search_path_stack = tf.transpose(search_path.stack(), [1, 0])

        with tf.name_scope("Simulation"):
            # Inside the search tree we use the dynamic function to obtain the next
            # hidden state given an action and the previous hidden state.

            parent = tf.gather(
                search_path_stack, search_path_length - 2, axis=1, batch_dims=1
            )  # parent = search_path[:,-2]
            # was set to zero if select continues after leaf found
            node = tf.gather(
                search_path_stack, search_path_length - 1, axis=1, batch_dims=1
            )
            hidden_state = tree.get_hidden_state(parent)
            network_output_raw = network.recurrent_inference(
                hidden_state,
                tf.gather(
                    history_action_stack, search_path_length - 1, axis=1, batch_dims=1
                ),
            )
            network_output = NetworkOutput(
                network_output_raw.value
                if config.support_size_value is None
                else support_to_scalar(
                    tf.nn.softmax(network_output_raw.value),
                    config.support_size_value,
                    config.support_scale_value,
                ),
                network_output_raw.reward
                if config.support_size_reward is None
                else support_to_scalar(
                    tf.nn.softmax(network_output_raw.reward),
                    config.support_size_reward,
                    config.support_scale_reward,
                ),
                network_output_raw.policy_logits,
                network_output_raw.hidden_state,
            )
        with tf.name_scope("Expansion"):
            to_play = tf.math.floormod(history_action_length, config.player_turn)
            tree = expand_node(
                tree,
                node,
                to_play,
                tf.ones([batch_size, config.action_space_size]),
                network_output,
                config.action_space_size,
            )

        with tf.name_scope("Backpropagation"):
            # tree_loop, _ = backpropagate_loop(  # min_max_stats
            #     config,
            #     tree,
            #     search_path_stack,
            #     search_path_length,
            #     network_output.value,
            #     to_play,
            #     config.discount,
            #     min_max_stats,
            # )

            tree, min_max_stats = backpropagate(
                config,
                tree,
                search_path_stack,
                search_path_length,
                network_output.value,
                to_play,
                config.discount,
                min_max_stats,
            )
            # tf.debugging.assert_less(tf.math.abs(tree_loop.value_sum - tree.value_sum), 0.001)

    return tree, min_max_stats


@tf.function(jit_compile=True)
def select_action(
    tree: Tree,
    action_count: int,
    temperature: tf.Tensor,
    prnd: tf.Tensor,
) -> tf.Tensor:

    visit_counts = tf.gather(
        tree.visit_count,
        Tree.nodeChildren(tree.rootNode(), action_count),
        axis=-1,
        batch_dims=-1,
    )

    action = softmax_sample(tf.cast(visit_counts, tf.float32), temperature, prnd)
    return action


def softmax_sample(distribution: tf.Tensor, temperature: float, prnd: tf.Tensor) -> tf.Tensor:
    f_distribution = tf.cast(distribution, tf.float32)
    temperature = tf.reshape(
        temperature,
        tf.concat(
            [
                tf.shape(temperature),
                tf.ones(tf.rank(f_distribution) - 1 - tf.rank(temperature), tf.int32),
            ],
            axis=0,
        ),
    )
    probs = tf.math.pow(f_distribution, 1 / tf.expand_dims(temperature, -1))
    probs = probs / tf.expand_dims(tf.reduce_sum(probs, axis=1), -1)
    sample = tfp.distributions.Categorical(probs=probs).sample(seed=prnd)

    return tf.where(
        temperature == 0,
        tf.math.argmax(distribution, axis=1, output_type=tf.int32),
        sample,
    )


# Select the child with the highest UCB score.
@tf.function(jit_compile=True)
def select_child(
    config: MuZeroConfig, tree: Tree, node: tf.Tensor, action_mask: tf.Tensor, min_max_stats: MinMaxStats, prnd:tf.Tensor
) -> Tuple[tf.Tensor, tf.Tensor]:

    expanded_node = tf.cast(
        tf.gather(tree.to_expanded, node, axis=1, batch_dims=1), tf.int32
    )
    children_node = Tree.nodeChildren(expanded_node, config.action_space_size)
    scores = ucb_score(config, tree, node, children_node, min_max_stats, prnd) * action_mask
    action = tf.math.argmax(
        scores,
        axis=1,
        output_type=tf.int32,
    )
    child = tf.gather(children_node, action, axis=1, batch_dims=1)
    return action, child


# The score for a node is based on its value, plus an exploration bonus based on
# the prior.
def ucb_score(
    config: MuZeroConfig,
    tree: Tree,
    parent: tf.Tensor,
    children: tf.Tensor,
    min_max_stats: MinMaxStats,
    prnd: tf.Tensor,
) -> tf.Tensor:
    compute_dtype = tf.float32
    parent_visit_count = tf.cast(
        tf.gather(tree.visit_count, parent, axis=1, batch_dims=1), compute_dtype
    )  # TODO: allow auto up-cast
    child_visit_count = tf.gather(tree.visit_count, children, axis=1, batch_dims=1)
    pb_c = (
        tf.math.log((parent_visit_count + config.pb_c_base) / config.pb_c_base)
        + config.pb_c_init
    )
    pb_c = (
        tf.expand_dims(pb_c, -1)
        * tf.expand_dims(tf.math.sqrt(parent_visit_count), -1)
        / tf.cast(child_visit_count + 1, compute_dtype)
    )

    prior_score = pb_c * tf.gather(tree.prior, children, axis=1, batch_dims=1)
    to_play_inv = 1.0 if config.player_turn == 1 else -1.0
    # TODO: issue on arxiv's paper as it doesn't normalize reward
    value_score = tf.where(
        child_visit_count > 0,
        config.discount
        * MinMaxStats.normalize(
            min_max_stats,
            tf.gather(tree.reward, children, axis=1, batch_dims=1)
            + tree.value(children) * to_play_inv,
        ),
        0.0,
    )
    noise_score = 1e-7 * tf.random.stateless_uniform(tf.shape(value_score), seed=prnd)
    return prior_score + value_score + noise_score


# We expand a node using the value, reward and policy prediction obtained from
# the neural network.
@tf.function(jit_compile=True)
def expand_node(
    tree: Tree,
    node: tf.Tensor,
    to_play: tf.Tensor,
    actions: tf.Tensor,
    network_output: NetworkOutput,
    action_count: int,
) -> Tree:

    assert isinstance(actions, tf.Tensor)
    # tf.debugging.assert_type(actions, tf.float32)
    # tf.debugging.assert_shapes([(network_output.policy_logits, ('M','N')), (actions, ('M','N'))])
    # tf.debugging.assert_rank(actions, 2)  # TODO: XLA fails with "Const op with type DT_STRING is not supported by XLA" ...
    # tf.assert_equal(tf.shape(actions)[0], tf.shape(tree.to_expanded)[0])  # fails too
    # tf.assert_equal(tf.shape(actions)[1], tree.action_space)  # fails too

    policy = tf.math.exp(network_output.policy_logits) * tf.cast(
        actions, dtype=network_output.policy_logits.dtype
    )
    policy_sum = tf.reduce_sum(policy, axis=1)
    # broadcasting "fails" : explicit dims expanding
    children_prior = policy / tf.expand_dims(policy_sum, -1)
    return tree.expandNode(
        node,
        to_play,
        network_output.hidden_state,
        network_output.reward,
        children_prior,
        action_count,
    )


# At the end of a simulation, we propagate the evaluation all the way up the
# tree to the root.
@tf.function(jit_compile=True)
def backpropagate_loop(
    config: MuZeroConfig,
    tree: Tree,
    search_path: tf.Tensor,
    search_path_length: tf.Tensor,
    value: tf.Tensor,
    to_play: tf.Tensor,
    discount: float,
    min_max_stats: MinMaxStats,
) -> Tuple[Tree, MinMaxStats]:
  indices_exp = tf.gather(tree.to_expanded, search_path, axis=-1, batch_dims=-1)
  node_to_play = tf.gather(tree.expanded_to_play, indices_exp, axis=-1, batch_dims=-1)
  node_reward = tf.gather(tree.reward, search_path, axis=-1, batch_dims=-1)
  value = tf.squeeze(value,-1)

  cond = lambda tree, i, val: tf.reduce_any(i>=0)
  def body(tree, i, val):
    node_indices = tf.gather(search_path, tf.math.maximum(i,0), axis=1, batch_dims=1)
    node_indices2D = tf.stack([tf.range(tf.shape(search_path)[0]), node_indices], -1)

    relative = tf.where(tf.gather(node_to_play, tf.math.maximum(i,0), axis=1, batch_dims=1)==to_play, 1.0, -1.0)
    val_toadd = tf.where(i>=0, val * relative, 0.0)
    value_sum = tf.tensor_scatter_nd_add(tree.value_sum, node_indices2D, val_toadd)

    visit_toadd = tf.where(i>=0, tf.ones(tf.shape(node_indices2D)[0], dtype=tree.visit_count.dtype), 0)
    visit_count = tf.tensor_scatter_nd_add(tree.visit_count, node_indices2D, visit_toadd)
    tree = Tree(visit_count, tree.prior, value_sum, tree.reward, tree.to_expanded, tree.expanded_to_play, tree.expanded_hidden_state, tree.expanded_size)

    # val = val*discount + tf.gather(node_reward, tf.math.maximum(i,0), axis=1, batch_dims=1)  # TODO: this is code if reward is relative to parent (the action initier)
    reward_relative = 1.0 if config.player_turn == 1 else relative * -1.0  # TODO: remove config.player_turn test -- this line is bugged
    val = val*discount + tf.gather(node_reward, tf.math.maximum(i,0), axis=1, batch_dims=1) * reward_relative

    return (tree, i-1, val)

  loop_vars = (tree, search_path_length-1, value)
  new_tree,_,_ = tf.while_loop(cond, body, loop_vars)

  tree_values = Tree.value(new_tree, search_path)
  min_max_stats.update(tf.reduce_min(tree_values))
  min_max_stats.update(tf.reduce_max(tree_values))
  return new_tree, min_max_stats


# At the end of a simulation, we propagate the evaluation all the way up the
# tree to the root.
@tf.function(jit_compile=True)
def backpropagate(
    config: MuZeroConfig,
    tree: Tree,
    search_path: tf.Tensor,
    search_path_length: tf.Tensor,
    value: tf.Tensor,
    to_play: tf.Tensor,
    discount: float,
    min_max_stats: MinMaxStats,
) -> Tuple[Tree, MinMaxStats]:
    """
    we remove loop, using discount.
    Reward needs to be discount-inverted before cumsum applied and final discount
    """
    indices_exp = tf.gather(tree.to_expanded, search_path, axis=-1, batch_dims=-1)
    node_to_play = tf.gather(tree.expanded_to_play, indices_exp, axis=-1, batch_dims=-1)
    node_reward = tf.gather(tree.reward, search_path, axis=-1, batch_dims=-1)
    value = tf.squeeze(value, -1)
    value_relative = tf.where(node_to_play == tf.expand_dims(to_play, -1), 1.0, -1.0)

    cur_visit_count = tree.visit_count
    cur_value_sum = tree.value_sum
    node_indices = search_path
    base_indices = tf.tile(
        tf.expand_dims(tf.range(tf.shape(search_path)[0]), -1),
        [1, tf.shape(search_path)[1]],
    )
    node_indices2D = tf.stack([base_indices, node_indices], -1)
    visit_toadd = tf.cast(search_path != 0, dtype=cur_visit_count.dtype)

    new_visit_count = tf.tensor_scatter_nd_add(
        cur_visit_count, node_indices2D, visit_toadd
    )

    discount_depth = tf.clip_by_value(
        tf.expand_dims(search_path_length, -1)
        - tf.expand_dims(tf.range(tf.shape(search_path)[1]), 0)
        - 1,
        0,
        tf.int32.max,
    )
    discount_curve = tf.math.pow(discount, tf.cast(discount_depth, tf.float32))
    val_toadd = tf.expand_dims(value, -1) * discount_curve * value_relative
    val_toadd *= tf.cast(visit_toadd, tf.float32)

    # convert reward from relative to node to a reward relative to this player
    node_reward *= 1.0 if config.player_turn==1 else tf.where(node_to_play == tf.expand_dims(to_play, -1), -1.0, 1.0)
    reward_toadd = (
        tf.cumsum(node_reward / discount_curve, axis=1, reverse=True, exclusive=True)
        * discount_curve
        * value_relative
    )
    reward_toadd *= tf.cast(visit_toadd, tf.float32)

    new_value_sum = tf.tensor_scatter_nd_add(
        cur_value_sum, node_indices2D, val_toadd + reward_toadd
    )

    new_tree = Tree(
        new_visit_count,
        tree.prior,
        new_value_sum,
        tree.reward,
        tree.to_expanded,
        tree.expanded_to_play,
        tree.expanded_hidden_state,
        tree.expanded_size,
    )

    tree_values = new_tree.value(search_path)
    min_max_stats = min_max_stats.update(tf.reduce_min(tree_values))
    min_max_stats = min_max_stats.update(tf.reduce_max(tree_values))
    return new_tree, min_max_stats


# At the start of each search, we add dirichlet noise to the prior of the root
# to encourage the search to explore new actions.
@tf.function(jit_compile=True)
def add_exploration_noise(
    config:MuZeroConfig,
    prnd:tf.Tensor,
    tree:Tree,
    action_mask:Optional[tf.Tensor] = None
) -> Tree:

    batch_size = tf.shape(tree.to_expanded)[0]
    node_indices = Tree.nodeChildren(tree.rootNode(), config.action_space_size)

    prior = tf.gather(tree.prior, node_indices, axis=1, batch_dims=1)
    flat_action_mask = (
        tf.reshape(tf.cast(action_mask, tf.float32), tf.shape(prior))
        if action_mask is not None
        else 1.0
    )
    
    if config.root_dirichlet_adaptive:
        dir_alpha = tf.tile(
          1.0 / tf.math.sqrt(tf.reduce_sum(flat_action_mask, -1))[..., None],
          (1, config.action_space_size)
          )
    else:
        dir_alpha = tf.fill([batch_size, config.action_space_size], config.root_dirichlet_alpha)
    distribution = tfp.distributions.Dirichlet(
        dir_alpha * flat_action_mask
    )
    noise = tf.cast(distribution.sample(seed=prnd), dtype=prior.dtype)
    frac = tf.cast(config.root_exploration_fraction, dtype=prior.dtype)
    new_prior = prior * (1 - frac) + noise * frac
    node_indices2D = tf.stack(
        [
            tf.tile(
                tf.expand_dims(tf.range(batch_size), -1), [1, config.action_space_size]
            ),
            node_indices,
        ],
        -1,
    )
    prior = tf.tensor_scatter_nd_update(tree.prior, node_indices2D, new_prior)
    return Tree(
        tree.visit_count,
        prior,
        tree.value_sum,
        tree.reward,
        tree.to_expanded,
        tree.expanded_to_play,
        tree.expanded_hidden_state,
        tree.expanded_size,
    )


######### End Self-Play ##########
##################################
