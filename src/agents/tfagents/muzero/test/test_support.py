import logging
from parameterized import parameterized  # type: ignore
import tensorflow as tf  # type: ignore
import unittest

from .. import support


class TestSupport(unittest.TestCase):
    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed')
        # logging.basicConfig(level=logging.INFO,
        #         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        #         datefmt='%Y-%m-%d %H:%M:%S',
        #         )
        # tf.config.run_functions_eagerly(True) # to debug
        return super().setUp()

    @parameterized.expand(
        [
            ("scale", True, 2),
            ("no scale", False, 8),
            ("linear", 0.5, 4),
        ]
    )
    def test_support(self, name, scale, support_size: int):
        logging.info("")
        epsilon = tf.constant(1e-2)
        for i in range(-10, 10 + 2, 2):
            input = tf.convert_to_tensor([[i]], tf.float32)
            probabilities = support.scalar_to_support(input, support_size, scale)
            x = support.support_to_scalar(probabilities, support_size, scale)
            self.assertEqual(tf.rank(input), tf.rank(x))
            logging.info(f"({input}) => ({probabilities}) => ({x})")
            if i >= -5 and i <= 5:
                self.assertLessEqual(x - i, epsilon)
            else:
                self.assertLessEqual(x - i, 3)
