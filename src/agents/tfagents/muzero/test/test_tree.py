import collections
import logging
import tensorflow as tf  # type: ignore
import unittest

from .. import common as muzero_common
from .. import play as muzero_play
from . import tictactoe_tf as tictactoe
from . import tictactoe_devtools


class TestTree(unittest.TestCase):
    def setUp(self) -> None:
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        # np.set_printoptions(precision=4, floatmode='fixed', suppress=True)
        # tf.config.run_functions_eagerly(True)  # to debug
        return super().setUp()

    def test_z_value(self):
        Config = collections.namedtuple(
            "Config", ["action_space_size", "num_simulations", "rp_shape"]
        )
        batch_size = 4
        config = Config(3 * 3, 10, tf.TensorSpec((3, 3, 64), tf.float32))

        z_value_node = tf.zeros([1, 1], tf.int32)

        tree = muzero_play.Tree.init(
            batch_size,
            config.action_space_size,
            config.num_simulations,
            config.rp_shape,
        )
        root_node = tree.rootNode()
        children = muzero_play.Tree.nodeChildren(
            tf.cast(
                tf.gather(tree.to_expanded, root_node, axis=1, batch_dims=1), tf.int32
            ),
            config.action_space_size,
        )
        tf.assert_equal(children, z_value_node)

    def test_tree_size(self):
        num_simulations = 10
        Config = collections.namedtuple(
            "Config", ["action_space_size", "num_simulations", "rp_shape"]
        )
        batch_size = 4
        config = Config(3 * 3, num_simulations, tf.TensorSpec((3, 3, 64), tf.float32))
        tree = muzero_play.Tree.init(
            batch_size,
            config.action_space_size,
            config.num_simulations,
            config.rp_shape,
        )

        tf.random.set_seed(123456)
        env = tictactoe.TictactoeEnvironment.init(batch_size)
        node = tree.rootNode()
        to_play = tf.cast(tictactoe.TictactoeEnvironment.to_play(env.turn), tf.int32)
        hidden_state = tf.random.uniform([batch_size, 3, 3, 64], 0, 1)
        reward = tf.random.uniform([batch_size, 1], 0, 1)
        children_prior = tf.random.uniform([batch_size, config.action_space_size], 0, 1)

        # root expansion
        tree = muzero_play.Tree.expandNode(
            tree,
            node,
            to_play,
            hidden_state,
            reward,
            children_prior,
            config.action_space_size,
        )
        children_node = muzero_play.Tree.nodeChildren(
            tf.cast(tf.gather(tree.to_expanded, node, axis=1, batch_dims=1), tf.int32),
            config.action_space_size,
        )

        for _ in range(num_simulations):
            to_play = tf.cast(
                tictactoe.TictactoeEnvironment.to_play(env.turn), tf.int32
            )
            hidden_state = tf.random.uniform([batch_size, 3, 3, 64], 0, 1)
            reward = tf.random.uniform([batch_size, 1], 0, 1)
            children_prior = tf.random.uniform(
                [batch_size, config.action_space_size], 0, 1
            )

            node = tf.gather(
                children_node, tf.argmax(children_prior, axis=1), axis=1, batch_dims=1
            )
            tree = muzero_play.Tree.expandNode(
                tree,
                node,
                to_play,
                hidden_state,
                reward,
                children_prior,
                config.action_space_size,
            )
            children_node = muzero_play.Tree.nodeChildren(
                tf.cast(
                    tf.gather(tree.to_expanded, node, axis=1, batch_dims=1), tf.int32
                ),
                config.action_space_size,
            )

    def test_tree_expand(self):
        Config = collections.namedtuple(
            "Config", ["action_space_size", "num_simulations", "rp_shape"]
        )
        batch_size = 16
        config = Config(7, 10, tf.TensorSpec((6, 7, 64), tf.float32))

        tree = muzero_play.Tree.init(
            batch_size,
            config.action_space_size,
            config.num_simulations,
            config.rp_shape,
        )

        def test_expand(tree, node):
            env = tictactoe.TictactoeEnvironment.init(batch_size)
            to_play = tf.cast(
                tictactoe.TictactoeEnvironment.to_play(env.turn), tf.int32
            )
            hidden_state = tf.random.uniform([batch_size, 6, 7, 64], 0, 1)
            reward = tf.random.uniform([batch_size, 1], 0, 1)
            children_prior = tf.random.uniform(
                [batch_size, config.action_space_size], 0, 1
            )

            tree = muzero_play.Tree.expandNode(
                tree,
                node,
                to_play,
                hidden_state,
                reward,
                children_prior,
                config.action_space_size,
            )

            # re-read from tree
            reward_p = tf.expand_dims(
                tf.gather(tree.reward, node, axis=1, batch_dims=1), -1
            )
            tf.assert_equal(reward_p, reward)
            self.assertTrue(tf.reduce_all(tf.equal(reward_p, reward)))
            prior_p = tf.gather(
                tree.prior,
                muzero_play.Tree.nodeChildren(
                    tf.cast(
                        tf.gather(tree.to_expanded, node, axis=1, batch_dims=1),
                        tf.int32,
                    ),
                    config.action_space_size,
                ),
                axis=1,
                batch_dims=1,
            )
            tf.assert_equal(prior_p, children_prior)
            self.assertTrue(tf.reduce_all(tf.equal(prior_p, children_prior)))
            to_play_p = tf.cast(
                tf.gather(
                    tree.expanded_to_play,
                    tf.cast(
                        tf.gather(tree.to_expanded, node, axis=1, batch_dims=1),
                        tf.int32,
                    ),
                    axis=1,
                    batch_dims=1,
                ),
                tf.int32,
            )
            tf.assert_equal(to_play_p, to_play)
            self.assertTrue(tf.reduce_all(tf.equal(to_play_p, to_play)))
            hidden_state_p = tf.gather(
                tree.expanded_hidden_state,
                tf.cast(
                    tf.gather(tree.to_expanded, node, axis=1, batch_dims=1), tf.int32
                ),
                axis=1,
                batch_dims=1,
            )
            tf.assert_equal(hidden_state_p, hidden_state)
            self.assertTrue(tf.reduce_all(tf.equal(hidden_state_p, hidden_state)))

            return tree

        tf.random.set_seed(123456)
        node = tree.rootNode()
        tree = test_expand(tree, node)
        node = tf.fill([batch_size], 3)
        tree = test_expand(tree, node)
        node = tf.fill([batch_size], 4)
        tree = test_expand(tree, node)
        children_node = muzero_play.Tree.nodeChildren(node, config.action_space_size)
        node = tf.gather(
            children_node,
            tf.random.uniform([batch_size], 0, config.action_space_size, tf.int32),
            axis=1,
            batch_dims=1,
        )
        test_expand(tree, node)

    def test_select_child(self):
        """
        test unbiased child selection with nodes sharing value
        """
        config = muzero_common.MuZeroConfig()
        batch_size = 1024

        rp_shape = tf.TensorSpec((3, 3, 64), tf.float32)
        tree = muzero_play.Tree.init(
            batch_size, config.action_space_size, config.num_simulations, rp_shape
        )
        min_max_stats = muzero_common.MinMaxStats.init(config.known_bounds)

        node = tf.ones(batch_size, tf.int32)
        action, child = muzero_play.select_child(
            config,
            tree,
            node,
            action_mask=1,
            min_max_stats=min_max_stats,
            prnd=tf.convert_to_tensor([0, 42]),
            )
        self.assertTrue(tf.reduce_any(action == 0))
        self.assertTrue(tf.reduce_any(action == 1))
        self.assertTrue(tf.reduce_any(action == 2))
        self.assertTrue(tf.reduce_any(action == 3))
        self.assertTrue(tf.reduce_any(action == 4))
        self.assertTrue(tf.reduce_any(action == 5))
        self.assertTrue(tf.reduce_any(action == 6))

    def test_backpropagate(self):
        board = tf.convert_to_tensor(
            [
                [
                    [
                        [1, 0, 1],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ]
        )
        environment = tictactoe.TictactoeEnvironment.init(board=board)
        tictactoe.TictactoeEnvironment.render(environment)
        rp_shape = tf.TensorSpec(tictactoe_devtools.g_rp_shape, tf.int32)
        action_count = 3 * 3
        tree = muzero_play.Tree.init(
            batch_size=1,
            action_count=action_count,
            num_simulations=16,
            hidden_state_spec=rp_shape,
        )
        muzero_play.Tree.render(tree, action_count=action_count)

        to_play = tf.math.floormod(environment.turn, 2)
        value = tf.convert_to_tensor([0.0])[..., None]
        reward = tf.convert_to_tensor([0.0])[..., None]
        nw_output = muzero_common.NetworkOutput(
            value=value,
            reward=reward,
            policy_logits=tf.zeros(action_count)[None, ...],
            hidden_state=tf.zeros(rp_shape.shape)[None, ...],
        )
        # expand root node
        tree = muzero_play.expand_node(
            tree,
            tree.rootNode(),
            to_play,
            actions=tf.ones([1, action_count]),
            network_output=nw_output,
            action_count=action_count,
        )

        to_play = tf.math.floormod(environment.turn + 1, 2)
        nodes = muzero_play.Tree.nodeChildren(tree.rootNode(), action_count)
        node = nodes[0, 1]
        tree = muzero_play.expand_node(
            tree,
            node[None],
            to_play,
            actions=tf.ones([1, action_count]),
            network_output=nw_output,
            action_count=action_count,
        )

        def path2str(search_path, search_path_length):
            path = ""
            for path_index in range(search_path_length[0]):
                path += "N" + str(search_path[0, path_index].numpy()) + ":"
            return path

        search_path = tf.convert_to_tensor([[1, node]])
        search_path_length = tf.convert_to_tensor([2])
        logging.info(
            path2str(search_path, search_path_length) + "=>" + str(value[0].numpy())
        )
        tree, _ = muzero_play.backpropagate(
            collections.namedtuple("Config", {"player_turn":2}),
            tree,
            search_path=search_path,
            search_path_length=search_path_length,
            value=value,
            to_play=to_play,
            discount=1.0,
            min_max_stats=muzero_common.MinMaxStats(-1.0, 1.0),
        )
        muzero_play.Tree.render(tree, action_count=action_count)


if __name__ == "__main__":
    # tf.config.run_functions_eagerly(True) # for debugging
    unittest.main()
