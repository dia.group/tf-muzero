import gym  # type: ignore
import numpy as np
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from tf_agents.specs import tensor_spec
from tf_agents.replay_buffers.episodic_replay_buffer import EpisodicReplayBuffer, StatefulEpisodicReplayBuffer
from tf_agents.replay_buffers import reverb_utils
import unittest

from .. import reverb_binding


class ReverbBindingTester(gym.Env):
    def __init__(self, value=None):
        self.action_space = gym.spaces.Discrete(2)
        self.observation_space = gym.spaces.Box(
            low=0, high=10, shape=(), dtype=np.int32
        )
        self._value = 10 if value is None else value

    def reset(self, value=None):
        self._value = 10 if value is None else value
        observation = self._value
        return observation

    def step(self, action):
        self._value -= 1
        done = True if self._value < 0 else False
        observation = self._value
        reward = 1 if self._value == 0 else 0
        return observation, reward, done, {}


class TestReverbBinding(unittest.TestCase):

    table_name = "test_table_name"

    @unittest.skipIf(True, "reverb utilities functions doesn't support batched environments")
    def testPushPopOne(self):
        import reverb  # type: ignore

        max_steps = 10
        replay_buffer_capacity = 100
        env = tf_agents.environments.suite_gym.wrap_env(
            ReverbBindingTester(), max_episode_steps=max_steps, auto_reset=False
        )
        # env = tf_agents.environments.suite_gym.load('CartPole-v1', max_episode_steps=max_steps)
        tf_env = tf_agents.environments.tf_py_environment.TFPyEnvironment(env)
        action_spec = tf_env.action_spec()
        time_step_spec = tf_env.time_step_spec()

        # reverb replay buffer
        policy = tf_agents.policies.random_tf_policy.RandomTFPolicy(
            time_step_spec=time_step_spec, action_spec=action_spec
        )
        replay_buffer_signature = tensor_spec.from_spec(
          policy.collect_data_spec)
        replay_buffer_signature = tensor_spec.add_outer_dim(
          replay_buffer_signature)
        table = reverb.Table(
          self.table_name,
          max_size=replay_buffer_capacity,
          sampler=reverb.selectors.Uniform(),
          remover=reverb.selectors.Fifo(),
          rate_limiter=reverb.rate_limiters.MinSize(1),
          signature=replay_buffer_signature)
        reverb_server = reverb.Server([table])

        reverb_replay_buffer = tf_agents.replay_buffers.ReverbReplayBuffer(
            data_spec=policy.collect_data_spec,
            table_name=self.table_name,
            sequence_length=max_steps,
            local_server=reverb_server,
        )
        rb_observer = reverb_utils.ReverbAddEpisodeObserver(
            reverb_replay_buffer.py_client,
            self.table_name,
            replay_buffer_capacity
        )
        # rb_observer = reverb_utils.ReverbAddTrajectoryObserver(
        #     reverb_replay_buffer.py_client,
        #     self.table_name,
        #     sequence_length=2,
        #     stride_length=1
        # )


        # (
        #     reverb_server,
        #     reverb_table_episode,
        #     reverb_tfclient,
        #     _,
        # ) = reverb_binding.create_replay_buffer(
        #     self.table_name, policy.collect_data_spec, max_steps=max_steps
        # )
        # traj_obs_reverb = reverb_binding.ReverbAddTFEpisodeObserver(
        #     reverb_tfclient,
        #     [self.table_name],
        #     policy.collect_data_spec,
        #     batch_size=tf_env.batch_size,
        #     max_steps=max_steps,
        # )

        # local replay buffer
        replay_buffer = (
            EpisodicReplayBuffer(
                completed_only=True, data_spec=policy.collect_data_spec, capacity=4
            )
        )
        traj_obs_classic = StatefulEpisodicReplayBuffer(
            replay_buffer, num_episodes=tf_env.batch_size
        ).add_batch

        # traj_obs_reverb = StatefulEpisodicReplayBuffer(
        #     reverb_replay_buffer, num_episodes=tf_env.batch_size
        # ).add_batch
        

        current_time_step = tf_env.reset()
        for _ in range(max_steps + 1):
            policy_step = policy.action(current_time_step)
            next_time_step = tf_env.step(policy_step.action)
            traj = tf_agents.trajectories.from_transition(
                current_time_step, policy_step, next_time_step
            )
            rb_observer(traj)
            traj_obs_classic(traj)
            current_time_step = next_time_step

        # dataset_reverb = reverb.trajectory_dataset.TrajectoryDataset.from_table_signature(
        #     server_address=f"localhost:{reverb_server.port}",
        #     table=self.table_name,
        #     max_in_flight_samples_per_worker=2,
        # )
        dataset_reverb = reverb_replay_buffer.as_dataset(
            sample_batch_size=2,
        )

        iterator = iter(dataset_reverb)
        buffer_info, raw_data = next(iterator)
        experience_reverb, position = reverb_binding.trajectory_from_data_dict(
            raw_data, policy.collect_data_spec
        )

        dataset_classic = replay_buffer.as_dataset()
        iterator = iter(dataset_classic)
        experience_classic, buffer_info = next(iterator)

        tf.nest.map_structure(
            lambda exp_r, exp_c: tf.debugging.assert_equal(exp_r, exp_c),
            experience_reverb,
            experience_classic,
        )

        reverb_server.stop()

    @unittest.skipIf(True, "reverb utilities functions doesn't support batched environments")
    def testPushPopMany(self):
        import reverb  # type: ignore

        max_steps = 10
        batch_size = 4
        env = [
            tf_agents.environments.suite_gym.wrap_env(
                ReverbBindingTester(), max_episode_steps=max_steps, auto_reset=False
            )
            for _ in range(batch_size)
        ]
        # env = [tf_agents.environments.suite_gym.load('CartPole-v1', max_episode_steps=max_steps) for _ in range(batch_size)]
        env = tf_agents.environments.batched_py_environment.BatchedPyEnvironment(env)
        tf_env = tf_agents.environments.tf_py_environment.TFPyEnvironment(env)
        action_spec = tf_env.action_spec()
        time_step_spec = tf_env.time_step_spec()

        # reverb replay buffer
        policy = tf_agents.policies.random_tf_policy.RandomTFPolicy(
            time_step_spec=time_step_spec, action_spec=action_spec
        )
        (
            reverb_server,
            reverb_table_episode,
            reverb_tfclient,
            _,
        ) = reverb_binding.create_replay_buffer(
            self.table_name, policy.collect_data_spec, max_steps=max_steps
        )
        traj_obs_reverb = reverb_binding.ReverbAddTFEpisodeObserver(
            reverb_tfclient,
            [self.table_name],
            policy.collect_data_spec,
            batch_size=tf_env.batch_size,
            max_steps=max_steps,
        )

        # local replay buffer
        replay_buffer = (
            tf_agents.replay_buffers.episodic_replay_buffer.EpisodicReplayBuffer(
                completed_only=True, data_spec=policy.collect_data_spec, capacity=4
            )
        )
        traj_obs_classic = tf_agents.replay_buffers.episodic_replay_buffer.StatefulEpisodicReplayBuffer(
            replay_buffer, num_episodes=tf_env.batch_size
        ).add_batch

        init_policy = tf_agents.policies.random_tf_policy.RandomTFPolicy(
            time_step_spec, action_spec
        )
        collect_policy_init_state = init_policy.get_initial_state(tf_env.batch_size)
        initial_collect_driver = (
            tf_agents.drivers.dynamic_episode_driver.DynamicEpisodeDriver(
                tf_env,
                init_policy,
                observers=[traj_obs_reverb] + [traj_obs_classic],
                num_episodes=8,
            )
        )
        env_init_state = tf_env.reset()
        initial_collect_driver.run(
            time_step=env_init_state, policy_state=collect_policy_init_state
        )

        dataset_reverb = reverb.trajectory_dataset.TrajectoryDataset.from_table_signature(
            server_address=f"localhost:{reverb_server.port}",
            table=self.table_name,
            max_in_flight_samples_per_worker=1,
        )

        iterator = iter(dataset_reverb)

        for _ in range(20):
            buffer_info, raw_data = next(iterator)
            experience_reverb, position = reverb_binding.trajectory_from_data_dict(
                raw_data, policy.collect_data_spec
            )
            self.assertEqual(experience_reverb.step_type[0], 0)
            self.assertEqual(experience_reverb.step_type[1], 1)
            self.assertEqual(experience_reverb.next_step_type[0], 1)

        reverb_server.stop()
