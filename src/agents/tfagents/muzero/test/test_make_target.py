import logging
import unittest
import tensorflow as tf  # type: ignore

from .. import train as muzero_train
from . import tictactoe_devtools


class TestMakeTarget(unittest.TestCase):
    # test td_steps below and behind config.num_unroll_steps, and with value opponent
    td_steps = [8, 2, 3]
    discount = 0.9

    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed')
        # logging.basicConfig(level=logging.INFO,
        #           format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        #           datefmt='%Y-%m-%d %H:%M:%S',
        #           )
        # tf.config.run_functions_eagerly(True) # to debug
        return super().setUp()

    def test_value_rewards_when_P0_wins(self):
        """
        Test returned parameters of make_target when P0 wins
        - first step: all values must be positives, but lower than 1.0 due to discount. rewards must be negative
        - one step before the last: values must be negative, as we are P1. next step contains reward at 1.0
        - last step: value must be positive high, reward must be 1.0, and both must be 0.0 after
        """
        gameXp, _ = tictactoe_devtools.build_tictactoe_gameXp(
            tictactoe_devtools.tictactoe_seq_P0_wins
        )
        logging.info(f"P0 wins with history: {gameXp.history}")
        logging.info(f"ref_value: {gameXp.root_values}")
        logging.info(f"ref_reward: {gameXp.rewards}")

        for td_steps in TestMakeTarget.td_steps:
            logging.info(f"*td_steps at {td_steps}")
            config = tictactoe_devtools.make_muzero_tictactoe_config(
                discount=TestMakeTarget.discount, td_steps=td_steps
            )
            state_index = 0  # P0 to play
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - 0.478) < 0.01
            assert abs(target_value[1, 0] - (-0.531)) < 0.01
            assert abs(target_value[2, 0] - 0.590) < 0.01

            assert abs(target_pc[0, 0] - 0.564) < 0.01
            assert abs(target_pc[1, 0] - (-0.627)) < 0.01
            assert abs(target_pc[2, 0] - 0.696) < 0.01

            assert tf.reduce_all(target_reward == 0.0)

            state_index = 5  # P1 to play, one state before last
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - (-0.810)) < 0.01
            assert abs(target_value[1, 0] - (0.900)) < 0.01
            assert abs(target_value[2, 0]) < 0.01

            assert abs(target_pc[0, 0] - (-0.570)) < 0.01
            assert abs(target_pc[1, 0] - (0.450)) < 0.01
            assert abs(target_pc[2, 0]) < 0.01

            assert abs(target_reward[0, 0]) < 0.01
            assert abs(target_reward[1, 0]) < 0.01
            assert abs(target_reward[2, 0] - 1.000) < 0.01

            state_index = 6  # P0 to play, last state
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - (0.900)) < 0.01
            assert abs(target_value[1, 0]) < 0.01
            assert abs(target_value[2, 0]) < 0.01

            assert abs(target_pc[0, 0] - (0.450)) < 0.01
            assert abs(target_pc[1, 0]) < 0.01
            assert abs(target_pc[2, 0]) < 0.01

            assert abs(target_reward[0, 0]) < 0.01
            assert abs(target_reward[1, 0] - (1.000)) < 0.01
            assert abs(target_reward[2, 0]) < 0.01

    def test_value_rewards_when_P1_wins(self):
        """
        Test returned parameters of make_target when P1 wins
        - first step: all values must be positives, but lower than 1.0 due to discount. rewards must be negative
        - one step before the last: values must be negative, as we are P1. one reward at 1.0
        - last step: value must be 1.0, next reward must be 1.0, and both must be 0.0 after
        """
        gameXp, _ = tictactoe_devtools.build_tictactoe_gameXp(
            tictactoe_devtools.tictactoe_seq_P1_wins
        )
        logging.info(f"P1 wins with history: {gameXp.history}")
        logging.info(f"ref_value: {gameXp.root_values}")
        logging.info(f"ref_reward: {gameXp.rewards}")

        for td_steps in TestMakeTarget.td_steps:
            logging.info(f"*td_steps at {td_steps}")
            config = tictactoe_devtools.make_muzero_tictactoe_config(
                discount=TestMakeTarget.discount, td_steps=td_steps
            )
            state_index = 1  # P1 to play
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - 0.478) < 0.01
            assert abs(target_value[1, 0] - (-0.531)) < 0.01
            assert abs(target_value[2, 0] - 0.590) < 0.01

            assert abs(target_pc[0, 0] - 0.564) < 0.01
            assert abs(target_pc[1, 0] - (-0.627)) < 0.01
            assert abs(target_pc[2, 0] - 0.696) < 0.01

            assert tf.reduce_all(target_reward == 0.0)

            state_index = 6  # P0 to play, one state before last
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - (-0.810)) < 0.01
            assert abs(target_value[1, 0] - 0.900) < 0.01
            assert abs(target_value[2, 0]) < 0.01

            assert abs(target_pc[0, 0] - (-0.570)) < 0.01
            assert abs(target_pc[1, 0] - 0.450) < 0.01
            assert abs(target_pc[2, 0]) < 0.01

            assert abs(target_reward[0, 0]) < 0.01
            assert abs(target_reward[1, 0]) < 0.01
            assert abs(target_reward[2, 0] - 1.000) < 0.01

            state_index = 7  # P1 to play, last state
            tf_state_index = tf.fill(tf.shape(gameXp.history_length), state_index)
            target_value, target_pc, target_reward, target_policy, target_qvalue = muzero_train.Game.make_target(
                gameXp, tf_state_index, config
            )
            logging.info(f"state_index {state_index}")
            logging.info(f"value: {tf.transpose(target_value)}")
            logging.info(f"reward: {tf.transpose(target_reward)}")

            assert abs(target_value[0, 0] - (0.900)) < 0.01
            assert abs(target_value[1, 0]) < 0.01
            assert abs(target_value[2, 0]) < 0.01

            assert abs(target_pc[0, 0] - (0.450)) < 0.01
            assert abs(target_pc[1, 0]) < 0.01
            assert abs(target_pc[2, 0]) < 0.01

            assert abs(target_reward[0, 0]) < 0.01
            assert abs(target_reward[1, 0] - (1.000)) < 0.01
            assert abs(target_reward[2, 0]) < 0.01


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    # tf.config.run_functions_eagerly(True) # to debug

    unittest.main()
