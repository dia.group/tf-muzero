import logging
import numpy as np
import os
import tensorflow as tf  # type: ignore
import unittest

from ... import muzero
from .. import __devtools__ as muzero_devtools
from . import tictactoe_tf as tictactoe
from . import tictactoe_devtools


SLOW_TESTS = int(os.getenv("SLOW_TESTS", "0"))


class TestMCTS(unittest.TestCase):
    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed', suppress=True)
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        # tf.config.run_functions_eagerly(True) # debug
        return super().setUp()

    @staticmethod
    @tf.function
    def _check_eager():
        # assert(tf.executing_eagerly()==False)  # test graph mode as this test is too slow to compute in eager mode
        pass

    def test_openning(self):
        """
        Test relative distribution of policy on hard-code tictactoe game steps
        """
        self._check_eager()
        logging.info("-- test_mcts_openning --")
        # 2GB max / tensor : num_simulations * batch_size * 6 * 7 * g_num_filters(64) * 4(float)
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=96
        )
        batch_size = 1024
        # local_config.batch_size = 7*1000 # boost for reliability

        environment = tictactoe.TictactoeEnvironment.init(batch_size)
        tictactoe.TictactoeEnvironment.render(environment)
        network = muzero_devtools.MOCKNetworkSimuValueStrategy(local_config)

        # cross
        rp_shape = tf.TensorSpec(tictactoe_devtools.g_rp_shape, tf.int32)
        tree = muzero.play.Tree.init(
            batch_size,
            local_config.action_space_size,
            local_config.num_simulations,
            rp_shape,
        )
        current_observation = tictactoe.TictactoeEnvironment.observation(environment)
        tree = muzero.play.expand_node(
            tree,
            tree.rootNode(),
            local_config.to_play(environment.turn),
            tf.ones([batch_size, local_config.action_space_size]),
            network.initial_inference(current_observation),
            local_config.action_space_size,
        )
        # tree = add_exploration_noise(config, tree) # needed ?
        tree, _ = muzero.play.run_mcts(
            config=local_config,
            tree=tree,
            history_depth=environment.turn,
            network=network,
            prnd=tf.convert_to_tensor([0, 42]),
            )
        root_children = muzero.play.Tree.nodeChildren(
            tree.rootNode(), local_config.action_space_size
        )
        visit_counts = tf.gather(
            tree.visit_count, root_children, axis=-1, batch_dims=-1
        )
        visit_counts = tf.reduce_sum(visit_counts, axis=0)
        logging.info("cross visit count: " + str(visit_counts))
        visit_acc = tf.cast(visit_counts, tf.float32)
        visit_acc /= tf.reduce_sum(visit_acc)
        logging.info("cross visit ratio: " + str(visit_acc))

        temperature = local_config.visit_softmax_temperature_fn(environment.turn, network.training_steps())
        action = muzero.play.select_action(
            tree=tree,
            action_count=local_config.action_space_size,
            temperature=temperature,
            prnd=tf.convert_to_tensor([0, 42]),
        )
        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"MCTS cross actions ratio :\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        tf.assert_greater(
            a4 / (batch_size / local_config.action_space_size), 1.0 + 0.01
        )
        tf.assert_greater(a4, a0)
        tf.assert_greater(a4, a2)
        tf.assert_greater(a4, a6)
        tf.assert_greater(a4, a8)

        corner_min = tf.math.minimum(tf.math.minimum(a0, a2), tf.math.minimum(a6, a8))
        tf.assert_greater(corner_min, a1)
        tf.assert_greater(corner_min, a3)
        tf.assert_greater(corner_min, a5)
        tf.assert_greater(corner_min, a7)

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )

        # tf.assert_less(a4, a4_mc)
        # tf.assert_greater(a0+a2+a6+a8, a0_mc+a2_mc+a6_mc+a8_mc)

        logging.info("cross plays 4")
        action = tf.fill([batch_size], 4, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )
        tictactoe.TictactoeEnvironment.render(environment)

        # circle
        tree = muzero.play.Tree.init(
            batch_size,
            local_config.action_space_size,
            local_config.num_simulations,
            rp_shape,
        )
        current_observation = tictactoe.TictactoeEnvironment.observation(environment)
        tree = muzero.play.expand_node(
            tree,
            tree.rootNode(),
            local_config.to_play(environment.turn),
            tf.ones([batch_size, local_config.action_space_size]),
            network.initial_inference(current_observation),
            local_config.action_space_size,
        )
        # tree = add_exploration_noise(config, tree) # needed ?
        tree, _ = muzero.play.run_mcts(
            config=local_config,
            tree=tree,
            history_depth=environment.turn,
            network=network,
            prnd=tf.convert_to_tensor([0, 42]),
            )
        visit_counts = tf.gather(
            tree.visit_count,
            muzero.play.Tree.nodeChildren(
                tree.rootNode(), local_config.action_space_size
            ),
            axis=-1,
            batch_dims=-1,
        )
        visit_counts = tf.reduce_sum(visit_counts, axis=0)
        logging.info("circle visit count: " + str(visit_counts))
        visit_acc = tf.cast(visit_counts, tf.float32)
        visit_acc /= tf.reduce_sum(visit_acc)
        logging.info("circle visit ratio: " + str(visit_acc))

        temperature = local_config.visit_softmax_temperature_fn(environment.turn, network.training_steps())
        action = muzero.play.select_action(
            tree=tree,
            action_count=local_config.action_space_size,
            temperature=temperature,
            prnd=tf.convert_to_tensor([0, 42]),
        )
        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"MCTS circle actions ratio:\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        tf.assert_equal(a4, 0.0)

        corner_min = tf.math.minimum(tf.math.minimum(a0, a2), tf.math.minimum(a6, a8))
        tf.assert_greater(corner_min, a1)
        tf.assert_greater(corner_min, a3)
        tf.assert_greater(corner_min, a5)
        tf.assert_greater(corner_min, a7)

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )
        tf.assert_equal(a4_mc, 0.0)
        # tf.assert_greater(a4, a4_mc)
        # tf.assert_greater(a0+a2+a6+a8, a0_mc+a2_mc+a6_mc+a8_mc)

        logging.info("circle plays 0")
        action = tf.fill([batch_size], 0, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )

        logging.info("cross plays 1")
        action = tf.fill([batch_size], 1, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )

        logging.info("circle plays 7")
        action = tf.fill([batch_size], 7, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )
        tictactoe.TictactoeEnvironment.render(environment)

        tree = muzero.play.Tree.init(
            batch_size,
            local_config.action_space_size,
            local_config.num_simulations,
            rp_shape,
        )
        current_observation = tictactoe.TictactoeEnvironment.observation(environment)
        tree = muzero.play.expand_node(
            tree,
            tree.rootNode(),
            local_config.to_play(environment.turn),
            tf.ones([batch_size, local_config.action_space_size]),
            network.initial_inference(current_observation),
            local_config.action_space_size,
        )
        tree, _ = muzero.play.run_mcts(
            config=local_config,
            tree=tree,
            history_depth=environment.turn,
            network=network,
            prnd=tf.convert_to_tensor([0, 42]),
            )

        temperature = local_config.visit_softmax_temperature_fn(environment.turn, network.training_steps())
        action = muzero.play.select_action(
            tree=tree,
            action_count=local_config.action_space_size,
            temperature=temperature,
            prnd=tf.convert_to_tensor([0, 42]),
        )
        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"cross actions ratio :\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        tf.assert_equal(a0, 0.0)
        tf.assert_equal(a1, 0.0)
        tf.assert_equal(a4, 0.0)
        tf.assert_equal(a7, 0.0)

        tf.assert_greater(a6, a2)
        tf.assert_greater(a6, a3)
        tf.assert_greater(a6, a5)
        tf.assert_greater(a6, a8)

        logging.info("cross plays 6")
        action = tf.fill([batch_size], 6, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )
        tictactoe.TictactoeEnvironment.render(environment)

        tree = muzero.play.Tree.init(
            batch_size,
            local_config.action_space_size,
            local_config.num_simulations,
            rp_shape,
        )
        current_observation = tictactoe.TictactoeEnvironment.observation(environment)
        tree = muzero.play.expand_node(
            tree,
            tree.rootNode(),
            local_config.to_play(environment.turn),
            tf.ones([batch_size, local_config.action_space_size]),
            network.initial_inference(current_observation),
            local_config.action_space_size,
        )
        tree, _ = muzero.play.run_mcts(
            config=local_config,
            tree=tree,
            history_depth=environment.turn,
            network=network,
            prnd=tf.convert_to_tensor([0, 42]),
            )

        temperature = local_config.visit_softmax_temperature_fn(environment.turn, network.training_steps())
        action = muzero.play.select_action(
            tree=tree,
            action_count=local_config.action_space_size,
            temperature=temperature,
            prnd=tf.convert_to_tensor([0, 42]),
        )
        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"circle actions ratio :\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        tf.assert_equal(a0, 0.0)
        tf.assert_equal(a1, 0.0)
        tf.assert_equal(a4, 0.0)
        tf.assert_equal(a6, 0.0)
        tf.assert_equal(a7, 0.0)

        tf.assert_greater(a2 / a_acc, 0.5)

        tf.assert_less(a3 / a_acc, 0.1)
        tf.assert_less(a5 / a_acc, 0.1)
        tf.assert_less(a8 / a_acc, 0.1)

    def test_nextstep(self):
        """
        test MCTS is more efficient than MC to select move based on step after this one
        """
        board = tf.convert_to_tensor(
            [
                [
                    [
                        [1, 0, 1],
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ]
        )
        environment = tictactoe.TictactoeEnvironment.init(board=board)
        tictactoe.TictactoeEnvironment.render(environment)

        batch_size = 1024
        environment = tictactoe_devtools.TictactoeEnvironment_tile(
            environment, batch_size
        )

        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=96
        )

        network = muzero_devtools.MOCKNetworkSimuValueStrategy(local_config)
        action = muzero_devtools.select_action_nn_mcts(
            environment, local_config, network
        )

        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"MCTS circle actions ratio:\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )

        tf.assert_greater(a1, a1_mc)

        logging.info("circle plays 1")
        action = tf.fill([batch_size], 1, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )
        tictactoe.TictactoeEnvironment.render(environment)

        action = muzero_devtools.select_action_nn_mcts(
            environment, local_config, network
        )

        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        logging.info(
            f"MCTS circle actions ratio:\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )

        tf.assert_greater(a7, a7_mc)

    def test_nextnextstep(self):
        """
        test MCTS is more efficient than MC to select move based on step after this one
        """
        board = tf.convert_to_tensor(
            [
                [
                    [
                        [1, 0, 0],
                        [0, 0, 1],
                        [0, 0, 0],
                    ],
                    [
                        [0, 0, 0],
                        [0, 1, 0],
                        [0, 0, 0],
                    ],
                ]
            ]
        )
        environment = tictactoe.TictactoeEnvironment.init(board=board)
        tictactoe.TictactoeEnvironment.render(environment)

        batch_size = 1024
        environment = tictactoe_devtools.TictactoeEnvironment_tile(
            environment, batch_size
        )

        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=256
        )

        network = muzero_devtools.MOCKNetworkSimuValueStrategy(local_config)
        action = muzero_devtools.select_action_nn_mcts(
            environment, local_config, network
        )

        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        a_acc = batch_size
        logging.info(
            f"MCTS circle actions ratio:\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )

        tf.assert_greater(a1, a1_mc)

        logging.info("circle plays 1")
        action = tf.fill([batch_size], 1, tf.int32)
        environment, _ = tictactoe.TictactoeEnvironment.step(
            environment, action, tf.constant(True)
        )
        tictactoe.TictactoeEnvironment.render(environment)

        action = muzero_devtools.select_action_nn_mcts(
            environment, local_config, network
        )

        a0 = tf.reduce_sum(tf.cast(action == 0, tf.float32))
        a1 = tf.reduce_sum(tf.cast(action == 1, tf.float32))
        a2 = tf.reduce_sum(tf.cast(action == 2, tf.float32))
        a3 = tf.reduce_sum(tf.cast(action == 3, tf.float32))
        a4 = tf.reduce_sum(tf.cast(action == 4, tf.float32))
        a5 = tf.reduce_sum(tf.cast(action == 5, tf.float32))
        a6 = tf.reduce_sum(tf.cast(action == 6, tf.float32))
        a7 = tf.reduce_sum(tf.cast(action == 7, tf.float32))
        a8 = tf.reduce_sum(tf.cast(action == 8, tf.float32))

        logging.info(
            f"MCTS circle actions ratio:\n{a0/a_acc:.04}, {a1/a_acc:.04}, {a2/a_acc:.04},\n{a3/a_acc:.04}, {a4/a_acc:.04}, {a5/a_acc:.04},\n{a6/a_acc:.04}, {a7/a_acc:.04}, {a8/a_acc:.04}"
        )

        # compare with MC
        action_mc = tictactoe_devtools.select_action_mc_sequential(
            environment, local_config.num_simulations
        )
        a0_mc = tf.reduce_sum(tf.cast(action_mc == 0, tf.float32))
        a1_mc = tf.reduce_sum(tf.cast(action_mc == 1, tf.float32))
        a2_mc = tf.reduce_sum(tf.cast(action_mc == 2, tf.float32))
        a3_mc = tf.reduce_sum(tf.cast(action_mc == 3, tf.float32))
        a4_mc = tf.reduce_sum(tf.cast(action_mc == 4, tf.float32))
        a5_mc = tf.reduce_sum(tf.cast(action_mc == 5, tf.float32))
        a6_mc = tf.reduce_sum(tf.cast(action_mc == 6, tf.float32))
        a7_mc = tf.reduce_sum(tf.cast(action_mc == 7, tf.float32))
        a8_mc = tf.reduce_sum(tf.cast(action_mc == 8, tf.float32))
        logging.info(
            f"MC cross actions ratio :\n{a0_mc/a_acc:.04}, {a1_mc/a_acc:.04}, {a2_mc/a_acc:.04},\n{a3_mc/a_acc:.04}, {a4_mc/a_acc:.04}, {a5_mc/a_acc:.04},\n{a6_mc/a_acc:.04}, {a7_mc/a_acc:.04}, {a8_mc/a_acc:.04}"
        )

        tf.assert_greater(a7, a7_mc)

    @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_NoStrategy(self):
        """
        Test MCTS with leaf simulator: constant value
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=64
        )
        batch_size = 1024
        network = muzero_devtools.MOCKNetwork(local_config)

        logging.info("\nMCTS-NoStrategy_VS_random")

        def p1(environment):
            return muzero_devtools.select_action_nn_mcts(
                environment, local_config, network
            )

        def p2(environment):
            return tictactoe_devtools.select_action_random(environment)

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n{play_stats}")
        play_sum = play_stats[0, 0] + play_stats[0, 2]
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        self.assertGreater(cross_win_ratio, 0.55)
        self.assertGreater(circle_win_ratio, 0.51)

    @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_RandomStrategy(self):
        """
        Test MCTS with C4 leaf simulator: random
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=64
        )
        batch_size = 1024
        network = muzero_devtools.MOCKNetworkRandomStrategy(local_config)

        logging.info("\nMCTS-RandomStrategy_VS_random")

        def p1(environment):
            return muzero_devtools.select_action_nn_mcts(
                environment, local_config, network
            )

        def p2(environment):
            return tictactoe_devtools.select_action_random(environment)

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n{play_stats}")
        play_sum = play_stats[0, 0] + play_stats[0, 2]
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        self.assertGreater(cross_win_ratio, 0.55)
        self.assertGreater(cross_win_ratio + circle_win_ratio, 0.55 * 2)

    def _test_mcts_vs_rnd(self, local_config, batch_size, network, archi_name):
        logging.info(f"\n{archi_name}_VS_random")

        def p1(environment):
            return muzero_devtools.select_action_nn_mcts(
                environment, local_config, network
            )

        def p2(environment):
            return tictactoe_devtools.select_action_random(environment)

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n {play_stats}")
        # + play_stats[0,1] : remove draw games
        play_sum = play_stats[0, 0] + play_stats[0, 2]
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        self.assertGreater(cross_win_ratio, 0.55)
        self.assertGreater(circle_win_ratio + circle_win_ratio, 0.55 * 2)

    def _test_mcts_vs_mc(self, local_config, batch_size, network, archi_name):
        logging.info(f"\n{archi_name}_VS_MC{local_config.num_simulations}")

        def p1(environment):
            return muzero_devtools.select_action_nn_mcts(
                environment, local_config, network
            )

        def p2(environment):
            return tictactoe_devtools.select_action_mc_sequential(
                environment, local_config.num_simulations
            )

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n {play_stats}")
        # + play_stats[0,1] : remove draw games
        play_sum = play_stats[0, 0] + play_stats[0, 2]
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        self.assertGreater(cross_win_ratio, 0.55)
        self.assertGreater(cross_win_ratio + circle_win_ratio, 0.55 * 2)

    # @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_SimuValueStrategy(self):
        """
        Test MCTS with C4 leaf simulator: C4 game simulator
        -- pseudo classical MCTS / policy isn't used in classical MCTS ucb computing --
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=64
        )  # 2GB max / tensor : num_simulations * batch_size * 6 * 7 * g_num_filters(64) * 4(float)
        batch_size = 1024
        network = muzero_devtools.MOCKNetworkSimuValueStrategy(local_config)

        archi_name = f"\nMCTS{local_config.num_simulations}-SimuValueStrategy"
        self._test_mcts_vs_rnd(local_config, batch_size, network, archi_name)
        self._test_mcts_vs_mc(local_config, batch_size, network, archi_name)

    @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_MultiSimuValueStrategy(self):
        """
        Test MCTS with leaf simulator: parallel game simulator + Policy ablation
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=45
        )  # 2GB max / tensor : num_simulations * batch_size * 6 * 7 * g_num_filters(64) * 4(float)
        batch_size = 1024
        num_tile = 16
        network = muzero_devtools.ProxyNetworkPolicyAblation(
            batch_size,
            local_config.action_space_size,
            muzero_devtools.ProxyNetworkCheckRange(
                muzero_devtools.MOCKNetworkMultiSimuStrategy(local_config, num_tile)
            ),
        )

        archi_name = (
            f"MCTS{local_config.num_simulations}*MultiSimuValueStrategy{num_tile}"
        )
        self._test_mcts_vs_rnd(local_config, batch_size, network, archi_name)
        self._test_mcts_vs_mc(local_config, batch_size, network, archi_name)

    @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_MultiSimuPolicyStrategy(self):
        """
        Test MCTS with leaf simulator: parallel game simulator + Value ablation
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=45
        )
        batch_size = 1024
        num_tile = 16
        network = muzero_devtools.ProxyNetworkValueAblation(
            batch_size,
            local_config.action_space_size,
            muzero_devtools.ProxyNetworkCheckRange(
                muzero_devtools.MOCKNetworkMultiSimuStrategy(local_config, num_tile)
            ),
        )

        archi_name = (
            f"MCTS{local_config.num_simulations}*MultiSimuPolicyStrategy{num_tile}"
        )
        self._test_mcts_vs_rnd(local_config, batch_size, network, archi_name)
        # self._test_mcts_vs_mc(local_config, batch_size, network, archi_name)  # not strong enough to beat MC

    @unittest.skipIf(not SLOW_TESTS, "slow")
    def test_MultiSimuPVStrategy(self):
        """
        Test MCTS with leaf simulator: parallel game simulator
        """
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(
            num_simulations=45
        )  # 2GB max / tensor : num_simulations * batch_size * 6 * 7 * g_num_filters(64) * 4(float)
        batch_size = 1024
        num_tile = 16
        network = muzero_devtools.ProxyNetworkCheckRange(
            muzero_devtools.MOCKNetworkMultiSimuStrategy(local_config, num_tile)
        )

        archi_name = f"MCTS{local_config.num_simulations}*MultiSimuStrategy{num_tile}"
        self._test_mcts_vs_rnd(local_config, batch_size, network, archi_name)
        self._test_mcts_vs_mc(local_config, batch_size, network, archi_name)

    # @unittest.skip("TODO: issues to fix")

    def test_mock_value_reward(self):
        local_config = tictactoe_devtools.make_muzero_tictactoe_config(discount=0.9)
        batch_size = 1

        num_tile = 1024
        network = muzero_devtools.MOCKNetworkMultiSimuStrategy(local_config, num_tile)

        for seq in [
            tictactoe_devtools.tictactoe_seq_P0_wins,
            tictactoe_devtools.tictactoe_seq_P1_wins,
        ]:
            logging.info(f"sequence: {np.squeeze(seq)}")
            gameXp, _ = tictactoe_devtools.build_tictactoe_gameXp(seq)

            # skip start of the game
            environment = tictactoe.TictactoeEnvironment.init(batch_size)
            end_of_game = gameXp.history_length[0] - 3
            for action_index in range(end_of_game):
                action = tf.fill(
                    [batch_size], gameXp.history[0, action_index], tf.int32
                )
                environment, _ = tictactoe.TictactoeEnvironment.step(
                    environment, action, tf.constant(True)
                )

            logging.info(f"mid game:\n{environment.board[0]}")
            logging.info(f"remain sequence:{np.squeeze(seq[end_of_game:])}")
            for action_index in range(end_of_game, gameXp.history_length[0]):
                (
                    target_value,
                    target_pc,
                    target_reward,
                    target_policy,
                    target_qvalue,
                ) = muzero.train.Game.make_target(
                    gameXp,  # make_target handle player relative value
                    state_index=tf.convert_to_tensor([action_index]),
                    config=local_config,
                )

                current_observation = tictactoe.TictactoeEnvironment.observation(
                    environment
                )

                nw_output = network.initial_inference(current_observation)
                logging.info(
                    f"NW MOCK cur value {nw_output.value[0]} VS ref {target_value[0, 0]}"
                )
                # self.assertLess(abs(nw_output.value[0] - target_value[0, 0]), 0.5)

                action = tf.fill(
                    [batch_size], gameXp.history[0, action_index], tf.int32
                )

                nw_output = network.recurrent_inference(nw_output.hidden_state, action)
                logging.info(
                    f"NW MOCK reward {nw_output.reward[0]} VS ref {target_reward[0, 0]}"
                )
                # self.assertLess(abs(nw_output.reward[0] - target_reward[0, 0]), 0.5)
                logging.info(
                    f"NW MOCK next value {nw_output.value[0]} VS ref {target_value[1, 0]}"
                )
                # self.assertLess(abs(nw_output.value[0] - target_value[1, 0]), 0.5)

                logging.info(f"playing action: {action}")
                environment, _ = tictactoe.TictactoeEnvironment.step(
                    environment, action, tf.constant(True)
                )
            logging.info(f"end game:\n{environment.board[0]}")


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    logging.info("Eager execution: {}".format(tf.executing_eagerly()))
    # tf.config.run_functions_eagerly(True) # debug
    # tf.config.optimizer.set_jit(True)

    unittest.main()
    # t = time.perf_counter()

    # test_mcts = TestMCTS()
    # test_mcts.test_openning()
    # test_mcts.test_NoStrategy()
    # test_mcts.test_RandomStrategy()
    # test_mcts.test_FixedStrategy()
    # test_mcts.test_SimuValueStrategy()
    # test_mcts.test_MultiSimuValueStrategy()
    # test_mcts.test_MultiSimuPolicyStrategy()
    # test_mcts.test_MultiSimuPVStrategy()
    # test_mcts.test_mock_value_reward()

    # elapsed_time = time.perf_counter() - t
    # logging.info(f"tests took {elapsed_time:04f} secondes")
