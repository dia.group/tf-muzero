"""
  Helpers for muzero's tictactoe unit tests
"""
import math
import numpy as np
import tensorflow as tf  # type: ignore

from . import tictactoe_tf as tictactoe
from .. import common as muzero_common


# 0 1 2
# 3 4 5
# 6 7 8

tictactoe_seq_P0_wins = np.array(
    [
        0,
        4,
        1,
        8,
        3,
        7,
        2,  # P0 wins playing on row 0 column 3 with 7 steps
    ]
)

tictactoe_seq_P1_wins = np.array(
    [
        4,
        0,
        5,
        1,
        7,
        3,
        8,
        2,  # P1 wins playing on column 3 with 8 steps
    ]
)

g_num_filters = 2  # aka num_features of network processings
g_rp_shape = (3, 3, 3)  # internal representation shape


def build_tictactoe_gameXp(history, discount=0.9):
    action_space = 3 * 3  # tictactoe action space
    env = tictactoe.TictactoeEnvironment.init()
    rewards = []
    values = []
    observations = [tictactoe.TictactoeEnvironment.observation(env)]
    for action in history:
        env, reward = tictactoe.TictactoeEnvironment.step(env, action=action)
        observations.append(tictactoe.TictactoeEnvironment.observation(env))
        rewards.append(reward[0])
        values.append(0.0)
        for value_index in range(1, len(values) + 1):
            value_relative = 1.0 if value_index % 2 == 1 else -1.0
            values[-value_index] += (
                math.pow(discount, value_index) * reward[0] * value_relative
            )

    gameXp = muzero_common.GameXp(
        history=tf.convert_to_tensor([history]),
        rewards=tf.convert_to_tensor([rewards]),
        history_length=tf.convert_to_tensor([len(history)]),
        child_visits=tf.expand_dims(tf.one_hot(history, action_space, dtype=tf.int32), 0),
        q_values=tf.expand_dims(tf.one_hot(history, action_space, dtype=tf.float32), 0),
        root_values=tf.convert_to_tensor([values]),
    )
    gameXp.assert_struct()
    return gameXp, tf.cast(tf.stack(observations, axis=1), tf.float32)


# declare function out of scope to avoid retracing
def _temperature_fn(num_moves, training_steps):
    return 0.0


def make_muzero_tictactoe_config(num_simulations=50, **kwargs):
    config = muzero_common.MuZeroConfig(
        player_turn=2,  # Tictactoe is a 2 player step per game state
        action_space_size=3 * 3,
        num_simulations=num_simulations,
        visit_softmax_temperature_fn=_temperature_fn,
        pb_c_init=1.0,
        known_bounds=muzero_common.KnownBounds(-1.0, 1.0),
        **kwargs,
    )
    return config


@tf.function
def TictactoeEnvironment_tile(env: tictactoe.TictactoeEnvironment, tile_size: int):
    board = tf.tile(env.board, [tile_size, 1, 1, 1])
    turn = tf.tile(env.turn, [tile_size])
    done = tf.tile(env.done, [tile_size])
    winner = tf.tile(env.winner, [tile_size])
    return tictactoe.TictactoeEnvironment(board, turn, done, winner)


@tf.function
def TictactoeEnvironment_untile(
    env: tictactoe.TictactoeEnvironment, tile_size: int
):
    batch_size = tf.shape(env.board)[0] // tile_size
    board = tf.reshape(env.board, [tile_size, batch_size, 2, 3, 3])
    turn = tf.reshape(env.turn, [tile_size, batch_size])
    done = tf.reshape(env.done, [tile_size, batch_size])
    winner = tf.reshape(env.winner, [tile_size, batch_size])
    return tictactoe.TictactoeEnvironment(board, turn, done, winner)


@tf.function
def select_action_random(environment: tictactoe.TictactoeEnvironment) -> tf.Tensor:
    batch_size = tf.shape(environment.turn)[0]
    actions = tictactoe.TictactoeEnvironment.legal_actions(environment)
    rnd = tf.random.uniform(tf.shape(actions), 0, 1)
    action = tf.math.argmax(
        tf.reshape(tf.cast(actions, tf.float32) * rnd, (batch_size, 3 * 3)),
        axis=1,
        output_type=tf.int32,
    )
    return action


@tf.function(jit_compile=True)
def select_action_mc_sequential(
    environment: tictactoe.TictactoeEnvironment, num_simulations: int
) -> tf.Tensor:
    """
    sequential computing of mc (a bit slower, but needs less memory than parallel computing)
    """
    batch_size = tf.shape(environment.board)[0]
    legal_actions = tf.reshape(
        tf.cast(tictactoe.TictactoeEnvironment.legal_actions(environment), tf.float32),
        (batch_size, 3 * 3),
    )  # list available actions
    actions_counter = tf.zeros_like(legal_actions, tf.int32)
    actions_accumulator = tf.zeros_like(legal_actions, tf.int32)
    with tf.name_scope("simulation"):
        # for _ in tf.range(num_simulations): # TODO: GPU allocation fails
        def simulation_cond(
            _actions_counter, _actions_accumulator, _simulation_counter
        ):
            return _simulation_counter > 0

        def simulation_loop(
            _actions_counter, _actions_accumulator, _simulation_counter
        ):
            r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
            branch = tf.math.argmax(legal_actions * r, axis=-1, output_type=tf.int32)
            mc_env, _ = tictactoe.TictactoeEnvironment.step(environment, branch)
            turn_to_end = 3 * 3 - tf.cast(tf.reduce_min(mc_env.turn), tf.int32)
            with tf.name_scope("forward"):  # execute the whole game to the end
                # for _ in tf.range(turn_to_end): # TODO: GPU allocation fails
                def forward_cond(mc_env, forward_counter):
                    return forward_counter > 0

                def forward_loop(mc_env, forward_counter):
                    actions = tf.reshape(
                        tf.cast(
                            tictactoe.TictactoeEnvironment.legal_actions(mc_env),
                            tf.float32,
                        ),
                        (batch_size, 3 * 3),
                    )
                    r = tf.random.uniform(tf.shape(actions), 0, 1)
                    action = tf.math.argmax(actions * r, axis=-1, output_type=tf.int32)
                    mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, action)
                    return mc_env, forward_counter - 1

                mc_env, _ = tf.while_loop(
                    forward_cond, forward_loop, (mc_env, turn_to_end)
                )

            node_indices2D = tf.stack([tf.range(tf.shape(branch)[0]), branch], -1)
            new_actions_counter = tf.tensor_scatter_nd_add(
                _actions_counter, node_indices2D, tf.ones_like(branch)
            )

            p0 = tf.cast(mc_env.winner == tictactoe.Player.cross.value, tf.int32)
            p1 = tf.cast(mc_env.winner == tictactoe.Player.circle.value, tf.int32)
            w = tf.where(
                tictactoe.TictactoeEnvironment.to_play(environment.turn)
                == tictactoe.Player.cross.value,
                p0,
                p1,
            )
            new_actions_accumulator = tf.tensor_scatter_nd_add(
                _actions_accumulator, node_indices2D, w
            )
            return new_actions_counter, new_actions_accumulator, _simulation_counter - 1

        actions_counter, actions_accumulator, _ = tf.while_loop(
            simulation_cond,
            simulation_loop,
            (actions_counter, actions_accumulator, num_simulations),
        )

    win_ratio = tf.where(
        actions_counter > 0,
        tf.cast(actions_accumulator, tf.float32) / tf.cast(actions_counter, tf.float32),
        0.0,
    )
    r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
    best_action = tf.math.argmax(
        tf.cast(
            tf.expand_dims(tf.reduce_max(win_ratio, -1), -1) == win_ratio, tf.float32
        )
        * r,
        axis=-1,
        output_type=tf.int32,
    )
    return best_action


@tf.function
def compute_action_mc_parallel(environment, tile):
    """
    parallel computing of MC (faster than sequential computing, but needs more memory)
    @return action simulation counter and action simulation where current player wins
    """
    assert tile > 0

    batch_size = tf.shape(environment.turn)[0]
    action_space = 9
    max_moves = 3 * 3

    mc_env = TictactoeEnvironment_tile(environment, tile)
    legal_actions = tf.reshape(
        tictactoe.TictactoeEnvironment.legal_actions(mc_env),
        [batch_size * tile, action_space],
    )  # list available actions
    r = tf.random.uniform(tf.shape(legal_actions), 0, 1)
    branch = tf.math.argmax(
        tf.cast(legal_actions, tf.float32) * r, axis=-1, output_type=tf.int32
    )
    mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, branch)
    turn_to_end = max_moves - tf.cast(tf.reduce_min(mc_env.turn), tf.int32)
    for _ in tf.range(turn_to_end):  # execute the whole game
        actions = tictactoe.TictactoeEnvironment.legal_actions(mc_env)
        r = tf.random.uniform(tf.shape(actions), 0, 1)
        action = tf.math.argmax(
            tf.reshape(
                tf.cast(actions, tf.float32) * r, (batch_size * tile, action_space)
            ),
            axis=-1,
            output_type=tf.int32,
        )
        mc_env, _ = tictactoe.TictactoeEnvironment.step(mc_env, action)

    mc_final = TictactoeEnvironment_untile(mc_env, tile)
    branch_untile = tf.transpose(tf.reshape(branch, [tile, batch_size]))
    winner = tf.transpose(mc_final.winner)
    winner_relative = tf.where(
        tf.expand_dims(tictactoe.TictactoeEnvironment.to_play(environment.turn), -1)
        == winner,
        1,
        0,
    )
    actions_counter = tf.zeros([batch_size, action_space], tf.int32)
    actions_accumulator = tf.zeros([batch_size, action_space], tf.int32)
    for action_index in tf.range(action_space):
        node_indices2D = tf.stack(
            [tf.range(batch_size), tf.fill([batch_size], action_index)], -1
        )
        action_count = tf.reduce_sum(
            tf.cast(branch_untile == action_index, tf.int32), axis=1
        )
        actions_counter = tf.tensor_scatter_nd_add(
            actions_counter, node_indices2D, action_count
        )
        w = tf.reduce_sum(
            tf.cast(branch_untile == action_index, tf.int32) * winner_relative, axis=1
        )
        actions_accumulator = tf.tensor_scatter_nd_add(
            actions_accumulator, node_indices2D, w
        )
    return actions_counter, actions_accumulator, winner


@tf.function
def select_action_mc_parallel(environment, num_simulations):
    """
    parallel computing of monte-carlo
    num_simulations is for all actions, tictactoe.action_space_size is 9, so each active has num_simulations/9
    """

    actions_counter, actions_accumulator, _ = compute_action_mc_parallel(
        environment, num_simulations
    )
    win_ratio = tf.where(
        actions_counter > 0,
        tf.cast(actions_accumulator, tf.float32) / tf.cast(actions_counter, tf.float32),
        0.0,
    )
    r = tf.random.uniform(tf.shape(win_ratio), 0, 1)
    best_action = tf.math.argmax(
        tf.cast(
            tf.expand_dims(tf.reduce_max(win_ratio, -1), -1) == win_ratio, tf.float32
        )
        * r,
        axis=-1,
        output_type=tf.int32,
    )
    return best_action
