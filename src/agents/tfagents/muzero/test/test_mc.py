import logging
import unittest

from .. import __devtools__ as muzero_devtools
from . import tictactoe_devtools


class TestMonteCarlo(unittest.TestCase):
    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed', suppress=True)
        # logging.basicConfig(level=logging.INFO,
        #           format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        #           datefmt='%Y-%m-%d %H:%M:%S',
        #           )
        # tf.config.run_functions_eagerly(True) # to debug
        return super().setUp()

    def test_mc_sequential(self):
        """
        Test Sequential MonteCarlo impl versus random
        Sequential is slower but needs less memory
        """
        batch_size = 1024
        num_simulations = 10

        logging.info(f"\nMC{num_simulations}-seq_VS_random")

        def p1(environment):
            return tictactoe_devtools.select_action_mc_sequential(
                environment, num_simulations
            )

        def p2(environment):
            return tictactoe_devtools.select_action_random(environment)

        play_stats = muzero_devtools.battle(batch_size=batch_size, P1=p1, P2=p2).numpy()
        logging.info(f"results = \n{play_stats}")
        play_sum = play_stats[0, 0] + play_stats[0, 2]  # remove draw games
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        assert cross_win_ratio > 0.6
        assert circle_win_ratio > 0.6

    def test_mc_parallel(self):
        """
        Test Parallel MonteCarlo impl versus random
        Parallel is faster but needs more memory
        """
        batch_size = 1024
        num_simulations = 10

        logging.info(f"\nMC{num_simulations}-parallel_VS_random")

        def p1(environment):
            return tictactoe_devtools.select_action_mc_parallel(
                environment, num_simulations
            )

        def p2(environment):
            return tictactoe_devtools.select_action_random(environment)

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n{play_stats}")
        play_sum = play_stats[0, 0] + play_stats[0, 2]  # remove draw games
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        assert cross_win_ratio > 0.6
        assert circle_win_ratio > 0.6

    def test_mc_seq_vs_par(self):
        """
        Test MonteCarlo Parallel impl versus Sequential Impl
        Results must be similar
        """
        batch_size = 1024
        num_simulations = 10

        logging.info(f"\nMC{num_simulations}-seq_VS_MC{num_simulations}-parallel")

        def p1(environment):
            return tictactoe_devtools.select_action_mc_sequential(
                environment, num_simulations
            )

        def p2(environment):
            return tictactoe_devtools.select_action_mc_parallel(
                environment, num_simulations
            )

        play_stats = muzero_devtools.battle(batch_size, p1, p2).numpy()
        logging.info(f"results = \n{play_stats}")
        play_sum = play_stats[0, 0] + play_stats[0, 2]
        cross_win_ratio = play_stats[0, 0] / play_sum if play_sum > 0 else 0.0
        play_sum = play_stats[1, 0] + play_stats[1, 2]
        circle_win_ratio = play_stats[1, 0] / play_sum if play_sum > 0 else 0.0
        logging.info(f"p1:cross_win_ratio = {cross_win_ratio}")
        logging.info(f"p1:circle_win_ratio = {circle_win_ratio}")
        assert cross_win_ratio < 0.7
        assert cross_win_ratio > 0.2
        assert circle_win_ratio < 0.7
        assert circle_win_ratio > 0.2


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    # tf.config.run_functions_eagerly(True) # debug
    # tf.config.optimizer.set_jit(True)

    unittest.main()
