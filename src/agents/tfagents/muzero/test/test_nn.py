import logging
import os
import tensorflow as tf  # type: ignore
import unittest

from .. import __devtools__ as muzero_devtools
from . import tictactoe_devtools


SLOW_TESTS = int(os.getenv("SLOW_TESTS", "0"))


class TestBackprop(unittest.TestCase):
    def setUp(self) -> None:
        # np.set_printoptions(precision=4, floatmode='fixed', suppress=True)
        # logging.basicConfig(level=logging.INFO,
        #           format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        #           datefmt='%Y-%m-%d %H:%M:%S',
        #           )
        # tf.config.run_functions_eagerly(True) # to debug
        return super().setUp()

    @staticmethod
    def _create_DenseBackpropNetwork(config):
        action_space = 3 * 3  # tictactoe action space
        rp_features = 256
        input = tf.keras.Input(shape=[3, 3, 3])
        input_a = tf.keras.Input(shape=[1])
        rp = tf.keras.Input(shape=[rp_features])

        rep_0 = tf.keras.layers.Flatten()(input)
        rep_2 = tf.keras.layers.Dense(rp_features, activation=tf.nn.tanh, name="rep_2")(
            rep_0
        )
        rep_out = tf.keras.layers.Lambda(lambda x: (x, 0))(rep_2)
        representation = tf.keras.Model(inputs=input, outputs=rep_out)

        pred_v = tf.keras.layers.Dense(1, activation=tf.nn.leaky_relu, name="pred_v")(
            rp
        )
        pred_p = tf.keras.layers.Dense(
            action_space, activation=tf.nn.leaky_relu, name="pred_p"
        )(rp)
        pred_out = tf.keras.layers.Lambda(lambda x: (x, 0))((pred_p, pred_v))
        prediction = tf.keras.Model(inputs=rp, outputs=pred_out)

        input_a_encode = tf.keras.layers.experimental.preprocessing.CategoryEncoding(
            num_tokens=action_space
        )(input_a)
        dyn_rp0 = tf.keras.layers.Dense(
            rp_features, activation=tf.nn.leaky_relu, name="dyn_rp0"
        )(tf.concat([rp, input_a_encode], axis=-1))
        dyn_rp = tf.keras.layers.Dense(
            rp_features, activation=tf.nn.tanh, name="dyn_rp"
        )(dyn_rp0)
        dyn_r = tf.keras.layers.Dense(1, activation=tf.nn.leaky_relu, name="dyn_r")(
            dyn_rp0
        )
        dyn_out = tf.keras.layers.Lambda(lambda x: (x, 0))((dyn_rp, dyn_r))
        dynamic = tf.keras.Model(inputs=[rp, input_a], outputs=dyn_out)

        network = muzero_devtools.TUBackpropNetwork(
            representation, prediction, dynamic, config
        )
        return network

    @staticmethod
    def _create_ConvBackpropNetwork(config):
        import tf_agents  # type: ignore
        from .. import networks as muzero_networks

        num_res_block = 1
        num_conv_features = 64
        observation_spec = tf.TensorSpec(shape=(3, 3, 3))
        action_spec = tf_agents.specs.BoundedTensorSpec(
            shape=[1], dtype=tf.int32, minimum=0, maximum=8
        )
        latent_state_specs = tf.TensorSpec(shape=(3, 3, num_conv_features))

        nw_rep = muzero_networks.NWConv2DRepresentation(
            observation_spec, num_res_block, num_conv_features, num_conv_features
        )
        nw_pred = muzero_networks.NWConv2DPrediction(
            latent_state_specs, action_spec, num_res_block, num_conv_features
        )
        nw_dyn = muzero_networks.NWConv2DDynamic_Tictactoe(
            latent_state_specs, action_spec, num_res_block, num_conv_features
        )

        network = muzero_devtools.TUBackpropNetwork(nw_rep, nw_pred, nw_dyn, config)

        return network

    @unittest.skip("still some issue")
    def test_variable_update(self):
        tf.random.set_seed(123456)
        config = tictactoe_devtools.make_muzero_tictactoe_config(
            td_steps=1, num_unroll_steps=1, weight_decay=0.0
        )
        gameXp, observations = tictactoe_devtools.build_tictactoe_gameXp(
            tictactoe_devtools.tictactoe_seq_P0_wins
        )

        # tictactoe_network = self._create_DenseBackpropNetwork(config)
        tictactoe_network = self._create_ConvBackpropNetwork(config)
        tictactoe_network.compile()

        logging.info("### every network vars must be updated through backprop")
        v_before = [
            tf.convert_to_tensor(v) for v in tictactoe_network.trainable_variables
        ]
        tictactoe_network.fit((gameXp, observations))
        for (b, v) in zip(v_before, tictactoe_network.trainable_variables):
            a = tf.convert_to_tensor(v)
            if not tf.reduce_any(b != a):
                logging.info(f"Test update: {v.name} : {tf.reduce_any(b!=a)}")
            self.assertTrue(tf.reduce_any(b != a), msg=v.name)

        logging.info("### network inference musn't update vars")
        v_before = [
            tf.convert_to_tensor(v) for v in tictactoe_network.trainable_variables
        ]
        _ = tictactoe_network.predict((observations[0, 1:], gameXp.history[0]))
        for (b, v) in zip(v_before, tictactoe_network.trainable_variables):
            a = tf.convert_to_tensor(v)
            self.assertTrue(tf.reduce_all(b == a), msg=v.name)

        logging.info(
            "### if we have no unroll step, then dynamic network musn't update"
        )
        config_dict = config._asdict()
        config_dict["num_unroll_steps"] = 0
        # tictactoe_network = self._create_DenseBackpropNetwork(config)
        tictactoe_network = self._create_ConvBackpropNetwork(config)
        tictactoe_network.compile()
        v_before = [
            tf.convert_to_tensor(v)
            for v in tictactoe_network.nw_dynamic.trainable_variables
        ]
        # network.fit((gameXp, observations))  # TODO: fix this TU
        for (b, v) in zip(v_before, tictactoe_network.nw_dynamic.trainable_variables):
            a = tf.convert_to_tensor(v)
            if not tf.reduce_all(b == a):
                logging.info(f"Test equals: {v.name} : {tf.reduce_all(b==a)}")
            self.assertTrue(tf.reduce_all(b == a), msg=v.name)

    # @unittest.skipIf(not SLOW_TESTS, "slow")

    def test_train_on_labels(self):
        tf.random.set_seed(123456)
        batch_size = 256
        config = tictactoe_devtools.make_muzero_tictactoe_config(
            td_steps=8, num_unroll_steps=5, discount=0.9
        )
        gameXp, observations = tictactoe_devtools.build_tictactoe_gameXp(
            tictactoe_devtools.tictactoe_seq_P0_wins, discount=config.discount
        )

        # tictactoe_network = self._create_DenseBackpropNetwork(config)
        tictactoe_network = self._create_ConvBackpropNetwork(config)
        optimizer = tf.keras.optimizers.Adam()
        tictactoe_network.compile(optimizer=optimizer, loss="mse")

        # gameXp = buildMCGameXp() # build a reference game XP
        gameXp = muzero_devtools.GameXp_tile(gameXp, batch_size)
        observations = tf.tile(observations, [batch_size, 1, 1, 1, 1])

        ref_policy = gameXp.child_visits[0]
        ref_values = tf.expand_dims(gameXp.root_values[0], -1)
        ref_next_policy = tf.concat(
            [gameXp.child_visits[0, 1:], [gameXp.child_visits[0, -1]]], axis=0
        )
        ref_next_value = tf.expand_dims(
            tf.pad(gameXp.root_values[0, 1:], [[0, 1]]), -1
        )  # or call make_target
        ref_rewards = tf.expand_dims(gameXp.rewards[0], -1)
        references = (
            # nn predictions for this step
            ref_policy,
            ref_values,
            # immediate reward is zero, so useless to test it
            # nn predictions for next step
            ref_next_policy,
            ref_next_value,
            ref_rewards,  # rewards indices are shifted
        )

        p_begin, v_begin, p2_begin, v2_begin, r2_begin = tictactoe_network.predict(
            (observations[0, :-1, ...], gameXp.history[0])
        )  # remove latest observation
        eval_start = tictactoe_network.evaluate(
            (observations[0, :-1, ...], gameXp.history[0]), references
        )

        tictactoe_network.fit((gameXp, observations), epochs=100)

        p_end, v_end, p2_end, v2_end, r2_end = tictactoe_network.predict(
            (observations[0, :-1, ...], gameXp.history[0])
        )
        eval_end = tictactoe_network.evaluate(
            (observations[0, :-1, ...], gameXp.history[0]), references
        )

        logging.info(f"eval_start: {eval_start}")
        logging.info(f"eval_end: {eval_end}")
        # self.assertLess(eval_end[0], 0.3*eval_start[0])

        ### We could only compare one model to another, so we must be able to do a better job than a constant / mean value

        # current policy prediction
        policy_error_begin = tf.reduce_mean(tf.square(tf.cast(ref_policy, tf.float32) - p_begin))
        policy_error_end = tf.reduce_mean(tf.square(tf.cast(ref_policy, tf.float32) - p_end))
        logging.info(f"!!! display probas as [0;9] to fit in screen !!!")
        logging.info(f"ref_policy : \n{ref_policy*9}")
        logging.info(f"policy_end \n{p_end*9}")
        logging.info(f"policy_begin error : {policy_error_begin:0.4f}")
        logging.info(f"policy_end error : {policy_error_end:0.4f}")
        const_model_error = tf.reduce_mean(
            tf.reduce_mean(tf.square(tf.cast(ref_policy, tf.float32) - tf.reduce_mean(tf.cast(ref_policy, tf.float32))), axis=1)
        )
        logging.info(f"const Model error {const_model_error:0.4f}")

        self.assertLess(policy_error_end, policy_error_begin * 0.2)
        self.assertLess(policy_error_end, const_model_error * 0.2)

        # next policy prediction
        policy_error_begin = tf.reduce_mean(tf.square(tf.cast(ref_next_policy, tf.float32) - p2_begin))
        policy_error_end = tf.reduce_mean(tf.square(tf.cast(ref_next_policy, tf.float32) - p2_end))
        logging.info(f"ref_next_policy : \n{ref_next_policy*9}")
        logging.info(f"next_policy_end \n{tf.cast(p2_end*9, tf.int32)}")
        logging.info(f"next_policy_begin error : {policy_error_begin:0.4f}")
        logging.info(f"next_policy_end error : {policy_error_end:0.4f}")
        const_model_error = tf.reduce_mean(
            tf.reduce_mean(
                tf.square(tf.cast(ref_next_policy, tf.float32) - tf.reduce_mean(tf.cast(ref_next_policy, tf.float32))), axis=1
            )
        )
        logging.info(f"const Model error {const_model_error:0.4f}")

        self.assertLess(policy_error_end, policy_error_begin * 0.2)
        self.assertLess(policy_error_end, const_model_error * 0.2)

        # current value prediction
        values_error_begin = tf.reduce_mean(tf.square(ref_values - v_begin))
        values_error_end = tf.reduce_mean(tf.square(ref_values - v_end))
        logging.info(f"ref_values \n{tf.squeeze(ref_values, -1)}")
        logging.info(f"values_end \n{tf.squeeze(v_end, -1)}")
        logging.info(f"values_begin error : {values_error_begin:0.4f}")
        logging.info(f"values_end error : {values_error_end:0.4f}")
        const_model_error = tf.reduce_mean(
            tf.reduce_mean(tf.square(ref_values - tf.reduce_mean(ref_values)))
        )
        logging.info(f"const Model error {const_model_error:0.4f}")

        self.assertLess(values_error_end, values_error_begin * 0.2)
        self.assertLess(values_error_end, const_model_error * 0.2)

        # next value prediction
        values_error_begin = tf.reduce_mean(tf.square(ref_next_value - v2_begin))
        values_error_end = tf.reduce_mean(tf.square(ref_next_value - v2_end))
        logging.info(f"ref_next_value \n{tf.squeeze(ref_next_value, -1)}")
        logging.info(f"next_values_end \n{tf.squeeze(v2_end, -1)}")
        logging.info(f"next_values_begin error : {values_error_begin:0.4f}")
        logging.info(f"next_values_end error : {values_error_end:0.4f}")
        const_model_error = tf.reduce_mean(
            tf.reduce_mean(tf.square(ref_next_value - tf.reduce_mean(ref_next_value)))
        )
        logging.info(f"const Model error {const_model_error:0.4f}")

        self.assertLess(values_error_end, values_error_begin * 0.2)
        self.assertLess(values_error_end, const_model_error * 0.2)

        # reward prediction
        rewards_error_begin = tf.reduce_mean(tf.square(ref_rewards - r2_begin))
        rewards_error_end = tf.reduce_mean(tf.square(ref_rewards - r2_end))
        logging.info(f"ref_rewards \n{tf.squeeze(ref_rewards, -1)}")
        logging.info(f"rewards_end \n{tf.squeeze(r2_end, -1)}")
        logging.info(f"rewards_begin error : {rewards_error_begin:0.4f}")
        logging.info(f"rewards_end error : {rewards_error_end:0.4f}")
        const_model_error = tf.reduce_mean(
            tf.reduce_mean(tf.square(ref_rewards - tf.reduce_mean(ref_rewards)))
        )
        logging.info(f"const Model error {const_model_error:0.4f}")

        self.assertLess(rewards_error_end, rewards_error_begin * 0.1)
        self.assertLess(rewards_error_end, const_model_error * 0.1)


if __name__ == "__main__":
    # tf.config.set_visible_devices([], 'GPU')
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    # tf.config.run_functions_eagerly(True) # debug

    unittest.main()

    # test_backprop = TestBackprop()
    # test_backprop.test_variable_update()
    # test_backprop.test_train_on_labels()
