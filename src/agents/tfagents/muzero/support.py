"""
    Support <=> Scalar conversion
    As scalar precision is tough for neural networks, we map value to a vector, each entry maps a range
    This is similar to one-hot encoding for float value
    See paper appendix Network Architecture
    https://arxiv.org/abs/1805.11593
"""
import tensorflow as tf  # type: ignore
from typing import Union


@tf.function
def scalar_to_support(
    x: tf.Tensor, support_size: int, scale: Union[bool, float, int] = True
):
    """
    Transform a scalar to a categorical representation with (2 * support_size + 1) categories
    """
    x = tf.squeeze(x, -1)
    # Reduce the scale
    if isinstance(scale, bool):
        if scale:
            y = tf.math.sign(x) * (tf.math.sqrt(tf.math.abs(x) + 1) - 1) + 0.001 * x
        else:
            y = x
    else:
        y = x * scale

    # Encode on a vector
    y = tf.clip_by_value(y, -support_size, support_size)
    y += support_size
    low = tf.math.floor(y)
    high = tf.cast(tf.math.ceil(y), dtype=tf.int32)
    p = tf.expand_dims(y - low, axis=-1)
    p_high = p * tf.one_hot(high, 2 * support_size + 1)
    p_low = (1 - p) * tf.one_hot(tf.cast(low, dtype=tf.int32), 2 * support_size + 1)
    return p_high + p_low


@tf.function
def support_to_scalar(
    probabilities: tf.Tensor, support_size: int, scale: Union[bool, float, int] = True
):
    """
    Transform a categorical representation (probabilities) to a scalar
    Remember: NN ouputs -- logits -- could be transformed to probabilities with tf.nn.softmax(logits)
    """
    # Decode to a scalar
    support = tf.range(-support_size, support_size + 1, dtype=tf.float32)
    support_reshape = tf.concat(
        [tf.ones(tf.rank(probabilities), tf.int32)[:-1], tf.shape(support)], axis=-1
    )
    support = tf.reshape(support, support_reshape)
    y = tf.reduce_sum(support * probabilities, axis=-1)

    # Invert the scaling
    if isinstance(scale, bool):
        if scale:
            x = tf.math.sign(y) * (
                (
                    (tf.math.sqrt(1 + 4 * 0.001 * (tf.math.abs(y) + 1 + 0.001)) - 1)
                    / (2 * 0.001)
                )
                ** 2
                - 1
            )
        else:
            x = y
    else:
        x = y / scale

    return x[..., None]
