#from __future__ import annotations  # not compatible with kaggle
import collections
import gin  # type: ignore
import numpy as np
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from typing import Any, Callable, List, NamedTuple, Optional


##########################
####### Helpers ##########

MAXIMUM_FLOAT_VALUE = np.finfo(np.float16)  # float('inf')

KnownBounds = collections.namedtuple("KnownBounds", ["min", "max"])


class MinMaxStats(NamedTuple):
    """A class that holds the min-max values of the tree."""

    minimum: tf.Tensor
    maximum: tf.Tensor

    @staticmethod
    def init(known_bounds: Optional[KnownBounds]=None) -> 'MinMaxStats':
        # tf.keras.backend.floatx())
        minimum = tf.cast(
            known_bounds.min if known_bounds else MAXIMUM_FLOAT_VALUE.max, tf.float32
        )
        # tf.keras.backend.floatx())
        maximum = tf.cast(
            known_bounds.max if known_bounds else MAXIMUM_FLOAT_VALUE.min, tf.float32
        )
        return MinMaxStats(minimum, maximum)

    def update(self, value: float):
        minimum = tf.math.minimum(self.minimum, value)
        maximum = tf.math.maximum(self.maximum, value)
        return MinMaxStats(minimum, maximum)

    @staticmethod
    def normalize(self: 'MinMaxStats', value: float) -> float:
        # We normalize only when we have set the maximum and minimum values.
        return tf.where(
            self.maximum > self.minimum,
            (value - self.minimum) / (self.maximum - self.minimum),
            value,
        )


class GameXp(NamedTuple):
    history: tf.Tensor
    rewards: tf.Tensor
    history_length: tf.Tensor
    child_visits: tf.Tensor
    q_values: tf.Tensor
    root_values: tf.Tensor

    def assert_struct(self):
        """
        Check structure for GameXp class
        """
        batch_size = tf.shape(self.history)[0]
        step_count = tf.shape(self.history)[1]

        assert isinstance(self.history, tf.Tensor)
        tf.debugging.assert_type(self.history, tf.int32)
        tf.debugging.assert_rank(self.history, 2)
        tf.assert_equal(tf.shape(self.history)[0], batch_size)
        tf.assert_equal(tf.shape(self.history)[1], step_count)

        assert isinstance(self.rewards, tf.Tensor)
        tf.debugging.assert_type(self.rewards, tf.float32)
        tf.debugging.assert_rank(self.rewards, 2)
        tf.assert_equal(tf.shape(self.rewards)[0], batch_size)
        tf.assert_equal(tf.shape(self.rewards)[1], step_count)

        assert isinstance(self.history_length, tf.Tensor)
        tf.debugging.assert_type(self.history_length, tf.int32)
        tf.debugging.assert_rank(self.history_length, 1)
        tf.assert_equal(tf.shape(self.history_length)[0], batch_size)

        assert isinstance(self.child_visits, tf.Tensor)
        tf.debugging.assert_type(self.child_visits, tf.int32)
        tf.debugging.assert_rank(self.child_visits, 3)
        tf.assert_equal(tf.shape(self.child_visits)[0], batch_size)
        tf.assert_equal(tf.shape(self.child_visits)[1], step_count)
        # tf.assert_equal(tf.shape(self.child_visits)[2], 7) # action space

        assert isinstance(self.q_values, tf.Tensor)
        tf.debugging.assert_type(self.q_values, tf.float32)
        tf.debugging.assert_rank(self.q_values, 3)
        tf.assert_equal(tf.shape(self.q_values)[0], batch_size)
        tf.assert_equal(tf.shape(self.q_values)[1], step_count)

        assert isinstance(self.root_values, tf.Tensor)
        tf.debugging.assert_type(self.root_values, tf.float32)
        tf.debugging.assert_rank(self.root_values, 2)
        tf.assert_equal(tf.shape(self.root_values)[0], batch_size)
        tf.assert_equal(tf.shape(self.root_values)[1], step_count)


def sample_position(self: GameXp, num_unroll_steps: int, training_step: int) -> int:
    # Sample position from game either uniformly or according to some priority.
    high = self.history_length
    low = 0
    r = low + tf.cast(
        tf.random.stateless_uniform(
            tf.shape(high),
            seed = tf.random.get_global_generator().uniform([2], maxval=tf.int32.max, dtype=tf.int32),
            minval=0,
            maxval=1) * tf.cast(high - low, tf.float32),
        tf.int32,
    )
    tf.assert_less(r, self.history_length)
    return r


@gin.configurable
class MuZeroConfig(NamedTuple):
    ## common parameters
    # player turns to get a game turn (ie chess, connect4 has 2 player half-turns)
    player_turn: int = 1
    action_space_size: int = 7  # C4 value

    # Value is scaled (with almost sqrt) and one-hot encoded on a vector with a range of -support_size to support_size. Choose it so that support_size <= sqrt(max(abs(discounted reward)))
    support_size_value: Optional[int] = None
    support_scale_value: bool = True  # apply sqrt scaling before support
    support_size_reward: Optional[int] = None  # same for reward
    support_scale_reward: bool = True  # same for reward

    # tf.float16 tf.bfloat16 tf.int8; default: tf.float32
    tree_representation_storage: Optional[tf.DType] = None

    # MCTS discount value when backpropagation to the tree root; make_target discount
    discount: float = 1.0

    ## play parameters
    num_simulations: int = 5  # Atari=50, Go=800, suggest 10*action space
    visit_softmax_temperature_fn: Callable[
        [int, int], float
    ] = lambda num_moves, training_steps: 1.0

    # if True observation is a tuple and the last one is a mask of legal actions
    enable_action_masking: bool = False

    # Root prior exploration noise.
    root_dirichlet_adaptive: bool = True  # cf paper "planning in stochastic environments with a learned model" : replace root_dirichlet_alpha with 1/sqrt(num_legal_moves)
    # Go: 0.03, Chess: 0.3, Shogi: 0.15, Atari: 0.25.  but a reasonable first guess looks to be choosing ɑ = 10/n. (n= average number of legal moves)
    root_dirichlet_alpha: Optional[float] = None
    root_exploration_fraction: float = 0.25

    # UCB formula,
    pb_c_base: float = 19652
    # By increasing c_puct, we put more weight toward this exploration term. By decreasing it, we more strongly value exploiting the expected result
    # sweet value seem 3-4 for Connect4 https://medium.com/oracledevs/lessons-from-alphazero-part-3-parameter-tweaking-4dceb78ed1e5#8e97
    pb_c_init: float = 1.25

    # If we already have some information about which values occur in the
    # environment, we can use them to initialize the rescaling.
    # This is not strictly necessary, but establishes identical behaviour to
    # AlphaZero in board games.
    known_bounds: Optional[KnownBounds] = None  # KnownBounds(-1, 1)

    ## train parameters
    sample_position_fn: Callable[[GameXp, int, int], int] = sample_position
    weight_decay: float = 1e-4  # weight of L2 regularisation
    num_unroll_steps: int = 5  # number of steps for prediction learning
    # td for value computing (episode length for monte carlo estimation -- 6*7=>Connect4), min 1, Atari: 10
    td_steps: int = 6 * 7

    # Path consistency parameters: cf "Efficient Learning for AlphaZero via Path Consistency" https://proceedings.mlr.press/v162/zhao22h.html
    # add a loss for path consistency: try to match q-value's mean
    enable_path_consistency: bool = False
    path_consistency_post_steps: int = 4  # how many q-values taken to get an average ?
    path_consistency_lambda: float = 1.0  # loss strength

    # enable loss on latent space to match representation(observation[N+1]) with dynamic(representation(observation[N]), action)
    enable_dynamic_regularisation: bool = False

    # enable loss based on kulback leibler divergence with completed q-values for policy training
    # see paper "POLICY IMPROVEMENT BY PLANNING WITH GUMBEL" section "4 LEARNING AN IMPROVED POLICY"
    enable_kld_policy_loss: bool = False
    
    def to_play(self: 'MuZeroConfig', turn: tf.Tensor) -> tf.Tensor:
        return tf.math.floormod(turn, self.player_turn)


class NetworkOutput(NamedTuple):
    value: tf.Tensor  # shape: [batch_size, 1] or [batch_size, 1, support_size]
    reward: tf.Tensor  # shape: [batch_size, 1] or [batch_size, 1, support_size]
    policy_logits: tf.Tensor  # shape: [batch_size, action_count]
    hidden_state: tf.Tensor  # shape: [batch_size, *representation_shape]


class MuzeroNetwork_I:
    def initial_inference(
        self,
        observation: tf.Tensor,
        **kwargs
        ) -> NetworkOutput:
        raise NotImplementedError()

    def recurrent_inference(
        self,
        hidden_state: tf.Tensor,
        action: tf.Tensor,
        **kwargs
        ) -> NetworkOutput:
        raise NotImplementedError()


class MuzeroNetwork(tf.keras.Model, MuzeroNetwork_I):
    def __init__(
        self,
        config: MuZeroConfig,
        representation: tf.keras.Model,
        prediction: tf.keras.Model,
        dynamic: tf.keras.Model,
        dynamic_regularisation: Optional[Callable] = None,
        dynamic_regularisation_projector: Optional[tf.keras.Model] = None,
        optimizer: Optional[tf_agents.typing.types.Optimizer] = None,
        *args,
        **kwargs,
    ):
        assert isinstance(config, MuZeroConfig)
        assert isinstance(representation, tf.keras.Model)
        assert isinstance(prediction, tf.keras.Model)
        assert isinstance(dynamic, tf.keras.Model)
        if dynamic_regularisation:
          assert callable(dynamic_regularisation)
        if dynamic_regularisation_projector is not None:
          assert isinstance(dynamic_regularisation_projector, tf.keras.Model)


        super(MuzeroNetwork, self).__init__()

        self.nw_representation = representation
        self.nw_prediction = prediction
        self.nw_dynamic = dynamic
        self._dynamic_regularisation = dynamic_regularisation
        self._dynamic_regularisation_projector = dynamic_regularisation_projector
        self._optimizer = optimizer
        self._config = config

    def training_steps(self) -> tf.Tensor:
        return (
            self._optimizer.iterations
            if self._optimizer is not None
            else tf.constant(0, tf.int32)
        )

    @property
    def trainable_weights(self):
        if self._config.enable_dynamic_regularisation and self._dynamic_regularisation_projector is not None:
            proj_trainable_weights = self._dynamic_regularisation_projector.trainable_weights
        else:
            proj_trainable_weights = []
        return [
            *self.nw_representation.trainable_weights,
            *self.nw_prediction.trainable_weights,
            *self.nw_dynamic.trainable_weights,
            *proj_trainable_weights,
        ]

    @property
    def trainable_variables(self):
        if self._config.enable_dynamic_regularisation and self._dynamic_regularisation_projector is not None:
            proj_variables = self._dynamic_regularisation_projector.trainable_variables
        else:
            proj_variables = []
        return [
            *self.nw_representation.trainable_variables,
            *self.nw_prediction.trainable_variables,
            *self.nw_dynamic.trainable_variables,
            *proj_variables,
        ]

    def initial_inference(self, observation: tf.Tensor, **kwargs) -> NetworkOutput:
        # representation + prediction function
        hidden_state = self.nw_representation(observation, **kwargs)
        # mixed_precision workaround: NN output flips to f16, but expect input as f32
        hidden_state = tf.nest.map_structure(
            lambda t, s: tf.cast(t, s.dtype),
            hidden_state,
            self.nw_prediction.input,
        )
        policy, value = self.nw_prediction(hidden_state, **kwargs)

        # tf.debugging.assert_rank(value, 2)
        # tf.debugging.assert_equal(tf.shape(value)[-1], 1)
        # tf.debugging.assert_type(value, tf.float32) #assert(value.dtype in (tf.float64, tf.float32, tf.float16, tf.bfloat16))

        batch_size = tf.shape(policy)[0]
        # partial flatten of lower dims
        policy = tf.reshape(policy, tf.stack([batch_size, tf.convert_to_tensor(-1)]))
        # return NetworkOutput(value, tf.zeros_like(value), policy, hidden_state)
        if self._config.support_size_reward is None:
            reward = tf.zeros([batch_size, 1], dtype=tf.float32)
        else:
            full_support_size_reward = self._config.support_size_reward * 2 + 1
            reward = tf.zeros(
                [batch_size, full_support_size_reward], dtype=tf.float32
            )
        # policy = tf.ones_like(policy)  # policy ablation
        return NetworkOutput(
            tf.cast(value, tf.float32),
            reward,
            tf.cast(policy, tf.float32),
            hidden_state,
        )

    def recurrent_inference(
        self, hidden_state: tf.Tensor, action: tf.Tensor, **kwargs
    ) -> NetworkOutput:
        # dynamic + prediction function
        hidden_state = tf.nest.map_structure(
            lambda t, s: tf.cast(t, s.dtype),
            hidden_state,
            self.nw_dynamic.input[0],
        )
        next_hidden_state, reward = self.nw_dynamic(
            (hidden_state, action), **kwargs
        )

        # some versions of TF dislikes assert_equal
        # if tf.nest.is_nested(hidden_state):
        #   tf.debugging.assert_equal(tf.reduce_all([tf.reduce_all(tf.shape(t0)==tf.shape(t1)) for t0, t1 in zip(tf.nest.flatten(hidden_state),tf.nest.flatten(next_hidden_state))]), tf.constant(True))
        # else:
        #   tf.debugging.assert_equal(tf.shape(next_hidden_state), tf.shape(hidden_state))
        # tf.debugging.assert_rank(reward, 2)
        # tf.debugging.assert_equal(tf.shape(reward)[-1], 1)
        # tf.debugging.assert_type(reward, tf.float32) #assert(reward.dtype in (tf.float64, tf.float32, tf.float16, tf.bfloat16))

        next_hidden_state = tf.nest.map_structure(
            lambda t, s: tf.cast(t, s.dtype),
            next_hidden_state,
            self.nw_prediction.input,
        )
        policy, value = self.nw_prediction(next_hidden_state, **kwargs)
        # tf.debugging.assert_equal(tf.shape(value), tf.shape(reward))

        policy = tf.reshape(
            policy, tf.stack([tf.shape(policy)[0], tf.convert_to_tensor(-1)])
        )  # partial flatten of lower dims
        # policy = tf.ones_like(policy)  # policy ablation
        # return NetworkOutput(value, reward, policy, next_hidden_state)
        return NetworkOutput(
            tf.cast(value, tf.float32),
            tf.cast(reward, tf.float32),
            tf.cast(policy, tf.float32),
            next_hidden_state,
        )

    def dynamic_regularisation(self, observations, actions, latent_space_from_representation, latent_space_from_dynamic) -> tf.Tensor:
      """
      shapes: R unroll steps, B for batch
      """

      # flatten 2 first dims : [R,B,...] => [B*R,...]
      latent_space_from_representation_ = tf.nest.map_structure(
          lambda _t: tf.reshape(_t, tf.concat([[-1], tf.shape(_t)[2:]], -1)),
          latent_space_from_representation,
          )
      latent_space_from_dynamic_ = tf.nest.map_structure(
          lambda _t: tf.reshape(_t, tf.concat([[-1], tf.shape(_t)[2:]], -1)),
          latent_space_from_dynamic,
          )

      if self._dynamic_regularisation_projector is None:
          latent_space_from_representation_ = tf.nest.map_structure(
              lambda _x: tf.reshape(_x, [tf.shape(_x)[0], np.prod(_x.shape[1:])]),
              latent_space_from_representation_,
              )
          latent_space_from_dynamic_ = tf.nest.map_structure(
              lambda _y: tf.reshape(_y, [tf.shape(_y)[0], np.prod(_y.shape[1:])]),
              latent_space_from_dynamic_,
              )
      else:
          latent_space_from_representation_ = self._dynamic_regularisation_projector(latent_space_from_representation_)
          latent_space_from_dynamic_ = self._dynamic_regularisation_projector(latent_space_from_dynamic_)

      # tensors must be flat now
      tf.debugging.assert_rank(latent_space_from_representation_, 2)
      tf.debugging.assert_rank(latent_space_from_dynamic_, 2)

      reg_loss = tf.reduce_mean(self._dynamic_regularisation(
            tf.stop_gradient(tf.cast(latent_space_from_representation_, tf.float32)),
            tf.cast(latent_space_from_dynamic_, tf.float32)
          ))
      return reg_loss


def one_hot_shape(action_spec: tf_agents.typing.types.NestedTensorSpec) -> List[Any]:
    action_range = action_spec.maximum - action_spec.minimum + 1
    if np.isscalar(action_range):
        action_shape = [action_spec.shape.num_elements() * action_range]
    else:
        assert action_spec.shape.num_elements() == len(action_range)
        action_shape = action_range
    return action_shape
