import tensorflow as tf  # type: ignore
from typing import Tuple

# pylint: disable=relative-beyond-top-level
from .common import *
from .play import *
from .support import *


@tf.function()
def reanalyze_step(
    observations: tf.Tensor,
    positions: tf.Tensor,
    network: MuzeroNetwork,
    config: MuZeroConfig,
) -> Tuple[tf.Tensor, tf.Tensor]:

    batch_size = int(tf.shape(observations)[0])

    def map_state_spec(spec):
        return tf.TensorSpec(
            spec.shape,
            spec.dtype
            if config.tree_representation_storage == None
            else config.tree_representation_storage,
        )

    hidden_state_spec = tf.nest.map_structure(
        map_state_spec, network.nw_prediction.input_tensor_spec
    )

    tree = Tree.init(
        batch_size, config.action_space_size, config.num_simulations, hidden_state_spec
    )

    nw_output_raw = network.initial_inference(observations)
    nw_output = NetworkOutput(
        nw_output_raw.value
        if config.support_size_value is None
        else support_to_scalar(
            tf.nn.softmax(nw_output_raw.value),
            config.support_size_value,
            config.support_scale_value,
        ),
        nw_output_raw.reward
        if config.support_size_reward is None
        else support_to_scalar(
            tf.nn.softmax(nw_output_raw.reward),
            config.support_size_reward,
            config.support_scale_reward,
        ),
        nw_output_raw.policy_logits,
        nw_output_raw.hidden_state,
    )

    tree = expand_node(
        tree,
        tree.rootNode(),
        config.to_play(positions),
        tf.ones([batch_size, config.action_space_size]),
        nw_output,
        config.action_space_size,
    )

    tree = add_exploration_noise(config, tree)
    tree, _ = run_mcts(
        config=config,
        tree=tree,
        history_depth=positions,
        network=network,
        prnd=tf.convert_to_tensor([0, 42]),
        )

    # cf store_search_statistics
    root_values = Tree.value(tree, tree.rootNode())
    node_indices = Tree.nodeChildren(tree.rootNode(), config.action_space_size)
    visit_counts = tf.cast(
        tf.gather(tree.visit_count, node_indices, axis=1, batch_dims=1), tf.float32
    )
    sum_visits = tf.expand_dims(tf.reduce_sum(visit_counts, axis=-1), -1)
    child_visits = visit_counts / sum_visits

    # Post-checking
    tf.debugging.assert_all_finite(
        child_visits, "invalid child_visits for policy computing"
    )
    tf.debugging.assert_all_finite(
        root_values, "invalid root_values for policy computing"
    )

    return child_visits, root_values  # policy & value
