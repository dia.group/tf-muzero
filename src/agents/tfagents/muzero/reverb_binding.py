"""
  https://github.com/deepmind/reverb
"""
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from tf_agents.trajectories import trajectory  # type: ignore
from typing import Dict, List, Tuple
import sys


if sys.platform != "win32":
    import reverb

    def _flat_trajectory_tensor_spec(
        collect_data_spec: trajectory.Trajectory, max_steps: int = 1
    ):
        dict_collect_tensor_spec = {}
        for field, value in zip(collect_data_spec._fields, collect_data_spec):
            field_spec = tf.nest.flatten(value)
            dict_collect_tensor_spec[field] = [
                tf.TensorSpec(
                    [max_steps + 1] + spec.shape,
                    dtype=spec.dtype,
                    name=field + "/" + spec.name,
                )
                for spec in field_spec
            ]
        return trajectory.Trajectory(**dict_collect_tensor_spec)

    def create_replay_buffer(
        table_name: str,
        collect_data_spec: trajectory.Trajectory,
        max_steps: int = 100,
        replay_buffer_capacity: int = 1024,
        use_per: bool = False,
    ):
        flat_collect_data_spec = tf.nest.flatten(
            _flat_trajectory_tensor_spec(collect_data_spec, max_steps)
        )
        flat_collect_data_spec += [tf.TensorSpec((), dtype=tf.int32, name="step_index")]
        reverb_table_episode = reverb.Table(
            table_name,
            max_size=replay_buffer_capacity,
            sampler=reverb.selectors.Prioritized(priority_exponent=0.8)
            if use_per
            else reverb.selectors.Uniform(),
            remover=reverb.selectors.Fifo(),
            rate_limiter=reverb.rate_limiters.MinSize(1),
            signature={
                f"{i:02}_{t.name}": t for i, t in enumerate(flat_collect_data_spec)
            }  # seems reverb sort tensor by name
            # signature=collect_tensor_spec,
        )

        reverb_server = reverb.Server([reverb_table_episode], port=None)
        # replay_buffer = reverb_replay_buffer.ReverbReplayBuffer(
        #   data_spec = tuple(collect_tensor_spec),
        #   #data_spec = train_agent.collect_data_spec,  # TODO: nested policy info fails with mismatching tensor specs
        #   sequence_length=None,
        #   table_name=table_name,
        #   local_server=reverb_server,
        #   max_cycle_length=8*4)
        reverb_tfclient = reverb.TFClient(f"localhost:{reverb_server.port}")
        reverb_client = reverb.Client(f"localhost:{reverb_server.port}")
        return reverb_server, reverb_table_episode, reverb_tfclient, reverb_client

    class ReverbAddTFEpisodeObserver(object):
        def __init__(
            self,
            reverb_tfclient: reverb.TFClient,
            table_names: str,
            collect_data_spec: tf_agents.typing.types.NestedTensorSpec,
            batch_size: int,
            max_steps: int,
            insert_priority: float = 1000.0,
        ):
            self._reverb_tfclient = reverb_tfclient
            self._table_names = tf.convert_to_tensor(table_names)
            self._collect_data_spec = collect_data_spec
            self._batch_size = batch_size
            self._max_steps = max_steps
            self._insert_priority = insert_priority

            flat_collect_data_spec = tf.nest.flatten(
                _flat_trajectory_tensor_spec(collect_data_spec, max_steps=max_steps)
            )
            # flat_collect_data_spec += [tf.TensorSpec((), dtype=tf.int32, name="step_index")]
            self._writer = [
                tf.Variable(tf.zeros([batch_size] + s.shape, s.dtype))
                for s in flat_collect_data_spec
            ]
            self._writer_position = tf.Variable(
                tf.zeros([self._batch_size], dtype=tf.int32)
            )

        def reset(self):
            for v in self._writer:
                v.assign(tf.zeros_like(v))
            self._writer_position.assign(tf.zeros_like(self._writer_position))

        @tf.function()
        def __call__(self, traj: tf_agents.trajectories.trajectory.Trajectory):
            assert isinstance(traj, tf_agents.trajectories.trajectory.Trajectory)
            flat_traj = tf.nest.flatten(traj)
            tf.debugging.assert_less_equal(self._writer_position, self._max_steps)
            writer_indices2D = tf.stack(
                [tf.range(self._batch_size), self._writer_position], -1
            )
            for writer_elt, traj_elt in zip(self._writer, flat_traj):
                writer_elt.assign(
                    tf.tensor_scatter_nd_update(writer_elt, writer_indices2D, traj_elt)
                )
            self._writer_position.assign(self._writer_position + 1)

            traj_indexes = tf.where(traj.is_boundary())
            for traj_index in traj_indexes:
                traj_index = tf.squeeze(traj_index, -1)
                common_data = [episode[traj_index] for episode in self._writer]
                for step in tf.range(self._writer_position[traj_index]):
                    self._reverb_tfclient.insert(
                        data=common_data + [step],
                        tables=self._table_names,
                        # max: 1 for value + 1 for reward
                        priorities=tf.constant([self._insert_priority], tf.float64),
                    )
                # def insert_step(step):
                #   self._reverb_tfclient.insert(
                #       data = common_data + [step],
                #       tables = self._table_names,
                #       priorities = tf.ones([1], tf.float64),
                #       )
                # tf.vectorized_map(insert_step, tf.range(self._writer_position[traj_index]))

            clear_episodes = traj.is_boundary()
            for v in self._writer:
                new_shape = tf.concat(
                    [tf.shape(clear_episodes), tf.ones(tf.rank(v) - 1, tf.int32)],
                    axis=0,
                )
                v.assign(
                    tf.where(tf.reshape(clear_episodes, new_shape), tf.zeros_like(v), v)
                )
            self._writer_position.assign(
                tf.where(clear_episodes, 0, self._writer_position)
            )

    class ReverbAddPyEpisodeObserver(object):
        def __init__(
            self,
            reverb_client: reverb.Client,
            table_names: str,
            collect_data_spec: tf_agents.typing.types.NestedTensorSpec,
            batch_size: int,
            max_steps: int,
            insert_priority: float = 1000.0,
        ):
            self._reverb_client = reverb_client
            self._table_names = tf.convert_to_tensor(table_names)
            self._collect_data_spec = collect_data_spec
            self._batch_size = batch_size
            self._max_steps = max_steps
            self._insert_priority = insert_priority

            flat_collect_data_spec = tf.nest.flatten(
                _flat_trajectory_tensor_spec(collect_data_spec, max_steps=max_steps)
            )
            # flat_collect_data_spec += [tf.TensorSpec((), dtype=tf.int32, name="step_index")]
            self._writer = [
                tf.Variable(tf.zeros([batch_size] + s.shape, s.dtype))
                for s in flat_collect_data_spec
            ]
            self._writer_position = tf.Variable(
                tf.zeros([self._batch_size], dtype=tf.int32)
            )

        def reset(self):
            for v in self._writer:
                v.assign(tf.zeros_like(v))
            self._writer_position.assign(tf.zeros_like(self._writer_position))

        def __call__(self, traj: tf_agents.trajectories.trajectory.Trajectory):
            assert isinstance(traj, tf_agents.trajectories.trajectory.Trajectory)
            flat_traj = tf.nest.flatten(traj)
            tf.debugging.assert_less_equal(self._writer_position, self._max_steps)
            writer_indices2D = tf.stack(
                [tf.range(self._batch_size), self._writer_position], -1
            )
            for writer_elt, traj_elt in zip(self._writer, flat_traj):
                writer_elt.assign(
                    tf.tensor_scatter_nd_update(writer_elt, writer_indices2D, traj_elt)
                )
            self._writer_position.assign(self._writer_position + 1)

            traj_indexes = tf.where(traj.is_boundary())
            for traj_index in traj_indexes:
                traj_index = tf.squeeze(traj_index, -1)
                common_data = [episode[traj_index] for episode in self._writer]
                for step in tf.range(self._writer_position[traj_index]):
                    data=common_data + [step]
                    self._reverb_client.insert(data, {'episode_table': self._insert_priority})

            clear_episodes = traj.is_boundary()
            for v in self._writer:
                new_shape = tf.concat(
                    [tf.shape(clear_episodes), tf.ones(tf.rank(v) - 1, tf.int32)],
                    axis=0,
                )
                v.assign(
                    tf.where(tf.reshape(clear_episodes, new_shape), tf.zeros_like(v), v)
                )
            self._writer_position.assign(
                tf.where(clear_episodes, 0, self._writer_position)
            )

    def trajectory_from_data_dict(
        data: Dict, collect_data_spec: trajectory.Trajectory
    ) -> Tuple[trajectory.Trajectory, tf.Tensor]:
        """
        convert data from reverb dataset to tf_agents Trajectory
        output a tuple with (Trajectory, position) -- position is needed for PER
        """
        assert isinstance(data, dict)
        assert isinstance(collect_data_spec, trajectory.Trajectory)

        dict_traj_data = {}
        flat_traj_specs = _flat_trajectory_tensor_spec(collect_data_spec)
        i = 0
        for field, spec in zip(flat_traj_specs._fields, flat_traj_specs):
            field_data = []
            for t in spec:
                field_data.append(data[f"{i:02}_{t.name}"])
                i += 1
            if len(field_data) > 1:
                dict_traj_data[field] = tuple(field_data)
            elif len(field_data) > 0:
                dict_traj_data[field] = field_data[0]
            else:
                dict_traj_data[field] = ()

        experience = trajectory.Trajectory(**dict_traj_data)
        position = data[f"{i:02}_step_index"]
        return experience, position

    def trajectory_from_data_list(
        data: List, collect_data_spec: trajectory.Trajectory
    ) -> Tuple[trajectory.Trajectory, tf.Tensor]:
        """
        convert data from reverb dataset to tf_agents Trajectory
        output a tuple with (Trajectory, position) -- position is needed for PER
        """
        assert isinstance(data, list)
        assert isinstance(collect_data_spec, trajectory.Trajectory)

        dict_traj_data = {}
        flat_traj_specs = _flat_trajectory_tensor_spec(collect_data_spec)
        i = 0
        for field, spec in zip(flat_traj_specs._fields, flat_traj_specs):
            field_data = []
            for t in spec:
                field_data.append(data[i])
                i += 1
            if len(field_data) > 1:
                dict_traj_data[field] = tuple(field_data)
            elif len(field_data) > 0:
                dict_traj_data[field] = field_data[0]
            else:
                dict_traj_data[field] = ()

        experience = trajectory.Trajectory(**dict_traj_data)
        position = data[-1]
        return experience, position
