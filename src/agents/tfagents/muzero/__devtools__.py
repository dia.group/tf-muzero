"""
  Tools for testings
"""
import tensorflow as tf  # type: ignore
from typing import Callable

from . import common as muzero_common
from . import play as muzero_play
from . import train as muzero_train
from .. import muzero
from .test import tictactoe_tf as tictactoe
from .test import tictactoe_devtools


# Connect4 Battle between 2 agents
def battle(batch_size: int, P1: Callable, P2: Callable):
    results = tf.constant([[0, 0, 0], [0, 0, 0]], tf.int32)
    for i in range(0, 2):
        # env = tictactoe.TictactoeEnvironment.init(batch_size)
        env = tictactoe.TictactoeEnvironment.init(batch_size)
        first_turn = i % 2 == 0
        turn = first_turn

        while tf.reduce_any(env.done == False):
            if turn:
                action = P1(env)
            else:
                action = P2(env)
            # env,_ = tictactoe.TictactoeEnvironment.step(env, action, tf.constant(True))
            env, _ = tictactoe.TictactoeEnvironment.step(env, action, tf.constant(True))
            turn = not turn
        if first_turn:
            # P1 win as white, explicit code for readability
            p1_win = tf.reduce_sum(
                tf.cast(env.winner == [tictactoe.Player.cross.value], tf.int32)
            )
            results = tf.tensor_scatter_nd_add(results, [[0, 0]], [p1_win])
            draw = tf.reduce_sum(tf.cast(env.winner == 0, tf.int32))
            results = tf.tensor_scatter_nd_add(results, [[0, 1]], [draw])
            p2_win = tf.reduce_sum(
                tf.cast(env.winner == [tictactoe.Player.circle.value], tf.int32)
            )
            results = tf.tensor_scatter_nd_add(results, [[0, 2]], [p2_win])
        else:
            # P1 win as black
            p2_win = tf.reduce_sum(
                tf.cast(env.winner == [tictactoe.Player.cross.value], tf.int32)
            )
            results = tf.tensor_scatter_nd_add(results, [[1, 2]], [p2_win])
            draw = tf.reduce_sum(tf.cast(env.winner == 0, tf.int32))
            results = tf.tensor_scatter_nd_add(results, [[1, 1]], [draw])
            p1_win = tf.reduce_sum(
                tf.cast(env.winner == [tictactoe.Player.circle.value], tf.int32)
            )
            results = tf.tensor_scatter_nd_add(results, [[1, 0]], [p1_win])

    return results


def select_action_nn_policy(
    environment: tictactoe.TictactoeEnvironment,
    network: muzero_common.MuzeroNetwork,
) -> tf.Tensor:
    current_observation = tictactoe.TictactoeEnvironment.observation(environment)
    nn_output = network.initial_inference(current_observation)
    action = tf.math.argmax(nn_output.policy_logits, axis=1, output_type=tf.int32)
    return action


def select_action_nn_value(
    environment: tictactoe.TictactoeEnvironment,
    config,
    network: muzero_common.MuzeroNetwork,
) -> tf.Tensor:
    best_value = tf.fill(tf.shape(environment.turn), -1.0)
    best_action = tf.zeros_like(environment.turn, tf.int32)
    for action_index in tf.range(config.action_space_size):
        child_env, _ = tictactoe.TictactoeEnvironment.step(
            environment, tf.fill([tf.shape(environment.board)[0]], action_index)
        )
        child_obs = tictactoe.TictactoeEnvironment.observation(child_env)
        nn_output = network.initial_inference(child_obs)
        value = tf.squeeze(nn_output.value, -1) * -1.0  # add reward ?
        best_action = tf.where(value > best_value, action_index, best_action)
        best_value = tf.maximum(value, best_value)
    return best_action


def select_action_nn_mcts(
    environment: tictactoe.TictactoeEnvironment,
    config,
    network: muzero_common.MuzeroNetwork,
) -> tf.Tensor:
    rp_shape = tf.TensorSpec(tictactoe_devtools.g_rp_shape, tf.int32)
    batch_size = tf.shape(environment.turn)[0]
    tree = muzero_play.Tree.init(
        batch_size, config.action_space_size, config.num_simulations, rp_shape
    )
    current_observation = tictactoe.TictactoeEnvironment.observation(environment)
    tree = muzero_play.expand_node(
        tree,
        tree.rootNode(),
        config.to_play(environment.turn),
        tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(environment),
            (batch_size, 3 * 3),
        ),
        network.initial_inference(current_observation),
        config.action_space_size,
    )
    # tree = add_exploration_noise(config, tree) # no exploration to avoid unlegal moves
    tree, _ = muzero_play.run_mcts(
        config=config,
        tree=tree,
        history_depth=environment.turn,
        network=network,
        prnd=tf.convert_to_tensor([0, 42]),
        )
    action = muzero_play.select_action(
        tree,
        config.action_space_size,
        temperature = 0.0,
        prnd=tf.convert_to_tensor([0, 42]),
    )
    return action


def GameXp_tile(gameXp, tile_size):
    """
    duplicate game states with tile factor
    """
    # history = tf.tile(gameXp.history, [1,tile_size])
    # rewards = tf.tile(gameXp.rewards, [1,tile_size])
    # history_length = tf.tile(gameXp.history_length, [tile_size])
    # child_visits = tf.tile(gameXp.child_visits, [1,tile_size,1])
    # root_values = tf.tile(gameXp.root_values, [1,tile_size])
    history = tf.tile(gameXp.history, [tile_size, 1])
    rewards = tf.tile(gameXp.rewards, [tile_size, 1])
    history_length = tf.tile(gameXp.history_length, [tile_size])
    child_visits = tf.tile(gameXp.child_visits, [tile_size, 1, 1])
    q_values = tf.cast(child_visits, tf.float32)
    root_values = tf.tile(gameXp.root_values, [tile_size, 1])

    gameXp = muzero.common.GameXp(
        history=history,
        rewards=rewards,
        history_length=history_length,
        child_visits=child_visits,
        q_values=q_values,
        root_values=root_values,
    )
    gameXp.assert_struct()
    return gameXp


def buildMCGameXp(batch_size=1, num_simulations=64 * 1024):
    max_moves = 6 * 7
    action_space = 7

    history = tf.TensorArray(
        dtype=tf.int32,
        size=max_moves + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=[batch_size],
    )
    rewards = tf.TensorArray(
        dtype=tf.float32,
        size=max_moves + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=[batch_size],
    )
    history_length = tf.zeros([batch_size], dtype=tf.int32)

    child_visits = tf.TensorArray(
        dtype=tf.float32,
        size=max_moves + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=[batch_size, action_space],
    )
    root_values = tf.TensorArray(
        dtype=tf.float32,
        size=max_moves + 1,
        dynamic_size=False,
        infer_shape=False,
        element_shape=[batch_size],
    )
    stats_length = tf.constant(0)

    environment = tictactoe.TictactoeEnvironment.init(batch_size)

    for _ in tf.range(max_moves):
        (
            actions_counter,
            actions_accumulator,
            _,
        ) = tictactoe_devtools.compute_action_mc_parallel(environment, num_simulations)
        win_ratio = tf.where(
            actions_counter > 0,
            tf.cast(actions_accumulator, tf.float32)
            / tf.cast(tf.reduce_sum(actions_counter), tf.float32),
            0.0,
        )
        # todo: select win with value eq to max
        action = tf.math.argmax(
            tf.cast(win_ratio, tf.float32), axis=-1, output_type=tf.int32
        )
        root_value = (
            tf.cast(tf.reduce_sum(actions_accumulator), tf.float32) / num_simulations
        ) * 2.0 - 1.0

        append = tf.logical_not(environment.done)
        environment, reward = tictactoe.TictactoeEnvironment.step(environment, action)
        rewards = rewards.write(stats_length, tf.where(append, reward, 0))
        history = history.write(stats_length, tf.where(append, action, 0))
        child_visits = child_visits.write(
            stats_length, tf.where(tf.expand_dims(append, -1), win_ratio, [0])
        )
        root_values = root_values.write(stats_length, tf.where(append, root_value, 0))
        history_length = history_length + tf.cast(append, dtype=tf.int32)
        stats_length = stats_length + 1

    gameXp = muzero_common.GameXp(
        history.stack(),
        rewards.stack(),
        history_length,
        child_visits.stack(),
        root_values.stack(),
    )
    gameXp.assert_struct()
    return gameXp


# MOCK for testing
class MOCKNetwork(object):
    """
    This mock encodes tictactoe real environment in representation space
    it allows to use the MCTS with simulations
    """

    def __init__(self, local_config):
        self.action_space_size = local_config.action_space_size

    def initial_inference(self, image) -> muzero_common.NetworkOutput:
        env = tictactoe.TictactoeEnvironment.obs2env(image)
        to_play = tictactoe.TictactoeEnvironment.to_play(env.turn)

        v, p = self.compute_v_p(env, to_play)

        value_relative = tf.expand_dims(
            tf.where(to_play == tictactoe.Player.cross.value, 1.0, -1.0), -1
        )
        v *= value_relative
        h = image
        return muzero_common.NetworkOutput(v, tf.zeros_like(v), p, h)

    def recurrent_inference(self, hidden_state, action) -> muzero_common.NetworkOutput:
        env = tictactoe.TictactoeEnvironment.obs2env(hidden_state)
        next_env, reward = tictactoe.TictactoeEnvironment.step(env, action)
        to_play = tictactoe.TictactoeEnvironment.to_play(env.turn)

        v, p = self.compute_v_p(next_env, to_play)

        h = tictactoe.TictactoeEnvironment.observation(next_env)
        return muzero_common.NetworkOutput(v, tf.expand_dims(reward, -1), p, h)

    def training_steps(self) -> int:
        return 0

    def compute_v_p(self, env, to_play):
        batch_size = tf.shape(env.turn)[0]
        v = tf.zeros([batch_size, 1], tf.float32)
        p = tf.zeros([batch_size, self.action_space_size], tf.float32)
        return v, p


class MOCKNetworkRandomStrategy(MOCKNetwork):
    """
    return random policy, value is zero
    """

    def __init__(self, local_config):
        super().__init__(local_config)

    def compute_v_p(self, env, to_play):
        batch_size = tf.shape(env.turn)[0]
        v = tf.zeros([batch_size, 1], tf.float32)
        # random policy
        legal_actions = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env), (batch_size, 3 * 3)
        )
        rnd = tf.random.uniform(tf.shape(legal_actions), 0, 1)
        p = tf.cast(legal_actions, tf.float32) * rnd
        return v, p


class MOCKNetworkSimuValueStrategy(MOCKNetwork):
    """
    -- Classic MCTS way --
    return policy based on game simulation until the end
    """

    def __init__(self, local_config):
        super().__init__(local_config)

    @tf.function
    def compute_v_p(self, env, to_play):
        batch_size = tf.shape(env.turn)[0]
        simu_env = env
        turn_to_end = 3 * 3 - tf.cast(tf.reduce_min(env.turn), tf.int32)
        value = tf.zeros_like(env.turn, dtype=tf.float32)
        for _ in tf.range(turn_to_end):  # execute the whole game
            actions = tf.reshape(
                tictactoe.TictactoeEnvironment.legal_actions(simu_env),
                (batch_size, 3 * 3),
            )
            r = tf.random.uniform(tf.shape(actions), 0, 1)
            action = tf.math.argmax(
                tf.cast(actions, tf.float32) * r, axis=-1, output_type=tf.int32
            )
            reward_relative = tf.where(
                to_play == tictactoe.TictactoeEnvironment.to_play(simu_env.turn),
                -1.0,
                1.0,
            )
            simu_env, reward = tictactoe.TictactoeEnvironment.step(simu_env, action)
            value += reward * reward_relative
        value = tf.expand_dims(value, -1)
        legal_actions = tf.reshape(
            tictactoe.TictactoeEnvironment.legal_actions(env), (batch_size, 3 * 3)
        )
        p = tf.cast(legal_actions, tf.float32)
        return value, p


class MOCKNetworkMultiSimuStrategy(MOCKNetwork):
    """
    -- MCTS with leaf parallelisation --
    return policy based on a bunch(tile) of game simulations until the end
    """

    def __init__(self, local_config, tile):
        super().__init__(local_config)
        self.config = local_config
        self.tile = tile

    @tf.function
    def compute_v_p(self, env, to_play):
        (
            actions_counter,
            actions_accumulator,
            winner,
        ) = tictactoe_devtools.compute_action_mc_parallel(env, self.tile)
        w = tf.where(
            winner == tictactoe.Player.cross.value,
            1.0,
            tf.where(winner == tictactoe.Player.circle.value, -1.0, 0.0),
        )
        # computed value is relative to player
        value_relative = tf.where(to_play == tictactoe.Player.cross.value, -1.0, 1.0)
        # if we not win, we lose, no draw count
        v = tf.expand_dims(tf.reduce_mean(w, axis=1) * value_relative, -1)
        p = tf.where(
            actions_counter > 0,
            tf.cast(actions_accumulator, tf.float32)
            / tf.cast(
                tf.expand_dims(tf.reduce_sum(actions_counter, axis=-1), -1), tf.float32
            ),
            0.0,
        )
        return v, p


class ProxyNetworkCheckRange(object):
    """
    Wrapper/Utility to check values before returning them
    """

    def __init__(self, network):
        self.network = network

    def initial_inference(self, image) -> muzero_common.NetworkOutput:
        nw_output = self.network.initial_inference(image)
        tf.debugging.assert_greater_equal(nw_output.value, -1.0)
        tf.debugging.assert_less_equal(nw_output.value, 1.0)
        tf.debugging.assert_greater_equal(nw_output.policy_logits, 0.0)
        tf.debugging.assert_less_equal(nw_output.policy_logits, 1.0)
        return nw_output

    def recurrent_inference(self, hidden_state, action) -> muzero_common.NetworkOutput:
        nw_output = self.network.recurrent_inference(hidden_state, action)
        tf.debugging.assert_greater_equal(nw_output.value, -1.0)
        tf.debugging.assert_less_equal(nw_output.value, 1.0)
        tf.debugging.assert_greater_equal(nw_output.policy_logits, 0.0)
        tf.debugging.assert_less_equal(nw_output.policy_logits, 1.0)
        return nw_output

    def training_steps(self) -> int:
        return self.network.training_steps()


class ProxyNetworkPolicyAblation(object):
    """
    Wrapper/Utility to cleanup policy before returning it
    """

    def __init__(self, batch_size, action_space, network):
        self.action_space = action_space
        self.batch_size = batch_size
        self.network = network

    def initial_inference(self, image) -> muzero_common.NetworkOutput:
        nw_output = self.network.initial_inference(image)
        env = tictactoe.TictactoeEnvironment.obs2env(image)
        p = tf.reshape(
            tf.cast(tictactoe.TictactoeEnvironment.legal_actions(env), tf.float32),
            (self.batch_size, self.action_space),
        )
        return muzero_common.NetworkOutput(
            nw_output.value, nw_output.reward, p, nw_output.hidden_state
        )

    def recurrent_inference(self, hidden_state, action) -> muzero_common.NetworkOutput:
        nw_output = self.network.recurrent_inference(hidden_state, action)
        env = tictactoe.TictactoeEnvironment.obs2env(hidden_state)
        p = tf.reshape(
            tf.cast(tictactoe.TictactoeEnvironment.legal_actions(env), tf.float32),
            (self.batch_size, self.action_space),
        )
        return muzero_common.NetworkOutput(
            nw_output.value, nw_output.reward, p, nw_output.hidden_state
        )

    def training_steps(self) -> int:
        return self.network.training_steps()


class ProxyNetworkValueAblation(object):
    """
    Wrapper/Utility to cleanup value before returning it
    """

    def __init__(self, batch_size, action_space, network):
        self.action_space = action_space
        self.batch_size = batch_size
        self.network = network

    def initial_inference(self, image) -> muzero_common.NetworkOutput:
        nw_output = self.network.initial_inference(image)
        v = tf.zeros([self.batch_size, 1], tf.float32)
        return muzero_common.NetworkOutput(
            v, nw_output.reward, nw_output.policy_logits, nw_output.hidden_state
        )

    def recurrent_inference(self, hidden_state, action) -> muzero_common.NetworkOutput:
        nw_output = self.network.recurrent_inference(hidden_state, action)
        v = tf.zeros([self.batch_size, 1], tf.float32)
        return muzero_common.NetworkOutput(
            v, nw_output.reward, nw_output.policy_logits, nw_output.hidden_state
        )

    def training_steps(self) -> int:
        return self.network.training_steps()


class TUBackpropNetwork(muzero_common.MuzeroNetwork):
    def __init__(self, representation, prediction, dynamic, config):
        super(TUBackpropNetwork, self).__init__(
            config, representation, prediction, dynamic
        )
        self._config = config

    def train_step(self, data):
        gameXp, observations = data[0]

        game_pos = self._config.sample_position_fn(
            gameXp, num_unroll_steps=self._config.num_unroll_steps, training_step=0
        )

        batch_obs = tf.nest.map_structure(
            #lambda obs: tf.gather(obs, game_pos, axis=1, batch_dims=1), observations
            lambda obs: tf.gather(
                obs, game_pos[:, None] + tf.range(self._config.num_unroll_steps+1)[None], axis=1, batch_dims=1
            ),
            observations
        )

        history_index = tf.expand_dims(game_pos, -1) + tf.expand_dims(
            tf.range(self._config.num_unroll_steps), 0
        )
        actions = gameXp.history
        batch_actions = tf.transpose(
            tf.gather(
                tf.pad(actions, [[0, 0], [0, self._config.num_unroll_steps]]),
                history_index,
                axis=1,
                batch_dims=1,
            ),
            [1, 0],
        )

        batch_value, batch_pc, batch_reward, batch_policy, batch_qvalue = muzero_train.Game.make_target(
            gameXp, game_pos, self._config
        )
        batch = (batch_obs, batch_actions, batch_value, batch_pc, batch_reward, batch_policy, batch_qvalue)

        ret = muzero.train.update_weights(
            optimizer=self.optimizer,
            network=self,
            batch=batch,
            weights=tf.ones(()),
            config=self._config,
        )
        return {
            "p_loss": ret.policy_loss,
            "v_loss": ret.value_loss,
            "r_loss": ret.reward_loss,
        }

    def test_step(self, data):  # avoid data_adapter.expand_1d
        x, y = data
        y_pred = self(x, training=False)
        self.compiled_loss(y, y_pred)
        self.compiled_metrics.update_state(y, y_pred)
        # Collect metrics to return
        return_metrics = {}
        for metric in self.metrics:
            result = metric.result()
            if isinstance(result, dict):
                return_metrics.update(result)
            else:
                return_metrics[metric.name] = result
        return return_metrics

    def predict_step(self, data):  # avoid data_adapter.expand_1d
        x = data[0]
        return self(x, training=False)

    def call(self, inputs):
        observation, action = inputs
        v, r, p, h = self.initial_inference(observation)
        v2, r2, p2, h2 = self.recurrent_inference(h, action)
        return tf.nn.softmax(p), v, tf.nn.softmax(p2), v2, r2
