import tensorflow as tf  # type: ignore
from tensorflow_probability import distributions as tfd
from typing import NamedTuple, Tuple, Union

# pylint: disable=relative-beyond-top-level
from .common import *
from .support import *


class Game(object):
    @staticmethod
    @tf.function(jit_compile=True)
    def make_target(
        gameXp: GameXp, state_index: tf.Tensor, config: MuZeroConfig
    ) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        @param gameXp
        @return target value, reward, policy and qvalue for backpropagation
        """
        # tf.debugging.assert_rank(state_index, 1)  # incompatible with jit_compile
        # tf.debugging.assert_type(state_index, tf.int32)
        # tf.debugging.assert_equal(tf.shape(state_index), tf.shape(gameXp.history_length))
        # gameXp.assert_struct()

        target_value = []
        target_pc = []
        target_reward = []
        target_policy = []
        target_qvalue = []
        # The value target is the discounted root value of the search tree N steps
        # into the future, plus the discounted sum of all rewards until then.

        td_steps: int = config.td_steps
        num_unroll_steps: int = config.num_unroll_steps
        cur_root_values = tf.pad(
            gameXp.root_values, [[0, 0], [0, td_steps + num_unroll_steps]]
        )
        cur_rewards = tf.pad(gameXp.rewards, [[0, 0], [0, td_steps + num_unroll_steps]])
        cur_child_visits = gameXp.child_visits

        for j in range(num_unroll_steps + 1):
            current_index = state_index + j
            bootstrap_index = current_index + td_steps
            player = config.to_play(current_index)

            # invert root values for steps that are not mine
            to_play_inv = tf.where(config.to_play(bootstrap_index) == player, 1.0, -1.0)
            value = (
                to_play_inv
                * tf.gather(cur_root_values, bootstrap_index, axis=1, batch_dims=1)
                * tf.math.pow(config.discount, td_steps)
            )

            reward_indices = tf.expand_dims(current_index, -1) + tf.expand_dims(
                tf.range(td_steps), 0
            )
            reward = tf.gather(cur_rewards, reward_indices, axis=1, batch_dims=1)
            discount_range = tf.expand_dims(
                tf.math.pow(
                    config.discount, tf.cast(tf.range(1, td_steps + 1), tf.float32)
                ),
                0,
            )
            to_play_range_inv = tf.where(
                tf.expand_dims(player, -1)
                == config.to_play(
                    tf.expand_dims(tf.range(td_steps), 0)
                    + tf.expand_dims(current_index, -1)
                ),
                1.0,
                -1.0,
            )
            value += tf.reduce_sum(reward * discount_range * to_play_range_inv, axis=1)

            # path consistency computing
            # q_value is the value computed from MCTS (no TD-error)
            pc_indices = tf.expand_dims(current_index, -1) + tf.expand_dims(
                tf.range(config.path_consistency_post_steps), 0
            )
            to_play_range_inv_pc = tf.where(
                tf.expand_dims(player, -1)
                == config.to_play(
                    tf.expand_dims(tf.range(config.path_consistency_post_steps), 0)
                    + tf.expand_dims(current_index, -1)
                ),
                1.0,
                -1.0,
            )
            pc_values = to_play_range_inv_pc * tf.gather(cur_root_values, pc_indices, axis=1, batch_dims=1)
            pc = tf.reduce_sum(pc_values, axis=-1) / (tf.reduce_sum(tf.cast(pc_indices<=gameXp.history_length[..., None], tf.float32), axis=-1) + 1e-10)   # apply discount ?

            # For simplicity the network always predicts the most recently received
            # reward, even for the initial representation network where we already
            # know this reward.
            last_reward = tf.where(
                tf.logical_and(
                    current_index > 0, current_index <= gameXp.history_length
                ),
                tf.gather(
                    cur_rewards,
                    tf.math.maximum(current_index - 1, 0),
                    axis=1,
                    batch_dims=1,
                ),
                0,
            )

            # States past the end of games are treated as absorbing states.
            policy = tf.gather(
                cur_child_visits,
                tf.minimum(current_index, gameXp.history_length - 1),
                axis=1,
                batch_dims=1,
            )
            q_values = tf.gather(
                gameXp.q_values,
                tf.minimum(current_index, gameXp.history_length - 1),
                axis=1,
                batch_dims=1,
            )
            target_value.append(value)
            target_pc.append(pc)
            target_reward.append(last_reward)
            target_policy.append(policy)
            target_qvalue.append(q_values)

        return tf.stack(target_value), tf.stack(target_pc), tf.stack(target_reward), tf.stack(target_policy), tf.stack(target_qvalue)


##################################
####### Part 2: Training #########
def scale_gradient(
    tensor: Union[tf.Tensor, Tuple[tf.Tensor, ...]], scale: Union[float, tf.Tensor]
) -> Union[tf.Tensor, Tuple[tf.Tensor, ...]]:

    """Scales the gradient for the backward pass."""
    if isinstance(tensor, tuple):
        return tuple(
            [
                t * scale + tf.stop_gradient(t) * (1 - scale)
                if t.dtype in (tf.float16, tf.bfloat16, tf.float32, tf.float64)
                else t
                for t in tensor
            ]
        )
    else:
        return tensor * scale + tf.stop_gradient(tensor) * (1 - scale)


class UpdateWeightsReturn(NamedTuple):
    policy_loss: tf.Tensor
    value_loss: tf.Tensor
    pc_loss: tf.Tensor
    reward_loss: tf.Tensor
    dynamic_loss: tf.Tensor
    l2_loss: tf.Tensor
    priorities: tf.Tensor  # per sample priority -- based on error -- for PER
    actions: tf.Tensor
    value_predictions: tf.Tensor
    reward_predictions: tf.Tensor
    policy_predictions: tf.Tensor
    hidden_state_representation: tf.Tensor
    hidden_state_dynamic: tf.Tensor


@tf.function()  # TODO: fix jit_compile=True, experimental_follow_type_hints=True for complex environments
def update_weights(
    optimizer: tf.keras.optimizers,
    network: MuzeroNetwork,
    batch: Tuple[tf.Tensor, ...],
    weights: tf.Tensor,
    config: MuZeroConfig,
) -> UpdateWeightsReturn:

    (
        observations,
        actions,
        targets_value_scalar,
        targets_pc_scalar,
        targets_reward_scalar,
        targets_policy,
        targets_qvalue,
    ) = batch

    support_size_value = config.support_size_value
    support_scale_value = config.support_scale_value
    support_size_reward = config.support_size_reward
    support_scale_reward = config.support_scale_reward

    step_count = config.num_unroll_steps
    batch_size = actions.shape[1]
    action_space_size = config.action_space_size
    compute_dtype = tf.keras.backend.floatx()  # tf.float16

    with tf.GradientTape() as tape:
        # Initial step, from the real observation.
        value, reward, policy_logits, hidden_state = network.initial_inference(
            tf.nest.map_structure(lambda elt: elt[:, 0, ...], observations),
            training=True
        )

        predictions_policy_logits = tf.TensorArray(
            dtype=compute_dtype,
            size=step_count + 1,
            dynamic_size=False,
            infer_shape=False,
            element_shape=[batch_size, action_space_size],
        )
        value_shape = (
            [batch_size, 1]
            if support_size_value is None
            else [batch_size, support_size_value * 2 + 1]
        )
        predictions_value = tf.TensorArray(
            dtype=compute_dtype,
            size=step_count + 1,
            dynamic_size=False,
            infer_shape=False,
            element_shape=value_shape,
        )
        reward_shape = (
            [batch_size, 1]
            if support_size_reward is None
            else [batch_size, support_size_reward * 2 + 1]
        )
        predictions_reward = tf.TensorArray(
            dtype=compute_dtype,
            size=step_count + 1,
            dynamic_size=False,
            infer_shape=False,
            element_shape=reward_shape,
        )
        predictions_gradient_scale = tf.TensorArray(
            dtype=compute_dtype,
            size=step_count + 1,
            dynamic_size=False,
            infer_shape=False,
            element_shape=(),
        )

        hidden_state_spec = network.nw_prediction.input
        latent_state_from_rep = tf.nest.map_structure(
            lambda spec: tf.TensorArray(
                dtype=compute_dtype,
                size=step_count,
                dynamic_size=False,
                infer_shape=False,
                element_shape=spec.shape,
            ),
            hidden_state_spec)
        latent_state_from_dyn = tf.nest.map_structure(
            lambda spec: tf.TensorArray(
                dtype=compute_dtype,
                size=step_count,
                dynamic_size=False,
                infer_shape=False,
                element_shape=spec.shape,
            ),
            hidden_state_spec)

        write_index = 0
        predictions_policy_logits = predictions_policy_logits.write(
            write_index, tf.cast(policy_logits, compute_dtype)
        )
        predictions_value = predictions_value.write(
            write_index, tf.cast(value, compute_dtype)
        )
        predictions_reward = predictions_reward.write(
            write_index, tf.cast(reward, compute_dtype)
        )
        predictions_gradient_scale = predictions_gradient_scale.write(
            write_index, tf.constant(1.0, dtype=compute_dtype)
        )

        hidden_state_rep = hidden_state

        # Recurrent steps, from action and previous hidden state.
        for action in actions:
            value, reward, policy_logits, hidden_state = network.recurrent_inference(
                hidden_state, action, training=True
            )

            if network._dynamic_regularisation is not None:
                latent_state_from_dyn = tf.nest.map_structure(
                    lambda _array, _tensor: _array.write(write_index, tf.cast(_tensor, compute_dtype)),
                    latent_state_from_dyn,
                    hidden_state)
                _, _, _, hidden_state_from_observation = network.initial_inference(
                    tf.nest.map_structure(
                        lambda elt: elt[:, write_index+1, ...],
                        observations),
                    training=True
                )
                latent_state_from_rep = tf.nest.map_structure(
                    lambda _array, _tensor: _array.write(write_index, tf.cast(_tensor, compute_dtype)),
                    latent_state_from_rep,
                    hidden_state_from_observation)
            
            write_index += 1

            predictions_policy_logits = predictions_policy_logits.write(
                write_index, tf.cast(policy_logits, compute_dtype)
            )
            predictions_value = predictions_value.write(
                write_index, tf.cast(value, compute_dtype)
            )
            predictions_reward = predictions_reward.write(
                write_index, tf.cast(reward, compute_dtype)
            )
            predictions_gradient_scale = predictions_gradient_scale.write(
                write_index, 1.0 / tf.cast(tf.shape(actions)[0], compute_dtype)
            )

            hidden_state = scale_gradient(hidden_state, 0.5)

        predictions_policy_logits = predictions_policy_logits.stack()
        predictions_value = predictions_value.stack()
        predictions_reward = predictions_reward.stack()
        predictions_gradient_scale = predictions_gradient_scale.stack()
        targets_value = tf.cast(
            targets_value_scalar
            if support_size_value is None
            else scalar_to_support(
                targets_value_scalar[..., None], support_size_value, support_scale_value
            ),
            compute_dtype,
        )
        targets_pc = tf.cast(
            targets_pc_scalar
            if support_size_value is None
            else scalar_to_support(
                targets_pc_scalar[..., None], support_size_value, support_scale_value
            ),
            compute_dtype,
        )
        targets_reward = tf.cast(
            targets_reward_scalar
            if support_size_reward is None
            else scalar_to_support(
                targets_reward_scalar[..., None], support_size_reward, support_scale_reward
            ),
            compute_dtype,
        )

        if support_size_value is None:
            value_loss = tf.reduce_sum(
                scale_gradient(
                    scalar_loss(tf.squeeze(predictions_value, -1), targets_value),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )
        else:
            value_loss = tf.reduce_sum(
                scale_gradient(
                    tf.nn.softmax_cross_entropy_with_logits(
                        logits=predictions_value,
                        labels=tf.stop_gradient(targets_value),
                    ),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )

        if support_size_value is None:
            pc_loss = tf.reduce_sum(
                scale_gradient(
                    scalar_loss(tf.squeeze(predictions_value, -1), targets_pc),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )
        else:
            pc_loss = tf.reduce_sum(
                scale_gradient(
                    tf.nn.softmax_cross_entropy_with_logits(
                        logits=predictions_value,
                        labels=tf.stop_gradient(targets_pc),
                    ),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )

        if support_size_reward is None:
            reward_loss = tf.reduce_sum(
                scale_gradient(
                    scalar_loss(tf.squeeze(predictions_reward, -1), targets_reward),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )
        else:
            reward_loss = tf.reduce_sum(
                scale_gradient(
                    tf.nn.softmax_cross_entropy_with_logits(
                        logits=predictions_reward,
                        labels=tf.stop_gradient(targets_reward),
                    ),
                    predictions_gradient_scale[:, None],
                ),
                0,
            )

        # policy labels for cross_entropy loss
        # targets_policy_labels = tf.nn.softmax(tf.cast(targets_policy, predictions_policy_logits.dtype), axis=-1)
        targets_policy_labels = tf.cast(targets_policy, predictions_policy_logits.dtype) / \
            (tf.reduce_sum(tf.cast(targets_policy, predictions_policy_logits.dtype), -1)[..., None] + 1e-8)
        
        if config.enable_kld_policy_loss:
            # policy labels for kld loss on completed q-values, see paper "POLICY IMPROVEMENT BY PLANNING WITH GUMBEL" section 4 - LEARNING AN IMPROVED POLICY
            policy_dist = tfd.Categorical(logits=predictions_policy_logits)
            root_value = tf.reduce_sum(targets_qvalue * tf.cast(targets_policy, targets_qvalue.dtype), -1) / config.num_simulations  # to validate... (discount?)
            targets_policy_logits = predictions_policy_logits + tf.where(
                targets_policy!=0,
                targets_qvalue,
                tf.tile(root_value[..., None], tf.concat([[1,1], [tf.shape(targets_policy)[-1]]], -1)))
            labels_dist = tfd.Categorical(logits=tf.stop_gradient(targets_policy_logits))
            # labels_dist = tfd.Categorical(probs=targets_policy_labels+1e-8)  # kld loss using MCTS node counts (same labels as crossentropy loss)
            policy_loss = tfd.kl_divergence(
                labels_dist,
                policy_dist,
                )
        else:
            policy_loss = tf.nn.softmax_cross_entropy_with_logits(
                logits=predictions_policy_logits,
                labels=targets_policy_labels,
                )
            
        policy_loss = tf.reduce_sum(
            scale_gradient(
                policy_loss,
                predictions_gradient_scale[:, None],
            ),
            0,
        )

        elt_loss = value_loss + reward_loss + policy_loss

        if config.enable_path_consistency:
            elt_loss += config.path_consistency_lambda * pc_loss

        if network._dynamic_regularisation is not None:
            latent_state_from_dyn = tf.nest.map_structure(
                lambda _array: _array.stack(),
                latent_state_from_dyn)
            latent_state_from_rep = tf.nest.map_structure(
                lambda _array: _array.stack(),
                latent_state_from_rep)

            dyn_loss = tf.cast(network.dynamic_regularisation(
                observations,
                actions,
                latent_state_from_dyn,
                latent_state_from_rep
                ), compute_dtype)
        else:
            dyn_loss = 0.0
            
        if config.enable_dynamic_regularisation:
            elt_loss += dyn_loss

        loss = tf.reduce_mean(weights * elt_loss)

        l2_loss = sum(config.weight_decay * tf.nn.l2_loss(tf.cast(weights, compute_dtype)) for weights in network.trainable_weights)
        loss += l2_loss

        if isinstance(optimizer, tf.keras.mixed_precision.LossScaleOptimizer):
            scaled_loss = optimizer.get_scaled_loss(loss)
            scaled_grads = tape.gradient(scaled_loss, network.trainable_variables)
            grads = optimizer.get_unscaled_gradients(scaled_grads)
        else:
            grads = tape.gradient(loss, network.trainable_variables)
          
    optimizer.apply_gradients(zip(grads, network.trainable_variables))

    predictions_value_scalar = (
        tf.squeeze(predictions_value, -1)
        if support_size_value is None
        else tf.squeeze(support_to_scalar(
            tf.nn.softmax(predictions_value), support_size_value, support_scale_value
        ), -1)
    )

    predictions_reward_scalar = (
        tf.squeeze(predictions_reward, -1)
        if support_size_reward is None
        else tf.squeeze(support_to_scalar(
            tf.nn.softmax(predictions_reward), support_size_reward, support_scale_reward
        ), -1)
    )

    priorities = tf.reduce_mean(
        tf.math.abs(
            predictions_value_scalar - targets_value_scalar
        ),
        axis=0,
    ) + tf.reduce_mean(
        tf.math.abs(
            predictions_reward_scalar - targets_reward_scalar
        ),
        axis=0,
    )  # MAE

    ret = UpdateWeightsReturn(
        policy_loss=tf.reduce_mean(tf.cast(policy_loss, tf.float32)),
        value_loss=tf.reduce_mean(tf.cast(value_loss, tf.float32)),
        pc_loss=tf.reduce_mean(tf.cast(pc_loss, tf.float32)),
        reward_loss=tf.reduce_mean(tf.cast(reward_loss, tf.float32)),
        dynamic_loss=tf.reduce_mean(tf.cast(dyn_loss, tf.float32)),
        l2_loss=l2_loss,
        priorities=tf.cast(priorities, tf.float32),
        actions=actions,
        value_predictions=predictions_value_scalar,
        reward_predictions=predictions_reward_scalar,
        policy_predictions=tf.nn.softmax(predictions_policy_logits),
        hidden_state_representation=hidden_state_rep,
        hidden_state_dynamic=hidden_state,
    )
    return ret


def scalar_loss(prediction, target) -> float:
    # MSE in board games, cross entropy between categorical values in Atari.
    # no mean as the input is already a scalar
    error = tf.math.square(prediction - target)
    return error


######### End Training ###########
##################################
