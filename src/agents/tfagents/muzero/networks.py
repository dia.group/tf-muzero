import logging
import numpy as np
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
from typing import Optional

from .common import *


def load_model(muzero_network: MuzeroNetwork, filename: str):
    if isinstance(filename, str):
        logging.info(f"loading model from file {filename}...")
    npzfile = np.load(filename, allow_pickle=True)
    rep_w = [npzfile[k] for k in npzfile.keys() if k.startswith('representation:')]  # Note: expect same order, otherwize sort ids
    pred_w = [npzfile[k] for k in npzfile.keys() if k.startswith('prediction:')]
    dyn_w = [npzfile[k] for k in npzfile.keys() if k.startswith('dynamic:')]

    if len(muzero_network.nw_representation.get_weights()) != len(rep_w):
        raise Exception(
            "nw_representation mismatch weight counts: not loading same architecture"
        )
    muzero_network.nw_representation.set_weights(rep_w)

    if len(muzero_network.nw_prediction.get_weights()) != len(pred_w):
        raise Exception(
            "nw_prediction mismatch weight counts: not loading same architecture"
        )
    muzero_network.nw_prediction.set_weights(pred_w)

    if len(muzero_network.nw_dynamic.get_weights()) != len(dyn_w):
        raise Exception(
            "nw_dynamic mismatch weight counts: not loading same architecture"
        )
    muzero_network.nw_dynamic.set_weights(dyn_w)

    logging.info(f"model loaded!")


def save_model(muzero_network: MuzeroNetwork, filename: str):
    logging.info(f"saving model to file {filename}...")
    save_dict = {}
    for i, w in enumerate(muzero_network.nw_representation.get_weights()):
        save_dict["representation:"+str(i)] = w
    for i, w in enumerate(muzero_network.nw_prediction.get_weights()):
        save_dict["prediction:"+str(i)] = w
    for i, w in enumerate(muzero_network.nw_dynamic.get_weights()):
        save_dict["dynamic:"+str(i)] = w
    np.savez_compressed(
        filename, **save_dict
    )
    logging.info(f"model saved!")


class Conv1DResidualBlock(tf.keras.layers.Layer):
    def __init__(self, filters):
        super().__init__()
        self.conv1 = tf.keras.layers.Conv1D(
            filters, [1], activation=tf.nn.leaky_relu, padding="same"
        )
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.conv2 = tf.keras.layers.Conv1D(
            filters, [1], activation=tf.nn.leaky_relu, padding="same"
        )
        self.bn2 = tf.keras.layers.BatchNormalization()
        # attention mecanism
        self._query = [
            # tf.keras.layers.Conv1D(filters, [1]),
            # tf.keras.layers.LeakyReLU(),
            tf.keras.layers.Conv1D(
                filters, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
        ]

    def call(self, x):
        x1 = self.bn1(self.conv1(x))
        x2 = self.bn2(self.conv2(x1))
        query = x2
        for q in self._query:
            query = q(query)
        return tf.keras.layers.Concatenate()(
            [tf.keras.layers.Attention()([query, x2]), x2]
        )
        # return x2 + x


class NWFCRepresentation(tf.keras.Model):
    """Conversion from observation to inner abstract state"""

    def __init__(
        self,
        observation_spec: tf.TensorSpec,
        # num_features: int,
        name="Representation",
    ):
        inputs=tf.keras.Input(observation_spec.shape, dtype=observation_spec.dtype)
        d = tf.keras.layers.Flatten()(inputs)

        self._num_features = 512

        d = tf.keras.layers.Dense(
            self._num_features, activation=tf.nn.leaky_relu, name="rep"
        )(d)

        d = tf.keras.layers.Dense(
            self._num_features, activation=tf.nn.leaky_relu
        )(d)

        d = tf.keras.layers.LayerNormalization()(d)
        outputs = tf.keras.layers.Activation(
            tf.keras.activations.tanh, name="representation-output_rp"
        )(d)

        super(NWFCRepresentation, self).__init__(
          inputs = inputs,
          outputs = outputs,
          name=name
        )


class NWFCPrediction(tf.keras.Model):
    """Policy and value prediction from inner abstract state"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        value_support: Optional[int] = None,
        name="Prediction",
    ):

        actions_range = action_spec.maximum - action_spec.minimum + 1

        if np.isscalar(actions_range):
            num_actions = action_spec.shape.num_elements() * actions_range
        else:
            num_actions = 1
            for dim in actions_range:
                num_actions = num_actions * dim
        self._num_actions = num_actions

        inputs=tf.keras.Input(hidden_state_spec.shape, dtype=hidden_state_spec.dtype)
        d = tf.keras.layers.Dense(
            512, activation=tf.nn.leaky_relu, name="rep"
        )(inputs)

        policy = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu, name="policy")(d)
        # tf.keras.layers.BatchNormalization(),
        policy = tf.keras.layers.Dense(actions_range, name="prediction-output_policy")(policy)

        value = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu, name="value")(d)
        
        if value_support is None:
            value = tf.keras.layers.Dense(1, name="prediction-output_value")(value)
        else:
            full_support = 2 * value_support + 1
            value = tf.keras.layers.Dense(full_support, name="prediction-output_value")(value)
            # self._value.append(tf.keras.layers.Reshape([1, full_support]))
        outputs = [policy, value]
        super(NWFCPrediction, self).__init__(
          inputs = inputs,
          outputs = outputs,
          name=name
        )


class NWFCDynamic(tf.keras.Model):
    """Abstract state transition"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        reward_support: Optional[int] = None,
        name="Dynamic",
    ):

        self._action_spec = action_spec

        actions_range = action_spec.maximum - action_spec.minimum + 1

        inputs_hidden_state=tf.keras.Input(hidden_state_spec.shape, dtype=hidden_state_spec.dtype)
        inputs_action=tf.keras.Input(action_spec.shape, dtype=action_spec.dtype)
        inputs = [inputs_hidden_state, inputs_action]
        d = tf.keras.layers.Concatenate()([inputs_hidden_state, tf.one_hot(inputs_action, actions_range)])

        d = tf.keras.layers.Dense(
            512, activation=tf.nn.leaky_relu, name="rep"
        )(d)

        rp_outputs = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu)(d)
        rp_outputs = tf.keras.layers.LayerNormalization()(rp_outputs)
        # tf.keras.layers.BatchNormalization(),
        rp_outputs = tf.keras.layers.Activation(
            tf.keras.activations.tanh, name="dynamic-output_rp"
        )(rp_outputs)

        reward = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu)(d)
        if reward_support is None:
            reward = tf.keras.layers.Dense(1, name="dynamic-output_reward")(reward)
        else:
            full_support = 2 * reward_support + 1
            reward = tf.keras.layers.Dense(full_support, name="dynamic-output_reward")(reward)

        outputs = [rp_outputs, reward]
        super(NWFCDynamic, self).__init__(
          inputs = inputs,
          outputs = outputs,
          name=name
        )


class NWConv1DRepresentation(tf_agents.networks.network.Network):
    """Conversion from observation to inner abstract state"""

    def __init__(
        self,
        observation_spec: tf.TensorSpec,
        hidden_state_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        name="Representation",
    ):
        assert tf.rank(hidden_state_spec.shape) == tf.rank(observation_spec.shape)

        super(NWConv1DRepresentation, self).__init__(
            input_tensor_spec=observation_spec, state_spec=(), name=name
        )

        self._hidden_state_spec = hidden_state_spec
        self._num_features = hidden_state_spec.shape[-1]
        self._res_blk = [
            tf.keras.layers.Conv1D(
                num_conv_features, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
        ]
        self._res_blk += [
            Conv1DResidualBlock(num_conv_features) for _ in range(num_res_block)
        ]
        self._res_blk += [
            tf.keras.layers.Conv1D(
                self._num_features, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
        ]

        self._norm = tf.keras.layers.LayerNormalization()
        self._norm_fn = tf.keras.layers.Activation(
            tf.keras.activations.tanh, name="representation-output_rp"
        )

    def call(self, observations, step_type=(), network_state=()):
        obs_f = tf.keras.backend.cast(observations, dtype=tf.float32)
        d = obs_f
        for res_blk in self._res_blk:
            d = res_blk(d)
        rep = self._norm_fn(self._norm(d))
        return rep, network_state


class NWConv1DPrediction(tf_agents.networks.network.Network):
    """Policy and value prediction from inner abstract state"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        name="Prediction",
    ):

        super(NWConv1DPrediction, self).__init__(
            input_tensor_spec=hidden_state_spec, state_spec=(), name=name
        )

        actions_range = action_spec.maximum - action_spec.minimum + 1

        if np.isscalar(actions_range):
            num_actions = action_spec.shape.num_elements() * actions_range
        else:
            num_actions = 1
            for dim in actions_range:
                num_actions = num_actions * dim
        self._num_actions = num_actions

        self._num_features = hidden_state_spec.shape[-1]
        self._res_blk = [
            tf.keras.layers.Conv1D(
                num_conv_features, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
        ]
        self._res_blk += [
            Conv1DResidualBlock(num_conv_features) for _ in range(num_res_block)
        ]

        self._p = [
            tf.keras.layers.Conv1D(
                (self._num_features + actions_range[1]) // 2,
                [1],
                activation=tf.nn.leaky_relu,
                padding="same",
            ),  # fullConv
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(
                actions_range[1], [1], activation=tf.nn.leaky_relu, padding="same"
            ),  # fullConv
            tf.keras.layers.Softmax(name="prediction-output_policy"),
        ]

        self._v = [
            tf.keras.layers.Conv1D(
                self._num_features // 2,
                [1],
                activation=tf.nn.leaky_relu,
                padding="same",
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(
                1, [1], activation=tf.nn.leaky_relu, name="prediction-output_value"
            ),  # fullConv
        ]

    def call(self, hidden_state, step_type=(), network_state=()):
        h = hidden_state
        for res_blk in self._res_blk:
            h = res_blk(h)

        policy = h
        for layer in self._p:
            policy = layer(policy)

        value = h
        for layer in self._v:
            value = layer(value)

        value = tf.reduce_sum(value, axis=1)
        # policy = tf.reshape(policy * select, [tf.shape(hidden_state)[0], self._num_actions])

        return (policy, value), network_state


class NWConv1DDynamic(tf_agents.networks.network.Network):
    """Abstract state transition"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        name="Dynamic",
    ):

        self._action_spec = action_spec
        flatten_action_specs = tf_agents.specs.TensorSpec(
            (), tf.int32
        )  # flattened action space
        super(NWConv1DDynamic, self).__init__(
            input_tensor_spec=(hidden_state_spec, flatten_action_specs),
            state_spec=(),
            name=name,
        )
        self._num_features = hidden_state_spec.shape[-1]

        self._res_blk = [
            tf.keras.layers.Conv1D(
                num_conv_features, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
        ]
        self._res_blk += [
            Conv1DResidualBlock(num_conv_features) for _ in range(num_res_block)
        ]
        self._rp_outputs = [
            tf.keras.layers.Conv1D(self._num_features, [1], padding="same"),
            # tf.keras.layers.LayerNormalization(),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation(
                tf.keras.activations.tanh, name="dynamic-output_rp"
            ),
        ]

        self._reward = [
            tf.keras.layers.Conv1D(
                num_conv_features, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
            # (hidden_state_spec.shape.num_elements() + 1)//2),
            tf.keras.layers.Conv1D(
                num_conv_features // 2, [1], activation=tf.nn.leaky_relu, padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(
                1,
                [1],
                activation=tf.nn.leaky_relu,
                padding="same",
                name="dynamic-output_reward",
            ),
        ]

    def call(self, hidden_state_shape_with_action, step_type=(), network_state=()):
        hidden_state, action = hidden_state_shape_with_action
        action_shape = one_hot_shape(self._action_spec)
        batch_size = tf.shape(action)[0]
        a_inputs = tf.reshape(
            tf.one_hot(action, np.prod(action_shape)), [batch_size, *action_shape]
        )  # one_hot multi-dim decoding

        h = tf.keras.layers.Concatenate()([hidden_state, a_inputs])

        # dynamic
        for res_blk in self._res_blk:
            h = res_blk(h)

        rp_outputs = h
        for layer in self._rp_outputs:
            rp_outputs = layer(rp_outputs)

        # reward
        reward = h
        for layer in self._reward:
            reward = layer(reward)

        indices = tf.math.floordiv(action, action_shape[1])
        reward = tf.gather(reward, indices, axis=1, batch_dims=1)

        return (rp_outputs, reward), network_state


class Conv2DResidualBlock(tf.keras.layers.Layer):
    def __init__(self, filters):
        super().__init__()
        self.conv1 = tf.keras.layers.Conv2D(filters, 3, padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()  # TODO: test Layer Normalization + pre-activation (cf paper "Planning in stochastic environments with a learned model" 2022)
        self.fn1 = tf.keras.layers.Activation(tf.nn.leaky_relu)
        self.conv2 = tf.keras.layers.Conv2D(filters, 3, padding="same")
        self.bn2 = tf.keras.layers.BatchNormalization()
        self.fn2 = tf.keras.layers.Activation(tf.nn.leaky_relu)

    def call(self, x):
        # residual
        x1 = self.fn1(self.bn1(self.conv1(x)))
        x2 = self.fn2(self.bn2(self.conv2(x1)) + x)
        # pre-activation residual
        # x1 = self.conv1(self.fn1(self.bn1(x)))
        # x2 = self.conv2(self.fn2(self.bn2(x1))) + x
        return x2


class NWConv2DRepresentation(tf.keras.Model):
    """Conversion from observation to inner abstract state"""

    def __init__(
        self,
        observation_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        num_features: int,
        name="Representation",
    ):
        compute_dtype = tf.keras.backend.floatx()
        inputs=tf.keras.Input(observation_spec.shape, dtype=observation_spec.dtype)
        d = tf.cast(inputs, compute_dtype)
        
        d = tf.keras.layers.Conv2D(
            num_conv_features, 4, padding="same", activation=tf.nn.leaky_relu
        )(d)
        d = tf.keras.layers.BatchNormalization()(d)
        for blk in range(num_res_block):
            d = Conv2DResidualBlock(num_conv_features)(d)
        # shrink features for hidden_state
        d = tf.keras.layers.Conv2D(num_features, (1, 1), padding="same")(d)
        d = tf.keras.layers.LayerNormalization()(d)
        outputs = tf.keras.layers.Activation(tf.keras.activations.tanh, name="representation-output_rp")(d)

        super(NWConv2DRepresentation, self).__init__(
          inputs = inputs,
          outputs = outputs,
          name=name
        )


class NWConv2DPrediction(tf.keras.Model):
    """Policy and value prediction from inner abstract state"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        value_support: Optional[int] = None,
        name="Prediction",
    ):
        inputs=tf.keras.Input(hidden_state_spec.shape, dtype=hidden_state_spec.dtype)

        actions_range = action_spec.maximum - action_spec.minimum + 1

        if np.isscalar(actions_range):
            num_actions = action_spec.shape.num_elements() * actions_range
        else:
            num_actions = 1
            for dim in actions_range:
                num_actions = num_actions * dim
        self._num_actions = num_actions

        # expand features from hidden_state
        d = tf.keras.layers.Conv2D(num_conv_features, (1, 1), padding="same")(inputs)
        for _ in range(num_res_block):
          d = Conv2DResidualBlock(num_conv_features)(d)
        d = tf.keras.layers.Conv2D(16, (1, 1), padding="same")(d)
        
        p = self.compute_policy(d)

        v = tf.keras.layers.Flatten()(d)
        v = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(v)
        v = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(v)

        if value_support is None:
            v = tf.keras.layers.Dense(
              1, activation=tf.nn.tanh, name="prediction-output_value"
              )(v)
        else:
            full_support = value_support * 2 + 1
            v = tf.keras.layers.Dense(full_support, name="prediction-output_value")(v)

        super(NWConv2DPrediction, self).__init__(
            inputs = inputs,
            outputs = [p, v],
            name = name
        )

    def compute_policy(self, backbone) -> tf.Tensor:
        p = tf.keras.layers.Flatten()(backbone)
        p = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(p)
        p = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(p)
        p = tf.keras.layers.Dense(self._num_actions, name="prediction-output_policy")(p)
        return p


class NWConv2DPrediction_Tictactoe(NWConv2DPrediction):
    def compute_policy(self, backbone) -> tf.Tensor:
        p = tf.keras.layers.Conv2D(16, (1, 1))(backbone)
        p = tf.keras.layers.Conv2D(1, (1, 1))(p)
        p = tf.keras.layers.Flatten(name="prediction-output_policy")(p)
        return p
    

class NWConv2DDynamic(tf.keras.Model):
    """Abstract state transition"""

    def __init__(
        self,
        hidden_state_spec: tf.TensorSpec,
        action_spec: tf.TensorSpec,
        num_res_block: int,
        num_conv_features: int,
        reward_support: Optional[int] = None,
        name="Dynamic",
    ):
        input_h = tf.keras.Input(hidden_state_spec.shape, dtype=hidden_state_spec.dtype)
        input_a = tf.keras.Input(action_spec.shape, dtype=action_spec.dtype)
        h = tf.keras.layers.Conv2D(
            num_conv_features, (1, 1), padding="same"
        )(input_h)  # expand features from hidden_state
        d = self.concatenate_action(h, input_a)
        d = tf.keras.layers.Conv2D(
            num_conv_features, 3, padding="same", activation=tf.nn.leaky_relu
        )(d)
        for _ in range(num_res_block):
          d = Conv2DResidualBlock(num_conv_features)(d)

        output = tf.keras.layers.Conv2D(hidden_state_spec.shape[-1], (1, 1), padding="same")(d)
        output = tf.keras.layers.LayerNormalization()(output)
        output = tf.keras.layers.Activation(tf.keras.activations.tanh, name="dynamic-output_rp")(output)

        reward = tf.keras.layers.Conv2D(16, (1, 1), padding="same")(d)
        reward = tf.keras.layers.Flatten()(reward)
        reward = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(reward)
        reward = tf.keras.layers.Dense(256, activation=tf.nn.leaky_relu)(reward)
        if reward_support is None:
            reward = tf.keras.layers.Dense(
                    1, activation=tf.nn.tanh, name="dynamic-output_reward"
                )(reward)
        else:
            full_support = reward_support * 2 + 1
            reward = tf.keras.layers.Dense(full_support, name="dynamic-output_reward")(reward)

        super(NWConv2DDynamic, self).__init__(
            inputs=(input_h, input_a),
            outputs=(output, reward),
            name=name,
        )

    def concatenate_action(self, hidden_state, action) -> tf.Tensor:
        raise NotImplemented


# Map flat action (index) to NN Dynamic network
class NWConv2DDynamic_C4(NWConv2DDynamic):
  def concatenate_action(self, hidden_state, action):
    a = tf.expand_dims(
      tf.tile(tf.expand_dims(tf.one_hot(action, 7), 1), (1, 6, 1)), -1
    )  # C4 one_hot decoding
    d = tf.keras.layers.Concatenate()([hidden_state, a])
    return d


class NWConv2DDynamic_Tictactoe(NWConv2DDynamic):
  def concatenate_action(self, hidden_state, action):
    batch_size = tf.shape(action)[0]
    a = tf.reshape(tf.one_hot(action, 9), (batch_size, 3, 3, 1))
    d = tf.keras.layers.Concatenate()([hidden_state, a])
    return d

class NWConv2DDynamic_Abalone(NWConv2DDynamic):
  def concatenate_action(self, hidden_state, action):
    batch_size = tf.shape(action)[0]
    a = tf.reshape(tf.one_hot(action, 9*9*6*5), (batch_size, 9, 9, 6*5))
    d = tf.keras.layers.Concatenate()([hidden_state, a])
    return d
