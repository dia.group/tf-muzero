import gin  # type: ignore
import numpy as np
import tensorflow as tf  # type: ignore
import tf_agents  # type: ignore
import tf_agents.trajectories.time_step as time_step  # type: ignore
import tf_agents.typing.types as types  # type: ignore
from typing import Any, Optional, Text, Tuple

# pylint: disable=relative-beyond-top-level
from .muzero.common import *
from .muzero.play import *
from .muzero.train import *
from .muzero.support import *


def action_inner_to_environment(
    action: tf.Tensor, action_spec: tf_agents.typing.types.NestedTensorSpec
) -> tf.Tensor:
    """
    convert action from inner (for MCTS) representation to environment one
    """
    action_shape = one_hot_shape(action_spec)
    if len(action_shape) == 1:
        action_out = action
    else:
        action_out = tf.stack(
            [action // action_shape[1], action % action_shape[1]], axis=-1
        )
    action_out = tf.cast(action_out, action_spec.dtype)
    tf_agents.utils.nest_utils.assert_same_structure(action_out, action_spec)
    return action_out


def action_environment_to_inner(
    action: tf.Tensor, action_spec: tf_agents.typing.types.NestedTensorSpec
) -> tf.Tensor:
    """
    convert action from environment representation to inner (MCTS) -- flatten space
    """
    tf_agents.utils.nest_utils.assert_same_structure(action, action_spec)
    action_shape = one_hot_shape(action_spec)
    if len(action_shape) == 1:
        action_out = action
    else:
        assert len(action_shape) == 2  # only 2D actions are supported
        action_out = action[:, :, 0] * action_shape[1] + action[:, :, 1]
    action_out = tf.cast(action_out, tf.int32)
    return action_out


class MuzeroPolicy(tf_agents.policies.tf_policy.TFPolicy):
    """Class to build Muzero Policies."""

    def __init__(
        self,
        config: MuZeroConfig,
        time_step_spec: time_step.TimeStep,
        action_spec: types.NestedTensorSpec,
        policy_state_spec: types.NestedTensorSpec = None,
        info_spec: types.NestedTensorSpec = None,
        name: Optional[Text] = None,
    ):

        self._action_space_size = int(np.prod(one_hot_shape(action_spec)))
        policy_state_spec = (
            policy_state_spec
            if policy_state_spec is not None
            else tf_agents.specs.TensorSpec(
                (), dtype=tf.int32, name="policy_state_spec"
            )
        )
        minimum = (
            config.known_bounds.min
            if config is not None and config.known_bounds is not None
            else -float("inf")
        )
        maximum = (
            config.known_bounds.max
            if config is not None and config.known_bounds is not None
            else float("inf")
        )
        self._info_spec = (
            info_spec
            if info_spec is not None
            else (
                tf_agents.specs.BoundedTensorSpec(
                    [self._action_space_size],
                    dtype=tf.int32,
                    minimum=0,
                    maximum=config.num_simulations,
                    name="policy",
                ),
                tf_agents.specs.BoundedTensorSpec(
                    [self._action_space_size],
                    dtype=tf.float32,
                    minimum=0.0,
                    maximum=1.0,
                    name="qvalue",
                ),
                tf_agents.specs.BoundedTensorSpec(
                    (), dtype=tf.float32, minimum=minimum, maximum=maximum, name="value"
                ),
            )
        )

        super(MuzeroPolicy, self).__init__(
            time_step_spec=time_step_spec,
            action_spec=action_spec,
            policy_state_spec=policy_state_spec,
            info_spec=self._info_spec,
            name=name,
        )
        self._config = config

    @property
    def action_space_size(self) -> int:
        return self._action_space_size

    def _get_initial_state(self, batch_size: int) -> types.NestedTensor:
        # Keep track of history length
        return tf.zeros(batch_size, tf.int32)


class MuzeroNetworkPolicy(MuzeroPolicy):
    def __init__(
        self,
        config: MuZeroConfig,
        time_step_spec: time_step.TimeStep,
        action_spec: types.NestedTensorSpec,
        muzero_network: MuzeroNetwork,
        training: bool = False,
        name: Optional[Text] = None,
    ):

        super(MuzeroNetworkPolicy, self).__init__(
            config=config,
            time_step_spec=time_step_spec,
            action_spec=action_spec,
            name=name,
        )

        # skip is_compatible_with due to mixed_precision type mismatch
        assert(tf.nest.map_structure(
          lambda t0, t1: t0.type_spec.shape.is_compatible_with(t1.type_spec.shape),
          muzero_network.nw_prediction.input, 
          muzero_network.nw_representation.output
          ))
        assert(tf.nest.map_structure(
          lambda t0, t1: t0.type_spec.shape.is_compatible_with(t1.type_spec.shape),
          muzero_network.nw_dynamic.input[0],
          muzero_network.nw_representation.output
          ))
        assert(tf.nest.map_structure(
          lambda t0, t1: t0.type_spec.shape.is_compatible_with(t1.type_spec.shape),
          muzero_network.nw_dynamic.output[0],
          muzero_network.nw_prediction.input
          ))

        self._muzero_network = muzero_network
        self._training = training
        min_max_stats = MinMaxStats.init(config.known_bounds)
        self._min_max_stats = MinMaxStats(
            tf.Variable(min_max_stats.minimum, dtype=tf.float32),
            tf.Variable(min_max_stats.maximum, dtype=tf.float32),
        )

    # def _distribution(
    #   self, time_step: time_step.TimeStep,
    #   policy_state: types.NestedTensorSpec
    #   ) -> policy_step.PolicyStep:
    #   pass

    def _action(
        self,
        time_step: time_step.TimeStep,
        policy_state: types.NestedTensorSpec,
        seed: Optional[types.Seed],
    ) -> tf_agents.trajectories.policy_step.PolicyStep:
        # # action;state;info
        observations = (
            time_step.observation
        )  # if tf.rank(time_step.observation) == 4 else tf.expand_dims(time_step.observation, -1)

        history_length = policy_state
        batch_size = tf.shape(time_step.step_type)[0]
        tf.debugging.assert_equal(tf.shape(policy_state)[0], batch_size)

        assert (
            self._config.tree_representation_storage == None
            or self._config.tree_representation_storage
            in (tf.float16, tf.bfloat16, tf.int8)
        )

        if not seed:
            seed = tf.random.get_global_generator().uniform([2], maxval=tf.int32.max, dtype=tf.int32)

        def map_state_spec(spec):
            return tf.TensorSpec(  # remove batch dim and set type from tree
                tf.squeeze(spec, 0).shape,
                spec.dtype if self._config.tree_representation_storage == None else self._config.tree_representation_storage,
            )

        hidden_state_spec = tf.nest.map_structure(
            map_state_spec, self._muzero_network.nw_prediction.input
        )

        if self._config.enable_action_masking:
          action_mask = tf.cast(tf.reshape(observations[-1], [tf.shape(observations[-1])[0], -1]), tf.float32)
          observations_unwrap = observations[0]
        else:
          action_mask = tf.ones([batch_size, self._action_space_size])
          observations_unwrap = observations

        assert(
          all([
            tf.nest.map_structure(
              lambda s, t: s.type_spec.is_compatible_with(t),
              self._muzero_network.nw_representation.input,
              observations_unwrap
              )
            ])
          )
        nw_output_raw = self._muzero_network.initial_inference(observations_unwrap)
        nw_output = NetworkOutput(
            nw_output_raw.value
            if self._config.support_size_value is None
            else support_to_scalar(
                tf.nn.softmax(nw_output_raw.value),
                self._config.support_size_value,
                self._config.support_scale_value,
            ),
            nw_output_raw.reward
            if self._config.support_size_reward is None
            else support_to_scalar(
                tf.nn.softmax(nw_output_raw.reward),
                self._config.support_size_reward,
                self._config.support_scale_reward,
            ),
            nw_output_raw.policy_logits,
            nw_output_raw.hidden_state,
        )
        new_history_length = history_length + 1

        if True:
          tree = Tree.init(
              batch_size,
              self._action_space_size,
              self._config.num_simulations,
              hidden_state_spec,
          )

          tree = expand_node(
              tree,
              tree.rootNode(),
              self._config.to_play(history_length),
              action_mask,
              nw_output,
              self._config.action_space_size,
          )

          if self._training:
              tree = add_exploration_noise(
                 config=self._config,
                 prnd=seed,
                 tree=tree,
                 action_mask=action_mask)

          # We then run a Monte Carlo Tree Search using only action sequences and the
          # model learned by the network.
          if self._config.known_bounds:
              min_max_stats = MinMaxStats.init(self._config.known_bounds)
          else:
              min_max_stats = MinMaxStats(
                  self._min_max_stats.minimum.value(),
                  self._min_max_stats.maximum.value(),
              )
          
          tree, _ = run_mcts(
              config=self._config,
              tree=tree,
              history_depth=history_length,
              network=self._muzero_network,
              prnd=seed,
              action_mask=action_mask,
              min_max_stats=min_max_stats,
          )
          temperature = self._config.visit_softmax_temperature_fn(history_length, self._muzero_network.training_steps())
          action = select_action(
              tree=tree,
              action_count=self._config.action_space_size,
              temperature=temperature,
              prnd=seed,
          )

          # if self._config.action_masking:
          #   tf.debugging.assert_equal(tf.reduce_all(tf.gather(action_mask, action, axis=1, batch_dims=1)>0.0), tf.constant(True))

          # cf store_search_statistics
          root_values = Tree.value(tree, tree.rootNode())
          node_indices = Tree.nodeChildren(
              tree.rootNode(), self._config.action_space_size
          )
          child_visits = tf.gather(tree.visit_count, node_indices, axis=1, batch_dims=1)
          q_values = tf.where(self._config.to_play(history_length)==self._config.to_play(history_length+1), 1.0, -1.0)[..., None] * \
              Tree.value(tree, node_indices) + \
              tf.gather(tree.reward, node_indices, axis=1, batch_dims=1)

          # Post-checking
          tf.debugging.assert_all_finite(
            q_values, "invalid q_values for policy computing"
          )
          tf.debugging.assert_all_finite(
              root_values, "invalid root_values for policy computing"
          )

          action_out = action_inner_to_environment(action, self.action_spec)
          infos_out = (child_visits, q_values, root_values)
          assert all(
              s.is_compatible_with(d[0]) for s, d in zip(self._info_spec, infos_out)
          )  # wrap out batch dimension to check specs

          ret_classic = tf_agents.trajectories.policy_step.PolicyStep(
          action_out, new_history_length, (child_visits, q_values, root_values)
          )

        return ret_classic


class MuzeroRandomPolicy(MuzeroPolicy):
    def __init__(
        self,
        config: MuZeroConfig,
        time_step_spec: time_step.TimeStep,
        action_spec: types.NestedTensorSpec,
        name: Optional[Text] = None,
    ):
        super(MuzeroRandomPolicy, self).__init__(
            config=config,
            time_step_spec=time_step_spec,
            action_spec=action_spec,
            name=name,
        )

    def _action(
        self,
        time_step: time_step.TimeStep,
        policy_state: types.NestedTensorSpec,
        seed: Optional[types.Seed],
    ) -> tf_agents.trajectories.policy_step.PolicyStep:
        outer_dims = tf_agents.utils.nest_utils.get_outer_shape(
            time_step, self._time_step_spec
        )

        history_length = policy_state
        new_history_length = history_length + 1

        if self._config.enable_action_masking:
            assert(isinstance(time_step.observation, tuple))
            action_mask = tf.cast(time_step.observation[-1], tf.float32)
            distribution = action_mask * tf.random.stateless_uniform(
                tf.shape(action_mask),
                seed=seed if seed else tf.random.get_global_generator().uniform([2], maxval=tf.int32.max, dtype=tf.int32)
                )

            flat_distribution = tf.reshape(
              distribution,
              [tf.shape(distribution)[0], -1],
              )
            
            action = tf.argmax(flat_distribution, axis=-1, output_type=tf.int32)

            action_shape = one_hot_shape(self.action_spec)
            if len(action_shape)>1:
              action = tf.transpose(tf.unravel_index(action, action_shape))

            flat_distribution /= tf.expand_dims(
                tf.reduce_sum(flat_distribution + 0.00001, -1), -1
            )
            policy_info = (
                tf.cast(tf.math.round(flat_distribution * self._config.num_simulations), tf.int32),
                flat_distribution,
                # tf_agents.specs.tensor_spec.sample_spec_nest(
                #     self._info_spec[1], seed=seed, outer_dims=outer_dims
                # ),
                tf.zeros(outer_dims),
            )
        else:
            action = tf_agents.specs.tensor_spec.sample_spec_nest(
                self._action_spec, seed=seed, outer_dims=outer_dims
            )
            policy_info = tf_agents.specs.tensor_spec.sample_spec_nest(
                self._info_spec, outer_dims=outer_dims
            )
            policy_info = (
                policy_info[0]+1,  # then sum > 0
                policy_info[1],
                tf.zeros(outer_dims),  # force value to 0
            )

        assert(self.action_spec.is_compatible_with(action[0]))
        assert(self.policy_state_spec.is_compatible_with(new_history_length[0]))
        assert all(
            s.is_compatible_with(d[0])
            for s, d in zip(self.info_spec, policy_info)
        )  # wrap out batch dimension to check specs
        return tf_agents.trajectories.policy_step.PolicyStep(
            action, new_history_length, policy_info
        )


# https://github.com/tensorflow/agents/blob/master/tf_agents/agents/tf_agent.py
@gin.configurable
class MuzeroAgent(tf_agents.agents.TFAgent):
    """A Muzero Agent.

    Implements the Muzero algorithm from
    https://arxiv.org/abs/1911.08265
    """

    def __init__(
        self,
        time_step_spec: time_step.TimeStep,
        action_spec: types.TensorSpec,  # types.NestedTensorSpec,
        muzero_network: MuzeroNetwork,
        config: MuZeroConfig,
        optimizer: Optional[types.Optimizer] = None,
        summarize_grads_and_vars: bool = False,
        train_step_counter: Optional[tf.Variable] = None,
        name: Optional[Text] = None,
    ):
        """
        Create a muZero Agent

        name: The name of this agent. All variables in this module will fall
          under that name. Defaults to the class name.
        """
        tf.Module.__init__(self, name=name)
        # self._check_action_spec(action_spec)
        # self._actor_network = q_network
        # isinstance(q_network, network.Network)

        collect_policy = MuzeroNetworkPolicy(
            config=config,
            time_step_spec=time_step_spec,
            action_spec=action_spec,
            muzero_network=muzero_network,
            training=True,
        )

        policy = MuzeroNetworkPolicy(
            config=config,
            time_step_spec=time_step_spec,
            action_spec=action_spec,
            muzero_network=muzero_network,
            training=False,
        )

        super(MuzeroAgent, self).__init__(
            time_step_spec,
            action_spec,
            policy=policy,
            collect_policy=collect_policy,
            train_sequence_length=None,
            num_outer_dims=2,
            summarize_grads_and_vars=summarize_grads_and_vars,
            train_step_counter=train_step_counter,
        )

        self._muzero_network = muzero_network
        self._optimizer = optimizer
        self._config = config

    def _preprocess_sequence(
        self, experience: types.NestedTensor
    ) -> types.NestedTensor:
        return experience  # not supported by episode replay buffer ???

    def _train(
        self,
        experience: types.NestedTensor,
        weights: types.Tensor = 1.0,
        **kwargs,
    ) -> Tuple[tf_agents.agents.tf_agent.LossInfo, Any]:

        if weights is None:
            weights = tf.ones(())

        # tf_agents.utils.nest_utils.assert_same_structure(experience, self.time_step_spec)

        assert self.time_step_spec.reward.is_compatible_with(
            experience.reward[0, 0]
        )  # unwrap batch and time dimensions
        if tf.nest.is_nested(experience.observation):
            assert(
              all(
                tf.nest.map_structure(
                  lambda s, t: s.is_compatible_with(t[0, 0]),
                  self.time_step_spec.observation,
                  experience.observation)
                )
              )
        else:
            assert self.time_step_spec.observation.is_compatible_with(
                experience.observation[0, 0]
            )

        (rb_child_visits, rb_qvalue, rb_value) = experience.policy_info

        tf.debugging.assert_all_finite(
            rb_qvalue, "invalid qvalue from experience"
        )
        # tf.debugging.assert_less(tf.math.abs(tf.reduce_sum(rb_child_visits, -1)[..., :3] - 1.0), 0.001)  # TODO: fix -- test softmax on first elements as padding break this rule
        tf.debugging.assert_all_finite(rb_value, "invalid value from experience")
        tf.debugging.assert_all_finite(
            experience.reward, "invalid rewards from experience"
        )

        tf.debugging.assert_equal(
            tf.argmax(
                experience.step_type == time_step.StepType.FIRST,
                axis=1,
                output_type=tf.int32,
            ),
            0,
            "First step_type is not type FIRST",
        )
        tf.debugging.assert_greater(
            tf.argmax(
                experience.step_type == time_step.StepType.MID,
                axis=1,
                output_type=tf.int32,
            ),
            0,
            "No step type MID found in step_type",
        )
        tf.debugging.assert_equal(
            tf.argmax(
                experience.next_step_type == time_step.StepType.MID,
                axis=1,
                output_type=tf.int32,
            ),
            0,
            "First next_step_type is not type MID",
        )
        tf.debugging.assert_greater(
            tf.argmax(
                experience.next_step_type == time_step.StepType.LAST,
                axis=1,
                output_type=tf.int32,
            ),
            0,
            "No step type LAST found in next_step_type",
        )

        history_length = tf.argmax(
            experience.step_type == time_step.StepType.LAST,
            axis=1,
            output_type=tf.int32,
        )
        game = GameXp(
            history=experience.action,
            rewards=experience.reward,
            history_length=history_length,
            child_visits=rb_child_visits,
            q_values=rb_qvalue,
            root_values=rb_value,
        )

        seed = kwargs.get(
            "seed", tf.random.get_global_generator().uniform([2], maxval=tf.int32.max, dtype=tf.int32)
        )
        num_unroll_steps = self._config.num_unroll_steps
        game_pos = self._config.sample_position_fn(
            game, num_unroll_steps, self._optimizer.iterations
        )
        position = kwargs.get(
            "position", None
        )  # if we provide a valid position, override it
        game_pos = position if position is not None else game_pos
        if self._config.enable_action_masking:
            observations_unwrap = experience.observation[0]
        else:
            observations_unwrap = experience.observation
        batch_obs = tf.nest.map_structure(
            lambda obs: tf.gather(
                obs,
                tf.math.minimum(
                    tf.shape(obs)[1]-1,
                    game_pos[:, None] + tf.range(num_unroll_steps+1)[None]
                ),
                axis=1,
                batch_dims=1
            ),
            observations_unwrap,
        )

        # final state is absorbing: random action to learn that any action has effect anymore
        actions_padding = tf.random.stateless_uniform(
            tf.shape(experience.action),
            seed=seed,
            maxval=self._config.action_space_size,
            dtype=experience.action.dtype,
        )
        actions_padding = action_environment_to_inner(actions_padding, self.action_spec)
        actions_from_experience = action_environment_to_inner(
            experience.action, self.action_spec
        )
        actions = tf.where(
            tf.expand_dims(tf.range(tf.shape(actions_from_experience)[1]), 0)
            < tf.expand_dims(history_length, -1),
            actions_from_experience,
            actions_padding,
        )

        # extract actions for batch
        history_index = tf.expand_dims(game_pos, -1) + tf.expand_dims(
            tf.range(num_unroll_steps), 0
        )
        batch_actions = tf.transpose(
            tf.gather(
                tf.pad(actions, [[0, 0], [0, num_unroll_steps]]),
                history_index,
                axis=1,
                batch_dims=1,
            ),
            [1, 0],
        )

        # TODO: projetter les valeurs sur l'espace d'action + hotmap observation (voir knn pour les archétypes de situation->action et vice versa)
        # tf.summary.histogram('replay_buffer_observation', experience.observation, step=self._optimizer.iterations)  # tf.reduce_mean(experience.observation, axis=[0,1,-1])
        # tf.summary.histogram('replay_buffer_action', experience.action, step=self._optimizer.iterations)
        # tf.summary.histogram('replay_buffer_value', rb_value, step=self._optimizer.iterations)
        # tf.summary.histogram('replay_buffer_reward', experience.reward, step=self._optimizer.iterations)
        # tf.summary.histogram('replay_buffer_policy', rb_child_visits, step=self._optimizer.iterations)

        batch_value, batch_pc, batch_reward, batch_policy, batch_qvalue = Game.make_target(
            game, game_pos, self._config
        )

        batch = (batch_obs, batch_actions, batch_value, batch_pc, batch_reward, batch_policy, batch_qvalue)
        ret = update_weights(
            optimizer=self._optimizer,
            network=self._muzero_network,
            batch=batch,
            weights=weights,
            config=self._config,
        )

        # agent has no feedbacks from collect, so update here
        min_max_stats = MinMaxStats(
            self.collect_policy._min_max_stats.minimum.value(),
            self.collect_policy._min_max_stats.maximum.value(),
            )
        min_max_stats.update(tf.reduce_min(batch_value))
        min_max_stats.update(tf.reduce_max(batch_value))
        self.collect_policy._min_max_stats.minimum.assign(min_max_stats.minimum)
        self.collect_policy._min_max_stats.maximum.assign(min_max_stats.maximum)
        self.policy._min_max_stats.minimum.assign(min_max_stats.minimum)
        self.policy._min_max_stats.maximum.assign(min_max_stats.maximum)

        if self._optimizer.iterations % 50 == 0:
            # tf.summary.histogram('gradients', sum(tf.reduce_mean(g) for g in ret.grads if g is not None), step=optimizer.iterations)
            """
            dynamic_grads = next(g for (g,v) in zip(grads, network.trainable_variables) if v.name == network.dynamic.trainable_variables[0].name)
            tf.summary.histogram('dynamic_head_grads', dynamic_grads, step=optimizer.iterations)
            prediction_grads = next(g for (g,v) in zip(grads, network.trainable_variables) if v.name == network.prediction.trainable_variables[0].name)
            tf.summary.histogram('prediction_head_grads', prediction_grads, step=optimizer.iterations)
            representation_grads = next(g for (g,v) in zip(grads, network.trainable_variables) if v.name == network.representation.trainable_variables[0].name)
            tf.summary.histogram('representation_head_grads', representation_grads, step=optimizer.iterations)
            """
            tf.summary.histogram("actions", actions, step=self._optimizer.iterations)

            tf.summary.histogram(
                "value_targets", batch_value, step=self._optimizer.iterations
            )
            
            if self._config.enable_path_consistency:
                tf.summary.histogram(
                    "value_pc_targets", batch_pc, step=self._optimizer.iterations
                )
            
            tf.summary.histogram(
                "value_predictions",
                ret.value_predictions,
                step=self._optimizer.iterations,
            )

            tf.summary.histogram(
                "qvalue_targets", batch_qvalue, step=self._optimizer.iterations
            )

            tf.summary.histogram(
                "reward_targets", batch_reward, step=self._optimizer.iterations
            )
            tf.summary.histogram(
                "reward_predictions",
                ret.reward_predictions,
                step=self._optimizer.iterations,
            )

            tf.summary.histogram(
                "policy_targets", batch_policy, step=self._optimizer.iterations
            )
            tf.summary.histogram(
                "policy_predictions",
                ret.policy_predictions,
                step=self._optimizer.iterations,
            )

            if tf.nest.is_nested(ret.hidden_state_representation):
                for i, h in enumerate(tf.nest.flatten(ret.hidden_state_representation)):
                    tf.summary.histogram(
                        "hidden_state_rep_" + str(i), h, step=self._optimizer.iterations
                    )
            else:
                tf.summary.histogram(
                    "hidden_state_rep",
                    ret.hidden_state_representation,
                    step=self._optimizer.iterations,
                )

            if tf.nest.is_nested(ret.hidden_state_dynamic):
                for i, h in enumerate(tf.nest.flatten(ret.hidden_state_dynamic)):
                    tf.summary.histogram(
                        "hidden_state_dyn_" + str(i), h, step=self._optimizer.iterations
                    )
            else:
                tf.summary.histogram(
                    "hidden_state_dyn",
                    ret.hidden_state_dynamic,
                    step=self._optimizer.iterations,
                )

        # Incrementing the step counter.
        self.train_step_counter.assign_add(1)

        # Returning 0 loss.
        return tf_agents.agents.tf_agent.LossInfo(
            {
                "policy_loss": ret.policy_loss,
                "value_loss": ret.value_loss,
                "pc_loss": ret.pc_loss,
                "reward_loss": ret.reward_loss,
                "dyn_loss": ret.dynamic_loss,
                "l2_loss": ret.l2_loss,
            },
            ret.priorities
        )


def _pad_tensor(t, pad_length):
  t_paddings = tf.zeros([tf.rank(t)-1, 2], tf.int32)
  t_paddings = tf.concat([[[0, pad_length]], t_paddings], axis=0)
  out = tf.pad(t, t_paddings)
  return out


@tf.function(jit_compile=True)
def episode_pad_fn(x: tf_agents.trajectories.trajectory.Trajectory, y, max_actions: int):
  step_count = tf.shape(x.step_type)[0]
  if tf.executing_eagerly():
    tf.debugging.assert_less_equal(step_count, max_actions+1)
  pad_length = max_actions+1 - step_count

  step_type = tf.pad(x.step_type, [[0, pad_length]])
  observation = tf.nest.map_structure(lambda t: _pad_tensor(t, pad_length), x.observation)

  # fill every other dimensions with zeros
  action_paddings = tf.zeros([tf.rank(x.action)-1, 2], tf.int32)
  action_paddings = tf.concat([[[0, pad_length]], action_paddings], axis=0)
  action = tf.pad(x.action, action_paddings)

  tf.debugging.assert_rank(x.policy_info[0], 2)
  tf.debugging.assert_rank(x.policy_info[1], 2)
  tf.debugging.assert_rank(x.policy_info[2], 1)

  policy_info_0 = tf.pad(x.policy_info[0], [[0, pad_length], [0, 0]])
  policy_info_1 = tf.pad(x.policy_info[1], [[0, pad_length], [0, 0]])
  policy_info_2 = tf.pad(x.policy_info[2], [[0, pad_length]])

  next_step_type = tf.pad(x.next_step_type, [[0, pad_length]])
  reward = tf.pad(x.reward, [[0, pad_length]])
  discount = tf.pad(x.discount, [[0, pad_length]])
  
  return tf_agents.trajectories.trajectory.Trajectory(step_type, observation, action, (policy_info_0, policy_info_1, policy_info_2), next_step_type, reward, discount), y


