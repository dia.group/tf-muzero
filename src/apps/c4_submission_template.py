# ===== predict function ======

rep_res_block = 4
dyn_res_block = 4
pred_res_block = 8
num_features = 64
num_conv_features = 128
rp_node_type = tf.float32
max_steps = 6 * 7

action_spec = tf_agents.specs.BoundedTensorSpec(
    [], tf.int32, minimum=0, maximum=6, name="action"
)
action_mask_spec = tf_agents.specs.BoundedTensorSpec(
    [7], tf.uint8, minimum=0, maximum=6, name="action"
)
observation_spec = tf_agents.specs.BoundedTensorSpec(
    [6, 7, 3], tf.uint8, minimum=0, maximum=1, name="observation"
)
reward_spec = tf_agents.specs.BoundedTensorSpec(
    (), tf.float32, minimum=0.0, maximum=1.0, name="reward"
)

hidden_state_specs = tf_agents.specs.TensorSpec(
    (*observation_spec.shape[:-1], num_features), rp_node_type
)

time_step_spec = time_step.time_step_spec((observation_spec, action_mask_spec), reward_spec)

def visit_softmax_temperature(num_moves, training_steps):
  return 0.0

"""
  with num_simulations at 80, meets some time out on kaggle (>2s)
  local test on 3 runs was 1.42 / 1.30 / 1.30 s (runs on Ryzen 1700 CPU)
  => set at 70 (fails but don't know why)
  => set at 60
"""
config = MuZeroConfig(
    ## common parameters
    player_turn=2,  # C4 is a 2 player step per game state
    action_space_size=int(np.prod(one_hot_shape(action_spec))),
    known_bounds=KnownBounds(-1, 1),
    support_size_value=1,
    support_scale_value=False,
    support_size_reward=1,
    support_scale_reward=False,
    ## play parameters
    discount=0.96,
    num_simulations=60,
    visit_softmax_temperature_fn=visit_softmax_temperature,
    enable_action_masking=True,
    pb_c_init=1.0,
)

nw_rep = NWConv2DRepresentation(
    observation_spec, rep_res_block, num_conv_features, num_features
)
nw_pred = NWConv2DPrediction(
    hidden_state_specs,
    action_spec,
    pred_res_block,
    num_conv_features,
    value_support=config.support_size_value,
)
nw_dyn = NWConv2DDynamic_C4(
    hidden_state_specs,
    action_spec,
    dyn_res_block,
    num_conv_features,
    reward_support=config.support_size_reward,
)

muzero_network = MuzeroNetwork(config, nw_rep, nw_pred, nw_dyn)

tf_agent = MuzeroAgent(
    time_step_spec=time_step_spec,
    action_spec=action_spec,
    muzero_network=muzero_network,
    config=config,
)
tf_agent.initialize()


def muzero_agent(observation, configuration):
    print(observation)  # {board: [...], mark: 1}
    print(configuration)  # {rows: 10, columns: 8, inarow: 5}
    board = tf.reverse(tf.reshape(tf.convert_to_tensor(observation.board, tf.uint8), (6, 7)), [0])
    #board = tf.reverse(board, [1])
    board_tf = tf.stack([
      tf.cast(board==1, tf.uint8), 
      tf.cast(board==2, tf.uint8), 
      tf.fill([6,7], tf.cast(observation.mark-1, tf.uint8))  # player [1;2] => [0;1]
    ], axis=-1)

    ts = tf_agents.trajectories.time_step.transition(
      observation=(board_tf[None,...], tf.cast((board[-1]==0)[None], tf.uint8)),
      reward=tf.zeros((1))
    )
    step = tf_agent.policy.action(
      time_step = ts,
      policy_state = tf.fill((1), tf.cast(observation.step, tf.int32)),
      )

    action = int(step.action[0])

    return action

if not _test:
  in_memory = io.BytesIO(decoded)
  load_model(muzero_network, in_memory)


if _test:
  # import kaggle
  # pip install kaggle-environments
  from kaggle_environments import make

  env = make("connectx", debug=True)
  # env = make(
  #     "connectx",
  #     {"rows": 6, "columns": 7, "inarow": 4, "actTimeout": 10, "agentTimeout": 30},
  # )

  # print doc
  dir(env)
  # help(env.reset)
  # Print schemas from the specification.
  print(env.specification.observation)
  print(env.specification.configuration)
  print(env.specification.action)
  # The list of available default agents.
  print(*env.agents)

  #env.render(mode="human")

  # Run an episode using the agent above vs the default random agent.
  import time
  start_time = time.time()
  #env.run([muzero_agent, "random"])
  #env.run([muzero_agent, "negamax"])
  env.run([muzero_agent, muzero_agent])
  stage_time = time.time()-start_time
  print(f"time spent {stage_time:.4f}s -- avg {stage_time/len(env.steps):.4f}s")
  print("Success!" if env.state[0].status == env.state[1].status == "DONE" else "Failed...")


