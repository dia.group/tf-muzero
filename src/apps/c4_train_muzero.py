import datetime
from functools import partial
import gin
import importlib.util
import logging
import mlflow
import numpy as np
import os
import psutil
import shutil
import sys
if not os.getcwd() in sys.path:
  sys.path.append(os.getcwd())
import time
import tempfile
import tensorflow as tf  # type: ignore
import tf_agents   # type: ignore
from tf_agents import specs
from tf_agents.drivers import dynamic_episode_driver
from tf_agents.replay_buffers import episodic_replay_buffer
from tf_agents.trajectories import trajectory
from tf_agents.environments import suite_gym
from tf_agents.environments import tf_py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import batched_py_environment
from tf_agents.metrics import tf_metrics
from typing import Dict

# locals
from src.agents.tfagents.muzero_agent import *
from src.agents.tfagents.muzero.networks import *
from src.agents.tfagents.muzero.common import *
from src.agents.tfagents.muzero.reanalyze import *
from src.agents.tfagents.muzero.tools import vicreg
from src.envs.connect4.refmoves_kaggle import *


@gin.configurable
def tf_config():
  physical_devices = tf.config.experimental.list_physical_devices("GPU")
  for gpu in physical_devices:
    tf.config.experimental.set_memory_growth(gpu, True)
  if len(physical_devices) > 0:
    tf.config.set_visible_devices(physical_devices[0], 'GPU')
  # tf.config.set_visible_devices([], 'GPU')
  tf.config.set_soft_device_placement(True)
  #tf.debugging.set_log_device_placement(True)
  #tf.data.experimental.enable_debug_mode()
  #tf.config.run_functions_eagerly(True) # debug
  tf.config.optimizer.set_jit(True)


def create_env(env_fn: callable, env_kwargs: Dict, max_steps: int, batch_size: int = 1):
  tf_env = env_fn(**env_kwargs)
  if isinstance(tf_env, tf_environment.TFEnvironment):
    tf_env = env_fn(batch_size=batch_size, **env_kwargs)
  else:
    py_env = batched_py_environment.BatchedPyEnvironment(
        [suite_gym.wrap_env(env_fn(**env_kwargs)) for _ in range(batch_size)], multithreading=False)
    #py_env = parallel_py_environment.ParallelPyEnvironment([lambda : suite_gym.wrap_env(env_fn()) for _ in range(batch_size)])
    tf_env = tf_py_environment.TFPyEnvironment(py_env)

  return tf_env


def sample_position(game: GameXp, num_unroll_steps: int, training_step: int) -> int:
  # Sample position from game either uniformly or according to some priority.
  high = game.history_length
  low = 0 # uniform
  # low = tf.math.maximum(0, high-1-num_unroll_steps -
  #                       tf.cast(training_step//100, tf.int32))  # bootstarp at the end
  r = low + tf.cast(tf.random.uniform(tf.shape(high), 0, 1)
                    * tf.cast(high-low, tf.float32), tf.int32)
  tf.assert_less(r, game.history_length)
  return r


def visit_softmax_temperature(num_moves, training_steps, training_steps_max):
  # https://medium.com/oracledevs/lessons-from-alphazero-part-3-parameter-tweaking-4dceb78ed1e5
  # 
  # cf https://arxiv.org/pdf/1911.08265.pdf p31:
  # "Specifically, for the first 500k training steps a temperature of 1 is used, for the next 250k steps a temperature of 0.5 and for the remaining 250k a temperature of 0.25. This ensures that the action selection becomes greedier as training progresses.""
  if training_steps<training_steps_max * 2 // 4:
    tau = 1.0
  elif training_steps<training_steps_max * 3 // 4:
    tau = 0.5
  else:
    tau = 0.25

  # r = tf.range(tf.size(num_moves), dtype=tf.float32) / tf.size(num_moves, out_type=tf.float32)
  # tau = tau * r

  # top config:
  # if training_steps<training_steps_max * 1 // 3:
  #   tau = 0.25
  # else:
  #   tau = 0.0

  tau = tf.where(
    num_moves < 30,
    tau,
    0.0
  )
  return tau


@gin.configurable
def create_agent(
    tf_env,
    max_steps: int,
    rep_res_block: int = 2,
    dyn_res_block: int = 2,
    pred_res_block: int = 2,
    num_features: int = 16,
    num_conv_features: int = 24,
    learning_rate: int = 0.01,
    train_steps: int = 2,
    mixed_precision: bool = False,
):

  mlflow.log_param("rep_res_block", rep_res_block)
  mlflow.log_param("dyn_res_block", dyn_res_block)
  mlflow.log_param("pred_res_block", pred_res_block)
  mlflow.log_param("num_features", num_features)
  mlflow.log_param("num_conv_features", num_conv_features)
  mlflow.log_param("learning_rate", learning_rate)
  mlflow.log_param("mixed_precision", mixed_precision)

  if mixed_precision:
    tf.keras.mixed_precision.set_global_policy('mixed_float16')

  config = MuZeroConfig(
    player_turn=2,   # C4 is a 2 player step per game state
    action_space_size=int(
        np.prod(one_hot_shape(tf_env.action_spec()))),
    visit_softmax_temperature_fn=partial(visit_softmax_temperature, training_steps_max=train_steps),
    known_bounds=KnownBounds(-1, 1),
    td_steps=max_steps,
    sample_position_fn=sample_position,
    pb_c_init=4.0,  # https://medium.com/oracledevs/lessons-from-alphazero-part-3-parameter-tweaking-4dceb78ed1e5#8e97
    # tree_representation_storage = tf.float16,
    tree_representation_storage = tf.int8,
    enable_action_masking = tf_env.enable_action_masking,
  )

  for field, value in zip(config._fields, config):
    if not callable(value):
      mlflow.log_param(field, value)

  rp_node_type = tf.float32
  #rp_node_type = tf.float16
  obs_specs = tf_env.observation_spec()[0] if config.enable_action_masking else tf_env.observation_spec()
  nw_rep = NWConv2DRepresentation(
    obs_specs,
    rep_res_block,
    num_conv_features,
    num_features,
    )
  latent_state_specs = tf.nest.map_structure(
    lambda t: specs.TensorSpec(shape=t.shape, dtype=rp_node_type),
    tf_agents.utils.nest_utils.unbatch_nested_tensors(nw_rep.output))
  nw_pred = NWConv2DPrediction(
    latent_state_specs,
    tf_env.action_spec(),
    pred_res_block,
    num_conv_features,
    value_support=config.support_size_value,
    )
  nw_dyn = NWConv2DDynamic_C4(
    latent_state_specs,
    tf_env.action_spec(),
    dyn_res_block,
    num_conv_features,
    reward_support=config.support_size_reward,
    )

  
  projector = tf.keras.Sequential(
    [
      tf.keras.layers.Input(shape=latent_state_specs.shape),
      tf.keras.layers.Flatten(),
      tf.keras.layers.Dense(2048, activation="leaky_relu", name="layer1"),
      tf.keras.layers.Dense(2048, activation="leaky_relu", name="layer2"),
      tf.keras.layers.Dense(2048, name="layer3"),
    ]
  )
  
  # cf https://arxiv.org/src/1911.08265v2/anc/pseudocode.py :
  lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    learning_rate,
    decay_steps=0.4*train_steps,  # original paper 400e3 for 1M steps
    decay_rate=0.1)

  # lr_schedule = tf.keras.optimizers.schedules.CosineDecay(
  #   initial_learning_rate=learning_rate,
  #   decay_steps=train_steps
  # )
  

  optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)
  if mixed_precision:
    optimizer = tf.keras.mixed_precision.LossScaleOptimizer(optimizer)

  muzero_network=MuzeroNetwork(
    config=config,
    representation=nw_rep,
    prediction=nw_pred,
    dynamic=nw_dyn,
    # dynamic_regularisation=tf.keras.losses.Huber(),
    dynamic_regularisation=vicreg.VicReg(),
    # dynamic_regularisation_projector=projector,
    optimizer=optimizer,
  )

  tf_agent = MuzeroAgent(
    time_step_spec=tf_env.time_step_spec(),
    action_spec=tf_env.action_spec(),
    muzero_network=muzero_network,
    config=config,
    optimizer=optimizer,
  )
  tf_agent.initialize()

  return tf_agent


def reanalyze_fn(py_client, table_name: str, trajectory_count: int, train_agent: MuzeroAgent):
  from src.agents.tfagents.muzero import reverb_binding

  # pull samples from replay buffer
  raw_data = py_client.sample(table=table_name, num_samples=trajectory_count)
  trajectories = [(traj[0].info.key, tf.nest.map_structure(lambda t: tf.convert_to_tensor(t), traj[0].data)) for traj in raw_data]
  trajectories = [(k, reverb_binding.trajectory_from_data_list(v, train_agent.collect_data_spec)) for k,v in trajectories]

  # Tuple with (Trajectory, Position)
  observation = tf.stack([v[0].observation for k, v in trajectories])
  positions = tf.stack([v[1] for k, v in trajectories])
  observation_step = tf.gather(observation, positions, axis=1, batch_dims=1)

  # call inner reanalyse
  new_policy_step, new_value_step = reanalyze_step(
      observation_step, positions, train_agent._muzero_network, train_agent._config)

  indices2D = tf.stack([tf.range(trajectory_count), positions], -1)
  policies = tf.stack([v[0].policy_info[0] for k,v in trajectories])
  values = tf.stack([v[0].policy_info[1] for k,v in trajectories])
  new_policies = tf.tensor_scatter_nd_update(policies, indices2D, new_policy_step)
  new_values = tf.tensor_scatter_nd_update(values, indices2D, new_value_step)

  # write new steps
  for traj_idx, (_, traj) in enumerate(trajectories):
    #policy, value = traj[0].policy_info
    #policy[positions[traj_idx]] = new_policy_step[traj_idx]
    new_traj = [
        # trajectory.Trajectory
        traj[0].step_type,
        traj[0].observation,
        traj[0].action,
        (new_policies[traj_idx], new_values[traj_idx]),
        traj[0].next_step_type,
        traj[0].reward,
        traj[0].discount,
        # position
        traj[1]]  

    py_client.insert(
        data=new_traj,
        priorities={table_name: 1.0},
    )

  # drop old trajectories
  py_client.mutate_priorities(table_name, deletes=[k for k,v in trajectories])


# apply random left<->right flip
@tf.function(jit_compile=True)
def c4_data_augmentation(x: trajectory.Trajectory, y):
  """
    x: tf_agent trajectory
    y:
  """
  batch_size = tf.shape(x.step_type)[0]
  flip = tf.random.uniform(batch_size[None])>0.5
  if isinstance(x.observation, tuple):
    observation = (
      tf.where(flip[:, None, None, None, None], tf.reverse(x.observation[0], [3]), x.observation[0]),
      tf.where(flip[:, None, None], tf.reverse(x.observation[1], [2]), x.observation[1]),
    )
  else:
    observation = tf.where(flip[:, None, None, None, None], tf.reverse(x.observation[0], [3]), x.observation[0])

  action = tf.where(flip[:, None], 6-x.action, x.action)
  policy_info_0 = tf.where(flip[:, None, None], tf.reverse(x.policy_info[0], [2]), x.policy_info[0])  # policy
  policy_info_1 = tf.where(flip[:, None, None], tf.reverse(x.policy_info[1], [2]), x.policy_info[1])  # q_value
  policy_info_2 = x.policy_info[2]  # value

  return trajectory.Trajectory(
    x.step_type,
    observation,
    action,
    (policy_info_0, policy_info_1, policy_info_2),
    x.next_step_type,
    x.reward,
    x.discount
    ), y


@gin.configurable("entrypoint")
def train_eval(
    out_path: str = 'out',
    run_name: str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S"),
    run_prefix: str = '',
    env_fn: callable = gin.REQUIRED,
    env_kwargs: Dict = {},
    restore_model_path: str = '',
    collect_new_xp: bool = True,
    collect_batch_size: int = 4,
    eval_batch_size: int = 4,
    train_batch_size: int = 4,
    train_steps: int = 10000,
    train_steps_per_iteration: int = 100,
    max_steps: int = 6*7,  # C4 max steps
    replay_buffer_capacity: int = 100000,
    replay_buffer_prefill: int = 1024,
    replay_buffer_prefill_policy: str = 'random',
    replay_buffer_reverb: bool = False,
    reanalyze_traj:bool = False,
    policy_checkpoint_interval: int = 5000,
    rb_checkpoint_interval: int = 5000,
    use_tf_functions: bool = False,
):

  if out_path == None:
    out_path = tempfile.gettempdir()
  out_path = os.path.join(os.path.expanduser(out_path),
                          'c4', run_prefix+run_name)
  os.makedirs(out_path)

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      filename=os.path.join(out_path, 'muzero.log'),
                      filemode='w')
  logging.getLogger().addHandler(logging.StreamHandler())
  logging.info(f"Summary writer to {out_path}")

  shutil.copyfile(os.path.realpath(__file__), os.path.join(
      out_path, os.path.basename(__file__)))

  if reanalyze_traj:
    # reverb replay buffer is mandatory
    assert(replay_buffer_reverb)

  #tf.random.set_seed(123456)  # easier run comparison -- disable for real training!
  with mlflow.start_run(run_name=run_name):
    mlflow.log_artifacts(out_path)
    mlflow.log_param("restore_model_path", restore_model_path)
    mlflow.log_param("collect_new_xp", collect_new_xp)
    mlflow.log_param("collect_batch_size", collect_batch_size)
    mlflow.log_param("eval_batch_size", eval_batch_size)
    mlflow.log_param("train_batch_size", train_batch_size)
    mlflow.log_param("train_steps", train_steps)
    mlflow.log_param("train_steps_per_iteration", train_steps_per_iteration)
    mlflow.log_param("max_steps", max_steps)
    mlflow.log_param("replay_buffer_capacity", replay_buffer_capacity)
    mlflow.log_param("replay_buffer_prefill", replay_buffer_prefill)
    mlflow.log_param("replay_buffer_prefill_policy",
                    replay_buffer_prefill_policy)
    mlflow.log_param("replay_buffer_reverb", replay_buffer_reverb)
    mlflow.log_param("reanalyze_traj", reanalyze_traj)
    mlflow.log_param("policy_checkpoint_interval", policy_checkpoint_interval)
    mlflow.log_param("rb_checkpoint_interval", rb_checkpoint_interval)
    mlflow.log_param("use_tf_functions", use_tf_functions)

    train_sample_ratio = (
        train_batch_size*train_steps_per_iteration) / (collect_batch_size*max_steps)
    logging.info(f'(train / collected sample)  ratio : {train_sample_ratio:.4f}')
    if train_sample_ratio <= 2:
      logging.warning(
          f'(train / collected sample)  ratio is low! ({train_sample_ratio:.4f})')
    elif train_sample_ratio >= 100:
      logging.warning(
          f'(train / collected sample)  ratio is high! ({train_sample_ratio:.4f})')

    summary_writer = tf.summary.create_file_writer(out_path)
    summary_writer.set_as_default()

    tf_env = create_env(env_fn, env_kwargs, max_steps, collect_batch_size)

    logging.info("tf_env.observation_spec:" + str(tf_env.observation_spec()))
    logging.info("tf_env.action_spec: " + str(tf_env.action_spec()))
    logging.info("tf_env.batch_size: " + str(tf_env.batch_size))

    train_agent = create_agent(
        tf_env, max_steps=max_steps, train_steps=train_steps)

    logging.info("agent time_step_spec (describing the observation and reward signatures of the environment this agent's policies operate in):\n"+str(train_agent.time_step_spec))
    logging.info("agent training_data_spec (describes the structure expected of the `experience` argument passed to `train`):\n" +
                str(train_agent.training_data_spec))

    # now that everything has been created we log used values
    with tf.io.gfile.GFile(os.path.join(out_path, "config.gin"), 'w') as f:
      f.write(gin.operative_config_str())

    train_agent._muzero_network.nw_representation.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_prediction.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_dynamic.summary(print_fn=logging.info)

    if importlib.util.find_spec("pyplot"):
      tf.keras.utils.plot_model(train_agent._muzero_network.nw_representation, to_file=os.path.join(out_path,'model_rep.png'), show_shapes=True, rankdir='TR') # TB for vertical & LR horizontal
      tf.keras.utils.plot_model(train_agent._muzero_network.nw_prediction, to_file=os.path.join(out_path, 'model_pred.png'), show_shapes=True, rankdir='TR')
      tf.keras.utils.plot_model(train_agent._muzero_network.nw_dynamic, to_file=os.path.join(out_path, 'model_dyn.png'), show_shapes=True, rankdir='TR')
    else:
      logging.info("install pyplot & graphviz to get network plots")

    optimizer = train_agent._optimizer

    env_steps = tf_metrics.EnvironmentSteps(name='step_count', prefix='collect')
    collect_average_return = tf_metrics.AverageReturnMetric(
        name='mean_reward',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    collect_average_ep_length = tf_metrics.AverageEpisodeLengthMetric(
        name='mean_ep_length',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    chosen_action = tf_metrics.ChosenActionHistogram(
        buffer_size=tf_env.batch_size,
    )
    collect_metrics = [
        tf_metrics.NumberOfEpisodes(name='ep_count', prefix='collect'),
        env_steps,
        collect_average_return,
        collect_average_ep_length,
        #chosen_action,
    ]

    collect_policy = train_agent.collect_policy
    eval_policy = train_agent.policy

    # train_checkpointer = common.Checkpointer(
    #   ckpt_dir=os.path.join(out_path, 'train'),
    #   agent=train_agent,
    #   global_step=optimizer.iterations,
    #   metrics=metric_utils.MetricsGroup(collect_metrics, 'collect_metrics'))
    # train_checkpointer.initialize_or_restore()

    # policy
    policy_checkpointer = tf_agents.utils.common.Checkpointer(
      ckpt_dir=os.path.join(out_path, 'policy'),
      max_to_keep=1,
      policy=eval_policy,
      global_step=optimizer.iterations)
    policy_checkpointer.initialize_or_restore()

    restore_model_filename = os.path.join(restore_model_path, 'latest_model.npz')
    #restore_model_filename = os.path.join(restore_model_path, 'model_{:012}.npz'.format(20000))
    if os.path.exists(restore_model_filename):
      try:
        load_model(train_agent._muzero_network, restore_model_filename)
      except Exception as e:
        logging.warning("Fails to load model: "+str(e))

    # replay buffer
    if replay_buffer_reverb:
      import reverb   # type: ignore
      from src.agents.tfagents.muzero import reverb_binding

      replay_buffer_PER = False
      table_name = 'episode_table'
      reverb_server, reverb_table_episode, reverb_tfclient, reverb_client = reverb_binding.create_replay_buffer(
          table_name, train_agent.collect_data_spec, max_steps=max_steps, replay_buffer_capacity=replay_buffer_capacity, use_per=replay_buffer_PER)
      PER_insert_priority = 1.0

      """
      # tf-agents way: fails to missing batch collect
      reverb_table_episode = reverb.Table(
          table_name,
          max_size=replay_buffer_capacity,
          sampler=reverb.selectors.Uniform(),
          remover=reverb.selectors.Fifo(),
          rate_limiter=reverb.rate_limiters.MinSize(1),
          # signature=train_agent.collect_data_spec,
          )

      reverb_server = reverb.Server([reverb_table_episode])

      reverb_replay = tf_agents.replay_buffers.ReverbReplayBuffer(
        train_agent.collect_data_spec,
        sequence_length=max_steps,
        table_name=table_name,
      )

      dataset = reverb_replay.as_dataset(
        sample_batch_size=tf_env.batch_size, num_steps=2).prefetch(50)
      """

      # traj_obs = reverb_binding.ReverbAddTFEpisodeObserver(
      #     reverb_tfclient,
      #     [table_name],
      #     collect_data_spec=train_agent.collect_data_spec,
      #     batch_size=tf_env.batch_size,
      #     max_steps=max_steps,
      #     insert_priority=PER_insert_priority,
      # )

      traj_obs = reverb_binding.ReverbAddPyEpisodeObserver(
          reverb_client,
          [table_name],
          collect_data_spec=train_agent.collect_data_spec,
          batch_size=tf_env.batch_size,
          max_steps=max_steps,
          insert_priority=PER_insert_priority,
      )

      #dataset = reverb.trajectory_dataset.TrajectoryDataset.from_table_signature(
      dataset = reverb.TimestepDataset.from_table_signature(
          server_address=f'localhost:{reverb_server.port}',
          table=table_name,
          max_in_flight_samples_per_worker=train_batch_size*4)
      dataset = dataset.shuffle(buffer_size=replay_buffer_capacity//4)
      dataset = dataset.batch(train_batch_size)
      dataset = dataset.prefetch(4)
    else:
      replay_buffer = episodic_replay_buffer.EpisodicReplayBuffer(
          completed_only=True,
          data_spec=train_agent.collect_data_spec,
          capacity=replay_buffer_capacity
      )

      rb_dir = os.path.join(out_path, 'replay_buffer')
      restore_rb_path = os.path.join(restore_model_path, 'replay_buffer')
      if os.path.exists(restore_rb_path):
        if not os.path.exists(rb_dir):
          os.makedirs(rb_dir)
        for filename in os.listdir(restore_rb_path):
          shutil.copyfile(src=os.path.join(restore_rb_path, filename),
                          dst=os.path.join(rb_dir, filename))
      rb_checkpointer = tf_agents.utils.common.Checkpointer(
          ckpt_dir=rb_dir,
          max_to_keep=1,
          replay_buffer=replay_buffer)
      rb_checkpointer.initialize_or_restore()

      traj_obs = episodic_replay_buffer.StatefulEpisodicReplayBuffer(
          replay_buffer, num_episodes=tf_env.batch_size).add_batch

      dataset = replay_buffer.as_dataset(num_parallel_calls=tf.data.AUTOTUNE)
      # TODO: add issue as unfinished episodes seems to be returned
      dataset = dataset.filter(
        lambda xp, y: tf.reduce_any(xp.step_type == tf_agents.trajectories.time_step.StepType.LAST))
      dataset = dataset.map(
        map_func=lambda x, y: episode_pad_fn(x, y, max_steps),
        num_parallel_calls=tf.data.AUTOTUNE
        )
      dataset = dataset.batch(
        batch_size=train_batch_size,
        num_parallel_calls=tf.data.AUTOTUNE
        )
      dataset = dataset.map(
        map_func=lambda x, y: c4_data_augmentation(x, y),
        num_parallel_calls=tf.data.AUTOTUNE
        )
      dataset = dataset.prefetch(4)

    num_episodes = tf_env.batch_size
    collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
        tf_env,
        collect_policy,
        observers=[traj_obs] + collect_metrics,
        num_episodes=num_episodes)

    iterator = iter(dataset)
    logging.info("datatset element_spec: "+str(dataset.element_spec))

    ref_observation, ref_moves = load_ref_moves()

    @tf.function()
    def train_fn(train_agent, it, steps: int):
      train_loss = (0.0, 0.0, 0.0, 0.0, 0.0)
      for _ in tf.range(steps):
        if replay_buffer_reverb:
          buffer_info, raw_data = next(it)
          experience, position = reverb_binding.trajectory_from_data_dict(
              raw_data, train_agent.collect_data_spec)
          probabilities = tf.cast(buffer_info.priority, tf.float32)
        else:
          experience, buffer_info = next(it)
          position = None
          probabilities = tf.ones(tf.shape(buffer_info))
        losses = train_agent.train(experience, position=position).loss
        train_loss = (
          train_loss[0]+losses["policy_loss"],
          train_loss[1]+losses["value_loss"],
          train_loss[2]+losses["pc_loss"],
          train_loss[3]+losses["reward_loss"],
          train_loss[4]+losses["dyn_loss"]
          )

      return tuple(l/steps for l in train_loss), {}

    if use_tf_functions:
      collect_driver.run = tf_agents.utils.common.function(collect_driver.run)
      #train_step = tf_agents.utils.common.function(train_step) # Warning

    """
    checkpointed = tf.train.Checkpoint(rb=replay_buffer)
    replay_dir = os.path.join(tempfile.gettempdir(), "replay")
    if not os.path.exists(replay_dir):
      os.makedirs(replay_dir)
    checkpointed.save(os.path.join(replay_dir, "replay"))
    replay_buffer.clear()
    checkpointed.restore(os.path.join(replay_dir, "replay-1"))
    #replay_buffer.get_next()
    """

    # initiate replay buffer
    init_policy_dict = {
        "random": MuzeroRandomPolicy(config=train_agent._config, time_step_spec=tf_env.time_step_spec(), action_spec=tf_env.action_spec()),
        "collect": collect_policy,
        "eval": eval_policy,
    }
    init_policy = init_policy_dict[replay_buffer_prefill_policy]
    initial_collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
      tf_env,
      init_policy,
      observers=[traj_obs] + collect_metrics,
      num_episodes=num_episodes)

    current_env_state = tf_env.reset()
    current_policy_state = init_policy.get_initial_state(tf_env.batch_size)

    replay_buffer_prefill = min(replay_buffer_prefill, replay_buffer_capacity)
    nb_loop = (replay_buffer_prefill+tf_env.batch_size-1)//tf_env.batch_size
    for loop in range(nb_loop):
      logging.info(f"filling replaybuffer... ({loop+1}/{nb_loop})")
      start_time = time.time()
      current_env_state, current_policy_state = initial_collect_driver.run(
          time_step=current_env_state, policy_state=current_policy_state)
      stage_time = time.time()-start_time
      logging.info(f"Time; collect; s; {stage_time:.4f}")
    if nb_loop > 0:
      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")
        mlflow.log_metric(f"collect/metric/{collect_metric.name}",
                          collect_metric.result().numpy())
      tf.summary.histogram(
          "collect/ep_length", collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)

      logging.info(f"train")
      (p_loss, v_loss, pc_loss, r_loss, dyn_loss), _ = train_fn(
          train_agent, iterator, train_steps_per_iteration)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_dyn', dyn_loss, step=optimizer.iterations)
      logging.info(
          f"p_loss/v_loss/pc_loss/r_loss/dyn_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}/{dyn_loss.numpy():.7f}")

    while optimizer.iterations <= (train_steps-train_steps_per_iteration):
      logging.info(f"Run \'{run_name}\' on step {optimizer.iterations.numpy()}")

      # Collect Scope
      logging.info("collect")
      start_time = time.time()
      start_step_count = env_steps.result().numpy()
      if collect_new_xp:
        try:
          current_env_state, current_policy_state = collect_driver.run(
            time_step=current_env_state, policy_state=current_policy_state)
        except Exception as e:
          save_model(train_agent._muzero_network, os.path.join(out_path, 'model_crash.npz'))
          raise e
      stage_time = time.time()-start_time
      tf.summary.scalar("time/collect/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; collect; s; {stage_time:.4f}")
      step_count = env_steps.result().numpy() - start_step_count
      tf.summary.scalar("time/collect/fps", step_count /
                        (stage_time+1e-8), step=optimizer.iterations)
      logging.info(f"Time; collect; fps; {step_count/(stage_time+1e-8):.4f}")

      if replay_buffer_reverb:
        logging.info(
            f"replay_buffer contains {reverb_table_episode.info.num_episodes} episodes")
        mlflow.log_metric("replay_buffer/episodes",
                  float(reverb_table_episode.info.num_episodes))
        tf.summary.scalar('replay_buffer/num_frames',
                          reverb_table_episode.info.current_size, step=optimizer.iterations)
        tf.summary.scalar('replay_buffer/episodes',
                          reverb_table_episode.info.num_episodes, step=optimizer.iterations)
      else:
        logging.info(
            f"replay_buffer contains {len(replay_buffer._completed_episodes())} episodes or {replay_buffer.num_frames()} frames")
        tf.summary.scalar('replay_buffer/num_frames',
                          replay_buffer.num_frames(), step=optimizer.iterations)
        tf.summary.scalar('replay_buffer/episodes',
                          len(replay_buffer._completed_episodes()), step=optimizer.iterations)
      # eps = [replay_buffer._get_episode(ep) for ep in replay_buffer._completed_episodes()]

      # Reanalysis
      if reanalyze_traj:
        logging.info("reanalyze")
        start_time = time.time()
        reanalyze_fn(reverb_client, table_name,
                  tf_env.batch_size, train_agent)
        stage_time = time.time() - start_time
        tf.summary.scalar("time/reanalyze/s", stage_time, step=optimizer.iterations)

      # Train Scope
      logging.info("train")
      start_time = time.time()
      try:
        (p_loss, v_loss, pc_loss, r_loss, dyn_loss), _ = train_fn(
            train_agent, iterator, train_steps_per_iteration)
      except Exception as e:
          save_model(train_agent._muzero_network, os.path.join(out_path, 'model_crash.npz'))
          raise e
      logging.info(
          f"{optimizer.iterations.numpy()} - train {train_steps_per_iteration} steps")
      tf.summary.histogram("collect/ep_length",
                          collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/train/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; train; s; {stage_time:.4f}")
      tf.summary.scalar("time/train/ups", train_steps_per_iteration /
                        stage_time, step=optimizer.iterations)
      logging.info(
          f"Time; train; ups; {train_steps_per_iteration/stage_time:.4f}")

      # Eval Scope
      logging.info("eval")
      start_time = time.time()
      perfect_move_count_nn, good_move_count_nn, good_value_count_nn = compute_ref_score(
          ref_observation, ref_moves, train_agent._muzero_network, train_agent._config)
      tf.summary.scalar('eval/perfect_move_count', perfect_move_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/good_move_count', good_move_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/good_value_count', good_value_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      perfect_move_count_mcts, good_move_count_mcts, good_value_count_mcts = compute_ref_score_mcts(
          ref_observation, ref_moves, train_agent._muzero_network, train_agent._config)
      tf.summary.scalar('eval/mcts/perfect_move_count', perfect_move_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/mcts/good_move_count', good_move_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/mcts/good_value_count', good_value_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/eval/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; eval; s; {stage_time:.4f}")

      # Debug Scope
      logging.info("debug_log")
      start_time = time.time()
      if isinstance(optimizer.learning_rate, tf.Tensor) or isinstance(optimizer.learning_rate, tf.Variable):
        tf.summary.scalar('train/learning_rate',
                          optimizer.learning_rate, step=optimizer.iterations)
      elif isinstance(optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
        tf.summary.scalar('train/learning_rate', optimizer.learning_rate(
            optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/softmax_temperature', tf.reduce_mean(train_agent._config.visit_softmax_temperature_fn(
          tf.zeros(tf_env.batch_size), optimizer.iterations)), step=optimizer.iterations)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_dyn', dyn_loss, step=optimizer.iterations)
      logging.info(
          f"p_loss/v_loss/pc_loss/r_loss/dyn_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}/{dyn_loss.numpy():.7f}")

      process = psutil.Process(os.getpid())
      tf.summary.scalar(f'mem/cpu', process.memory_info().rss /
                        (2**30), optimizer.iterations)

      if int(tf.version.VERSION.split('.')[0]) >= 2 and int(tf.version.VERSION.split('.')[1]) >= 5:
        for gpu in tf.config.list_logical_devices('GPU'):
          mem_infos = tf.config.experimental.get_memory_info(gpu.name)
          for name, info in mem_infos.items():
            tf.summary.scalar(f'mem/gpu{gpu.name}/{name}',
                              info/(2**30), step=optimizer.iterations)

      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")

      debug_render = []
      debug_observation = []
      debug_perfect_moves = []
      if len(debug_perfect_moves) == 0:
        # eval (connect4.gamesolver.org)
        debug_moves = [
            [3, [-2, -1,  0,  1,  0, -1, -2]],
            [3, [-4, -2, -2, -1, -2, -2, -4]],
            [3, [-3, -3, -2,  1, -2, -3, -3]],
            [3, [-4, -4, -3, -1, -3, -4, -4]],
            [3, [-2, -2, -2,  1, -2, -2, -2]],

            [3, [-1, -1, -1, -1, -1, -1, -1]],
            [2, [-1,  0,  1, -99,  1,  0, -1]],
            [1, [-16, -1, -16, -99, -2, -16, -16]],
            [5, [-1,  0, -1, -99, -1,  1, -2]],
            [4, [-16, -16, -16, -99, -1, -16, -16]],
        ]

        debug_env = create_env(env_fn, env_kwargs, max_steps)
        debug_env.reset()
        for move, weights in debug_moves:
          debug_render.append(tf.cast(debug_env.render(), tf.float32) / 255.0)
          debug_observation.append(debug_env.state_observation.value())
          debug_env.step(tf.convert_to_tensor(move)[None])
          tf_weights = tf.convert_to_tensor(weights, dtype=tf.float32)
          debug_perfect_moves.append(tf_weights)

        target_render = tf.concat(debug_render, axis=0)
        perfect_moves = tf.stack(debug_perfect_moves)[:, None, :, None]
        perfect_moves_max = tf.expand_dims(tf.reduce_max(perfect_moves, 2), -1)
        perfect_moves = tf.clip_by_value(
            perfect_moves, perfect_moves_max-8, perfect_moves_max)
        perfect_moves = (perfect_moves-tf.expand_dims(tf.reduce_min(perfect_moves, 2), -1)) / \
            (perfect_moves_max+0.0001-tf.expand_dims(tf.reduce_min(perfect_moves, 2), -1))
        perfect_moves = tf.tile(perfect_moves, [1, 1, 1, 3])

      nw_output = train_agent._muzero_network.initial_inference(
          tf.concat(debug_observation, axis=0))
      predicted_moves = tf.nn.softmax(
          nw_output.policy_logits, -1)[:, None, :, None]
      predicted_moves = (predicted_moves-tf.expand_dims(tf.reduce_min(predicted_moves, 2), -1)) / (
          tf.expand_dims(tf.reduce_max(predicted_moves, 2), -1)-tf.expand_dims(tf.reduce_min(predicted_moves, 2), -1))
      predicted_moves = tf.tile(predicted_moves, [1, 1, 1, 3])
      tf.summary.image('eval/predicted_moves', tf.concat([target_render, perfect_moves,
                      predicted_moves], axis=1), step=optimizer.iterations, max_outputs=len(debug_moves))

      # data stats ?
      stage_time = time.time()-start_time
      tf.summary.scalar("time/debug_log", stage_time, step=optimizer.iterations)
      logging.info(f"Time; debug; s; {stage_time:.4f}")

      # Backup Scope
      logging.info("backup")
      start_time = time.time()
      if not replay_buffer_reverb:
        if rb_checkpoint_interval is not None and optimizer.iterations.numpy() % rb_checkpoint_interval == 0:
          rb_checkpointer.save(global_step=optimizer.iterations)
      if optimizer.iterations.numpy() % policy_checkpoint_interval == 0:
        model_name = 'model_{:012}.npz'.format(optimizer.iterations.numpy())
        save_model(
            train_agent._muzero_network, os.path.join(out_path, model_name))
        shutil.copyfile(os.path.join(out_path, model_name),
                        os.path.join(out_path, 'latest_model.npz'))
        policy_checkpointer.save(global_step=optimizer.iterations)
      stage_time = time.time()-start_time
      tf.summary.scalar("time/backup", stage_time, step=optimizer.iterations)
      logging.info(f"Time; backup; s; {stage_time:.4f}")
      mlflow.log_metric("time/backup", stage_time)
      tf.summary.flush()

  #mlflow.end_run()
  # print result for HP search
  #print(np.mean(log_average_return))

  return v_loss, r_loss, p_loss


def main(argv):
  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')
  tf_config()
  tf.config.run_functions_eagerly(True)  # debug
  #tf.config.optimizer.set_jit(True)

  # import src.envs.connect4gym as c4env
  # env_fn = lambda : c4env.Connect4GymEnv()
  env_kwargs = {"action_masking": True}

  import src.envs.connect4_tfa as c4env
  def env_fn(batch_size=4, **kwargs): return c4env.Connect4TFEnv(batch_size, **kwargs)

  gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.support_size_value', 1)
  gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.enable_action_masking', True)  

  # debug
  train_eval(
    env_fn=env_fn,
    env_kwargs=env_kwargs,
    train_steps=4,
    train_steps_per_iteration=2,
    replay_buffer_prefill=2,
    use_tf_functions=False,
    #reanalyze_traj=True, replay_buffer_reverb=True
    )

  # train
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.num_simulations', 50)
  # import src.apps.c4_train_muzero
  # gin.bind_parameter('src.apps.c4_train_muzero.create_agent.num_features', 64)
  # train_eval(env_fn=env_fn, num_features=64, collect_batch_size=1024, train_batch_size=512, train_steps=25000, train_steps_per_iteration=100, num_simulations=45, use_tf_functions=True)


if __name__ == '__main__':
  tf_agents.system.multiprocessing.handle_main(main)
