import datetime
import gin
import logging
import mlflow
import numpy as np
import os
import psutil
import shutil
import sys

if not os.getcwd() in sys.path:
  sys.path.append(os.getcwd())
import time
import tempfile
import tensorflow as tf  # type: ignore
import tf_agents   # type: ignore
from tf_agents import specs
from tf_agents.drivers import dynamic_episode_driver
from tf_agents.replay_buffers import episodic_replay_buffer
from tf_agents.environments import suite_gym
from tf_agents.environments import tf_py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import batched_py_environment
from tf_agents.metrics import tf_metrics
from typing import Dict


# locals
from src.agents.tfagents.muzero_agent import *
from src.agents.tfagents.muzero.networks import *
from src.agents.tfagents.muzero.common import *
from src.agents.tfagents.muzero.tools import vicreg
from src.envs.tictactoe.refmoves_tictactoe import *
from src.envs.tictactoe.utils import *


@gin.configurable
def tf_config():
  physical_devices = tf.config.experimental.list_physical_devices("GPU")
  for gpu in physical_devices:
    tf.config.experimental.set_memory_growth(gpu, True)
  if len(physical_devices) > 0:
    tf.config.set_visible_devices(physical_devices[0], 'GPU')
  # tf.config.set_visible_devices([], 'GPU')
  tf.config.set_soft_device_placement(True)
  #tf.debugging.set_log_device_placement(True)
  #tf.data.experimental.enable_debug_mode()
  # tf.config.run_functions_eagerly(True) # debug
  tf.config.optimizer.set_jit(True)


def create_env(env_fn: callable, env_kwargs:Dict, batch_size: int = 1):
  tf_env = env_fn(**env_kwargs)
  if isinstance(tf_env, tf_environment.TFEnvironment):
    tf_env = env_fn(batch_size=batch_size, **env_kwargs)
  else:
    py_env = batched_py_environment.BatchedPyEnvironment(
        [suite_gym.wrap_env(env_fn(**env_kwargs)) for _ in range(batch_size)], multithreading=False)
    tf_env = tf_py_environment.TFPyEnvironment(py_env)

  return tf_env


def sample_position(game: GameXp, num_unroll_steps: int, training_step: int) -> int:
  # Sample position from game either uniformly or according to some priority.
  high = game.history_length
  #low = 0 # uniform
  low = tf.math.maximum(0, high-1-num_unroll_steps -
                        tf.cast(training_step//100, tf.int32))  # bootstarp at the end
  r = low + tf.cast(tf.random.uniform(tf.shape(high), 0, 1)
                    * tf.cast(high-low, tf.float32), tf.int32)
  tf.assert_less(r, game.history_length)
  return r


def visit_softmax_temperature(num_moves, training_steps):
  return 1.0


@gin.configurable
def create_agent(
    tf_env,
    td_steps: int,
    rep_res_block: int = 2,
    dyn_res_block: int = 2,
    pred_res_block: int = 2,
    num_features: int = 16,
    num_conv_features: int = 24,
    learning_rate: int = 0.01,
    mixed_precision: bool = False,
):

  mlflow.log_param("rep_res_block", rep_res_block)
  mlflow.log_param("dyn_res_block", dyn_res_block)
  mlflow.log_param("pred_res_block", pred_res_block)
  mlflow.log_param("num_features", num_features)
  mlflow.log_param("num_conv_features", num_conv_features)
  mlflow.log_param("learning_rate", learning_rate)
  mlflow.log_param("mixed_precision", mixed_precision)

  if mixed_precision:
    tf.keras.mixed_precision.set_global_policy('mixed_float16')

  config = MuZeroConfig(
      player_turn=2,   # TicTacToe is a 2 player step per game state
      action_space_size=int(
          np.prod(one_hot_shape(tf_env.action_spec()))),
      visit_softmax_temperature_fn=visit_softmax_temperature,
      known_bounds=KnownBounds(-1, 1),
      td_steps=td_steps,
      sample_position_fn=sample_position,
      pb_c_init=4.0,
      # tree_representation_storage=tf.int8,
      tree_representation_storage=tf.float16,
      support_size_value=1,
      support_size_reward=1,
      root_dirichlet_adaptive=False,
      root_dirichlet_alpha=0.35,
      enable_action_masking=tf_env.enable_action_masking,
  )

  for field, value in zip(config._fields, config):
    if not callable(value):
      mlflow.log_param(field, value)

  rp_node_type = tf.float32
  #rp_node_type = tf.float16
  observation_spec = tf_env.observation_spec() if not config.enable_action_masking else tf_env.observation_spec()[0]

  # nw_rep = NWConv2DRepresentation(
  #     observation_spec,
  #     rep_res_block,
  #     num_conv_features,
  #     num_features
  #     )
  nw_rep = NWFCRepresentation(
    observation_spec,
    # num_features,
  )
  latent_state_specs = tf.nest.map_structure(
    lambda t: specs.TensorSpec(shape=t.shape, dtype=rp_node_type),
    tf_agents.utils.nest_utils.unbatch_nested_tensors(nw_rep.output))
  # nw_pred = NWConv2DPrediction_Tictactoe(
  #   latent_state_specs, tf_env.action_spec(),
  #   pred_res_block,
  #   num_conv_features,
  #   value_support=config.support_size_value
  #   )
  nw_pred = NWFCPrediction(
    latent_state_specs, tf_env.action_spec(),
    value_support=config.support_size_value,
  )
  # nw_dyn = NWConv2DDynamic_Tictactoe(
  #   latent_state_specs,
  #   tf_env.action_spec(),
  #   dyn_res_block,
  #   num_conv_features,
  #   reward_support=config.support_size_reward
  #   )
  nw_dyn = NWFCDynamic(
    latent_state_specs,
    tf_env.action_spec(),
    reward_support=config.support_size_reward
  )
  projector = tf.keras.Sequential(
    [
      tf.keras.layers.Input(shape=latent_state_specs.shape),
      tf.keras.layers.Flatten(),
      tf.keras.layers.Dense(2048, activation="leaky_relu", name="layer1"),
      tf.keras.layers.Dense(2048, activation="leaky_relu", name="layer2"),
      tf.keras.layers.Dense(2048, name="layer3"),
    ]
  )

  """
  lr_decay = 0.3
  lr_schedule = tf.keras.optimizers.schedules.PiecewiseConstantDecay(
    boundaries=[train_steps//2, train_steps*3//4],
    values=[learning_rate, learning_rate*lr_decay, learning_rate*(lr_decay**2)])
  """
  optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
  if mixed_precision:
    optimizer = tf.keras.mixed_precision.LossScaleOptimizer(optimizer)
  muzero_network = MuzeroNetwork(
      config=config,
      representation=nw_rep,
      prediction=nw_pred,
      dynamic=nw_dyn,
      # dynamic_regularisation=tf.keras.losses.Huber(),
      dynamic_regularisation=vicreg.VicReg(),
      # dynamic_regularisation_projector=projector,
      optimizer=optimizer,
      )

  tf_agent = MuzeroAgent(
      time_step_spec=tf_env.time_step_spec(),
      action_spec=tf_env.action_spec(),
      muzero_network=muzero_network,
      config=config,
      optimizer=optimizer,
  )
  tf_agent.initialize()

  return tf_agent


@gin.configurable("entrypoint")
def train_eval(
    out_path: str = 'out',
    run_name: str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S"),
    run_prefix: str = '',
    env_fn: callable = gin.REQUIRED,
    env_kwargs: Dict = {},
    restore_model_path: str = '',
    collect_new_xp: bool = True,
    collect_batch_size: int = 4,
    eval_batch_size: int = 4,
    train_batch_size: int = 4,
    train_steps: int = 10000,
    train_steps_per_iteration: int = 100,
    max_steps: int = 9,  # TicTacToe max steps
    replay_buffer_capacity: int = 100000,
    replay_buffer_prefill: int = 1024,
    policy_checkpoint_interval: int = 1000,
    rb_checkpoint_interval: int = 1000,
    use_tf_functions: bool = False,
):

  if out_path == None:
    out_path = tempfile.gettempdir()
  out_path = os.path.join(os.path.expanduser(out_path),
                          'tictactoe', run_prefix+run_name)
  os.makedirs(out_path)

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      filename=os.path.join(out_path, 'muzero.log'),
                      filemode='w')
  logging.getLogger().addHandler(logging.StreamHandler())
  logging.info(f"Summary writer to {out_path}")

  shutil.copyfile(os.path.realpath(__file__), os.path.join(
      out_path, os.path.basename(__file__)))

  #tf.random.set_seed(123456)  # easier run comparison -- disable for real training!
  with mlflow.start_run(run_name=run_name):
    mlflow.log_artifacts(out_path)
    mlflow.log_param("restore_model_path", restore_model_path)
    mlflow.log_param("collect_new_xp", collect_new_xp)
    mlflow.log_param("collect_batch_size", collect_batch_size)
    mlflow.log_param("eval_batch_size", eval_batch_size)
    mlflow.log_param("train_batch_size", train_batch_size)
    mlflow.log_param("train_steps", train_steps)
    mlflow.log_param("train_steps_per_iteration", train_steps_per_iteration)
    mlflow.log_param("max_steps", max_steps)
    mlflow.log_param("replay_buffer_capacity", replay_buffer_capacity)
    mlflow.log_param("replay_buffer_prefill", replay_buffer_prefill)
    mlflow.log_param("policy_checkpoint_interval", policy_checkpoint_interval)
    mlflow.log_param("rb_checkpoint_interval", rb_checkpoint_interval)
    mlflow.log_param("use_tf_functions", use_tf_functions)

    train_sample_ratio = (
        train_batch_size*train_steps_per_iteration) / (collect_batch_size*max_steps)
    logging.info(f'(train / collected sample)  ratio : {train_sample_ratio:.4f}')
    if train_sample_ratio <= 2:
      logging.warning(
          f'(train / collected sample)  ratio is low! ({train_sample_ratio:.4f})')
    elif train_sample_ratio >= 100:
      logging.warning(
          f'(train / collected sample)  ratio is high! ({train_sample_ratio:.4f})')

    summary_writer = tf.summary.create_file_writer(out_path)
    summary_writer.set_as_default()

    tf_env = create_env(env_fn, env_kwargs, collect_batch_size)

    logging.info("tf_env.observation_spec:" + str(tf_env.observation_spec()))
    logging.info("tf_env.action_spec: " + str(tf_env.action_spec()))
    logging.info("tf_env.batch_size: " + str(tf_env.batch_size))

    train_agent = create_agent(
        tf_env, td_steps=max_steps)

    logging.info("agent time_step_spec (describing the observation and reward signatures of the environment this agent's policies operate in):\n"+str(train_agent.time_step_spec))
    logging.info("agent training_data_spec (describes the structure expected of the `experience` argument passed to `train`):\n" +
                str(train_agent.training_data_spec))

    # now that everything has been created we log used values
    with tf.io.gfile.GFile(os.path.join(out_path, "config.gin"), 'w') as f:
      f.write(gin.operative_config_str())

    train_agent._muzero_network.nw_representation.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_prediction.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_dynamic.summary(print_fn=logging.info)

    optimizer = train_agent._optimizer

    env_steps = tf_metrics.EnvironmentSteps(name='step_count', prefix='collect')
    collect_average_return = tf_metrics.AverageReturnMetric(
        name='mean_reward',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    collect_average_ep_length = tf_metrics.AverageEpisodeLengthMetric(
        name='mean_ep_length',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    chosen_action = tf_metrics.ChosenActionHistogram(
        buffer_size=tf_env.batch_size,
    )
    collect_metrics = [
        tf_metrics.NumberOfEpisodes(name='ep_count', prefix='collect'),
        env_steps,
        collect_average_return,
        collect_average_ep_length,
        #chosen_action,
    ]

    collect_policy = train_agent.collect_policy
    eval_policy = train_agent.policy

    # train_checkpointer = common.Checkpointer(
    #   ckpt_dir=os.path.join(out_path, 'train'),
    #   agent=train_agent,
    #   global_step=optimizer.iterations,
    #   metrics=metric_utils.MetricsGroup(collect_metrics, 'collect_metrics'))
    # train_checkpointer.initialize_or_restore()

    # policy
    policy_checkpointer = tf_agents.utils.common.Checkpointer(
      ckpt_dir=os.path.join(out_path, 'policy'),
      max_to_keep=1,
      policy=eval_policy,
      global_step=optimizer.iterations)
    policy_checkpointer.initialize_or_restore()

    restore_model_filename = os.path.join(restore_model_path, 'latest_model.npz')
    #restore_model_filename = os.path.join(restore_model_path, 'model_{:012}.npz'.format(20000))
    if os.path.exists(restore_model_filename):
      try:
        load_model(train_agent._muzero_network, restore_model_filename)
      except Exception as e:
        logging.warning("Fails to load model: "+str(e))

    replay_buffer = episodic_replay_buffer.EpisodicReplayBuffer(
        completed_only=True,
        data_spec=train_agent.collect_data_spec,
        capacity=replay_buffer_capacity
    )

    rb_dir = os.path.join(out_path, 'replay_buffer')
    restore_rb_path = os.path.join(restore_model_path, 'replay_buffer')
    if os.path.exists(restore_rb_path):
      if not os.path.exists(rb_dir):
        os.makedirs(rb_dir)
      for filename in os.listdir(restore_rb_path):
        shutil.copyfile(src=os.path.join(restore_rb_path, filename),
                        dst=os.path.join(rb_dir, filename))
    rb_checkpointer = tf_agents.utils.common.Checkpointer(
        ckpt_dir=rb_dir,
        max_to_keep=1,
        replay_buffer=replay_buffer)
    rb_checkpointer.initialize_or_restore()

    traj_obs = episodic_replay_buffer.StatefulEpisodicReplayBuffer(
        replay_buffer, num_episodes=tf_env.batch_size).add_batch

    dataset = replay_buffer.as_dataset(num_parallel_calls=tf.data.AUTOTUNE)
    # TODO: add issue as unfinished episodes seems to be returned
    dataset = dataset.filter(
      lambda xp, y: tf.reduce_any(xp.step_type == tf_agents.trajectories.time_step.StepType.LAST))
    dataset = dataset.map(
      map_func=lambda x, y: episode_pad_fn(x, y, max_steps),
      num_parallel_calls=tf.data.AUTOTUNE)
    dataset = dataset.batch(
      batch_size=train_batch_size,
      num_parallel_calls=tf.data.AUTOTUNE)
    # dataset = dataset.map(
    #   map_func=lambda x, y: ttt_data_augmentation(x, y),
    #   num_parallel_calls=tf.data.AUTOTUNE)
    dataset = dataset.prefetch(4)

    num_episodes = tf_env.batch_size
    collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
        tf_env,
        collect_policy,
        observers=[traj_obs] + collect_metrics,
        num_episodes=num_episodes)

    iterator = iter(dataset)
    logging.info("datatset element_spec: "+str(dataset.element_spec))

    ref_observation, ref_moves = load_ref_moves()

    @tf.function()
    def train_fn(train_agent, it, steps: int):
      train_loss = (0.0, 0.0, 0.0, 0.0, 0.0)
      for _ in tf.range(steps):
        experience, _ = next(it)
        position = None
        losses = train_agent.train(experience, position=position).loss
        train_loss = (
          train_loss[0]+losses["policy_loss"],
          train_loss[1]+losses["value_loss"],
          train_loss[2]+losses["pc_loss"],
          train_loss[3]+losses["reward_loss"],
          train_loss[4]+losses["dyn_loss"],
          )

      return tuple(l/steps for l in train_loss), {}

    if use_tf_functions:
      collect_driver.run = tf_agents.utils.common.function(collect_driver.run)
      #train_step = tf_agents.utils.common.function(train_step) # Warning

    """
    checkpointed = tf.train.Checkpoint(rb=replay_buffer)
    replay_dir = os.path.join(tempfile.gettempdir(), "replay")
    if not os.path.exists(replay_dir):
      os.makedirs(replay_dir)
    checkpointed.save(os.path.join(replay_dir, "replay"))
    replay_buffer.clear()
    checkpointed.restore(os.path.join(replay_dir, "replay-1"))
    #replay_buffer.get_next()
    """

    init_policy = MuzeroRandomPolicy(config=train_agent._config, time_step_spec=tf_env.time_step_spec(), action_spec=tf_env.action_spec())
    initial_collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
      tf_env,
      init_policy,
      observers=[traj_obs] + collect_metrics,
      num_episodes=num_episodes)

    current_env_state = tf_env.reset()
    current_policy_state = init_policy.get_initial_state(tf_env.batch_size)

    replay_buffer_prefill = min(replay_buffer_prefill, replay_buffer_capacity)
    nb_loop = (replay_buffer_prefill+tf_env.batch_size-1)//tf_env.batch_size
    for loop in range(nb_loop):
      logging.info(f"filling replaybuffer... ({loop+1}/{nb_loop})")
      start_time = time.time()
      current_env_state, current_policy_state = initial_collect_driver.run(
          time_step=current_env_state, policy_state=current_policy_state)
      stage_time = time.time()-start_time
      logging.info(f"Time; collect; s; {stage_time:.4f}")
    if nb_loop > 0:
      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")
        mlflow.log_metric(f"collect/metric/{collect_metric.name}",
                        collect_metric.result().numpy())
      tf.summary.histogram(
          "collect/ep_length", collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)

      logging.info(f"train")
      (p_loss, v_loss, pc_loss, r_loss, dyn_loss), _ = train_fn(
          train_agent, iterator, train_steps_per_iteration)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_dyn', dyn_loss, step=optimizer.iterations)
      logging.info(f"p_loss/v_loss/pc_loss/r_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}")

    while optimizer.iterations <= train_steps:
      logging.info(f"Run \'{run_name}\' on step {optimizer.iterations.numpy()}")

      # Collect Scope
      logging.info("collect")
      start_time = time.time()
      start_step_count = env_steps.result().numpy()
      if collect_new_xp:
        try:
          current_env_state, current_policy_state = collect_driver.run(
            time_step=current_env_state, policy_state=current_policy_state)
        except Exception as e:
          save_model(train_agent._muzero_network, os.path.join(out_path, 'model_crash.npz'))
          raise e
      stage_time = time.time()-start_time
      tf.summary.scalar("time/collect/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; collect; s; {stage_time:.4f}")
      step_count = env_steps.result().numpy() - start_step_count
      tf.summary.scalar("time/collect/fps", step_count /
                        (stage_time+1e-8), step=optimizer.iterations)
      logging.info(f"Time; collect; fps; {step_count/(stage_time+1e-8):.4f}")

      logging.info(
          f"replay_buffer contains {len(replay_buffer._completed_episodes())} episodes or {replay_buffer.num_frames()} frames")
      tf.summary.scalar('replay_buffer/num_frames',
                        replay_buffer.num_frames(), step=optimizer.iterations)
      tf.summary.scalar('replay_buffer/episodes',
                        len(replay_buffer._completed_episodes()), step=optimizer.iterations)
      # eps = [replay_buffer._get_episode(ep) for ep in replay_buffer._completed_episodes()]

      # Train Scope
      logging.info("train")
      start_time = time.time()
      (p_loss, v_loss, pc_loss, r_loss, dyn_loss), _ = train_fn(
          train_agent, iterator, train_steps_per_iteration)
      logging.info(
          f"{optimizer.iterations.numpy()} - train {train_steps_per_iteration} steps")
      tf.summary.histogram("collect/ep_length",
                          collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/train/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; train; s; {stage_time:.4f}")
      tf.summary.scalar("time/train/ups", train_steps_per_iteration /
                        stage_time, step=optimizer.iterations)

      if isinstance(optimizer.learning_rate, tf.Tensor) or isinstance(optimizer.learning_rate, tf.Variable):
        tf.summary.scalar('train/learning_rate',
                          optimizer.learning_rate, step=optimizer.iterations)
      elif isinstance(optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
        tf.summary.scalar('train/learning_rate', optimizer.learning_rate(
            optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/softmax_temperature', train_agent._config.visit_softmax_temperature_fn(
          0, optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_dyn', dyn_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      logging.info(f"p_loss/v_loss/pc_loss/r_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}")

      tf.summary.scalar(
          'collect/value/minimum',
          collect_policy._min_max_stats.minimum,
          step=optimizer.iterations
          )
      tf.summary.scalar(
          'collect/value/maximum',
          collect_policy._min_max_stats.maximum,
          step=optimizer.iterations
          )
      tf.summary.scalar(
          'eval/value/minimum',
          eval_policy._min_max_stats.minimum,
          step=optimizer.iterations
          )
      tf.summary.scalar(
          'eval/value/maximum',
          eval_policy._min_max_stats.maximum,
          step=optimizer.iterations
          )

      process = psutil.Process(os.getpid())
      tf.summary.scalar(f'mem/cpu', process.memory_info().rss /
                        (2**30), optimizer.iterations)

      if int(tf.version.VERSION.split('.')[0]) >= 2 and int(tf.version.VERSION.split('.')[1]) >= 5:
        for gpu in tf.config.list_logical_devices('GPU'):
          mem_infos = tf.config.experimental.get_memory_info(gpu.name)
          for name, info in mem_infos.items():
            tf.summary.scalar(f'mem/gpu{gpu.name}/{name}',
                              info/(2**30), step=optimizer.iterations)

      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")

      logging.info(
          f"Time; train; ups; {train_steps_per_iteration/stage_time:.4f}")


      # Debug Scope
      logging.info("debug")
      start_time = time.time()

      #number of perfect and good moves
      good_move_count = compute_ref_score(
          ref_observation, ref_moves, train_agent._muzero_network)
      tf.summary.scalar('eval/error_percent', 100 - good_move_count*100 /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      good_move_count = compute_ref_score_mcts(
        ref_observation, ref_moves, train_agent._muzero_network, train_agent._config)
      tf.summary.scalar('eval/mcts/error_percent', 100 - good_move_count*100 /
                      tf.shape(ref_observation)[0], step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/eval/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; eval; s; {stage_time:.4f}")

      # visualiation of ref states
      debug_render = []
      debug_observation = []
      debug_perfect_moves = []
      if len(debug_perfect_moves) == 0:
        debug_moves = [
            [6, [0, 0, 0, 0, 0, 0, 0, 0, 0]],
            [4, [0, 0, 0, 0, 0, 0, -2, 0, 0]],
            [2, [0, 0, 0, 0, -2, 0, -2, 0, 0]],
            [1, [-1, 0, -2, 0, -2, 0, -2, 0, -1]],
            [7, [-1, -2, -2, -1, -2, -1, -2, 0, -1]],
            [8, [-1, -2, -2, -1, -2, -1, -2, -2, 0]],
            [0, [0, -2, -2, -1, -2, -1, -2, -2, -2]],
            [3, [-2, -2, -2, 0, -2, -1, -2, -2, -2]],
            [5, [-2, -2, -2, -2, -2, 0, -2, -2, -2]]]
        debug_env = create_env(env_fn, env_kwargs, batch_size=1)
        debug_env.reset()
        for move, weights in debug_moves:
          debug_render.append(tf.cast(debug_env.render(), tf.float32) /255)
          debug_observation.append(debug_env.state_observation.value())
          debug_env.step(tf.convert_to_tensor(move))
          tf_weights = tf.convert_to_tensor(weights, dtype=tf.float32)
          debug_perfect_moves.append(tf_weights)

        target_render = tf.concat(debug_render, axis=0)
        perfect_moves = tf.stack(debug_perfect_moves)[:, None, :, None]
        perfect_moves_max = tf.expand_dims(tf.reduce_max(perfect_moves, 2), -1)
        perfect_moves = tf.clip_by_value(
            perfect_moves, perfect_moves_max-8, perfect_moves_max)
        perfect_moves = (perfect_moves-tf.expand_dims(tf.reduce_min(perfect_moves, 2), -1)) / \
            (perfect_moves_max+0.0001-tf.expand_dims(tf.reduce_min(perfect_moves, 2), -1))
        perfect_moves = tf.tile(perfect_moves, [1, 1, 1, 3])

      nw_output = train_agent._muzero_network.initial_inference(
          tf.concat(debug_observation, axis=0))
      predicted_moves = tf.squeeze(nw_output.policy_logits)
      predicted_moves = tf.nn.softmax(
          predicted_moves, -1)[:, None, :, None]
      predicted_moves = (predicted_moves-tf.expand_dims(tf.reduce_min(predicted_moves, 2), -1)) / (
          tf.expand_dims(tf.reduce_max(predicted_moves, 2), -1)-tf.expand_dims(tf.reduce_min(predicted_moves, 2), -1))
      predicted_moves = tf.tile(predicted_moves, [1, 1, 1, 3])

      def render_tree(observation, config, network, batch_size, name, nb_sampled_actions):

          def map_state_spec(spec):
              return tf.TensorSpec(
                  spec.shape[1:],
                  spec.dtype
                  if config.tree_representation_storage == None
                  else config.tree_representation_storage,
              )

          if config.enable_action_masking :
              action_mask = tf.reshape(observation, [observation.shape[0], 2, 3, 3])
              action_mask = action_mask[:, 0, :, :] + action_mask[:, 1, :, :]
              action_mask = 1 - tf.reshape(action_mask, [action_mask.shape[0], -1])
              action_mask = tf.cast(action_mask, tf.float32)# only for tictactoe
          else :
              action_mask = tf.ones([batch_size, config.action_space_size])

          history_length = tf.fill(batch_size, 0)

          hidden_state_spec = tf.nest.map_structure(
              map_state_spec, network.nw_prediction.input
          )
          tree = Tree.init(
              batch_size,
              config.action_space_size,
              config.num_simulations,
              hidden_state_spec,
          )

          nw_output_raw = network.initial_inference(observation)
          nw_output = NetworkOutput(
              nw_output_raw.value
              if config.support_size_value is None
              else support_to_scalar(
                  tf.nn.softmax(nw_output_raw.value),
                  config.support_size_value,
                  config.support_scale_value,
              ),
              nw_output_raw.reward
              if config.support_size_reward is None
              else support_to_scalar(
                  tf.nn.softmax(nw_output_raw.reward),
                  config.support_size_reward,
                  config.support_scale_reward,
              ),
              nw_output_raw.policy_logits,
              nw_output_raw.hidden_state,
          )

          tree = expand_node(
              tree,
              tree.rootNode(),
              tf.fill(batch_size, 0),
              action_mask,
              nw_output,
              config.action_space_size,
          )

          tree = add_exploration_noise(config, tree, action_mask)
          min_max_stats = MinMaxStats.init(config.known_bounds)
          tree, min_max_stats = run_mcts(
              config=config,
              tree=tree,
              history_depth=history_length,
              network=network,
              prnd=tf.convert_to_tensor([0, 42]),
              min_max_stats=min_max_stats,
          )

          tree_render = tree.render(config.nb_sampled_actions, "", 1, "")

          root_values = Tree.value(tree, tree.rootNode())
          node_indices = Tree.nodeChildren(
              tree.rootNode(), nb_sampled_actions
          )
          visit_counts = tf.cast(
              tf.gather(tree.visit_count, node_indices, axis=1, batch_dims=1), tf.float32
          )
          sum_visits = tf.expand_dims(tf.reduce_sum(visit_counts, axis=-1), -1)
          child_visits = visit_counts / sum_visits

          child_indices = tf.gather(tree.selected_indices, node_indices, axis=1, batch_dims=1)

          with open(os.path.join(out_path, name), 'w') as fw:
              fw.write(tree_render)
          return root_values, child_visits, child_indices,
      """
      N = 50
      debug_observations = tf.concat([debug_observation[i] for i in tf.range(len(debug_observation))], 0)
      root_values, child_visits, child_indices = render_tree(
          tf.reshape(tf.tile(debug_observations[:, None, :], [1, N, 1, 1, 1]), [N*tf.shape(debug_observations)[0], tf.shape(debug_observations)[1], tf.shape(debug_observations)[2],tf.shape(debug_observations)[3] ]),
          train_agent._config,
          train_agent._muzero_network,
          N*tf.shape(debug_observations)[0],
          f'tree_move',
          train_agent._config.action_space_size)
      index = tf.concat([tf.reshape(tf.tile(tf.expand_dims(tf.range(N*tf.shape(debug_observations)[0]), 1), [1, train_agent._config.nb_sampled_actions]), (train_agent._config.nb_sampled_actions*N*tf.shape(debug_observations)[0], 1)), tf.reshape(child_indices, (train_agent._config.nb_sampled_actions*N*tf.shape(debug_observations)[0], 1))],1)
      mean_policy_sampling = tf.fill([N*tf.shape(debug_observations)[0], config.action_space_size], 0.0)
      policy_sampling = tf.tensor_scatter_nd_update(mean_policy_sampling, index,
                                                    tf.reshape(child_visits, train_agent._config.nb_sampled_actions * N * tf.shape(debug_observations)[0]))
      policy_sampling = tf.reshape(policy_sampling, [tf.shape(debug_observations)[0], N, tf.shape(policy_sampling)[1]])
      policy_sampling = tf.reduce_sum(policy_sampling, 1)/N
      policy_sampling = tf.reshape(policy_sampling, [9, 3, 3])
      policies_sampling = tf.expand_dims(policy_sampling, -1)
      policies_sampling = tf.tile(policies_sampling, [1, 1, 1, 3])



      root_values_no_sampling, child_visits, child_indices, = render_tree(debug_observations,train_agent._config,train_agent._muzero_network, tf.shape(debug_observations)[0], f'tree_move_no_sampling',train_agent._config.action_space_size)
      index = tf.concat([tf.reshape(tf.tile(tf.expand_dims(tf.range(tf.shape(debug_observations)[0]), 1),
                                            [1, train_agent._config.action_space_size]),
                                    (train_agent._config.action_space_size * tf.shape(debug_observations)[0], 1)),
                        tf.reshape(child_indices,
                                    (train_agent._config.action_space_size * tf.shape(debug_observations)[0], 1))],
                        1)
      mean_policy_no_sampling = tf.fill([tf.shape(debug_observations)[0], train_agent._config.action_space_size], 0.0)
      policy_no_sampling = tf.tensor_scatter_nd_update(mean_policy_no_sampling, index,
                                                    tf.reshape(child_visits, train_agent._config.action_space_size *
                                                              tf.shape(debug_observations)[0]))
      policies_no_sampling = tf.reshape(policy_no_sampling, [9, 3, 3])
      policies_no_sampling = tf.expand_dims(policies_no_sampling, -1)
      policies_no_sampling = tf.tile(policies_no_sampling, [1, 1, 1, 3])


      tf.summary.image('eval/debug_moves', tf.concat([target_render,tf.reshape(perfect_moves, (9, 3, 3, 3)),
                      tf.reshape(predicted_moves, (9, 3, 3, 3)), policies_sampling, policies_no_sampling], axis=1),  step=optimizer.iterations, max_outputs=len(debug_moves))
      """

      logging.info(
          f"Time; train; ups; {train_steps_per_iteration/stage_time:.4f}")
  
      # data stats ?
      stage_time = time.time()-start_time
      tf.summary.scalar("time/debug_log", stage_time, step=optimizer.iterations)
      logging.info(f"Time; debug; s; {stage_time:.4f}")

      # Backup Scope
      logging.info("backup")
      start_time = time.time()
      if rb_checkpoint_interval is not None and optimizer.iterations.numpy() % rb_checkpoint_interval == 0:
        rb_checkpointer.save(global_step=optimizer.iterations)
      if optimizer.iterations.numpy() % policy_checkpoint_interval == 0:
        model_name = 'model_{:012}.npz'.format(optimizer.iterations.numpy())
        save_model(
            train_agent._muzero_network, os.path.join(out_path, model_name))
        shutil.copyfile(os.path.join(out_path, model_name),
                        os.path.join(out_path, 'latest_model.npz'))
        policy_checkpointer.save(global_step=optimizer.iterations)
      stage_time = time.time()-start_time
      tf.summary.scalar("time/backup", stage_time, step=optimizer.iterations)
      logging.info(f"Time; backup; s; {stage_time:.4f}")
      mlflow.log_metric("time/backup", stage_time)
      tf.summary.flush()

  #mlflow.end_run()
  # print result for HP search
  #print(np.mean(log_average_return))

  return v_loss, r_loss, p_loss


def main(argv):
  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')
  tf_config()
  tf.config.run_functions_eagerly(True)  # debug
  #tf.config.optimizer.set_jit(True)
  env_kwargs = {}

  # enable masking and tell muzero to unwrap observations with masking
  env_kwargs = {"action_masking": True}
  gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.enable_action_masking', True)

  import src.envs.tictactoe_tfa as tictactoeenv
  def env_fn(batch_size=4, **kwargs):
    return tictactoeenv.TicTacToeTFEnv(batch_size, **kwargs)

  gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.support_size_value', 1)

  # debug
  train_eval(env_fn=env_fn,
    env_kwargs=env_kwargs,
    train_steps=4,
    train_steps_per_iteration=2,
    replay_buffer_prefill=2,
    use_tf_functions=False,
    #reanalyze_traj=True,
    )

  # train
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.num_simulations', 50)
  # import src.apps.c4_train_muzero
  # gin.bind_parameter('src.apps.c4_train_muzero.create_agent.num_features', 64)
  # train_eval(env_fn=env_fn, num_features=64, collect_batch_size=1024, train_batch_size=512, train_steps=25000, train_steps_per_iteration=100, num_simulations=45, use_tf_functions=True)


if __name__ == '__main__':
  tf_agents.system.multiprocessing.handle_main(main)
