import logging
import unittest

import src.apps.gym_train_muzero as gym_train_muzero


class EntryPointTester(unittest.TestCase):

  def test_entryPoint(self):
    gym_train_muzero.train_eval(
        env_id='CartPole-v1',
        collect_batch_size=4,
        eval_batch_size=4,
        train_batch_size=4,
        train_steps=4,
        train_steps_per_iteration=2,
        max_steps=3,
        replay_buffer_prefill=2,
        use_tf_functions=False)


if __name__ == "__main__":
  # tf.config.run_functions_eagerly(True) # for debugging

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      )

  unittest.main()
