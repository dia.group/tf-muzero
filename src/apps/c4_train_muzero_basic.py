import datetime
import gin
import logging
import mlflow
import numpy as np
import os
import psutil
import shutil
import sys
if not os.getcwd() in sys.path:
  sys.path.append(os.getcwd())
import time
import tempfile
import tensorflow as tf  # type: ignore
import tf_agents   # type: ignore
from tf_agents import specs
from tf_agents.drivers import dynamic_episode_driver
from tf_agents.replay_buffers import episodic_replay_buffer
from tf_agents.trajectories import trajectory
from tf_agents.environments import suite_gym
from tf_agents.environments import tf_py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import batched_py_environment
from tf_agents.metrics import tf_metrics
from typing import Dict

# locals
from src.agents.tfagents.muzero_agent import *
from src.agents.tfagents.muzero.networks import *
from src.agents.tfagents.muzero.common import *
from src.envs.connect4.refmoves_kaggle import *


@gin.configurable
def tf_config():
  physical_devices = tf.config.experimental.list_physical_devices("GPU")
  for gpu in physical_devices:
    tf.config.set_logical_device_configuration(gpu, [tf.config.LogicalDeviceConfiguration(memory_limit=3584)])
  if len(physical_devices) > 0:
    tf.config.set_visible_devices(physical_devices[0], 'GPU')
  # tf.config.set_visible_devices([], 'GPU')
  tf.config.set_soft_device_placement(True)
  #tf.debugging.set_log_device_placement(True)
  #tf.data.experimental.enable_debug_mode()
  #tf.config.run_functions_eagerly(True) # debug
  tf.config.optimizer.set_jit(True)


def create_env(env_fn: callable, env_kwargs:Dict, max_steps: int, batch_size: int = 1):
  tf_env = env_fn(**env_kwargs)
  if isinstance(tf_env, tf_environment.TFEnvironment):
    tf_env = env_fn(batch_size=batch_size, **env_kwargs)
  else:
    py_env = batched_py_environment.BatchedPyEnvironment(
        [suite_gym.wrap_env(env_fn(**env_kwargs)) for _ in range(batch_size)], multithreading=False)
    #py_env = parallel_py_environment.ParallelPyEnvironment([lambda : suite_gym.wrap_env(env_fn()) for _ in range(batch_size)])
    tf_env = tf_py_environment.TFPyEnvironment(py_env)

  return tf_env


def sample_position(game: GameXp, num_unroll_steps: int, training_step: int) -> int:
  # Sample position from game either uniformly or according to some priority.
  high = game.history_length
  # low = 0 # uniform
  low = tf.math.maximum(0, high-1-num_unroll_steps -
                        tf.cast(training_step//100, tf.int32))  # bootstarp at the end
  r = low + tf.cast(tf.random.uniform(tf.shape(high), 0, 1)
                    * tf.cast(high-low, tf.float32), tf.int32)
  tf.assert_less(r, game.history_length)
  return r


def visit_softmax_temperature(num_moves, training_steps):
  return tf.where(num_moves < 30, 1.0, 0.0)
  # if training_steps<(train_steps//2):
  #   return 1.0
  # elif training_steps<(train_steps*3//4):
  #   return 0.5
  # else:
  #   return 0.25


@gin.configurable
def create_agent(
    tf_env,
    max_steps: int,
    rep_res_block: int = 2,
    dyn_res_block: int = 2,
    pred_res_block: int = 2,
    num_features: int = 16,
    num_conv_features: int = 24,
    learning_rate: int = 0.01,
    train_steps: int = 2,
    mixed_precision: bool = False,
):

  mlflow.log_param("rep_res_block", rep_res_block)
  mlflow.log_param("dyn_res_block", dyn_res_block)
  mlflow.log_param("pred_res_block", pred_res_block)
  mlflow.log_param("num_features", num_features)
  mlflow.log_param("num_conv_features", num_conv_features)
  mlflow.log_param("learning_rate", learning_rate)
  mlflow.log_param("mixed_precision", mixed_precision)

  if mixed_precision:
    tf.keras.mixed_precision.set_global_policy('mixed_float16')

  config = MuZeroConfig(
      player_turn=2,   # C4 is a 2 player step per game state
      action_space_size=int(
          np.prod(one_hot_shape(tf_env.action_spec()))),
      visit_softmax_temperature_fn=visit_softmax_temperature,
      known_bounds=KnownBounds(-1, 1),
      td_steps=max_steps,
      sample_position_fn=sample_position,
      pb_c_init=4.0,
      tree_representation_storage=tf.int8,
      # tree_representation_storage=tf.float16,
  )

  for field, value in zip(config._fields, config):
    if not callable(value):
      mlflow.log_param(field, value)

  rp_node_type = tf.float32
  #rp_node_type = tf.float16
  nw_rep = NWConv2DRepresentation(
    tf_env.observation_spec(),
    rep_res_block,
    num_conv_features,
    num_features)
  latent_state_specs = tf.nest.map_structure(
    lambda t: specs.TensorSpec(shape=t.shape, dtype=rp_node_type),
    tf_agents.utils.nest_utils.unbatch_nested_tensors(nw_rep.output))
  nw_pred = NWConv2DPrediction(
    latent_state_specs,
    tf_env.action_spec(),
    pred_res_block,
    num_conv_features,
    value_support=config.support_size_value)
  nw_dyn = NWConv2DDynamic_C4(
    latent_state_specs,
    tf_env.action_spec(),
    dyn_res_block,
    num_conv_features,
    reward_support=config.support_size_reward)
  optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
  if mixed_precision:
    optimizer = tf.keras.mixed_precision.LossScaleOptimizer(optimizer)
  muzero_network = MuzeroNetwork(
      config,
      nw_rep,
      nw_pred,
      nw_dyn,
      optimizer=optimizer,
      )

  tf_agent = MuzeroAgent(
      time_step_spec=tf_env.time_step_spec(),
      action_spec=tf_env.action_spec(),
      muzero_network=muzero_network,
      config=config,
      optimizer=optimizer,
  )
  tf_agent.initialize()

  return tf_agent


@gin.configurable("entrypoint")
def train_eval(
    out_path: str = 'out',
    run_name: str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S"),
    run_prefix: str = '',
    env_fn: callable = gin.REQUIRED,
    env_kwargs: Dict = {},
    restore_model_path: str = '',
    collect_new_xp: bool = True,
    collect_batch_size: int = 4,
    eval_batch_size: int = 4,
    train_batch_size: int = 4,
    train_steps: int = 10000,
    train_steps_per_iteration: int = 100,
    max_steps: int = 6*7,  # C4 max steps
    replay_buffer_capacity: int = 100000,
    replay_buffer_prefill: int = 1024,
    policy_checkpoint_interval: int = 5000,
    rb_checkpoint_interval: int = 5000,
    use_tf_functions: bool = False,
):

  if out_path == None:
    out_path = tempfile.gettempdir()
  out_path = os.path.join(os.path.expanduser(out_path),
                          'c4', run_prefix+run_name)
  os.makedirs(out_path)

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      filename=os.path.join(out_path, 'muzero.log'),
                      filemode='w')
  logging.getLogger().addHandler(logging.StreamHandler())
  logging.info(f"Summary writer to {out_path}")

  shutil.copyfile(os.path.realpath(__file__), os.path.join(
      out_path, os.path.basename(__file__)))

  #tf.random.set_seed(123456)  # easier run comparison -- disable for real training!
  with mlflow.start_run(run_name=run_name):
    mlflow.log_artifacts(out_path)
    mlflow.log_param("restore_model_path", restore_model_path)
    mlflow.log_param("collect_new_xp", collect_new_xp)
    mlflow.log_param("collect_batch_size", collect_batch_size)
    mlflow.log_param("eval_batch_size", eval_batch_size)
    mlflow.log_param("train_batch_size", train_batch_size)
    mlflow.log_param("train_steps", train_steps)
    mlflow.log_param("train_steps_per_iteration", train_steps_per_iteration)
    mlflow.log_param("max_steps", max_steps)
    mlflow.log_param("replay_buffer_capacity", replay_buffer_capacity)
    mlflow.log_param("replay_buffer_prefill", replay_buffer_prefill)
    mlflow.log_param("policy_checkpoint_interval", policy_checkpoint_interval)
    mlflow.log_param("rb_checkpoint_interval", rb_checkpoint_interval)
    mlflow.log_param("use_tf_functions", use_tf_functions)

    train_sample_ratio = (
        train_batch_size*train_steps_per_iteration) / (collect_batch_size*max_steps)
    logging.info(f'(train / collected sample)  ratio : {train_sample_ratio:.4f}')
    if train_sample_ratio <= 2:
      logging.warning(
          f'(train / collected sample)  ratio is low! ({train_sample_ratio:.4f})')
    elif train_sample_ratio >= 100:
      logging.warning(
          f'(train / collected sample)  ratio is high! ({train_sample_ratio:.4f})')

    summary_writer = tf.summary.create_file_writer(out_path)
    summary_writer.set_as_default()

    tf_env = create_env(env_fn, env_kwargs, max_steps, collect_batch_size)

    logging.info("tf_env.observation_spec:" + str(tf_env.observation_spec()))
    logging.info("tf_env.action_spec: " + str(tf_env.action_spec()))
    logging.info("tf_env.batch_size: " + str(tf_env.batch_size))

    train_agent = create_agent(
        tf_env, max_steps=max_steps, train_steps=train_steps)

    logging.info("agent time_step_spec (describing the observation and reward signatures of the environment this agent's policies operate in):\n"+str(train_agent.time_step_spec))
    logging.info("agent training_data_spec (describes the structure expected of the `experience` argument passed to `train`):\n" +
                str(train_agent.training_data_spec))

    # now that everything has been created we log used values
    with tf.io.gfile.GFile(os.path.join(out_path, "config.gin"), 'w') as f:
      f.write(gin.operative_config_str())

    train_agent._muzero_network.nw_representation.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_prediction.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_dynamic.summary(print_fn=logging.info)

    optimizer = train_agent._optimizer

    env_steps = tf_metrics.EnvironmentSteps(name='step_count', prefix='collect')
    collect_average_return = tf_metrics.AverageReturnMetric(
        name='mean_reward',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    collect_average_ep_length = tf_metrics.AverageEpisodeLengthMetric(
        name='mean_ep_length',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    chosen_action = tf_metrics.ChosenActionHistogram(
        buffer_size=tf_env.batch_size,
    )
    collect_metrics = [
        tf_metrics.NumberOfEpisodes(name='ep_count', prefix='collect'),
        env_steps,
        collect_average_return,
        collect_average_ep_length,
        #chosen_action,
    ]

    collect_policy = train_agent.collect_policy
    eval_policy = train_agent.policy

    # train_checkpointer = common.Checkpointer(
    #   ckpt_dir=os.path.join(out_path, 'train'),
    #   agent=train_agent,
    #   global_step=optimizer.iterations,
    #   metrics=metric_utils.MetricsGroup(collect_metrics, 'collect_metrics'))
    # train_checkpointer.initialize_or_restore()

    # policy
    policy_checkpointer = tf_agents.utils.common.Checkpointer(
      ckpt_dir=os.path.join(out_path, 'policy'),
      max_to_keep=1,
      policy=eval_policy,
      global_step=optimizer.iterations)
    policy_checkpointer.initialize_or_restore()

    restore_model_filename = os.path.join(restore_model_path, 'latest_model.npz')
    #restore_model_filename = os.path.join(restore_model_path, 'model_{:012}.npz'.format(20000))
    if os.path.exists(restore_model_filename):
      try:
        load_model(train_agent._muzero_network, restore_model_filename)
      except Exception as e:
        logging.warning("Fails to load model: "+str(e))

    replay_buffer = episodic_replay_buffer.EpisodicReplayBuffer(
        completed_only=True,
        data_spec=train_agent.collect_data_spec,
        capacity=replay_buffer_capacity
    )

    rb_dir = os.path.join(out_path, 'replay_buffer')
    restore_rb_path = os.path.join(restore_model_path, 'replay_buffer')
    if os.path.exists(restore_rb_path):
      if not os.path.exists(rb_dir):
        os.makedirs(rb_dir)
      for filename in os.listdir(restore_rb_path):
        shutil.copyfile(src=os.path.join(restore_rb_path, filename),
                        dst=os.path.join(rb_dir, filename))
    rb_checkpointer = tf_agents.utils.common.Checkpointer(
        ckpt_dir=rb_dir,
        max_to_keep=1,
        replay_buffer=replay_buffer)
    rb_checkpointer.initialize_or_restore()

    traj_obs = episodic_replay_buffer.StatefulEpisodicReplayBuffer(
        replay_buffer, num_episodes=tf_env.batch_size).add_batch

    dataset = replay_buffer.as_dataset(num_parallel_calls=tf.data.AUTOTUNE)
    # TODO: add issue as unfinished episodes seems to be returned
    dataset = dataset.filter(
      lambda xp, y: tf.reduce_any(xp.step_type == tf_agents.trajectories.time_step.StepType.LAST))
    dataset = dataset.map(
      lambda x, y: episode_pad_fn(x, y, max_steps),
      num_parallel_calls=tf.data.AUTOTUNE
      )
    dataset = dataset.batch(
      batch_size=train_batch_size,
      num_parallel_calls=tf.data.AUTOTUNE
      )
    dataset = dataset.prefetch(4)

    num_episodes = tf_env.batch_size
    collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
        tf_env,
        collect_policy,
        observers=[traj_obs] + collect_metrics,
        num_episodes=num_episodes)

    iterator = iter(dataset)
    logging.info("datatset element_spec: "+str(dataset.element_spec))

    ref_observation, ref_moves = load_ref_moves()

    @tf.function()
    def train_fn(train_agent, it, steps: int):
      train_loss = (0.0, 0.0, 0.0, 0.0)
      for _ in tf.range(steps):
        experience, _ = next(it)
        position = None
        losses = train_agent.train(experience, position=position).loss
        train_loss = (
          train_loss[0]+losses["policy_loss"],
          train_loss[1]+losses["value_loss"],
          train_loss[2]+losses["pc_loss"],
          train_loss[3]+losses["reward_loss"],
          )

      return (train_loss[0]/steps, train_loss[1]/steps, train_loss[2]/steps, train_loss[3]/steps), {}

    if use_tf_functions:
      collect_driver.run = tf_agents.utils.common.function(collect_driver.run)
      #train_step = tf_agents.utils.common.function(train_step) # Warning

    """
    checkpointed = tf.train.Checkpoint(rb=replay_buffer)
    replay_dir = os.path.join(tempfile.gettempdir(), "replay")
    if not os.path.exists(replay_dir):
      os.makedirs(replay_dir)
    checkpointed.save(os.path.join(replay_dir, "replay"))
    replay_buffer.clear()
    checkpointed.restore(os.path.join(replay_dir, "replay-1"))
    #replay_buffer.get_next()
    """

    init_policy = MuzeroRandomPolicy(config=train_agent._config, time_step_spec=tf_env.time_step_spec(), action_spec=tf_env.action_spec())
    initial_collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
      tf_env,
      init_policy,
      observers=[traj_obs] + collect_metrics,
      num_episodes=num_episodes)

    current_env_state = tf_env.reset()
    current_policy_state = init_policy.get_initial_state(tf_env.batch_size)

    nb_loop = (replay_buffer_prefill+tf_env.batch_size-1)//tf_env.batch_size
    for loop in range(nb_loop):
      logging.info(f"filling replaybuffer... ({loop+1}/{nb_loop})")
      start_time = time.time()
      current_env_state, current_policy_state = initial_collect_driver.run(
          time_step=current_env_state, policy_state=current_policy_state)
      stage_time = time.time()-start_time
      logging.info(f"Time; collect; s; {stage_time:.4f}")
    if nb_loop > 0:
      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")
        mlflow.log_metric(f"collect/metric/{collect_metric.name}",
                          collect_metric.result().numpy())
      tf.summary.histogram(
          "collect/ep_length", collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                            collect_average_return._buffer.data, step=optimizer.iterations)

    while optimizer.iterations < (train_steps-train_steps_per_iteration):
      logging.info(f"Run \'{run_name}\' on step {optimizer.iterations.numpy()}")

      # Collect Scope
      logging.info("collect")
      start_time = time.time()
      start_step_count = env_steps.result().numpy()
      if collect_new_xp:
        try:
          current_env_state, current_policy_state = collect_driver.run(
              time_step=current_env_state, policy_state=current_policy_state)
        except Exception as e:
          save_model(train_agent._muzero_network, os.path.join(out_path, 'model_crash.npz'))
          raise e
      stage_time = time.time()-start_time
      tf.summary.scalar("time/collect/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; collect; s; {stage_time:.4f}")
      step_count = env_steps.result().numpy() - start_step_count
      tf.summary.scalar("time/collect/fps", step_count /
                        (stage_time+1e-8), step=optimizer.iterations)
      logging.info(f"Time; collect; fps; {step_count/(stage_time+1e-8):.4f}")

      logging.info(
          f"replay_buffer contains {len(replay_buffer._completed_episodes())} episodes or {replay_buffer.num_frames()} frames")
      tf.summary.scalar('replay_buffer/num_frames',
                        replay_buffer.num_frames(), step=optimizer.iterations)
      tf.summary.scalar('replay_buffer/episodes',
                        len(replay_buffer._completed_episodes()), step=optimizer.iterations)
      # eps = [replay_buffer._get_episode(ep) for ep in replay_buffer._completed_episodes()]

      # Train Scope
      logging.info("train")
      start_time = time.time()
      try:
        (p_loss, v_loss, pc_loss, r_loss), _ = train_fn(
            train_agent, iterator, train_steps_per_iteration)
      except Exception as e:
        save_model(train_agent._muzero_network, os.path.join(out_path, 'model_crash.npz'))
        raise e
      logging.info(
          f"{optimizer.iterations.numpy()} - train {train_steps_per_iteration} steps")
      tf.summary.histogram("collect/ep_length",
                          collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/train/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; train; s; {stage_time:.4f}")
      tf.summary.scalar("time/train/ups", train_steps_per_iteration /
                        stage_time, step=optimizer.iterations)
      logging.info(
          f"Time; train; ups; {train_steps_per_iteration/stage_time:.4f}")

      # Eval Scope
      logging.info("eval")
      start_time = time.time()
      perfect_move_count_nn, good_move_count_nn, good_value_count_nn = compute_ref_score(
          ref_observation, ref_moves, train_agent._muzero_network, train_agent._config)
      tf.summary.scalar('eval/perfect_move_count', perfect_move_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/good_move_count', good_move_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/good_value_count', good_value_count_nn /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      perfect_move_count_mcts, good_move_count_mcts, good_value_count_mcts = compute_ref_score_mcts(
          ref_observation, ref_moves, train_agent._muzero_network, train_agent._config)
      tf.summary.scalar('eval/mcts/perfect_move_count', perfect_move_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/mcts/good_move_count', good_move_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      tf.summary.scalar('eval/mcts/good_value_count', good_value_count_mcts /
                        tf.shape(ref_observation)[0], step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/eval/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; eval; s; {stage_time:.4f}")

      # Debug Scope
      logging.info("debug_log")
      start_time = time.time()
      if isinstance(optimizer.learning_rate, tf.Tensor) or isinstance(optimizer.learning_rate, tf.Variable):
        tf.summary.scalar('train/learning_rate',
                          optimizer.learning_rate, step=optimizer.iterations)
      elif isinstance(optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
        tf.summary.scalar('train/learning_rate', optimizer.learning_rate(
            optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/softmax_temperature', train_agent._config.visit_softmax_temperature_fn(
          0, optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      logging.info(
          f"p_loss/v_loss/pc_loss/r_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}")

      process = psutil.Process(os.getpid())
      tf.summary.scalar(f'mem/cpu', process.memory_info().rss /
                        (2**30), optimizer.iterations)

      if int(tf.version.VERSION.split('.')[0]) >= 2 and int(tf.version.VERSION.split('.')[1]) >= 5:
        for gpu in tf.config.list_logical_devices('GPU'):
          mem_infos = tf.config.experimental.get_memory_info(gpu.name)
          for name, info in mem_infos.items():
            tf.summary.scalar(f'mem/gpu{gpu.name}/{name}',
                              info/(2**30), step=optimizer.iterations)

      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")

      # data stats ?
      stage_time = time.time()-start_time
      tf.summary.scalar("time/debug_log", stage_time, step=optimizer.iterations)
      logging.info(f"Time; debug; s; {stage_time:.4f}")

      # Backup Scope
      logging.info("backup")
      start_time = time.time()
      if rb_checkpoint_interval is not None and optimizer.iterations.numpy() % rb_checkpoint_interval == 0:
        rb_checkpointer.save(global_step=optimizer.iterations)
      if optimizer.iterations.numpy() % policy_checkpoint_interval == 0:
        model_name = 'model_{:012}.npz'.format(optimizer.iterations.numpy())
        save_model(
            train_agent._muzero_network, os.path.join(out_path, model_name))
        shutil.copyfile(os.path.join(out_path, model_name),
                        os.path.join(out_path, 'latest_model.npz'))
        policy_checkpointer.save(global_step=optimizer.iterations)
      stage_time = time.time()-start_time
      tf.summary.scalar("time/backup", stage_time, step=optimizer.iterations)
      logging.info(f"Time; backup; s; {stage_time:.4f}")
      mlflow.log_metric("time/backup", stage_time)
      tf.summary.flush()

  #mlflow.end_run()
  # print result for HP search
  #print(np.mean(log_average_return))

  return v_loss, r_loss, p_loss


def main(argv):
  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')
  tf_config()
  tf.config.run_functions_eagerly(True)  # debug
  #tf.config.optimizer.set_jit(True)

  # import src.envs.connect4gym as c4env
  # env_fn = lambda : c4env.Connect4GymEnv()

  import src.envs.connect4_tfa as c4env
  def env_fn(batch_size=4): return c4env.Connect4TFEnv(batch_size)

  gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.support_size_value', 1)

  # debug
  train_eval(
    env_fn=env_fn,
    train_steps=4,
    train_steps_per_iteration=2,
    replay_buffer_prefill=4,
    use_tf_functions=False,
    )

  # train
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.num_simulations', 50)
  # import src.apps.c4_train_muzero
  # gin.bind_parameter('src.apps.c4_train_muzero.create_agent.num_features', 64)
  # train_eval(env_fn=env_fn, num_features=64, collect_batch_size=1024, train_batch_size=512, train_steps=25000, train_steps_per_iteration=100, num_simulations=45, use_tf_functions=True)


if __name__ == '__main__':
  tf_agents.system.multiprocessing.handle_main(main)
