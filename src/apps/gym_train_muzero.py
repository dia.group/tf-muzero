import datetime
import gin
import logging
import mlflow
import numpy as np
import os
import psutil
import shutil
import sys
if not os.getcwd() in sys.path:
  sys.path.append(os.getcwd())
import time
import tempfile
import tensorflow as tf  # type: ignore
from typing import Dict, Optional
import tf_agents   # type: ignore
from tf_agents import specs
from tf_agents.drivers import dynamic_episode_driver
from tf_agents.replay_buffers import episodic_replay_buffer
from tf_agents.trajectories import trajectory
from tf_agents.environments import suite_gym
from tf_agents.environments import tf_py_environment
from tf_agents.environments import parallel_py_environment
from tf_agents.environments import batched_py_environment
from tf_agents.metrics import tf_metrics
from tf_agents.networks import encoding_network
from tf_agents.eval import metric_utils

# locals
from src.agents.tfagents import muzero_agent
from src.agents.tfagents.muzero import networks as muzero_networks
from src.agents.tfagents.muzero import common as muzero_common


@gin.configurable
def tf_config():
  physical_devices = tf.config.experimental.list_physical_devices("GPU")
  for gpu in physical_devices:
    tf.config.experimental.set_memory_growth(gpu, True)
  if len(physical_devices) > 0:
    tf.config.set_visible_devices(physical_devices[0], 'GPU')
  #tf.config.set_visible_devices([], 'GPU')
  tf.config.set_soft_device_placement(True)
  #tf.debugging.set_log_device_placement(True)
  #tf.data.experimental.enable_debug_mode()
  #tf.config.run_functions_eagerly(True) # debug
  tf.config.optimizer.set_jit(True)


#from src.agents.tfagents import muzero_agent
# pylint: disable=relative-beyond-top-level


def create_env(env_id: str, env_kwargs: Dict[str, object] = {}, max_steps: int = -1, batch_size: int = 1):
  # tf_env = env_fn()
  # if isinstance(tf_env, tf_environment.TFEnvironment):
  #   tf_env = env_fn(batch_size=batch_size)
  # else:

  #wrapped_env = [suite_gym.wrap_env(gym.make(env_id, **env_kwargs), max_episode_steps=max_steps, auto_reset=True) for _ in range(batch_size)]
  wrapped_env = [tf_agents.environments.suite_gym.load(
    env_id, max_episode_steps=max_steps, gym_kwargs=env_kwargs) for _ in range(batch_size)]
  # wrapped_env = [tf_agents.environments.HistoryWrapper(env) for env in wrapped_env]
  py_env = batched_py_environment.BatchedPyEnvironment(
      wrapped_env, multithreading=False)
  tf_env = tf_py_environment.TFPyEnvironment(py_env)

  return tf_env


def sample_position(game: muzero_common.GameXp, num_unroll_steps: int, training_step: tf.Tensor) -> tf.Tensor:
  # Sample position from game either uniformly or according to some priority.
  high = game.history_length
  low = 0 # uniform
  # low = tf.math.maximum(0, high-1-num_unroll_steps -
  #                       tf.cast(training_step//300, tf.int32))  # bootstarp at the end
  r = low + tf.cast(tf.random.uniform(tf.shape(high), 0, 1)
                    * tf.cast(high-low, tf.float32), tf.int32)
  tf.assert_less(r, game.history_length)
  return r


def visit_softmax_temperature(num_moves: tf.Tensor, training_steps: tf.Tensor) -> tf.Tensor:
  # return 1.0
  #return tf.ones_like(num_moves, dtype=tf.float32)
  r = tf.range(tf.size(num_moves), dtype=tf.float32) / tf.size(num_moves, out_type=tf.float32)
  return 0.25 + r * 0.75
  #tf.random.set_seed(123456)
  #return tf.random.uniform(tf.shape(num_moves), minval=0.1, maxval=1.0, seed=123456)


@gin.configurable
def create_agent(
    tf_env,
    num_features: int = 16,
    learning_rate: float = 0.001,
    mixed_precision: bool = False,
):


  mlflow.log_param("num_features", num_features)
  mlflow.log_param("learning_rate", learning_rate)
  mlflow.log_param("mixed_precision", mixed_precision)

  if mixed_precision:
    tf.keras.mixed_precision.set_global_policy('mixed_float16')

  config = muzero_common.MuZeroConfig(
      action_space_size=int(
          np.prod(muzero_common.one_hot_shape(tf_env.action_spec()))),
      visit_softmax_temperature_fn=visit_softmax_temperature,
      sample_position_fn=sample_position,
  )

  for field, value in zip(config._fields, config):
    if not callable(value):
      mlflow.log_param(field, value)

  observation_spec = tf_env.observation_spec()
  nw_rep = tf.keras.Sequential([
    tf.keras.Input(observation_spec.shape, dtype=observation_spec.dtype),
    # tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu),
    tf.keras.layers.Dense(num_features, tf.nn.tanh)
    ])

  rp_node_type = tf.float32
  # rp_node_type = tf.float16
  # hidden_state_specs = specs.TensorSpec(
  #     (*tf_env.observation_spec().shape[:-1], num_features), rp_node_type)
  hidden_state_specs = specs.TensorSpec(nw_rep.output.shape[1:], rp_node_type)

  input_ls = tf.keras.Input(hidden_state_specs.shape, dtype=hidden_state_specs.dtype)
  # flat_input_ls = tf.keras.layers.Flatten()(input_ls)
  p0 = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu)(input_ls)
  v0 = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu)(input_ls)
  p = tf.keras.layers.Dense(config.action_space_size)(p0)
  v = tf.keras.layers.Dense(config.support_size_value*2+1 if config.support_size_value is not None else 1)(v0)

  nw_pred = tf.keras.Model(inputs=input_ls, outputs=[p, v], name="prediction")
  #nw_pred.build(tf_agents.specs.tensor_spec.sample_spec_nest(hidden_state_specs, outer_dims=[1]))


  #tf.one_hot(hidden_state[1], config.action_space_size)
  action_spec = tf_env.action_spec()
  input_a = tf.keras.Input(action_spec.shape, dtype=action_spec.dtype)
  input_dyn = tf.keras.layers.Concatenate()([input_ls, tf.one_hot(input_a, action_spec.maximum-action_spec.minimum)])
  h0 = tf.keras.layers.Dense(512, activation=tf.nn.leaky_relu)(input_dyn)
  h = tf.keras.layers.Dense(num_features, activation=tf.nn.tanh)(h0)
  r = tf.keras.layers.Dense(config.support_size_reward*2+1 if config.support_size_reward is not None else 1)(h0)

  nw_dyn = tf.keras.Model(inputs=[input_ls, input_a], outputs=[h, r])
  #nw_dyn.build(tf_agents.specs.tensor_spec.sample_spec_nest(dyn_specs, outer_dims=[1]))

  optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
  if mixed_precision:
    optimizer = tf.keras.mixed_precision.LossScaleOptimizer(optimizer)
  muzero_network = muzero_common.MuzeroNetwork(
      config,
      nw_rep,
      nw_pred,
      nw_dyn,
      optimizer=optimizer,
      )

  tf_agent = muzero_agent.MuzeroAgent(
      time_step_spec=tf_env.time_step_spec(),
      action_spec=tf_env.action_spec(),
      muzero_network=muzero_network,
      config=config,
      optimizer=optimizer,
  )
  tf_agent.initialize()

  return tf_agent


@gin.configurable("entrypoint")
def train_eval(
    out_path: str = 'out',
    run_name: str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S"),
    run_prefix: str = '',
    env_id: str = gin.REQUIRED,
    env_kwargs: Dict[str, object] = {},
    restore_model_path: str = '',
    collect_new_xp: bool = True,
    collect_batch_size: int = 4,
    eval_batch_size: int = 4,
    train_batch_size: int = 4,
    train_steps: int = 10000,
    train_steps_per_iteration: int = 100,
    max_steps: int = 6*7,
    replay_buffer_capacity: int = 1024,
    replay_buffer_prefill: int = 1024,
    replay_buffer_prefill_policy: str = 'random',
    replay_buffer_reverb: bool = False,
    replay_buffer_PER: bool = False,
    policy_checkpoint_interval: Optional[int] = None,
    rb_checkpoint_interval: Optional[int] = None,
    eval_interval: Optional[int] = None,
    use_tf_functions: bool = False,
    debug_summaries: bool = False,
):

  if out_path == None:
    out_path = tempfile.gettempdir()
  out_path = os.path.join(os.path.expanduser(
      out_path), 'gym', env_id, run_prefix + run_name)
  os.makedirs(out_path)

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      filename=os.path.join(out_path, 'muzero.log'),
                      filemode='w')
  logging.getLogger().addHandler(logging.StreamHandler())
  logging.info(f"Summary writer to {out_path}")

  shutil.copyfile(os.path.realpath(__file__), os.path.join(
      out_path, os.path.basename(__file__)))

  if replay_buffer_PER:
    # PER needs a replay buffer with priority management
    assert(replay_buffer_reverb)

  with mlflow.start_run(run_name=run_name):
    mlflow.log_artifacts(out_path)
    mlflow.log_param("env_id", env_id)
    mlflow.log_param("env_kwargs", env_kwargs)
    mlflow.log_param("restore_model_path", restore_model_path)
    mlflow.log_param("collect_new_xp", collect_new_xp)
    mlflow.log_param("collect_batch_size", collect_batch_size)
    mlflow.log_param("eval_batch_size", eval_batch_size)
    mlflow.log_param("train_batch_size", train_batch_size)
    mlflow.log_param("train_steps", train_steps)
    mlflow.log_param("train_steps_per_iteration", train_steps_per_iteration)
    mlflow.log_param("max_steps", max_steps)
    mlflow.log_param("replay_buffer_capacity", replay_buffer_capacity)
    mlflow.log_param("replay_buffer_prefill", replay_buffer_prefill)
    mlflow.log_param("replay_buffer_prefill_policy",
                    replay_buffer_prefill_policy)
    mlflow.log_param("replay_buffer_reverb", replay_buffer_reverb)
    mlflow.log_param("replay_buffer_PER", replay_buffer_PER)
    mlflow.log_param("use_tf_functions", use_tf_functions)
    mlflow.log_param("debug_summaries", debug_summaries)

    train_sample_ratio = (
        train_batch_size*train_steps_per_iteration) / (collect_batch_size*max_steps)
    logging.info(f'train / collected sample ratio : {train_sample_ratio:.4f} (typical : 10)')
    if train_sample_ratio <= 2:
      logging.warning(f'ratio is low!')
    elif train_sample_ratio >= 100:
      logging.warning(f'ratio is high!')

    summary_writer = tf.summary.create_file_writer(out_path)
    summary_writer.set_as_default()

    tf_env = create_env(env_id, env_kwargs, max_steps, collect_batch_size)
    eval_tf_env = create_env(env_id, env_kwargs, max_steps, eval_batch_size)

    logging.info("tf_env.observation_spec:" + str(tf_env.observation_spec()))
    logging.info("tf_env.action_spec: " + str(tf_env.action_spec()))
    logging.info("tf_env.batch_size: " + str(tf_env.batch_size))

    train_agent = create_agent(tf_env)
    logging.info("agent time_step_spec (describing the observation and reward signatures of the environment this agent's policies operate in):\n"+str(train_agent.time_step_spec))
    logging.info("agent training_data_spec (describes the structure expected of the `experience` argument passed to `train`):\n" +
                str(train_agent.training_data_spec))

    # now that everything has been created we log used values
    with tf.io.gfile.GFile(os.path.join(out_path, "config.gin"), 'w') as f:
      f.write(gin.operative_config_str())

    optimizer = train_agent._optimizer

    if replay_buffer_reverb:
      import reverb   # type: ignore
      from src.agents.tfagents.muzero import reverb_binding

      table_name = 'episode_table'
      reverb_server, reverb_table_episode, reverb_tfclient, reverb_client = \
      reverb_binding.create_replay_buffer(
          table_name,
          train_agent.collect_data_spec,
          max_steps=max_steps,
          replay_buffer_capacity=replay_buffer_capacity,
          use_per=replay_buffer_PER
          )

      PER_insert_priority = 1e9
      # traj_obs = reverb_binding.ReverbAddTFEpisodeObserver(
      #     reverb_tfclient,
      #     [table_name],
      #     collect_data_spec=train_agent.collect_data_spec,
      #     batch_size=tf_env.batch_size,
      #     max_steps=max_steps,
      #     insert_priority=PER_insert_priority,
      # )
      traj_obs = reverb_binding.ReverbAddPyEpisodeObserver(
          reverb_client,
          [table_name],
          collect_data_spec=train_agent.collect_data_spec,
          batch_size=tf_env.batch_size,
          max_steps=max_steps,
          insert_priority=PER_insert_priority,
      )

      # dataset = reverb.TrajectoryDataset.from_table_signature(
      dataset = reverb.TimestepDataset.from_table_signature(
          server_address=f'localhost:{reverb_server.port}',
          table=table_name,
          max_in_flight_samples_per_worker=train_batch_size*4)
      dataset = dataset.shuffle(buffer_size=replay_buffer_capacity//4)
      dataset = dataset.batch(train_batch_size)
      dataset = dataset.prefetch(4)
    else:
      replay_buffer = episodic_replay_buffer.EpisodicReplayBuffer(
          completed_only=True,
          data_spec=train_agent.collect_data_spec,
          capacity=replay_buffer_capacity
      )
      traj_obs = episodic_replay_buffer.StatefulEpisodicReplayBuffer(
          replay_buffer, num_episodes=tf_env.batch_size).add_batch
      dataset = replay_buffer.as_dataset(num_parallel_calls=tf.data.AUTOTUNE)
      # TODO: add issue as unfinished episodes seems to be returned
      dataset = dataset.filter(
        lambda xp, y: tf.reduce_any(xp.step_type == tf_agents.trajectories.time_step.StepType.LAST))
      dataset = dataset.map(
        lambda x, y: muzero_agent.episode_pad_fn(x, y, max_steps),
        num_parallel_calls=tf.data.AUTOTUNE
        )
      dataset = dataset.batch(
        batch_size=train_batch_size,
        num_parallel_calls=tf.data.AUTOTUNE
        )
      dataset = dataset.prefetch(4)

    env_steps = tf_metrics.EnvironmentSteps(name='step_count', prefix='collect')
    collect_average_return = tf_metrics.AverageReturnMetric(
        name='mean_reward',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    collect_average_ep_length = tf_metrics.AverageEpisodeLengthMetric(
        name='mean_ep_length',
        prefix='collect',
        buffer_size=tf_env.batch_size,
        batch_size=tf_env.batch_size)
    collect_metrics = [
        tf_metrics.NumberOfEpisodes(name='ep_count', prefix='collect'),
        env_steps,
        collect_average_return,
        collect_average_ep_length,
    ]

    eval_average_return = tf_metrics.AverageReturnMetric(
        name='mean_reward',
        prefix='eval',
        buffer_size=eval_tf_env.batch_size,
        batch_size=eval_tf_env.batch_size)
    eval_average_ep_length = tf_metrics.AverageEpisodeLengthMetric(
        name='mean_ep_length',
        prefix='eval',
        buffer_size=eval_tf_env.batch_size,
        batch_size=eval_tf_env.batch_size)
    eval_metrics = [
        eval_average_return,
        eval_average_ep_length,
    ]

    collect_policy = train_agent.collect_policy
    eval_policy = train_agent.policy

    # train_checkpointer = common.Checkpointer(
    #   ckpt_dir=os.path.join(out_path, 'train'),
    #   agent=train_agent,
    #   global_step=optimizer.iterations,
    #   metrics=metric_utils.MetricsGroup(collect_metrics, 'collect_metrics'))
    # policy_checkpointer = common.Checkpointer(
    #   ckpt_dir=os.path.join(out_path, 'policy'),
    #   policy=eval_policy,
    #   global_step=optimizer.iterations)

    # train_checkpointer.initialize_or_restore()
    # policy_checkpointer.initialize_or_restore()

    if not replay_buffer_reverb:
      rb_dir = os.path.join(out_path, 'replay_buffer')
      restore_rb_path = os.path.join(restore_model_path, 'replay_buffer')
      if os.path.exists(restore_rb_path):
        if not os.path.exists(rb_dir):
          os.makedirs(rb_dir)
        for filename in os.listdir(restore_rb_path):
          shutil.copyfile(src=os.path.join(restore_rb_path, filename),
                          dst=os.path.join(rb_dir, filename))
      rb_checkpointer = tf_agents.utils.common.Checkpointer(
          ckpt_dir=rb_dir,
          max_to_keep=1,
          replay_buffer=replay_buffer)
      rb_checkpointer.initialize_or_restore()

    restore_model_filename = os.path.join(restore_model_path, 'latest_model.npz')
    if os.path.exists(restore_model_filename):
      muzero_networks.load_model(
          train_agent._muzero_network, restore_model_filename)

    train_agent._muzero_network.nw_representation.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_prediction.summary(print_fn=logging.info)
    train_agent._muzero_network.nw_dynamic.summary(print_fn=logging.info)

    num_episodes = tf_env.batch_size
    collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
        tf_env,
        collect_policy,
        observers=[traj_obs] + collect_metrics,
        num_episodes=num_episodes)
    """
    logging.info("eval")
    metric_utils.eager_compute(
      eval_metrics,
      eval_tf_env,
      eval_policy,
      num_episodes=eval_tf_env.batch_size,
      train_step=optimizer.iterations,
      summary_writer=summary_writer,
      summary_prefix='eval',
      use_function=use_tf_functions,
      )
      
    for eval_metric in eval_metrics:
      #eval_metric.tf_summaries(train_step=env_steps.result())
      logging.info(f"eval/metric: {eval_metric.name} = {eval_metric.result().numpy()}")
    """

    iterator = iter(dataset)
    logging.info("datatset element_spec: "+str(dataset.element_spec))

    beta_PER_fn = tf.keras.optimizers.schedules.PolynomialDecay(
        initial_learning_rate=1.00,
        end_learning_rate=0.00,
        decay_steps=train_steps)

    @tf.function()
    def train_fn(agent, it, steps: int):
      train_loss_p = tf.constant(0.0)
      train_loss_v = tf.constant(0.0)
      train_loss_pc = tf.constant(0.0)
      train_loss_r = tf.constant(0.0)
      for _ in tf.range(steps):
        buffer_info = None
        if replay_buffer_reverb:
          buffer_info, raw_data = next(it)
          experience, position = reverb_binding.trajectory_from_data_dict(
              raw_data, agent.collect_data_spec)
          probabilities = tf.cast(buffer_info.priority, tf.float32)
        else:
          experience, buffer_info = next(it)
          position = None
          probabilities = tf.ones(tf.shape(buffer_info))

        # calculate the learning weights as proposed in the PER paper and clip the probabilities to prevent division by zero
        learning_weights = tf.math.pow(
            probabilities / tf.reduce_max(probabilities), beta_PER_fn(optimizer.iterations))
        learning_weights = tf.ones_like(learning_weights)
        losses, per_error = agent.train(experience,
                                      weights=learning_weights, position=position)

        if replay_buffer_reverb and replay_buffer_PER:
          new_priorities = tf.clip_by_value(per_error, 0.01, PER_insert_priority)
          reverb_tfclient.update_priorities(
              table_name,
              keys=buffer_info.key,
              priorities=tf.cast(new_priorities, tf.float64)
          )

        train_loss_p += losses["policy_loss"]
        train_loss_v += losses["value_loss"]
        train_loss_pc += losses["pc_loss"]
        train_loss_r += losses["reward_loss"]

      return (train_loss_p/steps, train_loss_v/steps, train_loss_pc/steps, train_loss_r/steps), {}

    if use_tf_functions:
      collect_driver.run = tf_agents.utils.common.function(collect_driver.run)

    """
    checkpointed = tf.train.Checkpoint(rb=replay_buffer)
    replay_dir = os.path.join(tempfile.gettempdir(), "replay")
    if not os.path.exists(replay_dir):
      os.makedirs(replay_dir)
    checkpointed.save(os.path.join(replay_dir, "replay"))
    replay_buffer.clear()
    checkpointed.restore(os.path.join(replay_dir, "replay-1"))
    #replay_buffer.get_next()
    """
    # initiate replay buffer
    init_policy_dict = {
        "random": muzero_agent.MuzeroRandomPolicy(config=train_agent._config, time_step_spec=tf_env.time_step_spec(), action_spec=tf_env.action_spec()),
        "collect": collect_policy,
        "eval": eval_policy,
    }
    init_policy = init_policy_dict[replay_buffer_prefill_policy]
    initial_collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
      tf_env,
      init_policy,
      observers=[traj_obs] + collect_metrics,
      num_episodes=num_episodes)

    current_env_state = tf_env.reset()
    current_policy_state = init_policy.get_initial_state(tf_env.batch_size)

    replay_buffer_prefill = min(replay_buffer_prefill, replay_buffer_capacity)
    nb_loop = (replay_buffer_prefill+tf_env.batch_size-1)//tf_env.batch_size
    for loop in range(nb_loop):
      logging.info(f"filling replaybuffer... ({loop+1}/{nb_loop})")
      current_env_state, current_policy_state = initial_collect_driver.run(
          time_step=current_env_state, policy_state=current_policy_state)

    if not replay_buffer_reverb:
      stats_dataset = replay_buffer.as_dataset(single_deterministic_pass=True)
      max_reward_step = tf.constant(0.0)
      max_reward_episode = tf.constant(0.0)
      big_reward_episode = 0
      episode_count = 0
      for episode in stats_dataset:
        max_reward_step = tf.math.maximum(
            max_reward_step, tf.reduce_max(episode.reward))
        max_reward_episode = tf.math.maximum(
            max_reward_episode, tf.reduce_sum(episode.reward))
        if tf.reduce_sum(episode.reward) > 150.0:
          big_reward_episode += 1
        episode_count += 1
      logging.info(f"maximal reward step {max_reward_step:04f}")
      logging.info(f"maximal reward episode {max_reward_episode:04f}")
      logging.info(f"episodes with reward>150 : {big_reward_episode}")
      logging.info(f"capacity: {replay_buffer.capacity}")
      logging.info(f"episode_count : {episode_count}")
      logging.info(
          f"completed episodes: {len(replay_buffer._completed_episodes())}")
      logging.info(
          f"num_frames: {replay_buffer.num_frames()} avg({replay_buffer.num_frames()/(episode_count+1):04f})")

    if nb_loop > 0:
      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"collect/metric: {collect_metric.name} = {collect_metric.result().numpy()}")
        mlflow.log_metric(f"collect/metric/{collect_metric.name}",
                        collect_metric.result().numpy())
      tf.summary.histogram("collect/ep_length",
                          collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)

      (p_loss, v_loss, pc_loss, r_loss), _ = train_fn(
          train_agent, iterator, train_steps_per_iteration)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      logging.info(
          f"p_loss/v_loss/pc_loss/r_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}")

    while optimizer.iterations < train_steps:
      logging.info(f"Run \'{run_name}\' on step {optimizer.iterations.numpy()}")

      # Collect Scope
      logging.info("collect")
      start_time = time.time()
      start_step_count = env_steps.result().numpy()
      if collect_new_xp:
        if replay_buffer_reverb:
          traj_obs.reset()
        current_env_state, current_policy_state = collect_driver.run(
            time_step=current_env_state, policy_state=current_policy_state)
      stage_time = time.time()-start_time
      tf.summary.scalar("time/collect/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; collect; s; {stage_time:.4f}")
      step_count = env_steps.result().numpy() - start_step_count
      tf.summary.scalar("time/collect/fps", step_count /
                        stage_time, step=optimizer.iterations)
      logging.info(f"Time; collect; fps; {step_count/stage_time:.4f}")

      if replay_buffer_reverb:
        logging.info(
            f"replay_buffer contains {reverb_table_episode.info.num_episodes} episodes")
        mlflow.log_metric("replay_buffer/episodes",
                        float(reverb_table_episode.info.num_episodes))
        tf.summary.scalar('replay_buffer_frames',
                          reverb_table_episode.info.current_size, step=optimizer.iterations)
        tf.summary.scalar('replay_buffer_episodes',
                          reverb_table_episode.info.num_episodes, step=optimizer.iterations)
      else:
        logging.info(
            f"replay_buffer contains {len(replay_buffer._completed_episodes())} episodes or {replay_buffer.num_frames()} frames")
        tf.summary.scalar('replay_buffer/num_frames',
                          replay_buffer.num_frames(), step=optimizer.iterations)
        tf.summary.scalar('replay_buffer/episodes',
                          len(replay_buffer._completed_episodes()), step=optimizer.iterations)
        # eps = [replay_buffer._get_episode(ep) for ep in replay_buffer._completed_episodes()]

      # Train Scope
      logging.info("train")
      start_time = time.time()
      (p_loss, v_loss, pc_loss, r_loss), _ = train_fn(
          train_agent, iterator, train_steps_per_iteration)
      logging.info(
          f"{optimizer.iterations.numpy()} - train {train_steps_per_iteration} steps at {optimizer.learning_rate.numpy():.7f}")
      tf.summary.histogram("collect/ep_length",
                          collect_average_ep_length._buffer.data, step=optimizer.iterations)
      tf.summary.histogram("collect/average_return",
                          collect_average_return._buffer.data, step=optimizer.iterations)
      # tf.summary.scalar("replay_buffer/per_max", tf.reduce_max(per_error), step=optimizer.iterations)
      # tf.summary.histogram("replay_buffer/per_error", per_error, step=optimizer.iterations)
      stage_time = time.time() - start_time
      tf.summary.scalar("time/train/s", stage_time, step=optimizer.iterations)
      logging.info(f"Time; train; s; {stage_time:.4f}")
      tf.summary.scalar("time/train/ups", train_steps_per_iteration /
                        stage_time, step=optimizer.iterations)
      logging.info(
          f"Time; train; ups; {train_steps_per_iteration/stage_time:.4f}")

      # Eval Scope
      logging.info("eval")
      start_time = time.time()
      if eval_interval is not None and optimizer.iterations.numpy() % eval_interval == 0:
        metric_utils.eager_compute(
            eval_metrics,
            eval_tf_env,
            eval_policy,
            num_episodes=eval_tf_env.batch_size,
            train_step=optimizer.iterations,
            summary_writer=summary_writer,
            summary_prefix='eval',
            use_function=use_tf_functions,
        )
        for eval_metric in eval_metrics:
          #eval_metric.tf_summaries(train_step=env_steps.result())
          logging.info(
              f"eval/metric: {eval_metric.name} = {eval_metric.result().numpy()}")
          mlflow.log_metric(f"eval/metric/{eval_metric.name}",
                          eval_metric.result().numpy())
        eval_max_ep_length = tf.reduce_max(eval_average_ep_length._buffer.data)
        logging.info(f"eval/metric: max_ep_length = {eval_max_ep_length}")
        tf.summary.histogram(
            "eval/ep_length", eval_average_ep_length._buffer.data, step=optimizer.iterations)
        tf.summary.histogram(
            "eval/average_return", eval_average_return._buffer.data, step=optimizer.iterations)
        # log_average_ep_length.append(eval_average_ep_length.result().numpy())
        # log_average_return.append(eval_average_return.result().numpy())
      stage_time = time.time()-start_time
      logging.info(f"Time; eval; s; {stage_time:.4f}")
      mlflow.log_metric("time/eval", stage_time)
      tf.summary.scalar("time/eval", stage_time, step=optimizer.iterations)

      # Debug Scope
      logging.info("debug_log")
      start_time = time.time()
      if isinstance(optimizer.learning_rate, tf.Tensor) or isinstance(optimizer.learning_rate, tf.Variable):
        tf.summary.scalar('train/learning_rate',
                          optimizer.learning_rate, step=optimizer.iterations)
      elif isinstance(optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
        tf.summary.scalar('train/learning_rate', optimizer.learning_rate(
            optimizer.iterations), step=optimizer.iterations)
      tf.summary.scalar('train/softmax_temperature', tf.reduce_mean(train_agent._config.visit_softmax_temperature_fn(
          tf.zeros(tf_env.batch_size), optimizer.iterations)), step=optimizer.iterations)
      tf.summary.scalar('train/loss_policy', p_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_value', v_loss, step=optimizer.iterations)
      if train_agent._config.enable_path_consistency:
        tf.summary.scalar('train/loss_pc', pc_loss, step=optimizer.iterations)
      tf.summary.scalar('train/loss_reward', r_loss, step=optimizer.iterations)
      logging.info(
          f"p_loss/v_loss/pc_loss/r_loss {p_loss.numpy():.7f}/{v_loss.numpy():.7f}/{pc_loss.numpy():.7f}/{r_loss.numpy():.7f}")

      process = psutil.Process(os.getpid())
      tf.summary.scalar(f'mem/cpu', process.memory_info().rss /
                        (2**30), optimizer.iterations)

      if int(tf.version.VERSION.split('.')[0]) >= 2 and int(tf.version.VERSION.split('.')[1]) >= 5:
        for gpu in tf.config.list_logical_devices('GPU'):
          mem_infos = tf.config.experimental.get_memory_info(gpu.name)
          for name, info in mem_infos.items():
            tf.summary.scalar(f'mem/gpu{gpu.name}/{name}',
                              info/(2**30), step=optimizer.iterations)

      for collect_metric in collect_metrics:
        collect_metric.tf_summaries(train_step=optimizer.iterations)
        logging.info(
            f"train/metric: {collect_metric.name} = {collect_metric.result().numpy()}")

      if debug_summaries:
        import imageio
        from PIL import Image
        from PIL import ImageDraw
        def _label_with_episode_number(frame, episode_num):
          im = Image.fromarray(frame)
          drawer = ImageDraw.Draw(im)
          if np.mean(im) < 128:
              text_color = (255,255,255)
          else:
              text_color = (0,0,0)
          drawer.text((im.size[0]/20,im.size[1]/18), f'Episode: {episode_num+1}', fill=text_color)
          return np.array(im)

        render_tf_env = create_env(
            env_id, env_kwargs=env_kwargs, max_steps=max_steps, batch_size=1)
        policy_state = eval_policy.get_initial_state(render_tf_env.batch_size)
        current_time_step = render_tf_env.reset()
        filename = os.path.join(out_path, f'{optimizer.iterations.numpy():08}.gif')  # .mp4 needs ffmpeg
        with imageio.get_writer(filename, fps=20) as video:
          for _ in range(max_steps):
            policy_step = eval_policy.action(current_time_step, policy_state)
            frame = render_tf_env.render(mode='rgb_array')[0].numpy()
            frame = _label_with_episode_number(frame, episode_num=optimizer.iterations.numpy())
            video.append_data(frame)
            current_time_step = render_tf_env.step(policy_step.action)
            if current_time_step.is_last()[0]:
                break
            policy_state = policy_step.state
        render_tf_env.close()

      stage_time = time.time()-start_time
      tf.summary.scalar("time/debug_log", stage_time, step=optimizer.iterations)
      logging.info(f"Time; debug; s; {stage_time:.4f}")

      # Backup
      logging.info("backup")
      start_time = time.time()

      if rb_checkpoint_interval is not None and optimizer.iterations.numpy() % rb_checkpoint_interval == 0:
        if not replay_buffer_reverb:
            rb_checkpointer.save(global_step=optimizer.iterations)

      if policy_checkpoint_interval is not None and optimizer.iterations.numpy() % policy_checkpoint_interval == 0:
        #model_name = 'model_{:012}.npz'.format(optimizer.iterations.numpy())
        model_name = 'latest_model.npz'
        muzero_networks.save_model(
            train_agent._muzero_network, os.path.join(out_path, model_name))
        #shutil.copyfile(os.path.join(out_path, model_name), os.path.join(out_path, 'latest_model.npz'))
      stage_time = time.time()-start_time
      tf.summary.scalar("time/backup", stage_time, step=optimizer.iterations)
      logging.info(f"Time; backup; s; {stage_time:.4f}")
      tf.summary.flush()

  #mlflow.end_run()
  # print result for HP search
  #print(np.mean(log_average_return))

  return v_loss, r_loss, p_loss


def main(argv):
  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')

  tf_config()
  tf.config.run_functions_eagerly(True)

  env_id = 'CartPole-v1'
  # env_id = 'LunarLander-v2'
  # env_id = 'MountainCar-v0'
  # env_id = 'ALE/Breakout-v5'  # https://www.gymlibrary.dev/environments/atari/breakout/

  # debug
  gin.bind_parameter(
      'src.agents.tfagents.muzero.common.MuZeroConfig.num_simulations', 2)
  #gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.support_size_value', 15)
  #gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.support_size_reward', 15)
  train_eval(env_id=env_id, collect_batch_size=8, eval_batch_size=8, train_batch_size=4, train_steps=4,
            replay_buffer_reverb=False, replay_buffer_PER=False,
            train_steps_per_iteration=2, max_steps=3, replay_buffer_prefill=2, use_tf_functions=False)

  # train
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.num_simulations', 32)
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.td_steps', 10)
  # gin.bind_parameter('src.agents.tfagents.muzero.common.MuZeroConfig.discount', 0.997)
  # train_eval(env_id=env_id, collect_batch_size=1024, eval_batch_size=1024, train_batch_size=256, train_steps=25000, train_steps_per_iteration=1000, max_steps=1000, use_tf_functions=True)


if __name__ == '__main__':
  tf_agents.system.multiprocessing.handle_main(main)
