import logging
import tempfile
import shutil
import unittest

import src.envs.tictactoe_tfa as tictactoeenv
import src.apps.tictactoe_train_muzero as tictactoe_train_muzero


class EntryPointTester(unittest.TestCase):

    def setUp(self) -> None:
        self.temp_dir = tempfile.mkdtemp()
        return super().setUp()

    def tearDown(self) -> None:
        shutil.rmtree(self.temp_dir, ignore_errors=True)
        return super().tearDown()
    
    # @unittest.skip
    def test_entryPoint(self):
        def env_fn(**kwargs):
           return tictactoeenv.TicTacToeTFEnv(**kwargs)
        env_kwargs = {"action_masking": True}
        tictactoe_train_muzero.train_eval(
            out_path=self.temp_dir,
            env_fn=env_fn,
            env_kwargs=env_kwargs,
            collect_batch_size=4,
            eval_batch_size=4,
            train_batch_size=4,
            train_steps=4,
            train_steps_per_iteration=2,
            replay_buffer_prefill=2,
            use_tf_functions=False)


if __name__ == "__main__":
  # tf.config.run_functions_eagerly(True) # for debugging

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      )

  unittest.main()
