import logging
import unittest

import src.apps.c4_train_muzero as c4_train_muzero
from src.envs import connect4_tfa


class EntryPointTester(unittest.TestCase):

  env_fn = None

  def setUp(self) -> None:
    import tensorflow as tf
    tf.config.optimizer.set_experimental_options({'layout_optimizer': False})
    return super().setUp()

  def test_entryPoint_with_masking(self):
    env_fn = lambda batch_size=4: connect4_tfa.Connect4TFEnv(batch_size, action_masking=True)
    c4_train_muzero.train_eval(
        env_fn=env_fn,
        collect_batch_size=4,
        eval_batch_size=4,
        train_batch_size=4,
        train_steps=4,
        train_steps_per_iteration=2,
        replay_buffer_prefill=2,
        use_tf_functions=False)

  def test_entryPoint_without_masking(self):
    env_fn = lambda batch_size=4: connect4_tfa.Connect4TFEnv(batch_size, action_masking=False)
    c4_train_muzero.train_eval(
        env_fn=env_fn,
        collect_batch_size=4,
        eval_batch_size=4,
        train_batch_size=4,
        train_steps=4,
        train_steps_per_iteration=2,
        replay_buffer_prefill=2,
        use_tf_functions=False)


if __name__ == "__main__":
  import tensorflow as tf
  tf.config.run_functions_eagerly(True) # for debugging

  logging.basicConfig(level=logging.INFO,
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S',
                      )

  unittest.main()
