"""
https://www.kaggle.com/c/connectx/overview/getting-started
https://www.kaggle.com/c/connectx/overview/environment-rules
https://github.com/PaddlePaddle/PARL/blob/develop/benchmark/torch/AlphaZero/gen_submission.py
https://github.com/PaddlePaddle/PARL/blob/develop/benchmark/torch/AlphaZero/submission_template.py
https://github.com/Kaggle/kaggle-environments/blob/master/README.md
"""
import base64
import datetime
import os
import re
import pip
import tempfile

_test = False

tf_agents_filename = 'tf_agents-0.10.0-py3-none-any.whl'
gin_filename = 'gin_config-0.5.0-py3-none-any.whl'
temp_directory = tempfile.gettempdir()
# checking https://github.com/Kaggle/docker-python/blob/main/Dockerfile.tmpl ENV TENSORFLOW_VERSION=2.6.4 tfp=0.14.1, so tf-agents must be 0.10.0 (cf https://www.tensorflow.org/agents/overview)
# python version 3.7.12
pip.main(['download', 'tf_agents==0.10.0', '--no-deps', '-d', temp_directory])
pip.main(['download', 'gin-config==0.5.0', '--no-deps', '-d', temp_directory])


if False:
  assert len(sys.argv) == 2, "please specify model path."
  model_path = sys.argv[1]
else:
  model_path = "out/c4/ref_20220916_191310/latest_model.npz"

def encode_file(filepath):
  with open(filepath, 'rb') as f:
      raw_bytes = f.read()
      if _test:
        raw_bytes = raw_bytes[:4096]  # shrink model
      raw_data = base64.encodebytes(raw_bytes)
      return raw_data

# encode weights of model to byte string
encoded_weights = encode_file(model_path)
tf_agents_pkg = encode_file(os.path.join(temp_directory, tf_agents_filename))
gin_pkg = encode_file(os.path.join(temp_directory, gin_filename))

build_date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
submission_file = f"# build {build_date} from {model_path}\n"
submission_file += f"\n_test={_test}\n\n"

# inject all imports
submission_file += """
#from __future__ import annotations
import base64
import importlib
import os
import pip
import sys
print(f"sys="+str(sys.version))
import tempfile

import collections
import io
import logging
import numpy as np
import tensorflow as tf
print(f"tf="+str(tf.__version__))
import tensorflow_probability as tfp
print("tfp="+str(tfp.__version__))

try:
  importlib.import_module('gin')
except ImportError:
  temp_directory = tempfile.gettempdir()
  gin_pkg = base64.b64decode({})
  with open(os.path.join(temp_directory, 'gin_config-0.5.0-py3-none-any.whl'), 'wb') as f:
    f.write(gin_pkg)
  pip.main(['install', os.path.join(temp_directory, 'gin_config-0.5.0-py3-none-any.whl'), '--no-index', '--no-dependencies'])  #, '--find-links='+temp_directory
finally:
  #importlib.import_module('gin')
  globals()['gin'] = importlib.import_module('gin')


try:
  importlib.import_module('tf_agents')
except ImportError:
  temp_directory = tempfile.gettempdir()
  tf_agents_pkg = base64.b64decode({})
  with open(os.path.join(temp_directory, 'tf_agents-0.10.0-py3-none-any.whl'), 'wb') as f:
    f.write(tf_agents_pkg)
  pip.main(['install', os.path.join(temp_directory, 'tf_agents-0.10.0-py3-none-any.whl'), '--no-index', '--no-dependencies'])  #, '--find-links='+temp_directory
finally:
  #importlib.import_module('tf_agents')
  globals()['tf_agents'] = importlib.import_module('tf_agents')

import tf_agents
import tf_agents.trajectories.time_step as time_step  # type: ignore
import tf_agents.typing.types as types  # type: ignore
from typing import Any, Callable, List, NamedTuple, Optional, Text, Tuple, Union

decoded = base64.b64decode({})
""".format(gin_pkg, tf_agents_pkg, encoded_weights)

# insert muzero_agent library files
pattern = re.compile('^import .*|^from .*|^@gin.*')  # drop imports & gin decorator from files

lib_files = [os.path.join(os.getcwd(), 'src', 'agents', 'tfagents', 'muzero', f'{filename}.py') for filename in ['common', 'play', 'support', 'networks']]
lib_files += [os.path.join(os.getcwd(), 'src', 'agents', 'tfagents', 'muzero_agent.py')]

for filename in lib_files:
  with open(filename, 'r') as f:
    lines = [('# ' if pattern.match(l) else '') + l for l in f.readlines()]
    submission_file += ''.join(lines)

# insert specific code
with open(os.path.join(os.path.dirname(__file__), 'c4_submission_template.py'), 'r') as f:
    submission_file += ''.join(f.readlines())

# generate final submission file
with open('submission.py', 'w') as f:
    f.write(submission_file)
